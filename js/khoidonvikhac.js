var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục An toàn vệ sinh thực phẩm             ",
   "address": "59, Đường Phạm Văn Thuận, Thống Nhất, Thành phố Biên Hòa, Đồng Nai, Việt Nam",
   "Longtitude": 10.9582139,
   "Latitude": 106.8307811
 },
 {
   "STT": 2,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "address": "5 Cách Mạng Tháng Tám, Quyết Thắng, Thành phố Biên Hòa, Đồng Nai, Việt Nam",
   "Longtitude": 10.9464665,
   "Latitude": 106.8152108
 },
 {
   "STT": 3,
   "Name": "Trung tâm Bảo vệ Sức khoẻ lao động và Môi trường",
   "address": "Quốc lộ 51, Long Bình Tân, Thành phố Biên Hòa, Đồng Nai, Việt Nam",
   "Longtitude": 10.900073,
   "Latitude": 106.8528013
 },
 {
   "STT": 4,
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản",
   "address": "239 Phan Đình Phùng, Quyết Thắng, Thành phố Biên Hòa, Đồng Nai, Việt Nam",
   "Longtitude": 10.9489846,
   "Latitude": 106.8151878
 },
 {
   "STT": 5,
   "Name": "Trung tâm Giám định Y khoa",
   "address": "64 30 Tháng 4, Quyết Thắng, TP. Thanh Hóa, Đồng Nai, Việt Nam",
   "Longtitude": 10.948934,
   "Latitude": 106.8181479
 },
 {
   "STT": 6,
   "Name": "Trung tâm Pháp y ",
   "address": "22 Nguyễn Hiền Vương, Thanh Bình, TP. Thanh Hóa, Đồng Nai, Việt Nam",
   "Longtitude": 10.9466475,
   "Latitude": 106.8135637
 }
];