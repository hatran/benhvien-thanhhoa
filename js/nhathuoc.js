var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Quầy thuốc số 03",
   "address": "Ấp Tân Lập 1, xã Cây Gáo, Trảng Bom, Đồng Nai",
   "Longtitude": 11.0447707,
   "Latitude": 107.0734322
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc số 06",
   "address": "Khu 10, ấp 1, Gia Canh, Định Quán, Đồng Nai",
   "Longtitude": 11.1107256,
   "Latitude": 107.4528863
 },
 {
   "STT": 3,
   "Name": "Quầy thuốc số 07",
   "address": "139A, tổ 4, ấp Long Phú, Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.6776844,
   "Latitude": 107.0277678
 },
 {
   "STT": 4,
   "Name": "Quầy thuốc 09- Hồng Đức",
   "address": "25, Hưng Bình, Hưng Thịnh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9414521,
   "Latitude": 107.0811989
 },
 {
   "STT": 5,
   "Name": "Quầy thuốc số 11",
   "address": "129/4/1, ấp Quảng Đà, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9433737,
   "Latitude": 107.0711529
 },
 {
   "STT": 6,
   "Name": "Quầy thuốc số 12",
   "address": "51, tổ 7, khu 3, TT Tân Phú, Tân Phú, Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 7,
   "Name": "Quầy thuốc số 13",
   "address": "Ấp Cẩm Tân, xã Xuân Tân, thị xã Long Khánh, Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Tân An",
   "address": "1/1, An Hòa, Hóa An, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9249262,
   "Latitude": 106.8613194
 },
 {
   "STT": 9,
   "Name": "Quầy thuốc số 16",
   "address": "187/11A, tổ 16, KP2, Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9803043,
   "Latitude": 106.8548082
 },
 {
   "STT": 10,
   "Name": "Quầy thuốc số 17",
   "address": "34, khu 3, ấp 7, xã An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 11,
   "Name": "Quầy thuốc số 18",
   "address": "38/2, ấp 1, xã Lộ 25, Thống Nhất, Đồng Nai",
   "Longtitude": 10.8700501,
   "Latitude": 107.0907468
 },
 {
   "STT": 12,
   "Name": "Quầy thuốc số 21",
   "address": "Ấp Bình Phước, xã Tân Bình, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 13,
   "Name": "Quầy thuốc Diệp Lan",
   "address": "Ấp 2, xã Long An, Long Thành, Đồng Nai",
   "Longtitude": 10.7563293,
   "Latitude": 106.9865799
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc Nguyễn Du",
   "address": "212, CMT8, P. Quyết Thắng, Biên Hòa, Đồng Nai",
   "Longtitude": 10.945124,
   "Latitude": 106.8192185
 },
 {
   "STT": 15,
   "Name": "Quầy thuốc số 26",
   "address": "Ấp tập Phước, xã Long Thành",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 16,
   "Name": "Quầy thuốc Minh Hòang",
   "address": "291, ấp Tân Phát, xã Đồi 61, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 17,
   "Name": "Quầy thuốc Quỳnh Mai",
   "address": "140, ấp Bắc Hòa, xã Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9569601,
   "Latitude": 106.9655557
 },
 {
   "STT": 18,
   "Name": "Quầy thuốc Thùy Lâm",
   "address": "182/5 ấp Đòan Kết, Giang Điền, Trảng Bom, Đồng Nai",
   "Longtitude": 10.909694,
   "Latitude": 106.97377
 },
 {
   "STT": 19,
   "Name": "Quầy thuốc Số 31-Thanh Mai",
   "address": "2135, ấp Bùi Chu, xã Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 20,
   "Name": "Quầy thuốc Ái Thủy",
   "address": "4A, phố 1, ấp 2, xã Phú Vinh, Định Quán, Đồng Nai",
   "Longtitude": 11.2131589,
   "Latitude": 107.3614525
 },
 {
   "STT": 21,
   "Name": "Quầy thuốc số 38",
   "address": "136, tổ 1, ấp 2, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9782781,
   "Latitude": 107.0185123
 },
 {
   "STT": 22,
   "Name": "Quầy thuốc số 40",
   "address": "8, tổ 3, ấp 1C, xã Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.680821,
   "Latitude": 107.0262359
 },
 {
   "STT": 23,
   "Name": "Quầy thuốc Uyên Uyên",
   "address": "B3/70, ấp Lê LợI, xã Quang Trung, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9777207,
   "Latitude": 107.1513954
 },
 {
   "STT": 24,
   "Name": "Quầy thuốc số 42",
   "address": "Ấp 7, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 25,
   "Name": "Quầy thuốc Ngọc Phúc",
   "address": "Áp Ngũ Phú, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 26,
   "Name": "Quầy thuốc số 46",
   "address": "Tổ 15, ấp 6, xã Vĩnh Tân, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Phước Bình",
   "address": "22, KP 10, P. An Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.891648,
   "Latitude": 106.8502253
 },
 {
   "STT": 28,
   "Name": "Quầy thuốc số 49",
   "address": "923/D, ấp Thống Nhất, xã Phú Cường, Định Quán",
   "Longtitude": 11.1086911,
   "Latitude": 107.1641629
 },
 {
   "STT": 29,
   "Name": "Quầy thuốc số 50",
   "address": "17, KP9, Nguyễn Văn Tiên, Tân Phong, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9819539,
   "Latitude": 106.8449944
 },
 {
   "STT": 30,
   "Name": "Quầy thuốc số 52",
   "address": "KP2, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9588417,
   "Latitude": 107.0124501
 },
 {
   "STT": 31,
   "Name": "Quầy thuốc số 54",
   "address": "Khu 1, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9177403,
   "Latitude": 107.4113855
 },
 {
   "STT": 32,
   "Name": "Quầy thuốc số 55-Hồng Nhung",
   "address": "257, khu 7, ấp 5, xã Phú Tân, Định Quán, Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 33,
   "Name": "Quầy thuốc số 56",
   "address": "455, ấp Ngũ Phúc, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 34,
   "Name": "Quầy thuốc số 57-Bích Hà",
   "address": "1/6, phố 1, ấp 2, xã Phú Lợi, Định quán, Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 35,
   "Name": "Quầy thuốc số 58-Triệu Vy",
   "address": "Ấp 8, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 36,
   "Name": "Quầy thuốc số 59",
   "address": "KP2, Thị Trấn Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9588417,
   "Latitude": 107.0124501
 },
 {
   "STT": 37,
   "Name": "Quầy thuốc số  60",
   "address": "255/2, KP8A, P. Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9705705,
   "Latitude": 106.893577
 },
 {
   "STT": 38,
   "Name": "Quầy thuốc số 61",
   "address": "9A/11, tổ 6, KP9, P. Tân Hòa, Biên Hòa, Đồng Nai ",
   "Longtitude": 10.9668899,
   "Latitude": 106.8956446
 },
 {
   "STT": 39,
   "Name": "Quầy thuốc số 62",
   "address": "Ấp Cát Lái, xã Phú Hữu, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 40,
   "Name": "Quầy thuiốc số 65",
   "address": "424, ấp 10, xã Xuân Tây, cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 41,
   "Name": "Quầy thuốc số 66",
   "address": "Ấp Suối Nhát, xã Xuân Đông, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 42,
   "Name": "Quầy thuốc số 67",
   "address": "Ấp Bà Trường, xã Phước An, Nhơn Trạch",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 43,
   "Name": "Quầy thuốc số 70-Hồng Phúc",
   "address": "13/N, ấp Phúc Nhạc, Gia Tân 3, Trảng Bom",
   "Longtitude": 11.0501407,
   "Latitude": 107.1898113
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc số 72",
   "address": "G10, tổ 12, KP7, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9115755,
   "Latitude": 106.890473
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc số 73-Thiên Phượng",
   "address": "Ấp 3, Suối Nho, Định Quán, Đồng Nai",
   "Longtitude": 11.0335306,
   "Latitude": 107.3165886
 },
 {
   "STT": 46,
   "Name": "Quầy thuốc số 74",
   "address": "A 15, khu Phước Hải, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Hùynh Phúc",
   "address": "756, tổ 15, ấp 2, xã An Hòa, thành phố  Biên Hòa, Đồpng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 48,
   "Name": "Quầy thuốc số 81",
   "address": "Chợ Xuân Đà, xã Xuân Tâm, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8783753,
   "Latitude": 107.4398782
 },
 {
   "STT": 49,
   "Name": "Quầy thuốc số 83",
   "address": "Tổ 34, KP5, P. Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 11.0007506,
   "Latitude": 106.8541655
 },
 {
   "STT": 50,
   "Name": "Quầy thuốc số 86",
   "address": "QL 764, ấp La Hoa, xã Xuân Đông, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8410566,
   "Latitude": 107.37811
 },
 {
   "STT": 51,
   "Name": "Quầy thuốc số 87-Hà Yến Nhi",
   "address": "11A, tổ 11, ấp Long Đức 1, xã Tam Phước, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 52,
   "Name": "Quầy thuốc số 89-Oanh",
   "address": "249, ấp 1, xã Gia Canh, Định Quán, Đồng Nai",
   "Longtitude": 11.180107,
   "Latitude": 107.3862129
 },
 {
   "STT": 53,
   "Name": "Quầy thuốc số 90",
   "address": "18A, lô M3, KP1, P. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8994104,
   "Latitude": 106.8598763
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc số 91",
   "address": "Ấp 1, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 55,
   "Name": "Quầy thuốc số 93",
   "address": "20/1/1, KP1, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 56,
   "Name": "Quầy thuốc số 95",
   "address": "tổ 9, KP4, P. Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9684006,
   "Latitude": 106.859825
 },
 {
   "STT": 57,
   "Name": "Quầy thuốc số 96- Công ty cổ phần dược Đồng Nai",
   "address": "11/1C, Đông Bắc, Gia Kiệm, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 58,
   "Name": "Quầy thuốc số 97",
   "address": "43, ấp Sông Mây, xã Bắc Sơn, Trảng Bom. Đồng Nai",
   "Longtitude": 10.9931479,
   "Latitude": 106.9788095
 },
 {
   "STT": 59,
   "Name": "Quầy thuốc số 98",
   "address": "Ấp Ngọc Lâm 1, Phú Thanh, Tân Phú, Đồng Nai",
   "Longtitude": 11.2771227,
   "Latitude": 107.4748378
 },
 {
   "STT": 60,
   "Name": "Quầy thuốc số 99",
   "address": "21/4, ấp Cây Xăng, xã Phú Túc, Định Quán, Đồng Nai",
   "Longtitude": 11.0900382,
   "Latitude": 107.214569
 },
 {
   "STT": 61,
   "Name": "Quầy thuốc số 100",
   "address": "31, Ngọc Lâm 3, Phú Xuân, Tân Phú, Đồng Nai",
   "Longtitude": 11.278514,
   "Latitude": 107.4623053
 },
 {
   "STT": 62,
   "Name": "Quầy thuốc Số 101",
   "address": "Ấp 5, xã Xuân Tâm, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8734065,
   "Latitude": 107.4440254
 },
 {
   "STT": 63,
   "Name": "Quầy thuốc Số 102",
   "address": "Khu 2, Láng Lớn, Xuân Mỹ, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8274876,
   "Latitude": 107.2282225
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc Phòng Khám Đa Khoa An Bình Nasa",
   "address": "04, Bùi Văn Hòa, KP11, P. An Bình, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.927072,
   "Latitude": 106.878744
 },
 {
   "STT": 65,
   "Name": "Quầy thuốc Số 104-Nam Khang",
   "address": "Ấp Đòan Kết, xã Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6797884,
   "Latitude": 106.8568211
 },
 {
   "STT": 66,
   "Name": "Quầy thuốc Số 105",
   "address": "375A/3, ấp Dốc Mơ 2, Gia Tân 1, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 67,
   "Name": "Quầy thuốc Số 106",
   "address": "114, khu 1, Hòa Bình, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 68,
   "Name": "Quầy thuốc Số 107",
   "address": "Chợ Lộc Hòa, Tây Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.948597,
   "Latitude": 107.0486301
 },
 {
   "STT": 69,
   "Name": "Quầy thuốc Số 109",
   "address": "A4 /401B, Bùi Hữu Nghĩa, KP4, P. Tân Vạn, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9196521,
   "Latitude": 106.8249276
 },
 {
   "STT": 70,
   "Name": "Quầy thuốc Số 110",
   "address": "84, ấp Sông Mây, xã Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 71,
   "Name": "Quầy thuốc Số 111-Cẩm Vân",
   "address": "174/B, ấp Phúc Nhạc, xã Gia Tân 3, Thống Nhất, Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 72,
   "Name": "Quầy thuốc Số 112",
   "address": "Ấp Thành Công, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 73,
   "Name": "Quầy thuốc Số 114",
   "address": "Ấp Phú Mỹ 1, xã Phú Hội, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 74,
   "Name": "Quầy thuốc Số 115",
   "address": "Ấp Cẩm Tân, xã Xuân Tân, thị xã Long Khánh, Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 75,
   "Name": "Quầy thuốc Số 116",
   "address": "Lô R221, KP7, P. Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9115755,
   "Latitude": 106.890473
 },
 {
   "STT": 76,
   "Name": "Quầy thuốc Số 121-Vũ Vy",
   "address": "Ấp Bảo Vệ, Giang Điền, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9269822,
   "Latitude": 106.9846975
 },
 {
   "STT": 77,
   "Name": "Quầy thuốc Số 122",
   "address": "Ấp 6, xã Phú Lộc, Tân Phú, Đồng Nai",
   "Longtitude": 11.2986537,
   "Latitude": 107.4144526
 },
 {
   "STT": 78,
   "Name": "Quầy thuốc Số 123-Xuân Thái",
   "address": "38, Ngọc Lâm 2, Phú Xuân, Tân Phú, Đồng Nai",
   "Longtitude": 11.278514,
   "Latitude": 107.4623053
 },
 {
   "STT": 79,
   "Name": "Quầy thuốc Số 124",
   "address": "Ấp Vàm, xã Thiện Tân, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 80,
   "Name": "Quầy thuốc Số 126",
   "address": "KP2, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9588417,
   "Latitude": 107.0124501
 },
 {
   "STT": 81,
   "Name": "Quầy thuốc Số 127-Minh Duy",
   "address": "77, ấp 2, xã An Viễn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8810166,
   "Latitude": 106.9999856
 },
 {
   "STT": 82,
   "Name": "Quầy thuốc Số 128-Bảo Ngọc",
   "address": "Khu Kim Sơn, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7783288,
   "Latitude": 106.9429001
 },
 {
   "STT": 83,
   "Name": "Quầy thuốc Số 129-Hồng Ân",
   "address": "ấp Thanh Hóa, xã Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9572062,
   "Latitude": 106.942434
 },
 {
   "STT": 84,
   "Name": "Quầy thuốc Số 131",
   "address": "Ấp 2, xã Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7496769,
   "Latitude": 106.9441283
 },
 {
   "STT": 85,
   "Name": "Quầy thuốc Số 133",
   "address": "1230/E, ấp Phú Tân, Phú Cường, Định Quán, Đồng Nai",
   "Longtitude": 11.1227454,
   "Latitude": 107.1602683
 },
 {
   "STT": 86,
   "Name": "Quầy thuốc Số 134",
   "address": "Tổ 1, ấp Hàng Gòn, xã Lộc An, Long Thành, Đồng Nai",
   "Longtitude": 10.8111548,
   "Latitude": 106.9889904
 },
 {
   "STT": 87,
   "Name": "Quầy thuốc Số 136",
   "address": "chợ Xuân Mỹ, ấp Láng Lớn, xã Xuân Mỹ, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 88,
   "Name": "Quầy thuốc Số 137",
   "address": "418, ấp Phan Bội Châu, Bàu Hàm 2, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 89,
   "Name": "Quầy thuốc Số 138-Quyền Phong",
   "address": "Ấp 2,  xã Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 90,
   "Name": "Quầy thuốc Số 139",
   "address": "Ấp Thị Cầu, xã Phú Đông, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 91,
   "Name": "Quầy thuốc Số 140",
   "address": "Ấp 1, xã Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7513333,
   "Latitude": 106.9441969
 },
 {
   "STT": 92,
   "Name": "Quầy thuốc Số 142",
   "address": "Khu 6, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9366404,
   "Latitude": 107.4055621
 },
 {
   "STT": 93,
   "Name": "Nhà thuốc Công Minh",
   "address": "151/9, Bùi Trọng Nghĩa, P. Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9838413,
   "Latitude": 106.8624356
 },
 {
   "STT": 94,
   "Name": "Quầy thuốc Số 146",
   "address": "KP5, TT Vĩnh An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 95,
   "Name": "Nhà thuốc Minh Xuân",
   "address": "325Đ, Trần Quốc Toản, KP3, P. Bình Đa, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.938189,
   "Latitude": 106.8590579
 },
 {
   "STT": 96,
   "Name": "Quầy thuốc Số 148",
   "address": "A1/157A, tổ 6, KP1, P. Tân Vạn, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9135419,
   "Latitude": 106.8279781
 },
 {
   "STT": 97,
   "Name": "Quầy thuốc Số 150",
   "address": "03, khóm 3, khu 3, TT Tân Phú, huyện Tân Phú, Đồng Nai",
   "Longtitude": 11.2720224,
   "Latitude": 107.4366867
 },
 {
   "STT": 98,
   "Name": "Quầy thuốc Số 152",
   "address": "Ấp An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 99,
   "Name": "Quầy thuốc Số 153",
   "address": "Ấp 2, xã Bình Lộc. Thị xã Long Khánh, Đồng Nai",
   "Longtitude": 10.9822287,
   "Latitude": 107.2351027
 },
 {
   "STT": 100,
   "Name": "Quầy thuốc Số 155-Gia Hân",
   "address": "Ấp 2, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0130859,
   "Latitude": 106.8484394
 },
 {
   "STT": 101,
   "Name": "Quầy thuốc Số 156",
   "address": "239, ấp Hưng Bình, xã Hưng Thịnh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9203782,
   "Latitude": 107.085525
 },
 {
   "STT": 102,
   "Name": "Quầy thuốc Số 158",
   "address": "104, ấp Long Đức 1, xã Tam Phước, Biên Hòa, Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Số 159-Phú Tân",
   "address": "6/4, ấp Phú Quí 1, xã La Ngà, Định Quán, Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 104,
   "Name": "Quầy thuốc Số 160",
   "address": "Ấp 1, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 105,
   "Name": "Quầy thuốc Số 161",
   "address": "Ấp Phú Lâm 1, Phú Sơn, Tân Phú, Đồng Nai",
   "Longtitude": 11.3323297,
   "Latitude": 107.5235503
 },
 {
   "STT": 106,
   "Name": "Quầy thuốc Số 163-Hạnh Phúc",
   "address": "Ấp Hòa Bình, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 107,
   "Name": "Quầy thuốc Số 164",
   "address": "136, tổ 1, ấp 2, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9782781,
   "Latitude": 107.0185123
 },
 {
   "STT": 108,
   "Name": "Quầy thuốc Số 166",
   "address": "1888/8, KP6, P. Trung Dũng, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9570475,
   "Latitude": 106.8202791
 },
 {
   "STT": 109,
   "Name": "Quầy thuốc Số 167",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 110,
   "Name": "Quầy thuốc Số 169-Bảo Châu",
   "address": "19, ấp 1C, Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.680821,
   "Latitude": 107.0262359
 },
 {
   "STT": 111,
   "Name": "Quầy thuốc Số 170",
   "address": "1955E, tổ 7, ấp 5, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 112,
   "Name": "Quầy thuốc Số 171",
   "address": "286, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 113,
   "Name": "Quầy thuốc Số 175-Chính Phương",
   "address": "Ấp 1, Thanh Sơn, Định Quán, Đồng Nai",
   "Longtitude": 11.2322972,
   "Latitude": 107.2975882
 },
 {
   "STT": 114,
   "Name": "Quầy thuốc Số 176",
   "address": "Ấp Bến Đình, Phú Đông, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 115,
   "Name": "Quầy thuốc Số 179-Hà Thực",
   "address": "51/11, KP Long Điềm. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 116,
   "Name": "Quầy thuốc Số 180-Sơn Liệu",
   "address": "414, TỔ 15, Ấp 10, Xuân Tây, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 117,
   "Name": "Quầy thuốc Số 181",
   "address": "15, tổ 23, KP6, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 118,
   "Name": "Cơ Sở Bán Lẻ thuốc Thành Phẩm Đông Y, thuốc Thành Phẩm Từ Dược Liệu- Nhơn Hòa Đường",
   "address": "10A, Hùng Vương, Xuân Trung, thị xã Long Khánh, Đồng Nai",
   "Longtitude": 10.934334,
   "Latitude": 107.2429539
 },
 {
   "STT": 119,
   "Name": "Nhà thuốc Phúc Bình An",
   "address": "56/1, KP1, P. Bình Đa, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9355815,
   "Latitude": 106.8614105
 },
 {
   "STT": 120,
   "Name": "Cơ Sở Bán Lẻ thuốc Thành Phẩm Đông Y, thuốc Thành Phẩm Từ Dược Liệu- Trương Danh",
   "address": "360A, KP2, Phạm Văn Thuận, P. Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9575593,
   "Latitude": 106.8413329
 },
 {
   "STT": 121,
   "Name": "Cơ Sở Bán Lẻ thuốc Thành Phẩm Đông Y, thuốc Thành Phẩm Từ Dược Liệu- An Thọ",
   "address": "15/7B, KP2, Hùynh Văn Nghệ, P. Bửu Long, Biên Hòa, Đồng Nai",
   "Longtitude": 10.954578,
   "Latitude": 106.798018
 },
 {
   "STT": 122,
   "Name": "Quầy thuốc Số 190- Duy Hằng",
   "address": "130YT, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 123,
   "Name": "Quầy thuốc Số 192",
   "address": "31/23, KP2, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 124,
   "Name": "Quầy thuốc Số 193",
   "address": "Ấp Suối Chồn, xã Bảo Vinh, Long Khánh, Đồng Nai",
   "Longtitude": 10.9525117,
   "Latitude": 107.243565
 },
 {
   "STT": 125,
   "Name": "Quầy thuốc Số 195-Thu Hà",
   "address": "Ấp An Bình, xã Bình An, Long Thành, Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 107.05
 },
 {
   "STT": 126,
   "Name": "Quầy thuốc Số 196-Mỹ Vân",
   "address": "04, KDC 6, Thái Hòa 2, xã Phú Túc, Định Quán, Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252677
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc Số 199-Phát Yến",
   "address": "Tổ 12, khu Phước Hải, Nguyễn An Ninh, thị trấn Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 128,
   "Name": "Quầy thuốc Số 201",
   "address": "12A1, Võ Thị Sáu, Quyết Thắng, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9448637,
   "Latitude": 106.8212162
 },
 {
   "STT": 129,
   "Name": "Quầy thuốc Số 202",
   "address": "Ấp Tân Hạnh, xã Xuân Bảo, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 130,
   "Name": "Quầy thuốc Số 203",
   "address": "Tổ 6, ấp Cẩm Đường, xã Cẩm Đường, Long Thành, Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 131,
   "Name": "Quầy thuốc Số 204-Kim Quân",
   "address": "Khu 5, ấp 6, xã Phú Tân, Định Quán, Đồng Nai",
   "Longtitude": 11.3184174,
   "Latitude": 107.3463392
 },
 {
   "STT": 132,
   "Name": "Quầy thuốc Số 205",
   "address": "12/125B, Kp2, P. Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.960593,
   "Latitude": 106.8483173
 },
 {
   "STT": 133,
   "Name": "Quầy thuốc Số 206-Ngọc Mai",
   "address": "Ấp 7, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc Số 207",
   "address": "Tổ 2, KP5, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9413981,
   "Latitude": 106.9004472
 },
 {
   "STT": 135,
   "Name": "Quầy thuốc Số 208",
   "address": "1978E, ấp 5, tổ 7, xã Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 136,
   "Name": "Quầy thuốc Số 211-Thanh Bình",
   "address": "17, tỉnh lộ 765, tổ 13, ấp Việt Kiều, xã Suối Cát, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9074027,
   "Latitude": 107.3686391
 },
 {
   "STT": 137,
   "Name": "Quầy thuốc Số 213-Đông Á",
   "address": "Tổ 1, khu Phước Hải, thi trấn Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 138,
   "Name": "Quầy thuốc Số 214",
   "address": "ấp 2, xã Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 139,
   "Name": "Quầy thuốc Số 215",
   "address": "KP5, TT Vĩnh An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 140,
   "Name": "Nhà thuốc Ngọc Thu",
   "address": "32, đường 30-4, Quyết Thắng, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9506636,
   "Latitude": 106.8190236
 },
 {
   "STT": 141,
   "Name": "Quầy thuốc Số 218",
   "address": "Ấp 1, xã Bàu Cạn, Long Thành, Đồng Nai",
   "Longtitude": 10.7361529,
   "Latitude": 107.0657549
 },
 {
   "STT": 142,
   "Name": "Quầy thuốc Số 219-Thanh Tâm",
   "address": "Ấp Đồng Xòai, Túc Trưng, Định Quán, Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 143,
   "Name": "Quầy thuốc Số 221-Bích Huyền",
   "address": "Tổ 1, ấp 1, Tà Lài, Tân Phú, Đồng Nai",
   "Longtitude": 11.368242,
   "Latitude": 107.379052
 },
 {
   "STT": 144,
   "Name": "Quầy thuốc Số 222",
   "address": "Ấp 6, Tân Hiệp, Long Thành, Đồng Nai",
   "Longtitude": 10.6984529,
   "Latitude": 107.0598491
 },
 {
   "STT": 145,
   "Name": "Quầy thuốc Số 223",
   "address": "Ấp Tam Hiệp, Xuân Hiệp, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 146,
   "Name": "Quầy thuốc Số 225",
   "address": "821, ấp Trà Cổ, Bình Minh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9448786,
   "Latitude": 106.9807112
 },
 {
   "STT": 147,
   "Name": "Quầy thuốc Số 226",
   "address": "3521, ấp Phú Lâm 4, Phú Sơn, Tân Phú, Đồng Nai",
   "Longtitude": 11.3323297,
   "Latitude": 107.5235503
 },
 {
   "STT": 148,
   "Name": "Quầy thuốc Số 227-Hoàng Châu",
   "address": "51, KP4, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9498488,
   "Latitude": 107.0084808
 },
 {
   "STT": 149,
   "Name": "Quầy thuốc Số 229",
   "address": "246, ấp Bình Trung, Tân An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0259476,
   "Latitude": 106.947666
 },
 {
   "STT": 150,
   "Name": "Quầy thuốc Số 230-Lan Phương",
   "address": "ấp 2, xã Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 151,
   "Name": "Quầy thuốc Số 232",
   "address": "Ấp 3, Xuân Tây, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 152,
   "Name": "Nhà thuốc Hoàng Hà Yến",
   "address": "G19, KP5, P. Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9559802,
   "Latitude": 106.8639985
 },
 {
   "STT": 153,
   "Name": "Quầy thuốc Số 236",
   "address": "Chợ Hiệp Hòa, xã Hiệp Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9308124,
   "Latitude": 106.8322645
 },
 {
   "STT": 154,
   "Name": "Quầy thuốc Số 239",
   "address": "ấp 1C, Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.6787052,
   "Latitude": 107.0244171
 },
 {
   "STT": 155,
   "Name": "Quầy thuốc Số 242",
   "address": "Thôn Tây Lạc, Bùi Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 156,
   "Name": "Quầy thuốc Số 243",
   "address": "1641, Bùi Hữu Nghĩa, ấp 1B, Tân Hạnh, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9505762,
   "Latitude": 106.7869475
 },
 {
   "STT": 157,
   "Name": "Quầy thuốc Số 244",
   "address": "Ấp Suối Chồn, xã Bảo Vinh, Long Khánh, Đồng Nai",
   "Longtitude": 10.9525117,
   "Latitude": 107.243565
 },
 {
   "STT": 158,
   "Name": "Quầy thuốc Số 245",
   "address": "A 4/90B, Bùi Hữu Nghĩa, P. Tân Vạn, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9089724,
   "Latitude": 106.8291979
 },
 {
   "STT": 159,
   "Name": "Quầy thuốc Số 246-Thiên Ân Quyên",
   "address": "30, tổ 18, khu 80, ấp Long Đức 3, Tam Phước, Biên Hòa",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 160,
   "Name": "Quầy thuốc Số 248-Bảo Thiên",
   "address": "Ấp 7, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 161,
   "Name": "Nhà thuốc Phương Hiếu",
   "address": "Số 1, tổ 1B, Kp3, P. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9081787,
   "Latitude": 106.8585146
 },
 {
   "STT": 162,
   "Name": "Quầy thuốc Số 252-Tâm Minh",
   "address": "Số 01, Ấp 2, Lộ 25, Thống Nhất, Đồng Nai",
   "Longtitude": 10.8702403,
   "Latitude": 107.0904402
 },
 {
   "STT": 163,
   "Name": "Quầy thuốc Số 253",
   "address": "Ấp Phượng Vỹ, Suối Cao, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9812965,
   "Latitude": 107.3952351
 },
 {
   "STT": 164,
   "Name": "Quầy thuốc Số 254-Nguyên Quang",
   "address": "Số 02, phố 4, Ấp 2, Phú Lợi, Định Quán, Đồng Nai",
   "Longtitude": 11.2046485,
   "Latitude": 107.362844
 },
 {
   "STT": 165,
   "Name": "Quầy thuốc Số 255-Thủy Dưng",
   "address": "Số 28/4, KDC 4, ấp 4, Phú Ngọc, Định Quán, Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 166,
   "Name": "Quầy thuốc Số 256-Bình An",
   "address": "Ấp Trầu, Phước Thiền, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 167,
   "Name": "Quầy thuốc Số 257",
   "address": "Số 29, tổ 9, ấp Tân Mai 2, Phước Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8746531,
   "Latitude": 106.9103868
 },
 {
   "STT": 168,
   "Name": "Quầy thuốc Số 259-Hoàng Liên",
   "address": "Ấp Bến Cam, Phước Thiền, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 169,
   "Name": "Quầy thuốc Số 260",
   "address": "Số 4, áp Tân Đạt, Đồi 61, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 170,
   "Name": "Quầy thuốc Số 262",
   "address": "Ấp Bùi Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 171,
   "Name": "Quầy thuốc Số 264",
   "address": "G63, Hùng Vương, P. Xuân Bình, Long Khánh, Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 172,
   "Name": "Quầy thuốc Số 265",
   "address": "Ấp Câu Kê, Phú Hữu, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.744991,
   "Latitude": 106.79908
 },
 {
   "STT": 173,
   "Name": "Quầy thuốc Số 266",
   "address": "Ấp 5, Tam An, Long Thành, Đồng Nai",
   "Longtitude": 10.7593232,
   "Latitude": 106.9425975
 },
 {
   "STT": 174,
   "Name": "Quầy thuốc Số 268",
   "address": "Khu 9, TT Tân Phú, Tân Phú, Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 175,
   "Name": "Quầy thuốc Số 270",
   "address": "Số 300, ấp Xây Dựng, xã Giang Điền, Trảng Bom Đồng Nai",
   "Longtitude": 10.912027,
   "Latitude": 106.9860384
 },
 {
   "STT": 176,
   "Name": "Quầy thuốc Số 274",
   "address": "580, tổ 9, ấp Trần Cao Vân, Bàu Hàm 2, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 177,
   "Name": "Quầy thuốc Số 278",
   "address": "Ấp 1, Sông Ray, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7515541,
   "Latitude": 107.3514669
 },
 {
   "STT": 178,
   "Name": "Quầy thuốc Số 279",
   "address": "Ấp Xóm Gò, Long Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 179,
   "Name": "Quầy thuốc Số 280-An Ngọc Linh",
   "address": "Tổ 4, KP1, P. Tân Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9708772,
   "Latitude": 106.9093
 },
 {
   "STT": 180,
   "Name": "Nhà thuốc Pkđk Trường Cao Đẳng Y Tế Đồng Nai",
   "address": "5/4A, Đồng Khởi, KP1, P. Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.960353,
   "Latitude": 106.855269
 },
 {
   "STT": 181,
   "Name": "Quầy thuốc Số 282",
   "address": "Khu 13, Long Đức, Long Thành, Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 182,
   "Name": "Quầy thuốc Số 283",
   "address": "51, tổ 4, khu 6, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.926233,
   "Latitude": 107.403939
 },
 {
   "STT": 183,
   "Name": "Quầy thuốc Số 284-Nhu Ý",
   "address": "Tổ 13, khu Văn Hải, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7840803,
   "Latitude": 106.9488901
 },
 {
   "STT": 184,
   "Name": "Quầy thuốc Số 299",
   "address": "594A, tổ 13, ấp 3, Vĩnh Tân, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0175781,
   "Latitude": 106.9978606
 },
 {
   "STT": 185,
   "Name": "Quầy thuốc Số 286",
   "address": "55, Nguyễn Văn Hoa, KP2, P. Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9561995,
   "Latitude": 106.8362651
 },
 {
   "STT": 186,
   "Name": "Quầy thuốc Số 287",
   "address": "Số 9, ấp Vĩnh Cửu, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 187,
   "Name": "Quầy thuốc Số 288- An Nhàn",
   "address": "Ấp Suối Cả, Long Giao, cẩm mỹ, Đồng Nai",
   "Longtitude": 10.8274876,
   "Latitude": 107.2282225
 },
 {
   "STT": 188,
   "Name": "Quầy thuốc Số 290",
   "address": "Tổ 2, ấp Bình Lâm, xã Lộc An, Long Thành, Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 189,
   "Name": "Quầy thuốc Số 292- Việt Kiều Phát",
   "address": "62, tổ 17, KP6, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 190,
   "Name": "Quầy thuốc Số 293",
   "address": "Ấp 6, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 191,
   "Name": "Quầy thuốc Số 297-Nguyễn Du",
   "address": "24B/2, ấp Tân Hoa, Bàu Hàm, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 192,
   "Name": "Quầy thuốc Số 298",
   "address": "Tổ 11, ấp 1B, Tân Hạnh, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9509494,
   "Latitude": 106.7794935
 },
 {
   "STT": 193,
   "Name": "Quầy thuốc Số 300-Nhân An",
   "address": "Số 691, Đồng Khởi, KP8, P. Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 194,
   "Name": "Quầy thuốc ",
   "address": "Số 518C, Ấp An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 195,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, Thừa Đức, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7882504,
   "Latitude": 107.1510688
 },
 {
   "STT": 196,
   "Name": "Quầy thuốc Châu Hà",
   "address": "Số 11, ấp 3, xã Suối Nho, Định Quán, Đồng Nai",
   "Longtitude": 11.0335306,
   "Latitude": 107.3165886
 },
 {
   "STT": 197,
   "Name": "Quầy thuốc Phúc Trâm",
   "address": "Số 94, ấp 2, xã Lộ 25, Thống Nhất, Đồng Nai",
   "Longtitude": 10.8694548,
   "Latitude": 107.0899432
 },
 {
   "STT": 198,
   "Name": "Quầy thuốc Ngọc Thúy",
   "address": "Số 74/YT, ấp Thái Hòa, xã Thái Hòa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 199,
   "Name": "Quầy thuốc",
   "address": "60/1, Ấp Hòa Bình, xã Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 200,
   "Name": "Quầy thuốc",
   "address": " Ấp 3, xã Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 201,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 202,
   "Name": "Quầy thuốc Phương Trúc",
   "address": "Ấp Phước Lý, xã Đại Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 203,
   "Name": "Quầy thuốc",
   "address": "Ấp Đông Kim, xã Gia Kiệm, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0304592,
   "Latitude": 107.177428
 },
 {
   "STT": 204,
   "Name": "Quầy thuốc",
   "address": "Số 19, ấp 2, xã Lộ 25, Thống Nhất, Đồng Nai",
   "Longtitude": 10.8694548,
   "Latitude": 107.0899432
 },
 {
   "STT": 205,
   "Name": "Quầy thuốc",
   "address": "ấp 4, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 206,
   "Name": "Quầy thuốc",
   "address": "Số 3933, ấp Bảo Định, xã Xuân Định, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8850571,
   "Latitude": 107.2602242
 },
 {
   "STT": 207,
   "Name": "Quầy thuốc",
   "address": "Ấp Bình tân xã Xuân Phú, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9056703,
   "Latitude": 107.328528
 },
 {
   "STT": 208,
   "Name": "Quầy thuốc Phương Linh",
   "address": "104/4, Ấp Bạch Lâm, xã Gia tân 2, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 209,
   "Name": "Quầy thuốc Trọng Hoa",
   "address": "A 8/K80, ấp Long Đức 1, xã Tam Phước, Biên Hòa, Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 210,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Long Thọ, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 211,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 212,
   "Name": "Quầy thuốc Quỳnh Trang",
   "address": "Khu 3, ấp An Bình, xã Trung Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 213,
   "Name": "Quầy thuốc Dũng Phát",
   "address": "Số 67, ấp 2, xã An Viễn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8810166,
   "Latitude": 106.9999856
 },
 {
   "STT": 214,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Long Thọ, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 215,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 216,
   "Name": "Quầy thuốc",
   "address": "Số 141, khu 4, ấp 8, xã An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.862375,
   "Latitude": 106.963667
 },
 {
   "STT": 217,
   "Name": "Quầy thuốc Hà Châu",
   "address": "7A/5, KP Long Điềm, P. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.891472,
   "Latitude": 106.8502969
 },
 {
   "STT": 218,
   "Name": "Quầy thuốc Mỹ Tuyết",
   "address": "Số 300, tổ 3, ấp Long Khánh 3, xã Tam Phước, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8659901,
   "Latitude": 106.937049
 },
 {
   "STT": 219,
   "Name": "Quầy thuốc",
   "address": "Số 19, tổ 4, khu 3, TT Tân Phú, Tân Phú, Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 220,
   "Name": "Quầy thuốc",
   "address": "Số 1309, Phạm Văn Thuận, KP2, P. Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 221,
   "Name": "Quầy thuốc An Nguyên",
   "address": "Số 114, ấp Bình Phước, xã Tân Bình, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 222,
   "Name": "Quầy thuốc",
   "address": "Số 155A, ấp 1, Tân Hạnh, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9509494,
   "Latitude": 106.7794935
 },
 {
   "STT": 223,
   "Name": "Quầy thuốc Đỗ Minh",
   "address": "Số 134/8, tổ 90, KP 13, Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 224,
   "Name": "Quầy thuốc",
   "address": "Số 201, tổ 6, ấp 4, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9782781,
   "Latitude": 107.0185123
 },
 {
   "STT": 225,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Tổ 7, Kp8, TT Vĩnh An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 226,
   "Name": "Quầy thuốc",
   "address": "Chợ Lang Minh, ấp Đông Minh, Lang Minh, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 227,
   "Name": "Nhà thuốc Lê Khôi",
   "address": "51/22, pHẠM Văn Thuận, KP1, P. Tam Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9401514,
   "Latitude": 106.8676653
 },
 {
   "STT": 228,
   "Name": "Quầy thuốc",
   "address": "Số 138/YT, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 229,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Phước Bình, Long Thành, Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 230,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 231,
   "Name": "Quầy thuốc Nguyễn Thúy",
   "address": "Ấp Suối Cát 1, xã Suối Cát, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9200551,
   "Latitude": 107.3671431
 },
 {
   "STT": 232,
   "Name": "Quầy thuốc",
   "address": "Khu 1, ấp Phước Hòa, Long Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7075262,
   "Latitude": 107.0022941
 },
 {
   "STT": 233,
   "Name": "Quầy thuốc Đức Nguyên",
   "address": "Số 107, KP 10, P. Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 234,
   "Name": "Nhà thuốc Phước Thiện",
   "address": "Số 173/4, tổ 23, KP8, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 235,
   "Name": "Quầy thuốc Luyến Tiếu",
   "address": "Ấp 1, Lâm San, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7216641,
   "Latitude": 107.3394486
 },
 {
   "STT": 236,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "Ấp Cọ Dầu, Xuân Đông, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8419009,
   "Latitude": 107.3708275
 },
 {
   "STT": 237,
   "Name": "Quầy thuốc My My",
   "address": "Tổ 4, ấp 6, Thanh Sơn, Định Quán, Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 239,
   "Name": "Quầy thuốc",
   "address": "A 19, CC Long Điềm, P. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8908675,
   "Latitude": 106.8516406
 },
 {
   "STT": 240,
   "Name": "Quầy thuốc",
   "address": "Ấp 7, Phú An, Tân Phú, Đồng Nai",
   "Longtitude": 11.4190148,
   "Latitude": 107.4612752
 },
 {
   "STT": 241,
   "Name": "Quầy thuốc Ngọc Nhung",
   "address": "Ấp Hòan Quân, Long Giao, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8263722,
   "Latitude": 107.2319236
 },
 {
   "STT": 242,
   "Name": "Quầy thuốc Anh Tuấn",
   "address": "Số 160, ấp Hưng Hiệp, Hưng Lộc, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 243,
   "Name": "Quầy thuốc Bo Nga",
   "address": "Tổ 1, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8591326,
   "Latitude": 106.9294271
 },
 {
   "STT": 244,
   "Name": "Quầy thuốc Bích Phượng",
   "address": "Tổ 9, KP 8, TT Vĩnh An, huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 245,
   "Name": "Quầy thuốc ",
   "address": "KP 5, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 246,
   "Name": "Chi nhánh công ty cổ phần Traphaco tại Đồng Nai",
   "address": "A2-A3, Khu nhà ở Hóa An, Nguyễn Ái Quốc, Xã Hóa An, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9377684,
   "Latitude": 106.8043048
 },
 {
   "STT": 247,
   "Name": "Nhà thuốc Việt Hoàng",
   "address": "382B, Nguyễn Ái Quốc, KP 5, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9633117,
   "Latitude": 106.8400731
 },
 {
   "STT": 248,
   "Name": "Tủ thuốc trạm y tế xã Phú Lâm",
   "address": "Trạm Y tế xã Phú Lâm, ấp Thanh Thọ 1, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2785689,
   "Latitude": 107.4903294
 },
 {
   "STT": 249,
   "Name": "Quầy thuốc",
   "address": "Ấp 5, xã An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.8236322,
   "Latitude": 106.9283119
 },
 {
   "STT": 250,
   "Name": "Nhà thuốc Bệnh viện đa khoa Đồng Nai",
   "address": "Số 02, đường Đồng Khởi, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9559802,
   "Latitude": 106.8639985
 },
 {
   "STT": 251,
   "Name": "Tủ thuốc trạm y tế xã Phú Trung",
   "address": "Trạm Y tế xã Phú Trung, ấp Phú Thắng, xã Phú Trung, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3065609,
   "Latitude": 107.510672
 },
 {
   "STT": 252,
   "Name": "Tủ thuốc trạm y tế xã Phú Lộc",
   "address": "Trạm Y tế xã Phú Lộc, ấp 6, xã Phú Lộc, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.303164,
   "Latitude": 107.3976285
 },
 {
   "STT": 253,
   "Name": "Nhà thuốc Thanh Hường",
   "address": "63/A 2, tổ 2, ấp Nhị Hòa, Hiệp Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.932876,
   "Latitude": 106.8379803
 },
 {
   "STT": 254,
   "Name": "Quầy thuốc Đức Sang",
   "address": "Số 04, ấp 1, Suối Nho, Định Quán, Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 255,
   "Name": "Quầy thuốc ",
   "address": "374A, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 256,
   "Name": "Quầy thuốc",
   "address": "Chợ Bàu Hàm, xã Bàu Hàm, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9788619,
   "Latitude": 107.1032067
 },
 {
   "STT": 257,
   "Name": "Tủ thuốc trạm y tế xã Phú An",
   "address": "Trạm Y tế xã Phú An, ấp 3, xã Phú An, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.327505,
   "Latitude": 107.5180767
 },
 {
   "STT": 258,
   "Name": "Quầy thuốc Ý Hoa",
   "address": "280B/5, KP 2, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai (địa chỉ mới: 15, Phan Trung, KP 2, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai)",
   "Longtitude": 10.9266663,
   "Latitude": 106.8498825
 },
 {
   "STT": 259,
   "Name": "Nhà thuốc Nhất Đức",
   "address": "Số 68, Đồng Khởi, KP4, P. Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9528841,
   "Latitude": 106.8696182
 },
 {
   "STT": 260,
   "Name": "Quầy thuốc",
   "address": "207A, tổ 7, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8914633,
   "Latitude": 106.8490411
 },
 {
   "STT": 261,
   "Name": "Tủ thuốc trạm y tế xã Phú Thịnh",
   "address": "Trạm Y tế xã Phú Thịnh, ấp 5, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 262,
   "Name": "Quầy thuốc Tâm Thư",
   "address": "Ấp Bảo Thị, Xuân Định, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8855628,
   "Latitude": 107.2615975
 },
 {
   "STT": 263,
   "Name": "Tủ thuốc trạm y tế xã Đắc Lua",
   "address": "Trạm Y tế xã Đắc Lua, ấp 12, xã Đắc Lua, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.5421403,
   "Latitude": 107.3563854
 },
 {
   "STT": 264,
   "Name": "Quầy thuốc Vy Phương",
   "address": "Số nhà 2237, tổ 4, ấp Phương Lâm 1, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 265,
   "Name": "Tủ thuốc trạm y tế xã Phú Hòa",
   "address": "Trạm Y tế xã Phú Hòa, ấp 3, xã Phú Hòa, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2117776,
   "Latitude": 107.420787
 },
 {
   "STT": 266,
   "Name": "Tủ thuốc trạm y tế xã Phú Tân",
   "address": "Trạm Y tế xã Phú Tân, ấp 5, xã Phú Tân, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.303164,
   "Latitude": 107.3976285
 },
 {
   "STT": 267,
   "Name": "Tủ thuốc trạm y tế xã Phú Vinh",
   "address": "Trạm Y tế xã Phú Vinh, ấp 4, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2116296,
   "Latitude": 107.3615966
 },
 {
   "STT": 268,
   "Name": "Tủ thuốc trạm y tế xã Phú Lợi",
   "address": "Trạm Y tế xã Phú Lợi, ấp 4, xã Phú Lợi, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2236734,
   "Latitude": 107.3907968
 },
 {
   "STT": 269,
   "Name": "Tủ thuốc trạm y tế xã Ngọc Định",
   "address": "Trạm Y tế xã Ngọc Định, ấp Hòa Thành, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 270,
   "Name": "Tủ thuốc trạm y tế xã Thanh Sơn",
   "address": "Trạm Y tế xã Thanh Sơn, ấp 1, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2668955,
   "Latitude": 107.2856599
 },
 {
   "STT": 271,
   "Name": "Tủ thuốc trạm y tế xã Phú Ngọc",
   "address": "Trạm Y tế xã Phú Ngọc, ấp 3, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 272,
   "Name": "Quầy thuốc Thuận Hiền",
   "address": "ấp 3, Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6655903,
   "Latitude": 106.8656363
 },
 {
   "STT": 273,
   "Name": "Tủ thuốc trạm y tế xã Phú Túc",
   "address": "Trạm Y tế xã Phú Túc, ấp Chợ, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0891024,
   "Latitude": 107.1977007
 },
 {
   "STT": 274,
   "Name": "Tủ thuốc trạm y tế xã Phú Bình",
   "address": "Trạm Y tế xã Phú Bình, ấp Phú Tân, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 275,
   "Name": "Tủ thuốc trạm y tế xã Túc Trưng",
   "address": "Trạm Y tế xã Túc Trung, ấp Đồn Điền 3, xã Túc Trưng, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0913552,
   "Latitude": 107.2076368
 },
 {
   "STT": 276,
   "Name": "Tủ thuốc trạm y tế xã Phú Cường",
   "address": "Trạm Y tế xã Phú Cường, ấp Thống Nhất, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0888863,
   "Latitude": 107.1851223
 },
 {
   "STT": 277,
   "Name": "Tủ thuốc trạm y tế xã Suối Nho",
   "address": "Trạm Y tế xã Suối Nho, ấp 1, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0402632,
   "Latitude": 107.2822689
 },
 {
   "STT": 278,
   "Name": "Quầy thuốc Anh Trâm",
   "address": "Số 509, tỉnh lộ 766, ấp Tân Hòa, Xuân Thành, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9956725,
   "Latitude": 107.4271824
 },
 {
   "STT": 279,
   "Name": "Quầy thuốc",
   "address": "Số 39A, tổ 2, ấp 1, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.01287,
   "Latitude": 106.8509376
 },
 {
   "STT": 280,
   "Name": "Tủ thuốc trạm y tế xã Phú Sơn",
   "address": "Trạm Y tế xã Phú Sơn, ấp 3, xã Phú Sơn, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.327505,
   "Latitude": 107.5180767
 },
 {
   "STT": 281,
   "Name": "Quầy thuốc",
   "address": "KP4, Trường Chinh, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9510856,
   "Latitude": 107.0077683
 },
 {
   "STT": 282,
   "Name": "Quầy thuốc Khải Hiền",
   "address": "Ấp Bảo Định, Xuân Định, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8989588,
   "Latitude": 107.2576707
 },
 {
   "STT": 283,
   "Name": "Quầy thuốc Quang Trinh",
   "address": "Ấp 3, Xuân Hưng, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8622294,
   "Latitude": 107.4894036
 },
 {
   "STT": 284,
   "Name": "Quầy thuốc ",
   "address": "Ấp Phú Thắng, xã Phú Trung, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3065609,
   "Latitude": 107.510672
 },
 {
   "STT": 285,
   "Name": "Quầy thuốc",
   "address": "Ấp 2, Bàu Cạn, Long Thành, Đồng Nai",
   "Longtitude": 10.7361529,
   "Latitude": 107.0657549
 },
 {
   "STT": 286,
   "Name": "Quầy thuốc",
   "address": "Chợ Bàu Hàm, Bàu Hàm, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9788619,
   "Latitude": 107.1032067
 },
 {
   "STT": 287,
   "Name": "Quầy thuốc Minh Hiền",
   "address": "69, tổ 7, khu 12, Long Đức, Long Thành, Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 288,
   "Name": "Quầy thuốc Thành Phượng",
   "address": "Tổ 28, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8959925,
   "Latitude": 106.9082868
 },
 {
   "STT": 289,
   "Name": "Quầy thuốc Khánh Huyền",
   "address": "63, tổ 23, ấp Thái Hòa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 290,
   "Name": "Nhà thuốc Ngọc Duy",
   "address": "92A, KP 2, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9286926,
   "Latitude": 106.8582373
 },
 {
   "STT": 291,
   "Name": "Nhà thuốc Hương Khoa",
   "address": "487, Phạm Văn Thuận, KP 3, P.Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9420355,
   "Latitude": 106.8613744
 },
 {
   "STT": 292,
   "Name": "Quầy thuốc",
   "address": "Số 53/1, ấp Dốc Mơ, Gia Tân 1, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 293,
   "Name": "Quầy thuốc",
   "address": "Số 5/1, khu Phước Thuận, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811468,
   "Latitude": 106.9447146
 },
 {
   "STT": 294,
   "Name": "Quầy thuốc Bích Châu",
   "address": "tổ 9, ấp 2, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0111767,
   "Latitude": 106.8302688
 },
 {
   "STT": 295,
   "Name": "Quầy thuốc",
   "address": "Ấp Tân Hưng, Đồi 61, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9032262,
   "Latitude": 107.0234309
 },
 {
   "STT": 296,
   "Name": "Quầy thuốc",
   "address": "QL 51, ấp Xóm Gốc, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7647864,
   "Latitude": 106.9621677
 },
 {
   "STT": 297,
   "Name": "Quầy thuốc Nghĩa Hiệp",
   "address": "tổ 24, khu Phước Hải, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 298,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, Phước Bình, Long Thành, Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 299,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Tổ 14, ấp 1, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.01287,
   "Latitude": 106.8509376
 },
 {
   "STT": 300,
   "Name": "Quầy thuốc",
   "address": "ấp 3, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 301,
   "Name": "Nhà thuốc Khôi Vũ",
   "address": "10/3B, KP 11, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 302,
   "Name": "Quầy thuốc Quỳnh Đức",
   "address": "Ấp Thọ Bình, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9456025,
   "Latitude": 107.3501571
 },
 {
   "STT": 303,
   "Name": "Quầy thuốc ",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 304,
   "Name": "Nhà thuốc Điền Trung",
   "address": "18/80, KP Long Điềm, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916465,
   "Latitude": 106.8502385
 },
 {
   "STT": 305,
   "Name": "Quầy thuốc Thái Duy Hưng",
   "address": "274, Trần Quốc Toản, KP 4, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 306,
   "Name": "Quầy thuốc ",
   "address": "Ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7075262,
   "Latitude": 107.0022941
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc Long Phượng Thu",
   "address": "198, KP 1, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 308,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "Ấp 2, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 309,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "Phố 1, ấp 1, xã Phú Lợi, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.223673,
   "Latitude": 107.390797
 },
 {
   "STT": 310,
   "Name": "Quầy thuốc Thy Vân",
   "address": "Số 158/2C, KP1, P. Tân Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9419963,
   "Latitude": 106.8634047
 },
 {
   "STT": 311,
   "Name": "Nhà thuốc Nhân Hòa",
   "address": "Số 69, Bùi Hữu Nghĩa, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9084671,
   "Latitude": 106.8291804
 },
 {
   "STT": 312,
   "Name": "Quầy thuốc Thanh Minh",
   "address": "37/1/4, ấp Quảng Đà, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9433737,
   "Latitude": 107.0711529
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc ",
   "address": "396A, Phạm Văn Thuận, P.Trung Dũng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9577643,
   "Latitude": 106.8305678
 },
 {
   "STT": 314,
   "Name": "Tủ thuốc trạm y tế xã Bình Lợi",
   "address": "Trạm Y tế xã Bình Lợi, ấp 3, xã Bình Lợi, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0449779,
   "Latitude": 106.8171277
 },
 {
   "STT": 315,
   "Name": "Tủ thuốc trạm y tế xã Bình Hòa",
   "address": "Trạm Y tế xã Bình Hòa, ấp Bình Thạch, xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9991606,
   "Latitude": 106.8078596
 },
 {
   "STT": 316,
   "Name": "Tủ thuốc trạm y tế xã Tân An",
   "address": "Trạm Y tế xã Tân An, ấp 2, xã Tân An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0469722,
   "Latitude": 107.0189904
 },
 {
   "STT": 317,
   "Name": "Tủ thuốc trạm y tế thị trấn Vĩnh An",
   "address": "Trạm Y tế thị trấn Vĩnh An, KP 5, TT Vĩnh An, huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.087409,
   "Latitude": 107.0342226
 },
 {
   "STT": 318,
   "Name": "Tủ thuốc trạm y tế xã Mã Đà",
   "address": "Trạm Y tế xã Mã Đà, ấp 1, xã Mã Đà, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3049005,
   "Latitude": 107.0605278
 },
 {
   "STT": 319,
   "Name": "Tủ thuốc trạm y tế xã Vĩnh Tân",
   "address": "Trạm Y tế xã Vĩnh Tân, ấp 1, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0469722,
   "Latitude": 107.0189904
 },
 {
   "STT": 320,
   "Name": "Tủ thuốc trạm y tế xã Phú Lý",
   "address": "Trạm Y tế xã Phú Lý, ấp Lý Lịch, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3203867,
   "Latitude": 107.1631115
 },
 {
   "STT": 321,
   "Name": "Tủ thuốc trạm y tế xã Tân Bình",
   "address": "Trạm Y tế xã Tân Bình, ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9991606,
   "Latitude": 106.8078596
 },
 {
   "STT": 322,
   "Name": "Tủ thuốc trạm y tế xã Trị An",
   "address": "Trạm Y tế xã Trị An, ấp 1, xã Trị An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0996718,
   "Latitude": 106.9687885
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "Ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 324,
   "Name": "Quầy thuốc An Khang",
   "address": "Số 84, tổ 2 , ấp 3, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7447054,
   "Latitude": 107.0543005
 },
 {
   "STT": 325,
   "Name": "Quầy thuốc Cẩm Giang",
   "address": "Ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 326,
   "Name": "Quầy thuốc Lộc Thiên Phúc",
   "address": "17/1, Đỗ Văn Thi, ấp Nhất Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9333071,
   "Latitude": 106.8272567
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc Hồng Ân",
   "address": "21, ấp Đông Hải, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9791471,
   "Latitude": 106.9166514
 },
 {
   "STT": 328,
   "Name": "Quầy thuốc",
   "address": "144/403, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 329,
   "Name": "Quầy thuốc Lâm Thư",
   "address": "R204, Võ Thị Sáu, KP 7, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9470869,
   "Latitude": 106.8304255
 },
 {
   "STT": 330,
   "Name": "Nhà thuốc Châu Hà",
   "address": "14/21, tổ 21, KP4, P. Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.97206,
   "Latitude": 106.9097582
 },
 {
   "STT": 331,
   "Name": "Quầy thuốc ",
   "address": "51A, Nguyễn Ái Quốc, KP 2, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.966444,
   "Latitude": 106.854592
 },
 {
   "STT": 332,
   "Name": "Quầy thuốc Thiện Phúc",
   "address": "123/13A, KP 2, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9803043,
   "Latitude": 106.8548082
 },
 {
   "STT": 333,
   "Name": "Quầy thuốc",
   "address": "Số 798, ấp Trà Cổ, Bình Minh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9451841,
   "Latitude": 106.9804832
 },
 {
   "STT": 334,
   "Name": "Quầy thuốc Thảo Nguyên",
   "address": "Ấp Xuân Thiện, Xuân Thiện, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 335,
   "Name": "Quầy thuốc Nghĩa Hòa",
   "address": "Ấp 1, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 336,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Ấp Phước Lý, xã Đại Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 337,
   "Name": "Quầy thuốc ",
   "address": "220, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9641892,
   "Latitude": 106.9480419
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc Minh Hải",
   "address": "Đường Chu Văn An, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.782277,
   "Latitude": 106.9480058
 },
 {
   "STT": 339,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Vĩnh Khang",
   "address": "Số 25, ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 340,
   "Name": "Quầy thuốc Huy Hiệu",
   "address": "Tổ 9, ấp Đất Mới, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc",
   "address": "E1/027, Nam Sơn, Quang Trung, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc An Khánh Ngọc",
   "address": "91/1, KP 6, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 343,
   "Name": "Quầy thuốc Minh Anh",
   "address": "Tổ 19, khu 2, ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc ",
   "address": "Tổ 11, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc Vy Minh Trung 2",
   "address": "Tây Lạc, An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc An Lộc",
   "address": "Kios 1, chợ An Bình, ấp 8, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.8627628,
   "Latitude": 106.9650377
 },
 {
   "STT": 347,
   "Name": "Quầy thuốc Minh Thuận Phát",
   "address": "C1/4, KP 6, P.Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9546017,
   "Latitude": 106.8250331
 },
 {
   "STT": 348,
   "Name": "Quầy thuốc Thái Đức",
   "address": "2/1, ấp Tân Lập, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252675
 },
 {
   "STT": 349,
   "Name": "Quầy thuốc Thanh Hà 2",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 350,
   "Name": "Quầy thuốc",
   "address": "tổ 4, khu 12, Long Đức, Long Thành, Đồng Nai",
   "Longtitude": 10.7990234,
   "Latitude": 106.9580044
 },
 {
   "STT": 351,
   "Name": "Quầy thuốc Hoàng Hải",
   "address": "Tổ 2, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.981049,
   "Latitude": 107.019946
 },
 {
   "STT": 352,
   "Name": "Nhà thuốc Anh Tuyết",
   "address": "244, Phan Đình Phùng, KP 5, P.Trung Dũng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9487741,
   "Latitude": 106.8173453
 },
 {
   "STT": 353,
   "Name": "Nhà thuốc Hiền Lương 1",
   "address": "884C/6, Phạm Văn Thuận, KP3, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9413182,
   "Latitude": 106.8623827
 },
 {
   "STT": 354,
   "Name": "Quầy thuốc Nhật Vương",
   "address": "52/YT, Thái Hòa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 355,
   "Name": "Quầy thuốc",
   "address": "Tổ 10, KP 1, P. Tân Hòa, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9602942,
   "Latitude": 106.9089863
 },
 {
   "STT": 356,
   "Name": "Công ty cổ phần dược phẩm Ampharco U.S.A",
   "address": "Khu công nghiệp Nhơn Trạch 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7231049,
   "Latitude": 106.9447351
 },
 {
   "STT": 357,
   "Name": "Nhà thuốc Công ty cổ phần bệnh viện đa khoa Đồng Nai",
   "address": "02, Đồng Khởi, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9526833,
   "Latitude": 106.867861
 },
 {
   "STT": 358,
   "Name": "Quầy thuốc ",
   "address": "6, tổ 5, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 359,
   "Name": "Quầy thuốc Hưng Trinh",
   "address": "26, tổ 1, ấp 1, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9610006,
   "Latitude": 107.0281203
 },
 {
   "STT": 360,
   "Name": "Quầy thuốc Toàn Khiêm",
   "address": "Tổ 5, ấp 7, Thanh Sơn, Định Quán, Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 361,
   "Name": "Quầy thuốc Mai Uyên",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 362,
   "Name": "Quầy thuốc",
   "address": "154/4, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 363,
   "Name": "Quầy thuốc ",
   "address": "1406, tổ 22, ấp Vườn Dừa, Xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 364,
   "Name": "Quầy thuốc Ngọc Ly",
   "address": "Ấp Tín Nghĩa, Xuân Thiện, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 365,
   "Name": "Quầy thuốc",
   "address": "Tổ 13, khu 12, Long Đức, Long Thành, Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 366,
   "Name": "Quầy thuốc Minh Châu",
   "address": "69/4, Khu phố 2, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9509254,
   "Latitude": 106.938686
 },
 {
   "STT": 367,
   "Name": "Nhà thuốc Tân Vân",
   "address": "A2/103B, Bùi Hữu Nghĩa, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9091233,
   "Latitude": 106.8289141
 },
 {
   "STT": 368,
   "Name": "Quầy thuốc",
   "address": "20B, tổ 3, KP2, P. Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9671832,
   "Latitude": 106.8608579
 },
 {
   "STT": 369,
   "Name": "Quầy thuốc Thiên Phúc",
   "address": "SN 90B, Khu 9, đường Trà Cổ, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2310735,
   "Latitude": 107.4402467
 },
 {
   "STT": 370,
   "Name": "Quầy thuốc Ngọc Ánh",
   "address": "74A, ấp Tam Bung, Phú Cường, Định Quán, Đồng Nai",
   "Longtitude": 11.0696987,
   "Latitude": 107.2475893
 },
 {
   "STT": 371,
   "Name": "Quầy thuốc",
   "address": "Ấp Bình Trung, Tân An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0499492,
   "Latitude": 106.9254309
 },
 {
   "STT": 372,
   "Name": "Quầy thuốc",
   "address": "Chợ An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9629869,
   "Latitude": 106.9477583
 },
 {
   "STT": 373,
   "Name": "Quầy thuốc",
   "address": "ấp 7, Phú An, Tân Phú, Đồng Nai",
   "Longtitude": 11.4190148,
   "Latitude": 107.4612752
 },
 {
   "STT": 374,
   "Name": "Quầy thuốc Vĩnh Phúc",
   "address": "Chợ Đông Hòa, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9431534,
   "Latitude": 107.0673389
 },
 {
   "STT": 375,
   "Name": "Nhà thuốc Bạch Đào",
   "address": "Tổ 5, KP5, Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9416702,
   "Latitude": 106.8764069
 },
 {
   "STT": 376,
   "Name": "Quầy thuốc Long Vy",
   "address": "427, Quốc lộ 1A, ấp Hòa Bình, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9443289,
   "Latitude": 107.0639744
 },
 {
   "STT": 377,
   "Name": "Nhà thuốc",
   "address": "907, Bùi Văn Hòa, KP7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9131466,
   "Latitude": 106.8874839
 },
 {
   "STT": 378,
   "Name": "Quầy thuốc Duy Linh",
   "address": "118/1, ấp 3, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8810166,
   "Latitude": 106.9999856
 },
 {
   "STT": 379,
   "Name": "Quầy thuốc Thiên An",
   "address": " 189, ấp 3, Gia Canh, Định Quán, Đồng Nai",
   "Longtitude": 11.2098918,
   "Latitude": 107.4450231
 },
 {
   "STT": 380,
   "Name": "Quầy thuốc",
   "address": "Ấp Phúc Nhạc 1, Gia Tân 3, Thống Nhất, Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 381,
   "Name": "Quầy thuốc Cát Phượng",
   "address": "79/2, ấp Lộc Hòa, Tây Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 382,
   "Name": "Quầy thuốc Trường Giang",
   "address": "64, CMT8, KP4, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9458521,
   "Latitude": 107.008126
 },
 {
   "STT": 383,
   "Name": "Quầy thuốc",
   "address": "Tồ 6B, KP4, P. Trảng Dài, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9951615,
   "Latitude": 106.868093
 },
 {
   "STT": 384,
   "Name": "Quầy thuốc",
   "address": "Tổ 6B, KP4, P. Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9951615,
   "Latitude": 106.868093
 },
 {
   "STT": 385,
   "Name": "Quầy thuốc Hằng Đoàn",
   "address": "176. ấp 9/4, Xuân Thạnh, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9483523,
   "Latitude": 107.1602683
 },
 {
   "STT": 386,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "20, ấp Đồn Điền 1, Túc Trưng, Định Quán, Đồng Nai",
   "Longtitude": 11.0908761,
   "Latitude": 107.2073472
 },
 {
   "STT": 387,
   "Name": "Quầy thuốc Hồng Minh",
   "address": "Số 28A, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 388,
   "Name": "Quầy thuốc Vân Nga",
   "address": "396, tổ 14, khu 13, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 389,
   "Name": "Quầy thuốc ",
   "address": "Ấp An Lâm, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7587903,
   "Latitude": 106.9900204
 },
 {
   "STT": 390,
   "Name": "Quầy thuốc Minh Nhật",
   "address": "Khu 14, Long Đức, Long Thành, Đồng Nai",
   "Longtitude": 10.8193947,
   "Latitude": 106.9629589
 },
 {
   "STT": 391,
   "Name": "Quầy thuốc",
   "address": "26/5, Dốc Mơ 3, Gia Tân 1, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0599965,
   "Latitude": 107.168959
 },
 {
   "STT": 392,
   "Name": "Quầy thuốc Thanh Lan",
   "address": "Ấp Bầu Cối, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 393,
   "Name": "Quầy thuốc Phương Nga",
   "address": "Tổ 9, KP3, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9206738,
   "Latitude": 107.427994
 },
 {
   "STT": 394,
   "Name": "Quầy thuốc",
   "address": "Chợ 13, tổ 7, ấp 3, Phú Lập, Tân Phú, Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 395,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "903, QL 20, KP11, TT Tân Phú, Tân Phú, Đồng Nai",
   "Longtitude": 11.2715119,
   "Latitude": 107.4395201
 },
 {
   "STT": 396,
   "Name": "Quầy thuốc",
   "address": "90A/1, ấp Hưng Long, Hưng Thịnh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 397,
   "Name": "Quầy thuốc",
   "address": "C 47, tổ 2, KP3, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9539896,
   "Latitude": 106.8514218
 },
 {
   "STT": 398,
   "Name": "Quầy thuốc Thiện Tâm",
   "address": "Số 235, ấp Bảo Thị, Xuân Định, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8825811,
   "Latitude": 107.2646659
 },
 {
   "STT": 399,
   "Name": "Quầy thuốc Bình Minh",
   "address": "86/15, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 400,
   "Name": "Quầy thuốc Thành Đức",
   "address": "836, tổ 9, ấp Trần Hưng Đạo, Xuân Thạnh, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 401,
   "Name": "Quầy thuốc Xuân Hương",
   "address": "Số 126/9, ấp 5, Suối Nho, Định Quán, Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 402,
   "Name": "Tủ thuốc trạm y tế phường Bữu Hòa",
   "address": "Trạm Y tế P.Bửu Hoà, KP 1, Bùi Hữu Nghĩa, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9327466,
   "Latitude": 106.8194217
 },
 {
   "STT": 403,
   "Name": "Quầy thuốc ",
   "address": "A52, ấp Phước Lý, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 404,
   "Name": "Quầy thuốc ",
   "address": "104, tổ 1, KP 11, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.927241,
   "Latitude": 106.8571987
 },
 {
   "STT": 405,
   "Name": "Nhà thuốc",
   "address": "202 CMT8, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 406,
   "Name": "Quầy thuốc số 2128",
   "address": "15, Ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9955825,
   "Latitude": 106.9661135
 },
 {
   "STT": 407,
   "Name": "Quầy thuốc Thiện Phúc",
   "address": "Số 2590, ấp Tân Tiến, Xuân Hiệp, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9118405,
   "Latitude": 107.3935086
 },
 {
   "STT": 408,
   "Name": "Nhà thuốc Phòng khám đa khoa Sinh Hậu",
   "address": "27/13, KP 5, P.Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 409,
   "Name": "Quầy thuốc Trân Lộc",
   "address": "26, Ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 410,
   "Name": "Quầy thuốc Linh Chi",
   "address": "Ấp 2, xã Phú Ngọc, Định Quán, Đồng Nai",
   "Longtitude": 11.1364176,
   "Latitude": 107.3014846
 },
 {
   "STT": 411,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Lão Cần Tế 3",
   "address": "Chợ Long Thành, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7770943,
   "Latitude": 106.956174
 },
 {
   "STT": 412,
   "Name": "Quầy thuốc Đại Thiên Lộc",
   "address": "Ấp Gia Tỵ, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9846718,
   "Latitude": 107.3917612
 },
 {
   "STT": 413,
   "Name": "Quầy thuốc ",
   "address": "694, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 414,
   "Name": "Quầy thuốc ",
   "address": "321, tổ 7, ấp Thiên Bình, Xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8689637,
   "Latitude": 106.9229487
 },
 {
   "STT": 415,
   "Name": "Quầy thuốc ",
   "address": "4/5, tổ 8, KP4, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720567,
   "Latitude": 106.9097648
 },
 {
   "STT": 416,
   "Name": "Quầy thuốc Phương Thảo ",
   "address": "Ấp 2A, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 417,
   "Name": "Quầy thuốc Nhật Lâm",
   "address": "A6, QL 1, KP 2, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.866273,
   "Latitude": 106.78896
 },
 {
   "STT": 418,
   "Name": "Quầy thuốc Anh Đào",
   "address": "Khu 4, ấp Hòa Trung, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 419,
   "Name": "Nhà thuốc Đức Long 2",
   "address": "93/76C, Đồng Khởi, KP8, P. Tân Phong, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9698967,
   "Latitude": 106.8532091
 },
 {
   "STT": 420,
   "Name": "Quầy thuốc Thiện Dậu",
   "address": "85, KP 4, ,TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9498488,
   "Latitude": 107.0084808
 },
 {
   "STT": 421,
   "Name": "Quầy thuốc Minh Thư",
   "address": "79/25, ấp 5, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 422,
   "Name": "Nhà thuốc Bệnh viện Y dược cổ truyền Đồng Nai",
   "address": "Đường Đồng Khởi, KP 9, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9748871,
   "Latitude": 106.8486812
 },
 {
   "STT": 423,
   "Name": "Nhà thuốc Hải Hiền",
   "address": "40/6, KP 6, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 424,
   "Name": "Quầy thuốc công ty TNHH Phòng khám đa khoa Long Bình",
   "address": "Số 85, Bùi Văn Hòa, KP5, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8996295,
   "Latitude": 106.8910996
 },
 {
   "STT": 425,
   "Name": "Nhà thuốc ",
   "address": "57/12, Phạm Văn Thuận, KP 1, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.94062,
   "Latitude": 106.863743
 },
 {
   "STT": 426,
   "Name": "Nhà thuốc Linh Giang",
   "address": "28, Bùi Văn Hòa, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9319693,
   "Latitude": 106.8749571
 },
 {
   "STT": 427,
   "Name": "Quầy thuốc Anh Viễn",
   "address": "45/B, ấp An Hòa, xã Hóa An, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 428,
   "Name": "Quầy thuốc",
   "address": "Ấp 9, Sông Ray, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7590686,
   "Latitude": 107.351669
 },
 {
   "STT": 429,
   "Name": "Nhà thuốc Minh Quân",
   "address": "54/3, KP 2, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9286926,
   "Latitude": 106.8582373
 },
 {
   "STT": 430,
   "Name": "Quầy thuốc Hào Gia Hân",
   "address": "02, tổ 6, KP 3, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9292629,
   "Latitude": 106.8583352
 },
 {
   "STT": 431,
   "Name": "Quầy thuốc Bích Thủy",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 432,
   "Name": "Quầy thuốc",
   "address": "D8, tổ 8C, KP5, phường An Bình, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 433,
   "Name": "Công ty cổ phần dược Đồng Nai",
   "address": "221B, Phạm Văn Thuận, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9574332,
   "Latitude": 106.8419292
 },
 {
   "STT": 434,
   "Name": "Quầy thuốc Minh Anh",
   "address": "Ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 435,
   "Name": "Quầy thuốc Long Phượng Thu",
   "address": "3/6, KP 4A, phường Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9668904,
   "Latitude": 106.9079296
 },
 {
   "STT": 436,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Tế Sanh Đường",
   "address": "1B, Hùng Vương, P.Xuân Trung, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 437,
   "Name": "Quầy thuốc ",
   "address": "Số 206, khu 13, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 438,
   "Name": "Quầy thuốc ",
   "address": "267, ấp Hưng Hiệp, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 439,
   "Name": "Quầy thuốc",
   "address": "khu 9, TT. Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 440,
   "Name": "Nhà thuốc Trà My",
   "address": "16/8A, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720567,
   "Latitude": 106.9097648
 },
 {
   "STT": 441,
   "Name": "Quầy thuốc số 1287",
   "address": "24/117, KP3, P.Tân Mai, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9647896,
   "Latitude": 106.8651461
 },
 {
   "STT": 442,
   "Name": "Quầy thuốc Tuyết Mai",
   "address": "125/6, ấp Quảng Phát, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9449414,
   "Latitude": 106.9943646
 },
 {
   "STT": 443,
   "Name": "Quầy thuốc Trần Mai",
   "address": "30/8, khu A, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 444,
   "Name": "Nhà thuốc Ái Quốc",
   "address": "40A, KP6, phường Tân Tiến, thành phố  Biên Hòa",
   "Longtitude": 10.96499,
   "Latitude": 106.846695
 },
 {
   "STT": 445,
   "Name": "Quầy thuốc Ngọc Minh",
   "address": "ấp Bình Trung, xã Tân An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0499492,
   "Latitude": 106.9254309
 },
 {
   "STT": 446,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 447,
   "Name": "Quầy thuốc",
   "address": "Chợ Phú Lập, Tân Phú, Đồng Nai",
   "Longtitude": 11.3651863,
   "Latitude": 107.389791
 },
 {
   "STT": 448,
   "Name": "Nhà thuốc Mỹ Lộc",
   "address": "105A1, Hưng Đạo Vương, KP 1, P.Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9495988,
   "Latitude": 106.8246592
 },
 {
   "STT": 449,
   "Name": "Quầy thuốc Vân Anh",
   "address": "1777PD, ấp Phú Dòng, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0847609,
   "Latitude": 107.1728041
 },
 {
   "STT": 450,
   "Name": "Quầy thuốc",
   "address": "Ấp Vàm, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 451,
   "Name": "Nhà thuốc ",
   "address": "224K, tổ 8, Ấp Long Đức 3, Xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 452,
   "Name": "Quầy thuốc",
   "address": "A 15, Khu 94, Ấp Long Đức 1, xã Tam Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 453,
   "Name": "Quầy thuốc ",
   "address": "Tổ 3, ấp 1, xã Mã Đà, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 454,
   "Name": "Chi nhánh công ty TNHH Thương mại Dịch vụ Nam Giang",
   "address": "G243, Bùi Văn Hòa, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.915728,
   "Latitude": 106.895151
 },
 {
   "STT": 455,
   "Name": "Quầy thuốc Lương Bằng",
   "address": "Số 2121, tổ 7, ấp 1B, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6860681,
   "Latitude": 107.0411068
 },
 {
   "STT": 456,
   "Name": "Quầy thuốc",
   "address": "158, Lê Duẩn, tổ 2, khu Bàu Cá, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 457,
   "Name": "Quầy thuốc Tân Bình",
   "address": "468, tổ 12, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 458,
   "Name": "Quầy thuốc ",
   "address": "17, lô A4, đường 6, KDC An Bình, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9302826,
   "Latitude": 106.8858082
 },
 {
   "STT": 459,
   "Name": "Quầy thuốc Huỳnh Châu",
   "address": "Tổ 8, khu 6, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9281732,
   "Latitude": 107.4144526
 },
 {
   "STT": 460,
   "Name": "Quầy thuốc",
   "address": "65/2E, ấp Võ Dõng, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 461,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 462,
   "Name": "Quầy thuốc",
   "address": "19, tổ 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.95
 },
 {
   "STT": 463,
   "Name": "Quầy thuốc Minh Duyên",
   "address": "K4/79, KP 3, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 464,
   "Name": "Quầy thuốc Phương Hoa",
   "address": "Khu 4, Ấp 8, Xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 465,
   "Name": "Quầy thuốc",
   "address": "Khu chợ An Viễn, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8627628,
   "Latitude": 106.9650377
 },
 {
   "STT": 466,
   "Name": "Quầy thuốc Ngọc Bảo Vy",
   "address": "G14, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 467,
   "Name": "Quầy thuốc ",
   "address": "11/A1, ấp Đức Long, xã Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 468,
   "Name": "Quầy thuốc ",
   "address": "280, ấp Hưng Nhơn, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 469,
   "Name": "Quầy thuốc Thu Hiền",
   "address": "57/1A, Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9629522,
   "Latitude": 106.9436634
 },
 {
   "STT": 470,
   "Name": "Quầy thuốc ",
   "address": "ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 471,
   "Name": "Quầy thuốc",
   "address": "Ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6790847,
   "Latitude": 107.0267131
 },
 {
   "STT": 472,
   "Name": "Nhà thuốc Minh Tâm Thủy",
   "address": "140C/1, KP 5, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 473,
   "Name": "Quầy thuốc Hải Hiền",
   "address": "263, Quốc lộ 1A, ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 474,
   "Name": "Quầy thuốc Bích Lệ",
   "address": "Số 221, khóm 8, khu 3, TT Tân Phú, Tân Phú, Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 475,
   "Name": "Nhà thuốc Quang Hưng",
   "address": "Số 157, đường Khổng Tử, P.Xuân Trung, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9361885,
   "Latitude": 107.2454171
 },
 {
   "STT": 476,
   "Name": "Tủ thuốc trạm y tế thị trấn Định Quán",
   "address": "Trạm Y tế thị trấn Định Quán, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2116296,
   "Latitude": 107.3615966
 },
 {
   "STT": 477,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "A4/350, KP 4, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9117711,
   "Latitude": 106.8272458
 },
 {
   "STT": 478,
   "Name": "Quầy thuốc",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 479,
   "Name": "Quầy thuốc",
   "address": "Số 24, Khu 5, Ấp 8, Xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7372197,
   "Latitude": 107.1118549
 },
 {
   "STT": 480,
   "Name": "Quầy thuốc Đào Vy",
   "address": "K4/5A, KP 4, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.926897,
   "Latitude": 106.8160971
 },
 {
   "STT": 481,
   "Name": "Quầy thuốc Mỹ Lợi",
   "address": "SN 581, ấp 6, xã Phú Lộc, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2986537,
   "Latitude": 107.4144526
 },
 {
   "STT": 482,
   "Name": "Quầy thuốc",
   "address": "Tổ 3, Ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 483,
   "Name": "Quầy thuốc ",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 484,
   "Name": "Quầy thuốc",
   "address": "Ấp Láng Lớn, Xuân Mỹ, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 485,
   "Name": "Nhà thuốc Phan Chu Trinh",
   "address": "67, Phan Chu Trinh, P.Hòa Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9475252,
   "Latitude": 106.8145899
 },
 {
   "STT": 486,
   "Name": "Quầy thuốc ",
   "address": "Ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7075262,
   "Latitude": 107.0022941
 },
 {
   "STT": 487,
   "Name": "Quầy thuốc Hải Anh",
   "address": "Ấp 1, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 488,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 489,
   "Name": "Tủ thuốc trạm y tế xã La Ngà",
   "address": "Trạm Y tế xã La Ngà, ấp 3, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1590512,
   "Latitude": 107.2480574
 },
 {
   "STT": 490,
   "Name": "Quầy thuốc Thanh Mỹ",
   "address": "Ấp Sơn Hà, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 491,
   "Name": "Quầy thuốc Hồng Nghị",
   "address": "35/1, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 492,
   "Name": "Nhà thuốc Gia Hân",
   "address": "Số nhà 07, tổ 1, ấp Phương Mai 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 493,
   "Name": "Quầy thuốc",
   "address": "72, Phan Trung, KP 7, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9611154,
   "Latitude": 106.8514172
 },
 {
   "STT": 494,
   "Name": "Quầy thuốc Toàn Lan",
   "address": "Ấp Tân Hưng, xã Đồi 61, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9032262,
   "Latitude": 107.0234309
 },
 {
   "STT": 495,
   "Name": "Quầy thuốc ",
   "address": "ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 496,
   "Name": "Quầy thuốc Cao Linh",
   "address": "Ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 497,
   "Name": "Nhà thuốc",
   "address": "57A, Đồng Khởi, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9711598,
   "Latitude": 106.8530063
 },
 {
   "STT": 498,
   "Name": "Quầy thuốc Thanh Nguyệt",
   "address": "KP 5, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 499,
   "Name": "Quầy thuốc Quỳnh Anh 9",
   "address": "Tổ 8, ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 500,
   "Name": "Quầy thuốc",
   "address": "Ấp 5, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8681203,
   "Latitude": 107.4435418
 },
 {
   "STT": 501,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "15A, Nguyễn An Ninh, Khu 6, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9194604,
   "Latitude": 107.4157557
 },
 {
   "STT": 503,
   "Name": "Quầy thuốc Thắng Hương",
   "address": "38 ấp 4, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 504,
   "Name": "Quầy thuốc An Ngọc Minh",
   "address": "Hương lộ 21, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 505,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "Ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 506,
   "Name": "Quầy thuốc Quỳnh Như",
   "address": "Ấp 2, Sông Nhạn, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8422168,
   "Latitude": 107.113007
 },
 {
   "STT": 507,
   "Name": "Quầy thuốc ",
   "address": "Chợ Nhân Nghĩa, xã Nhân Nghĩa, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8373313,
   "Latitude": 107.2364545
 },
 {
   "STT": 508,
   "Name": "Quầy thuốc ",
   "address": "88/4, ấp 1, xã Xuân Đường, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8115555,
   "Latitude": 107.1869731
 },
 {
   "STT": 509,
   "Name": "Quầy thuốc ",
   "address": "Ấp 9, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7424691,
   "Latitude": 107.3493185
 },
 {
   "STT": 510,
   "Name": "Quầy thuốc Thanh Loan",
   "address": "Ấp Vũng Gấm, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 511,
   "Name": "Nhà thuốc Tùng Anh",
   "address": "Số 109, ấp An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 512,
   "Name": "Quầy thuốc",
   "address": "24, tổ 13, KP6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 513,
   "Name": "Quầy thuốc Kim Thiên ",
   "address": "567, tổ 9, khu3, ấp 3, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 514,
   "Name": "Tủ thuốc trạm y tế phường Long Bình",
   "address": "KP 3, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9413981,
   "Latitude": 106.9004472
 },
 {
   "STT": 515,
   "Name": "Quầy thuốc ",
   "address": "21/1, tổ 16, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 516,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 517,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "Ấp Phước Lương, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7405091,
   "Latitude": 106.7902063
 },
 {
   "STT": 518,
   "Name": "Quầy thuốc ",
   "address": "Ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7371768,
   "Latitude": 107.1120842
 },
 {
   "STT": 519,
   "Name": "Nhà thuốc Tuệ Tĩnh",
   "address": "105A, Nguyễn Ái Quốc, KP 8, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.966911,
   "Latitude": 106.8530485
 },
 {
   "STT": 520,
   "Name": "Quầy thuốc ",
   "address": "847, ấp Trần Hưng Đạo, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9436707,
   "Latitude": 107.156248
 },
 {
   "STT": 521,
   "Name": "Quầy thuốc Ngọc Hằng",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 522,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Tổ 13, khu 12, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 523,
   "Name": "Quầy thuốc ",
   "address": "A1/008, ấp Nguyễn Huệ, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 524,
   "Name": "Quầy thuốc Linh Đan",
   "address": "SN02, khu 6, ấp 7, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 525,
   "Name": "Quầy thuốc Quỳnh Châu",
   "address": "Ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 526,
   "Name": "Quầy thuốc ",
   "address": "Tổ 10, KP 3, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 527,
   "Name": "Quầy thuốc Hoàng Hải",
   "address": "Tổ 15, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.016134,
   "Latitude": 106.8289876
 },
 {
   "STT": 528,
   "Name": "Quầy thuốc Tuyết Trung",
   "address": "KP 8, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0925541,
   "Latitude": 107.0365307
 },
 {
   "STT": 529,
   "Name": "Quầy thuốc Đức Nam",
   "address": "44, tổ 1, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 530,
   "Name": "Quầy thuốc ",
   "address": "642, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9396847,
   "Latitude": 106.9808018
 },
 {
   "STT": 531,
   "Name": "Quầy thuốc",
   "address": "Tổ 4, ấp 5, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8622222,
   "Latitude": 106.9633333
 },
 {
   "STT": 532,
   "Name": "Quầy thuốc Diệp Lan",
   "address": "Khu tái định cư ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7577014,
   "Latitude": 106.9883589
 },
 {
   "STT": 533,
   "Name": "Quầy thuốc",
   "address": "A153/4, tổ 6, KP1, phường Long Bình, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 534,
   "Name": "Quầy thuốc Mỹ Kim",
   "address": "Số 3/3, ấp 3, xã Nhân Nghĩa, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8459835,
   "Latitude": 107.2353704
 },
 {
   "STT": 535,
   "Name": "Quầy thuốc Tuyết Hồng",
   "address": "Ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 536,
   "Name": "Quầy thuốc",
   "address": "5/56, KP 9, phường Hố Nai, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 537,
   "Name": "Quầy thuốc",
   "address": "E647B, tổ 13, KP 5A, phường Long Bình, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 538,
   "Name": "Quầy thuốc",
   "address": "200E, tổ 35A, KP 11, phường Tân Phong, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 539,
   "Name": "Quầy thuốc Ngọc Tuyết Nga",
   "address": "93/464B, KP 4, phường Tân Mai, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9413783,
   "Latitude": 106.8621359
 },
 {
   "STT": 540,
   "Name": "Quầy thuốc",
   "address": "440D/10, ấp Lập Thành, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.949412,
   "Latitude": 107.14339
 },
 {
   "STT": 541,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Vạn An Đường",
   "address": "28, KP1, Nguyễn Hiền Vương, P.Thanh Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9466475,
   "Latitude": 106.8157524
 },
 {
   "STT": 542,
   "Name": "Quầy thuốc Hà Trinh",
   "address": "Tổ 1, ấp 7, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 543,
   "Name": "Quầy thuốc công ty TNHH Phòng khám đa khoa Bách Thư",
   "address": "126B, Phan Đình Phùng, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9504193,
   "Latitude": 106.8179226
 },
 {
   "STT": 544,
   "Name": "Nhà thuốc Long Bình 4",
   "address": "42B, Bùi Văn Hòa, tổ 5, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9120253,
   "Latitude": 106.8905672
 },
 {
   "STT": 545,
   "Name": "Quầy thuốc",
   "address": "Số 1910, ấp 1C, Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.6773495,
   "Latitude": 107.0274601
 },
 {
   "STT": 546,
   "Name": "Quầy thuốc Hồng Hạnh",
   "address": "Chợ Bình Sơn, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7927597,
   "Latitude": 107.0186055
 },
 {
   "STT": 547,
   "Name": "Tủ thuốc trạm y tế xã Gia Kiệm",
   "address": "Trạm Y tế xã Gia Kiệm, ấp Võ Dõng 1, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0286464,
   "Latitude": 107.174769
 },
 {
   "STT": 548,
   "Name": "Quầy thuốc Tường Vy",
   "address": "Ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 549,
   "Name": "Quầy thuốc Hải Hà",
   "address": "Ấp Phước Lý, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 550,
   "Name": "Quầy thuốc",
   "address": "Ấp Bình Thạnh, xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9774416,
   "Latitude": 106.8015129
 },
 {
   "STT": 551,
   "Name": "Nhà thuốc",
   "address": "Số 15/15, KP 8, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8494318,
   "Latitude": 106.7826396
 },
 {
   "STT": 552,
   "Name": "Nhà thuốc Thiên Minh",
   "address": "92/4, Trần Quốc Toản, KP4, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9394961,
   "Latitude": 106.8615706
 },
 {
   "STT": 553,
   "Name": "Quầy thuốc Bảo Quyên",
   "address": "Khóm 8, khu 9, đường Trà Cổ, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 554,
   "Name": "Quầy thuốc Hoàng Đạt",
   "address": "17, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9653613,
   "Latitude": 106.939496
 },
 {
   "STT": 555,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Văn Minh",
   "address": "4A/72, KP 11, phường Hố Nai, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 556,
   "Name": "Quầy thuốc Nhã Hoa",
   "address": "Ấp Vĩnh An, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2202888,
   "Latitude": 107.172085
 },
 {
   "STT": 557,
   "Name": "Quầy thuốc Hồng Vi",
   "address": "Ấp Đông Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 558,
   "Name": "Nhà thuốc",
   "address": "Số 3, Nguyễn Ái Quốc, KP 7, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656457,
   "Latitude": 106.846072
 },
 {
   "STT": 559,
   "Name": "Quầy thuốc Hoàng Lan",
   "address": "Ấp 2, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9829292,
   "Latitude": 107.0268276
 },
 {
   "STT": 560,
   "Name": "Quầy thuốc ",
   "address": "1855, tổ 6, ấp Thọ Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.280634,
   "Latitude": 107.480343
 },
 {
   "STT": 561,
   "Name": "Nhà thuốc Ánh Mai",
   "address": "Số 9/13F, KP6, P. Tam Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 562,
   "Name": "Quầy thuốc ",
   "address": "563B, ấp Khương Phước, Xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9168346,
   "Latitude": 106.9448957
 },
 {
   "STT": 563,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "SN 1056/D, ấp Thống, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1086911,
   "Latitude": 107.1641629
 },
 {
   "STT": 564,
   "Name": "Quầy thuốc ",
   "address": "E934A, tổ 37, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916537,
   "Latitude": 106.8501757
 },
 {
   "STT": 565,
   "Name": "Quầy thuốc",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 566,
   "Name": "Nhà thuốc ",
   "address": "7/1A, KP7, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9890275,
   "Latitude": 106.8296372
 },
 {
   "STT": 567,
   "Name": "Quầy thuốc Hoàng Duy",
   "address": "Số 02A, Đồng Khởi, ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9677871,
   "Latitude": 106.8535132
 },
 {
   "STT": 568,
   "Name": "Quầy thuốc",
   "address": "922, ấp 2, Phước Khánh, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 569,
   "Name": "Quầy thuốc",
   "address": "165/18 Ấp Bến Đình, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 570,
   "Name": "Quầy thuốc Duẫn",
   "address": "Ấp Bến Nôm II, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1086911,
   "Latitude": 107.1641629
 },
 {
   "STT": 571,
   "Name": "Quầy thuốc Minh Phương",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 572,
   "Name": "Quầy thuốc Nguyễn Ngọc Trinh",
   "address": "Chợ Nam Hà, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 573,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Số 71, phố 6, ấp 3, Phú Vinh, Định Quán, Đồng Nai",
   "Longtitude": 11.2374121,
   "Latitude": 107.3434916
 },
 {
   "STT": 574,
   "Name": "Quầy thuốc Phi Nhạn",
   "address": "03, Tổ 5, Ấp Chợ, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 575,
   "Name": "Nhà thuốc Trang Tuấn",
   "address": "Tổ 25, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9722839,
   "Latitude": 106.9098855
 },
 {
   "STT": 576,
   "Name": "Quầy thuốc ",
   "address": "G73, tổ 7, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 577,
   "Name": "Quầy thuốc ",
   "address": "70/347, Phạm Văn Thuận, KP 4, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9421688,
   "Latitude": 106.8611627
 },
 {
   "STT": 578,
   "Name": "Quầy thuốc  07 Tờ",
   "address": "Chợ Phú Cường, Phú Tân, Phú Cường, Định Quán, Đồng Nai",
   "Longtitude": 11.088071,
   "Latitude": 107.1837001
 },
 {
   "STT": 579,
   "Name": "Quầy thuốc Nguyễn Nga",
   "address": "Ấp 3, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 580,
   "Name": "Quầy thuốc Ngọc Oanh",
   "address": "60, ấp Trung Nghĩa, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9572525,
   "Latitude": 107.4080686
 },
 {
   "STT": 581,
   "Name": "Quầy thuốc Long Nhi",
   "address": "420, ấp Gia Lào, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9708088,
   "Latitude": 107.3755567
 },
 {
   "STT": 582,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 583,
   "Name": "Quầy thuốc",
   "address": "Số 151, ấp 2, Xuân Quế, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8612799,
   "Latitude": 107.1602683
 },
 {
   "STT": 584,
   "Name": "Quầy thuốc Bảo An",
   "address": "33, khu 2, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8281006,
   "Latitude": 106.9318203
 },
 {
   "STT": 585,
   "Name": "Chi nhánh công ty cổ phần Dược Hậu Giang tại tỉnh Đồng Nai",
   "address": "Lô P73-P78, KP 7, Võ Thị Sáu, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9450519,
   "Latitude": 106.8212806
 },
 {
   "STT": 586,
   "Name": "Quầy thuốc ",
   "address": "15/4E, Huỳnh Văn Nghệ, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.954578,
   "Latitude": 106.798018
 },
 {
   "STT": 587,
   "Name": "Quầy thuốc",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 588,
   "Name": "Quầy thuốc Hoàng Yến",
   "address": "Tổ 15, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 589,
   "Name": "Quầy thuốc Tường Vi",
   "address": "Ấp 2, Sông Nhạn, Cẫm Mỹ, Đồng Nai",
   "Longtitude": 10.8422168,
   "Latitude": 107.113007
 },
 {
   "STT": 590,
   "Name": "Quầy thuốc ",
   "address": "F4/114, ấp Nguyễn Huệ, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 591,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "296, khu7, ấp 5, xã Phú Tân, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1775809,
   "Latitude": 107.3410392
 },
 {
   "STT": 592,
   "Name": "Quầy thuốc Hoàng Phương",
   "address": "598A, ấp Ngũ Phúc, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 593,
   "Name": "Quầy thuốc ",
   "address": "Phú Mỹ II, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 594,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Số 645, tổ 2, ấp 1, xã Phú Điền, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 595,
   "Name": "Nhà thuốc Thanh Vân",
   "address": "120, Nguyễn Ái Quốc, KP7, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9718309,
   "Latitude": 106.8906847
 },
 {
   "STT": 596,
   "Name": "Quầy thuốc Quang Anh",
   "address": "Số 43, đường 30/4, KP 3, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9485338,
   "Latitude": 106.8198859
 },
 {
   "STT": 597,
   "Name": "Quầy thuốc Mỹ Nhân",
   "address": "30, tổ 1, ấp 4, xã Phú Lộc, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2986537,
   "Latitude": 107.4144526
 },
 {
   "STT": 598,
   "Name": "Quầy thuốc Phúc Nguyên",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 599,
   "Name": "Quầy thuốc",
   "address": "Ấp 4, xã Phú Lập, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.358163,
   "Latitude": 107.3849497
 },
 {
   "STT": 600,
   "Name": "Quầy thuốc Minh Huyền",
   "address": "4421 H, Ấp Bến Nôm 2, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1086911,
   "Latitude": 107.1641629
 },
 {
   "STT": 601,
   "Name": "Quầy thuốc ",
   "address": "Ấp Giồng Ông Đông, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 602,
   "Name": "Quầy thuốc Đăng Khôi",
   "address": "Ấp Tân Bắc, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 603,
   "Name": "Quầy thuốc",
   "address": "Ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9108529,
   "Latitude": 107.3628616
 },
 {
   "STT": 604,
   "Name": "Quầy thuốc",
   "address": "A 27, Tổ 8C, KP 5, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9292629,
   "Latitude": 106.8583352
 },
 {
   "STT": 605,
   "Name": "Nhà thuốc Hải Hoàng",
   "address": "1423/11, tổ 20, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 606,
   "Name": "Quầy thuốc ",
   "address": "34A/23, Đồng Khởi, KP3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9713649,
   "Latitude": 106.8529635
 },
 {
   "STT": 607,
   "Name": "Nhà thuốc Cẩm Thúy",
   "address": "91, Nguyễn Ái Quốc, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.963171,
   "Latitude": 106.8381254
 },
 {
   "STT": 608,
   "Name": "Nhà thuốc Y Đức",
   "address": "18A, tổ 22C, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 609,
   "Name": "Quầy thuốc Kim Hiển",
   "address": "Ấp 2, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 610,
   "Name": "Nhà thuốc Nhân Huyền",
   "address": "16Đ, tổ 14, KP 2, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 611,
   "Name": "Quầy thuốc ",
   "address": "26, tổ 1, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 612,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 613,
   "Name": "Quầy thuốc Mã Lâm Hiếu",
   "address": "Ấp Bình Hòa , xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9060699,
   "Latitude": 107.323272
 },
 {
   "STT": 614,
   "Name": "Nhà thuốc Vy Minh Trung",
   "address": "3, KP 4, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9689888,
   "Latitude": 106.8945456
 },
 {
   "STT": 615,
   "Name": "Nhà thuốc ",
   "address": "105/7, KP 5A, QL 1A, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9689986,
   "Latitude": 106.8930586
 },
 {
   "STT": 616,
   "Name": "Quầy thuốc",
   "address": "193/5, Phan Trung, KP2, P. Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9600572,
   "Latitude": 106.8468475
 },
 {
   "STT": 617,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "1A57, Trần Hưng Đạo, KP 6, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9145632,
   "Latitude": 107.4118212
 },
 {
   "STT": 618,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Vĩnh Hưng",
   "address": "33, Phố 1, Ấp 1, Xã Phú Lợi, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 619,
   "Name": "Quầy thuốc Thu Hường",
   "address": "Tổ 14, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0159709,
   "Latitude": 106.8355372
 },
 {
   "STT": 620,
   "Name": "Quầy thuốc Kim Liên",
   "address": "Tổ 12, KP 5, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 621,
   "Name": "Quầy thuốc (trực thuộc công ty cổ phần Dược phẩm Đồng Nai)",
   "address": "Ấp 3, chợ Hiếu Liêm, xã Hiếu Liêm, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.1118386,
   "Latitude": 106.966643
 },
 {
   "STT": 622,
   "Name": "Quầy thuốc (trực thuộc công ty cổ phần Dược phẩm Đồng Nai)",
   "address": "Ấp Hiệp Quyết, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 623,
   "Name": "Nhà thuốc ",
   "address": "17/6C, Huỳnh Văn Nghệ, KP2, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9541547,
   "Latitude": 106.8001512
 },
 {
   "STT": 624,
   "Name": "Quầy thuốc",
   "address": "27A/1, ấp Quảng Đà, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9441142,
   "Latitude": 107.0711762
 },
 {
   "STT": 625,
   "Name": "Quầy thuốc Minh Thời",
   "address": "Tổ 57, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 626,
   "Name": "Quầy thuốc ",
   "address": "Ấp Tân Lập 2, xa Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 627,
   "Name": "Quầy thuốc Hoàng Sỹ",
   "address": "237, ấp Tân Bắc, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 628,
   "Name": "Quầy thuốc",
   "address": "11/5, KP6, P.Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 629,
   "Name": "Quầy thuốc Phúc Thuận",
   "address": "Khu 3, Tân Hoa, Bàu Hàm, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9855568,
   "Latitude": 107.106871
 },
 {
   "STT": 630,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 631,
   "Name": "Quầy thuốc Thành Tâm",
   "address": "Số 7, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9451841,
   "Latitude": 106.9804832
 },
 {
   "STT": 632,
   "Name": "Quầy thuốc ",
   "address": "89, Phạm Thị Nghĩa, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9616487,
   "Latitude": 106.8767652
 },
 {
   "STT": 633,
   "Name": "Quầy thuốc ",
   "address": "36, tổ 37, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 634,
   "Name": "Quầy thuốc",
   "address": "45A/2 KP3, P. Thống Nhất, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9522153,
   "Latitude": 106.8335215
 },
 {
   "STT": 635,
   "Name": "Quầy thuốc",
   "address": "1359 Ấp 3, Phú Lập, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 636,
   "Name": "Quầy thuốc ",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 637,
   "Name": "Nhà thuốc Hạnh Phước",
   "address": "38/10C, KP1, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 638,
   "Name": "Quầy thuốc Hoàng Ngân",
   "address": "SN 02, KDC5, ấp Thái Hòa 2, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252677
 },
 {
   "STT": 639,
   "Name": "Quầy thuốc",
   "address": "214, Phan Đình Phùng, KP5,  P.Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9521899,
   "Latitude": 106.8182004
 },
 {
   "STT": 640,
   "Name": "Quầy thuốc ",
   "address": "G22, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 641,
   "Name": "Quầy thuốc Xuân Thanh",
   "address": "Ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9522557,
   "Latitude": 107.3325721
 },
 {
   "STT": 642,
   "Name": "Quầy thuốc Ngọc Kim",
   "address": "8/3, tổ 3 , KP 6, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 643,
   "Name": "Quầy thuốc",
   "address": "1286, Tỉnh lộ 766, ấp Tân Hữu, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0231993,
   "Latitude": 107.4431187
 },
 {
   "STT": 644,
   "Name": "Quầy thuốc",
   "address": "7/2, KP8, Đồng Khởi, P. Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9742325,
   "Latitude": 106.8513069
 },
 {
   "STT": 645,
   "Name": "Quầy thuốc Việt Hà",
   "address": "110, ấp 2, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8711443,
   "Latitude": 107.0949002
 },
 {
   "STT": 646,
   "Name": "Quầy thuốc Trang Phương",
   "address": "Ấp 1, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8691364,
   "Latitude": 107.4667671
 },
 {
   "STT": 647,
   "Name": "Quầy thuốc ",
   "address": "46, QL 51, KP Bình Dương, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9037992,
   "Latitude": 106.8495219
 },
 {
   "STT": 648,
   "Name": "Quầy thuốc",
   "address": "Tổ 4, ấp Thành Công, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 649,
   "Name": "Quầy thuốc",
   "address": "76/2 KP5, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 650,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Xuân Quế, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.833333,
   "Latitude": 107.166667
 },
 {
   "STT": 651,
   "Name": "Quầy thuốc Trang Nguyễn",
   "address": "Ấp Bình Tiến, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8699319,
   "Latitude": 107.3348386
 },
 {
   "STT": 652,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Tâm Thịnh Thành",
   "address": "G31, KP7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9070264,
   "Latitude": 106.8909858
 },
 {
   "STT": 653,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "92 đường Nguyễn Văn Cừ, Xuân Hòa, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9289468,
   "Latitude": 107.2449622
 },
 {
   "STT": 654,
   "Name": "Quầy thuốc ",
   "address": "Ấp 6-7, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 655,
   "Name": "Quầy thuốc",
   "address": "10 tỉnh lộ 765, Ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9074501,
   "Latitude": 107.3685693
 },
 {
   "STT": 656,
   "Name": "Quầy thuốc Nguyên Khôi",
   "address": "Số 75,đường 30/4 (QL 1 cũ), P.Trung Dũng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9508799,
   "Latitude": 106.8203258
 },
 {
   "STT": 657,
   "Name": "Quầy thuốc Lê Ngân",
   "address": "Số 220, Phạm Văn Thuận (QL 15 cũ) , KP 2, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9578229,
   "Latitude": 106.8401514
 },
 {
   "STT": 658,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 659,
   "Name": "Quầy thuốc ",
   "address": "1042, tổ 2, khu 11, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2734767,
   "Latitude": 107.4437718
 },
 {
   "STT": 660,
   "Name": "Quầy thuốc ",
   "address": "1238 (số cũ 456), tổ 9, Ngọc Lâm 3, Phú Thanh, Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2354267,
   "Latitude": 107.4676861
 },
 {
   "STT": 661,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "SN 144A, tổ 4, ấp 3, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 662,
   "Name": "Quầy thuốc Yến Phương",
   "address": "82, ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 663,
   "Name": "Quầy thuốc",
   "address": "883 KP2, P.Bửu Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.960593,
   "Latitude": 106.8483173
 },
 {
   "STT": 664,
   "Name": "Quầy thuốc Nhật Phượng",
   "address": "25/1, Khu 2, ấp Tân Việt, xã Bàu Hàm, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9806475,
   "Latitude": 107.1070402
 },
 {
   "STT": 665,
   "Name": "Quầy thuốc Nga Dung",
   "address": "Tổ 13, KP5, Bùi Trọng Nghĩa, P.Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9838104,
   "Latitude": 106.8621202
 },
 {
   "STT": 666,
   "Name": "Quầy thuốc Kim Bằng",
   "address": "Ấp 2, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0097053,
   "Latitude": 106.8276976
 },
 {
   "STT": 667,
   "Name": "Quầy thuốc ",
   "address": "07, ấp 6, xã Nam Cát Tiên, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.405737,
   "Latitude": 107.4499404
 },
 {
   "STT": 668,
   "Name": "Quầy thuốc Sơn Tuyền",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 669,
   "Name": "Quầy thuốc Hải Nguyên",
   "address": "Ấp 3A, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8461842,
   "Latitude": 107.5092529
 },
 {
   "STT": 670,
   "Name": "Quầy thuốc Thanh Siêm",
   "address": "Ấp Tân Phát, Xã Đồi 61, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9420022,
   "Latitude": 107.0185823
 },
 {
   "STT": 671,
   "Name": "Quầy thuốc Chơn Tâm",
   "address": "74/31, CMT8, KP3, P.Quyết Thắng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9420903,
   "Latitude": 106.822236
 },
 {
   "STT": 672,
   "Name": "Quầy thuốc ",
   "address": "905, ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 673,
   "Name": "Quầy thuốc Tâm Phúc",
   "address": "Ấp Sông Mây, Xã Bắc Sơn, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 674,
   "Name": "Quầy thuốc",
   "address": "100, tổ 3, ấp 3, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7875218,
   "Latitude": 106.9365969
 },
 {
   "STT": 675,
   "Name": "Quầy thuốc ",
   "address": "82, đường Phi Trường, tổ 38, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9941649,
   "Latitude": 106.8153682
 },
 {
   "STT": 676,
   "Name": "Quầy thuốc",
   "address": "K4/234, Tân Bình, P. Bửu Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8014659,
   "Latitude": 106.6525974
 },
 {
   "STT": 677,
   "Name": "Quầy thuốc Huy Vy",
   "address": "17/1, tổ 1, KP 2, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9266663,
   "Latitude": 106.8498825
 },
 {
   "STT": 678,
   "Name": "Quầy thuốc Ngọc Danh",
   "address": "490 Ấp Thọ Lộc, Xã Xuân Thọ, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9607577,
   "Latitude": 107.3488873
 },
 {
   "STT": 679,
   "Name": "Quầy thuốc Thảo Lan",
   "address": "15/4, ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 680,
   "Name": "Quầy thuốc",
   "address": "Tổ 14, Khu 3, Ấp 7, Xã An Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 681,
   "Name": "Quầy thuốc Y Bình",
   "address": "13/4, tổ10, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9509254,
   "Latitude": 106.938686
 },
 {
   "STT": 682,
   "Name": "Nhà thuốc Phương Anh ",
   "address": "Số 102/507, Phạm Văn Thuận, KP 4, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9442003,
   "Latitude": 106.8581066
 },
 {
   "STT": 683,
   "Name": "Quầy thuốc Hoàng Liên",
   "address": "Đường Lô Hổ, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7371768,
   "Latitude": 107.1120842
 },
 {
   "STT": 684,
   "Name": "Quầy thuốc Hoàng An",
   "address": "Tổ 2, Ấp 3, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8039817,
   "Latitude": 106.9437237
 },
 {
   "STT": 685,
   "Name": "Quầy thuốc",
   "address": "Ấp Hòa Bình, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 686,
   "Name": "Nhà thuốc Thiện Tâm",
   "address": "31/C2, KP1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 687,
   "Name": "Tủ thuốc trạm y tế phường Thanh Bình",
   "address": "Trạm Y tế P.Thanh Bình, số 10, CMT8, P.Thanh Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9468843,
   "Latitude": 106.8171733
 },
 {
   "STT": 688,
   "Name": "Quầy thuốc Thái Ngọc Ngân",
   "address": "3/10, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 689,
   "Name": "Quầy thuốc Tú Nữ",
   "address": "783, tổ 23A, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 690,
   "Name": "Quầy thuốc Mai Khôi",
   "address": "Ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 691,
   "Name": "Quầy thuốc Duyên Ngọc",
   "address": "27 đường số 5, ấp Tân Tiến, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9118405,
   "Latitude": 107.3935086
 },
 {
   "STT": 692,
   "Name": "Quầy thuốc Vy Anh",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 693,
   "Name": "Quầy thuốc",
   "address": "43/10, KP 1, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 694,
   "Name": "Quầy thuốc Thanh Phương",
   "address": "KP 6, TT Vĩnh An, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 695,
   "Name": "Quầy thuốc Phương Nhàn",
   "address": "Tổ 10, khu phố 6, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0882151,
   "Latitude": 107.0332023
 },
 {
   "STT": 696,
   "Name": "Quầy thuốc Ngọc Trúc",
   "address": "Ấp Tam Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 697,
   "Name": "Tủ thuốc trạm y tế xã Trà Cổ",
   "address": "Trạm Y tế xã Trà Cổ, ấp 1, xã Trà Cổ, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.4275308,
   "Latitude": 107.36123
 },
 {
   "STT": 698,
   "Name": "Tủ thuốc trạm y tế thị trấn Tân Phú",
   "address": "Trạm Y tế thị trấn Tân Phú, khu 9, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 699,
   "Name": "Tủ thuốc trạm y tế xã Tà Lài",
   "address": "Trạm Y tế xã Tà Lài, ấp 6, xã Tà Lài, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.303164,
   "Latitude": 107.3976285
 },
 {
   "STT": 700,
   "Name": "Quầy thuốc Trúc Ngân",
   "address": "878, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.950815,
   "Latitude": 106.9751383
 },
 {
   "STT": 701,
   "Name": "Quầy thuốc",
   "address": "15/28B, KP3, P. Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 702,
   "Name": "Quầy thuốc Thảo Ly",
   "address": "Số 10, Lê Văn Hưu, KP 2, TT. Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 703,
   "Name": "Quầy thuốc Khải Ca",
   "address": "5/8, KP 3, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8490989,
   "Latitude": 106.782421
 },
 {
   "STT": 704,
   "Name": "Quầy thuốc",
   "address": "Tổ 8, ấp Long Đức 3, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 705,
   "Name": "Quầy thuốc Châu Nhân",
   "address": "Ấp Bình Thạch, xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9816185,
   "Latitude": 106.798952
 },
 {
   "STT": 706,
   "Name": "Quầy thuốc Minh Thu",
   "address": "Ấp Quảng Lộc, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9458223,
   "Latitude": 106.9904664
 },
 {
   "STT": 707,
   "Name": "Quầy thuốc ",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 708,
   "Name": "Nhà thuốc Hạnh Hùng",
   "address": "1/1, KP 1, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9406229,
   "Latitude": 106.8663254
 },
 {
   "STT": 709,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Tổ 13, khu 5, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 710,
   "Name": "Nhà thuốc bệnh viện đa khoa Đồng Nai",
   "address": "397, đường 30/4, P. Quyết Thắng, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9507358,
   "Latitude": 106.8219566
 },
 {
   "STT": 711,
   "Name": "Nhà thuốc Tín Đức",
   "address": "22/10, Cách Mạng Tháng 8, KP 3, P.Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9466449,
   "Latitude": 106.8173831
 },
 {
   "STT": 712,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "SN 1031, tổ 10, ấp 7, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 713,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "2/3D, Tây Nam, Gia Kiệm, Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 714,
   "Name": "Quầy thuốc bệnh viện đa khoa Dầu Giây",
   "address": "Ấp Nguyễn Thái Học, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9662312,
   "Latitude": 107.143842
 },
 {
   "STT": 715,
   "Name": "Tủ thuốc trạm y tế phường Tân Hòa",
   "address": "Trạm Y tế P.Tân Hoà, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9623154,
   "Latitude": 106.8679899
 },
 {
   "STT": 716,
   "Name": "Quầy thuốc Tú Uyên",
   "address": "SN 28, phố 1, ấp 1, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2131589,
   "Latitude": 107.3614525
 },
 {
   "STT": 717,
   "Name": "Quầy thuốc ",
   "address": "143, đường P792, Nguyễn Ái Quốc, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9684006,
   "Latitude": 106.859825
 },
 {
   "STT": 718,
   "Name": "Quầy thuốc Hải Anh",
   "address": "Tổ 13 Ấp Phước Hòa, Xã Long Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.710052,
   "Latitude": 107.006257
 },
 {
   "STT": 719,
   "Name": "Quầy thuốc Thu Cúc",
   "address": "100/1, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 720,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "Tổ 4, KP 2, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 721,
   "Name": "Quầy thuốc Thu Hiền",
   "address": "148/3 ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 722,
   "Name": "Nhà thuốc Xuân Nghĩa",
   "address": "16/G4, Khu phố 1, P.Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9419963,
   "Latitude": 106.8634047
 },
 {
   "STT": 723,
   "Name": "Nhà thuốc Hưng Thanh",
   "address": "120/15 KP6, P.Tân Tiến, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9622039,
   "Latitude": 106.8458626
 },
 {
   "STT": 724,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Giang Uyên",
   "address": "765, tổ 1, KP7, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9115755,
   "Latitude": 106.890473
 },
 {
   "STT": 725,
   "Name": "Quầy thuốc Khánh Thy",
   "address": "34/2, Bùi Trọng Nghĩa, KP 3, P.Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9725927,
   "Latitude": 106.8533296
 },
 {
   "STT": 726,
   "Name": "Quầy thuốc phòng khám đa khoa Liên Chi",
   "address": "Số 26, tổ 5, KP 6, Bùi Văn Hòa, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9319693,
   "Latitude": 106.8749571
 },
 {
   "STT": 727,
   "Name": "Quầy thuốc Đức Tiên",
   "address": "153/A, ấp Tam Bung, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0696987,
   "Latitude": 107.2475893
 },
 {
   "STT": 728,
   "Name": "Quầy thuốc Yến Phương",
   "address": "109, Tổ 3, KP Hiệp Đồng, TT Định Quán, Huyện Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 729,
   "Name": "Quầy thuốc ",
   "address": "Thôn 3, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7915635,
   "Latitude": 107.0231599
 },
 {
   "STT": 730,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Nhất Huyền",
   "address": "913E, tổ 33, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916522,
   "Latitude": 106.8501889
 },
 {
   "STT": 731,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Loan Hiếu",
   "address": "A435, Tổ 8, KP1, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 732,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "222, ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 733,
   "Name": "Quầy thuốc Hoàng Minh",
   "address": "SN 09, KP Hiệp Quyết, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 734,
   "Name": "Quầy thuốc Bảo Huy",
   "address": "107/4B, Ấp Thanh Hoá, Xã Hố Nai 3, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9509254,
   "Latitude": 106.938686
 },
 {
   "STT": 735,
   "Name": "Quầy thuốc Ngọc Tân",
   "address": "SN 145 G, tổ 3, khu 4, TT.Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 736,
   "Name": "Tủ thuốc trạm y tế phường Trung Dũng",
   "address": "Trạm Y tế P.Trung Dũng, P.Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9524549,
   "Latitude": 106.8235123
 },
 {
   "STT": 737,
   "Name": "Quầy thuốc Yến Oanh",
   "address": "246, tổ 3, KP11, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.928537,
   "Latitude": 106.8583508
 },
 {
   "STT": 738,
   "Name": "Quầy thuốc Nguyên Hãn",
   "address": "Ấp 1, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8691364,
   "Latitude": 107.4667671
 },
 {
   "STT": 739,
   "Name": "Quầy thuốc",
   "address": "4/7A, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9265399,
   "Latitude": 106.806079
 },
 {
   "STT": 740,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 741,
   "Name": "Quầy thuốc Phụng Hồng",
   "address": "1/34 (số cũ B7/22), KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891648,
   "Latitude": 106.8502253
 },
 {
   "STT": 742,
   "Name": "Quầy thuốc",
   "address": "77, tổ 15, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 743,
   "Name": "Quầy thuốc Dương Minh",
   "address": "K1/101, KP 2, P.Bửu Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.960593,
   "Latitude": 106.8483173
 },
 {
   "STT": 744,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Suối Trầu, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7447054,
   "Latitude": 107.0543005
 },
 {
   "STT": 745,
   "Name": "Quầy thuốc",
   "address": "1/15, KP 3, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 746,
   "Name": "Quầy thuốc Trúc Linh",
   "address": "Ấp 3, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 747,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Ngọc Mạnh",
   "address": "D396, tổ 8, KP 4, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8917194,
   "Latitude": 106.8502559
 },
 {
   "STT": 748,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Ngọc Thùy",
   "address": "Tổ 7, KP 2, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8939464,
   "Latitude": 106.8514777
 },
 {
   "STT": 749,
   "Name": "Quầy thuốc",
   "address": "Tổ 4, ấp 1, xã Mã Đà, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3049005,
   "Latitude": 107.0605278
 },
 {
   "STT": 750,
   "Name": "Quầy thuốc",
   "address": "Ấp Trung Hiếu, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0082873,
   "Latitude": 107.4329473
 },
 {
   "STT": 751,
   "Name": "Nhà thuốc Hương Dung",
   "address": "72A/5, KP 9, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 752,
   "Name": "Quầy thuốc",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 753,
   "Name": "Nhà thuốc Diễm Ngân",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 754,
   "Name": "Nhà thuốc Xuân Hoài",
   "address": "3/4, KP Long Điềm, P. Long Bình Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 755,
   "Name": "Quầy thuốc",
   "address": "32, Cọ Dầu, Ấp 3, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8677536,
   "Latitude": 107.4925656
 },
 {
   "STT": 756,
   "Name": "Quầy thuốc Quý Thảo",
   "address": "2168, Quốc lộ 1A, ấp 4, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8808391,
   "Latitude": 107.4357912
 },
 {
   "STT": 757,
   "Name": "Quầy thuốc",
   "address": "Ấp Hòa Hợp, Bảo Hòa, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 758,
   "Name": "Tủ thuốc trạm y tế xã Tam Phước",
   "address": "Trạm Y tế xã Tam Phước, Ấp Long Đức 3, Xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 759,
   "Name": "Quầy thuốc Hoàng Dương",
   "address": "TTTM Thiên Nhiên, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0118004,
   "Latitude": 106.8440668
 },
 {
   "STT": 760,
   "Name": "Quầy thuốc",
   "address": "Số nhà 200, tổ 5, ấp Phú Hợp A, Phú Bình, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 761,
   "Name": "Quầy thuốc ",
   "address": "Ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 762,
   "Name": "Quầy thuốc Anh Đức",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 763,
   "Name": "Quầy thuốc",
   "address": "75A, ấp Vàm , xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 764,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "257, Tổ 5, KP11, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.928537,
   "Latitude": 106.8583508
 },
 {
   "STT": 765,
   "Name": "Quầy thuốc",
   "address": "Chợ Phước Khánh, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6835553,
   "Latitude": 106.785028
 },
 {
   "STT": 766,
   "Name": "Quầy thuốc ",
   "address": "296, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 767,
   "Name": "Quầy thuốc Năm Ân",
   "address": "E835A, tổ 6, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 768,
   "Name": "Quầy thuốc Minh Phương",
   "address": "72, xóm 1, tổ 2, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 769,
   "Name": "Quầy thuốc Minh Hương",
   "address": "Ấp Bình Thạch, xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9816185,
   "Latitude": 106.798952
 },
 {
   "STT": 770,
   "Name": "Quầy thuốc ",
   "address": "ấp 3, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7364074,
   "Latitude": 107.3510492
 },
 {
   "STT": 771,
   "Name": "Quầy thuốc Hạnh Đức",
   "address": "Ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9108529,
   "Latitude": 107.3628616
 },
 {
   "STT": 772,
   "Name": "Tủ thuốc trạm y tế xã Phước Bình",
   "address": "Trạm Y tế xã Phước Bình, ấp 6, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 773,
   "Name": "Quầy thuốc Phúc An",
   "address": "Ấp Long Phú, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6839842,
   "Latitude": 107.0187634
 },
 {
   "STT": 774,
   "Name": "Quầy thuốc ",
   "address": "544C, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 775,
   "Name": "Quầy thuốc",
   "address": "Khu 2, ấp Hòa Bình, Bảo Hòa, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9022012,
   "Latitude": 107.2859484
 },
 {
   "STT": 776,
   "Name": "Tủ thuốc trạm y tế xã Long Hưng",
   "address": "Trạm Y tế xã Long Hưng, Ấp Phước Hội, Xã Long Hưng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8762722,
   "Latitude": 106.8632478
 },
 {
   "STT": 777,
   "Name": "Quầy thuốc ",
   "address": "ấp Bến Đình, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 778,
   "Name": "Quầy thuốc ",
   "address": "Số 254, ấp 1, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6835553,
   "Latitude": 106.785028
 },
 {
   "STT": 779,
   "Name": "Quầy thuốc Bình Duyên",
   "address": "275, Bùi Trọng Nghĩa, KP3,  P.Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9816342,
   "Latitude": 106.8561892
 },
 {
   "STT": 780,
   "Name": "Quầy thuốc Ngọc Nhân",
   "address": "Tổ 8, ấp Bình Lâm, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 781,
   "Name": "Chi nhánh công ty cổ phần Pymepharco tại tỉnh Đồng Nai",
   "address": "483, Huỳnh Văn Nghệ, KP 3, P.Bửu Long, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9551531,
   "Latitude": 106.7938942
 },
 {
   "STT": 782,
   "Name": "Nhà thuốc Bệnh viện Phổi Đồng Nai",
   "address": "Bệnh viện Phổi Đồng Nai, tổ 2, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8754537,
   "Latitude": 106.9190042
 },
 {
   "STT": 783,
   "Name": "Quầy thuốc ",
   "address": "Chợ Sông Thao, xã Sông Thao, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.96462,
   "Latitude": 107.0924028
 },
 {
   "STT": 784,
   "Name": "Quầy thuốc Vũ Song Huy",
   "address": "77, Ấp 2, Xã Tân Hạnh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9514129,
   "Latitude": 106.7787053
 },
 {
   "STT": 785,
   "Name": "Quầy thuốc ",
   "address": "Tổ 2, ấp 4, xã Núi Tượng, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.358163,
   "Latitude": 107.3849497
 },
 {
   "STT": 786,
   "Name": "Quầy thuốc Nguyên Trúc",
   "address": "259/2 KP 8A, P.Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9705705,
   "Latitude": 106.893577
 },
 {
   "STT": 787,
   "Name": "Quầy thuốc Gia Trí",
   "address": "Số 01, Hà Huy Giáp, KP 2, P.Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9497068,
   "Latitude": 106.822304
 },
 {
   "STT": 788,
   "Name": "Quầy thuốc Ánh Tuyết",
   "address": "SN 3416, xã Phú Sơn, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3358768,
   "Latitude": 107.5209304
 },
 {
   "STT": 789,
   "Name": "Nhà thuốc Đông Hưng",
   "address": "177, Đồng Khởi, KP 6, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9572,
   "Latitude": 106.8596335
 },
 {
   "STT": 790,
   "Name": "Quầy thuốc ",
   "address": "1301, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 791,
   "Name": "Quầy thuốc",
   "address": "AẤp Quảng Đà, Đồng Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 792,
   "Name": "Quầy thuốc ",
   "address": "15/2C, KP 5, P.Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 793,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0498878,
   "Latitude": 107.0211717
 },
 {
   "STT": 794,
   "Name": "Nhà thuốc Anh Trang",
   "address": "50/248, Phạm Văn Thuận, KP 1, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9549539,
   "Latitude": 106.8513375
 },
 {
   "STT": 795,
   "Name": "Quầy thuốc Cẩm Vân",
   "address": "409-411 Hùng Vương, khu 1, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9337654,
   "Latitude": 107.4045056
 },
 {
   "STT": 796,
   "Name": "Tủ thuốc trạm y tế phường Tân Phong",
   "address": "Trạm Y tế P.Tân Phong, KP 1, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9681087,
   "Latitude": 106.8356708
 },
 {
   "STT": 797,
   "Name": "Quầy thuốc Ngọc Quý",
   "address": "Ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 798,
   "Name": "Nhà thuốc Nhi Đồng",
   "address": "A 12B, KP4, P. Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9586742,
   "Latitude": 106.856797
 },
 {
   "STT": 799,
   "Name": "Nhà thuốc ",
   "address": "106/4, KP 10, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 800,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Vạn Đức",
   "address": "Số nhà 2253, ấp Phương Lâm 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 801,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "Ấp Chợ, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 802,
   "Name": "Quầy thuốc Thiện Chương",
   "address": "SN 02, ấp Hòa Bình, xã Túc Trưng, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 803,
   "Name": "Quầy thuốc Thái Lâm",
   "address": "Ấp Bình Ý, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 804,
   "Name": "Quầy thuốc Tín Huê",
   "address": "Tổ 5, ấp 3, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.048503,
   "Latitude": 107.0164887
 },
 {
   "STT": 805,
   "Name": "Quầy thuốc ",
   "address": "31A, tổ 10, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 806,
   "Name": "Quầy thuốc ",
   "address": "Ấp 4, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 807,
   "Name": "Quầy thuốc Gia Bạch",
   "address": "Ấp Câu Kê, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 808,
   "Name": "Nhà thuốc ",
   "address": "90/47, Đồng Khởi, KP 8, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9712305,
   "Latitude": 106.8529916
 },
 {
   "STT": 809,
   "Name": "Quầy thuốc Phú Thịnh 2",
   "address": "Ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 810,
   "Name": "Quầy thuốc ",
   "address": "101/4, ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 811,
   "Name": "Quầy thuốc Thy Anh",
   "address": "Ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.843933,
   "Latitude": 106.9319755
 },
 {
   "STT": 812,
   "Name": "Quầy thuốc Vân Sơn",
   "address": "50, Ấp Đồn Điền 2, xã Túc Trưng, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 813,
   "Name": "Quầy thuốc Vĩnh Phương",
   "address": "Chợ Sông Thao, xã Sông Thao, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.96462,
   "Latitude": 107.0924028
 },
 {
   "STT": 814,
   "Name": "Quầy thuốc",
   "address": "Ấp Láng Lớn, Xuân Mỹ, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 815,
   "Name": "Quầy thuốc Hưng Thịnh",
   "address": "21, tổ 4, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 816,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Tổ 1, ấp Hiền Hòa, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6911092,
   "Latitude": 107.020279
 },
 {
   "STT": 817,
   "Name": "Tủ thuốc trạm y tế phường Tân Hiệp",
   "address": "Trạm Y tế P.Tân Hiệp, Tổ 6, Phạm Văn Khoai, P.Tân Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9555507,
   "Latitude": 106.8616724
 },
 {
   "STT": 818,
   "Name": "Nhà thuốc Đỗ Quyên",
   "address": "483, đường 5, KP 4, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9238242,
   "Latitude": 106.8506352
 },
 {
   "STT": 819,
   "Name": "Quầy thuốc Ngô Châu",
   "address": "Ấp 4, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3001594,
   "Latitude": 107.150533
 },
 {
   "STT": 820,
   "Name": "Nhà thuốc Cây Chàm",
   "address": "16/2, CMT8, KP3, P.Quang Vinh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9463867,
   "Latitude": 106.8178172
 },
 {
   "STT": 821,
   "Name": "Quầy thuốc Khánh Hòa",
   "address": "Tổ 32, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 822,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Lão Cần Tế",
   "address": "7/1, Khu Phước Thuận, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 823,
   "Name": "Quầy thuốc Trần Di",
   "address": "50, ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 824,
   "Name": "Quầy thuốc ",
   "address": "35/3, tổ 3, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 825,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 826,
   "Name": "Quầy thuốc",
   "address": "Tổ 1, ấp 10, Bình Sơn, Long Thành, Đồng Nai",
   "Longtitude": 10.783333,
   "Latitude": 107.016667
 },
 {
   "STT": 827,
   "Name": "Quầy thuốc Ngọc Thanh",
   "address": "28, tổ 6, ấp Hòa Thành, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 828,
   "Name": "Quầy thuốc Bích Tuyền",
   "address": "146/2, KP1, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9574128,
   "Latitude": 106.8426871
 },
 {
   "STT": 829,
   "Name": "Tủ thuốc trạm y tế phường Long Bình Tân",
   "address": "Trạm Y tế phường Long Bình Tân, KP.Long Điềm, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8908675,
   "Latitude": 106.8516406
 },
 {
   "STT": 830,
   "Name": "Nhà thuốc Sơn Minh",
   "address": "143, Cách Mạng Tháng 8, KP 3, P.Hòa Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9502116,
   "Latitude": 106.8125086
 },
 {
   "STT": 831,
   "Name": "Quầy thuốc Tâm An",
   "address": "Ấp 1, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.157989,
   "Latitude": 107.2537993
 },
 {
   "STT": 832,
   "Name": "Quầy thuốc ",
   "address": "Số 41, Khu 5, Ấp 8, Xã An Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7372197,
   "Latitude": 107.1118549
 },
 {
   "STT": 833,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Tổ 12B, khu Văn Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7840906,
   "Latitude": 106.9440695
 },
 {
   "STT": 834,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "10/3, ấp Dốc Mơ 2, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 835,
   "Name": "Quầy thuốc Thùy Trâm",
   "address": "78/4, ấp Dốc Mơ 3, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0636523,
   "Latitude": 107.1656388
 },
 {
   "STT": 836,
   "Name": "Quầy thuốc Kim Dung",
   "address": "Đồng Bình, Ấp Bùi Chu, Xã Bắc Sơn, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 837,
   "Name": "Quầy thuốc Ngọc Linh",
   "address": "Tổ 31, Khu Phước Hải, TT.Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 838,
   "Name": "Quầy thuốc ",
   "address": "Số 52, tổ 35, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9819539,
   "Latitude": 106.8449944
 },
 {
   "STT": 839,
   "Name": "Quầy thuốc Chu Thảo Hiền",
   "address": "Tổ 5, ấp 7, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 840,
   "Name": "Quầy thuốc Thu Hiền",
   "address": "9/8, ấp 2, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 841,
   "Name": "Công ty TNHH Dược phẩm Ngọc Thu",
   "address": "32, đường 30-4, P.Quyết Thắng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9495928,
   "Latitude": 106.8210029
 },
 {
   "STT": 842,
   "Name": "Quầy thuốc Minh Đức",
   "address": "Tổ 1, ấp 5, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7946093,
   "Latitude": 106.9853783
 },
 {
   "STT": 843,
   "Name": "Quầy thuốc ",
   "address": "Ấp Đông Hải, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.986689,
   "Latitude": 106.927042
 },
 {
   "STT": 844,
   "Name": "Quầy thuốc Ngọc Trinh",
   "address": "32, ấp Hòa Hiệp, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 845,
   "Name": "Quầy thuốc Thy Nhung",
   "address": "36, KDC 1, ấp 2, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 846,
   "Name": "Quầy thuốc ",
   "address": "215 ấp 3, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8712223,
   "Latitude": 107.1065628
 },
 {
   "STT": 847,
   "Name": "Quầy thuốc ",
   "address": "35/2, ấp Vĩnh An, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 848,
   "Name": "Quầy thuốc Linh An",
   "address": "115/4/2, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 849,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Ấp 1, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 850,
   "Name": "Nhà thuốc Hiền Linh",
   "address": "B39A, Huỳnh Văn Nghệ, tổ 29, KP 5, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9681954,
   "Latitude": 106.7972342
 },
 {
   "STT": 851,
   "Name": "Tủ thuốc trạm y tế phường Tân Biên",
   "address": "Trạm Y tế P.Tân Biên, P.Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9700004,
   "Latitude": 106.8913346
 },
 {
   "STT": 852,
   "Name": "Nhà thuốc Tuấn Nghĩa",
   "address": "Số 28/40A, KP8, P. Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 853,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 854,
   "Name": "Nhà thuốc Mộc Trà",
   "address": "K4/255D, Nguyễn Tri Phương, tổ 44, KP 2, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.7603978,
   "Latitude": 106.6687542
 },
 {
   "STT": 855,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Lão Cần Tế 2",
   "address": "Tổ 31, khu Cầu Xéo, TT Long Thành, huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7732247,
   "Latitude": 106.956629
 },
 {
   "STT": 856,
   "Name": "Quầy thuốc Thùy Khuyên",
   "address": "Ấp Tân Thành, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 857,
   "Name": "Quầy thuốc Minh Trang",
   "address": "Số 14, Khu 4, Ấp 8, Xã An Phước, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.862375,
   "Latitude": 106.963667
 },
 {
   "STT": 858,
   "Name": "Quầy thuốc Ngọc Bảo An",
   "address": "Ấp 5, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7593232,
   "Latitude": 106.9425975
 },
 {
   "STT": 859,
   "Name": "Quầy thuốc Ngọc Diễm",
   "address": "16/1, ấp Lợi Hà, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 11.083389,
   "Latitude": 107.0763866
 },
 {
   "STT": 860,
   "Name": "Quầy thuốc ",
   "address": "Số 10, tổ 3, ấp Hàng Gòn, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8111548,
   "Latitude": 106.9889904
 },
 {
   "STT": 861,
   "Name": "Quầy thuốc Tam An",
   "address": "Tổ 59, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 862,
   "Name": "Quầy thuốc Công Hải",
   "address": "66/1E, Võ Dõng, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 863,
   "Name": "Nhà thuốc Anh Trúc",
   "address": "5/4D, KP1, P. Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 864,
   "Name": "Quầy thuốc ",
   "address": "Ấp Tân Hữu, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9940176,
   "Latitude": 107.4232248
 },
 {
   "STT": 865,
   "Name": "Quầy thuốc ",
   "address": "Ấp 12, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7670969,
   "Latitude": 107.296771
 },
 {
   "STT": 866,
   "Name": "Quầy thuốc ",
   "address": "Tổ 14, khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 867,
   "Name": "Nhà thuốc Mỹ Kim",
   "address": "Số 75C, đường Nguyễn Văn Cừ, KP 1, P.Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9263927,
   "Latitude": 107.2491024
 },
 {
   "STT": 868,
   "Name": "Quầy thuốc Nguyên Hy",
   "address": "20, ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9466204,
   "Latitude": 106.9524883
 },
 {
   "STT": 869,
   "Name": "Quầy thuốc",
   "address": "Tổ 20, ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.680821,
   "Latitude": 107.0262359
 },
 {
   "STT": 870,
   "Name": "Quầy thuốc ",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 871,
   "Name": "Quầy thuốc Thiên Ân Huệ",
   "address": "20/20A, Nguyễn Bảo Đức, KP 6, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9527788,
   "Latitude": 106.8601084
 },
 {
   "STT": 872,
   "Name": "Nhà thuốc Châu My",
   "address": "157 , Vũ Hồng Phô, KP 2, P.Bình Đa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.937369,
   "Latitude": 106.8604242
 },
 {
   "STT": 873,
   "Name": "Quầy thuốc Hùng Thăng",
   "address": "1118, khu 5, Tổ 21, ấp 2, An Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8902353,
   "Latitude": 106.8584837
 },
 {
   "STT": 874,
   "Name": "Quầy thuốc",
   "address": "17, Tổ 3, KP2, P.Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.960593,
   "Latitude": 106.8483173
 },
 {
   "STT": 875,
   "Name": "Quầy thuốc Phương An",
   "address": "21A, Bùi Hữu Nghĩa, tổ 12, ấp Bình Hóa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9072329,
   "Latitude": 106.8297573
 },
 {
   "STT": 876,
   "Name": "Quầy thuốc Kim Đồng",
   "address": "68 Ấp 1, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.180107,
   "Latitude": 107.3862129
 },
 {
   "STT": 877,
   "Name": "Quầy thuốc Minh Phương",
   "address": "84, tổ 23, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 878,
   "Name": "Quầy thuốc Hòa Hương",
   "address": "158/2, KP 1, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 879,
   "Name": "Nhà thuốc Lê Anh",
   "address": "P132, Tổ 25, KP3, P.Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.964785,
   "Latitude": 106.8651461
 },
 {
   "STT": 880,
   "Name": "Quầy thuốc Phương Nam",
   "address": "3/1B, Đức Long, xã Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 881,
   "Name": "Quầy thuốc ",
   "address": "Tổ 1, ấp Bàu Ao, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.942239,
   "Latitude": 107.135928
 },
 {
   "STT": 882,
   "Name": "Quầy thuốc",
   "address": "197, KP4, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9731501,
   "Latitude": 106.9079379
 },
 {
   "STT": 883,
   "Name": "Nhà thuốc Phượng Nghĩa",
   "address": "153, CMT8, KP 3, P.Hòa Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9504742,
   "Latitude": 106.8123094
 },
 {
   "STT": 884,
   "Name": "Quầy thuốc Thành Tâm",
   "address": "569, ấp Ngũ Phúc, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 885,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Ấp Chà Rang, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0095589,
   "Latitude": 107.3622501
 },
 {
   "STT": 886,
   "Name": "Quầy thuốc",
   "address": "Ấp Chợ, Phước Thiền, Nhơn Trạch , Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 887,
   "Name": "Quầy thuốc ",
   "address": "Ấp 2, xã Bàu Cạn, huyện Long Thành, tình Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 888,
   "Name": "Quầy thuốc ",
   "address": "77, ấp 4, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 11.0076953,
   "Latitude": 106.8406308
 },
 {
   "STT": 889,
   "Name": "Quầy thuốc",
   "address": "7B/89, Kp13, P. Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 890,
   "Name": "Quầy thuốc Kim Tuyến",
   "address": "57A, đường Trảng Bom - Cây Gáo, ấp 3, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538817,
   "Latitude": 107.0102882
 },
 {
   "STT": 891,
   "Name": "Quầy thuốc An Dược",
   "address": "Ki ốt số 24, chợ Long Thành, khu Cầu Xéo, TT.Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7770943,
   "Latitude": 106.956174
 },
 {
   "STT": 892,
   "Name": "Quầy thuốc Nhật Hương",
   "address": "55B/3, ấp An Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 893,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Ấp 5, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6867083,
   "Latitude": 107.0429349
 },
 {
   "STT": 894,
   "Name": "Quầy thuốc Tố Tâm",
   "address": "Số 80, tổ 11, khu 5, TT.Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 895,
   "Name": "Quầy thuốc Khanh Tâm",
   "address": "Ấp 1A, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8385394,
   "Latitude": 107.4921621
 },
 {
   "STT": 896,
   "Name": "Quầy thuốc Tố Hảo",
   "address": "SN 222, tổ 7, ấp 6, xã Phú Lộc, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.5673841,
   "Latitude": 107.3576689
 },
 {
   "STT": 897,
   "Name": "Quầy thuốc ",
   "address": "30A, khu 1, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 898,
   "Name": "Quầy thuốc Bích Ngân",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 899,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Khu B, ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 900,
   "Name": "Quầy thuốc Vũ Thuận",
   "address": "Thôn 4, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9505276,
   "Latitude": 106.977206
 },
 {
   "STT": 901,
   "Name": "Quầy thuốc Minh Hiền",
   "address": "Tổ 4, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 902,
   "Name": "Quầy thuốc",
   "address": "173/1, ấp Gia Yên, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 903,
   "Name": "Quầy thuốc Vân Anh",
   "address": "C1/013, ấp Bắc Sơn, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 904,
   "Name": "Quầy thuốc Vân Trinh",
   "address": "83, ấp Tân Hưng, xã Đồi 61, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 905,
   "Name": "Quầy thuốc",
   "address": "Sôố 33, ấp Phú Lâm 4, Phú Sơn, Tân Phú, Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 906,
   "Name": "Quầy thuốc",
   "address": "Số 7, Tổ 10, ấp Tân Mai 2, Xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8743054,
   "Latitude": 106.9099991
 },
 {
   "STT": 907,
   "Name": "Quầy thuốc ",
   "address": "77, Ấp 1, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 908,
   "Name": "Quầy thuốc ",
   "address": "23A/84, KP 12, P.Hố Nai, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 909,
   "Name": "Nhà thuốc Giang San",
   "address": "148 (số cũ 85), Phan Đình Phùng, KP 2, P.Quang Vinh, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9534476,
   "Latitude": 106.818105
 },
 {
   "STT": 910,
   "Name": "Quầy thuốc Hoàng Dũng",
   "address": "Tổ 2, Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.981049,
   "Latitude": 107.019946
 },
 {
   "STT": 911,
   "Name": "Quầy thuốc Ngọc Thảo",
   "address": "Đường Lê Quang Định, Tổ 26, Khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 912,
   "Name": "Quầy thuốc",
   "address": "Chợ Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.948597,
   "Latitude": 107.0486301
 },
 {
   "STT": 913,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Ấp Vũng Gấm, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 914,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 915,
   "Name": "Quầy thuốc An Khánh",
   "address": "SN 441, tổ 2, ấp 6, xã Trà Cổ, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.4275308,
   "Latitude": 107.36123
 },
 {
   "STT": 916,
   "Name": "Quầy thuốc Thùy Mai",
   "address": "Ấp Phước Lý, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 917,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Trị An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0986681,
   "Latitude": 106.9650823
 },
 {
   "STT": 918,
   "Name": "Quầy thuốc Thanh Lan",
   "address": "55/2, ấp Dốc Mơ 1, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 919,
   "Name": "Nhà thuốc Nghĩa Hưng",
   "address": "D379, tổ 8, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8917194,
   "Latitude": 106.8502559
 },
 {
   "STT": 920,
   "Name": "Quầy thuốc Mạnh Đức",
   "address": "Tổ 2, khu A, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8381743,
   "Latitude": 106.9334077
 },
 {
   "STT": 921,
   "Name": "Quầy thuốc Kiều Vy",
   "address": "Tổ 12, ấp 2B, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 922,
   "Name": "Quầy thuốc",
   "address": "63/5, KP9, P.Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9680077,
   "Latitude": 106.8985009
 },
 {
   "STT": 923,
   "Name": "Quầy thuốc",
   "address": "173, ấp 2, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8694548,
   "Latitude": 107.0899432
 },
 {
   "STT": 924,
   "Name": "Quầy thuốc Lê Hân",
   "address": "Ấp 7, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 925,
   "Name": "Quầy thuốc ",
   "address": "173/290 (số cũ 11/9), KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520451,
   "Latitude": 106.8772376
 },
 {
   "STT": 926,
   "Name": "Quầy thuốc Triều Thy",
   "address": "493, tổ 6, ấp Hương Phước, Xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 927,
   "Name": "Quầy thuốc",
   "address": "64/1, KP 6, P.Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 928,
   "Name": "Quầy thuốc Hòa Thuận",
   "address": "Tổ 3, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9810755,
   "Latitude": 107.025626
 },
 {
   "STT": 929,
   "Name": "Quầy thuốc Thiện Phú",
   "address": "Số 318, ấp Láng Me 1, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8523641,
   "Latitude": 107.4258363
 },
 {
   "STT": 930,
   "Name": "Quầy thuốc Thu Nga",
   "address": "Số 337, Hồ Thị Hương, KP 1, P.Xuân Thanh, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 931,
   "Name": "Quầy thuốc Thu Hương",
   "address": "Tổ 2, ấp Bình Lâm, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 932,
   "Name": "Quầy thuốc",
   "address": "Tây Lạc, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 933,
   "Name": "Quầy thuốc Nhật Sâm",
   "address": "Tổ 7, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 934,
   "Name": "Quầy thuốc Hiếu Bình Dân",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 935,
   "Name": "Quầy thuốc Phương Dung",
   "address": "SN 2344, ấp Phương Mai 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 936,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 937,
   "Name": "Quầy thuốc Đại Lâm",
   "address": "Ấp 1, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7515541,
   "Latitude": 107.3514669
 },
 {
   "STT": 938,
   "Name": "Quầy thuốc ",
   "address": "8/10B, KP 3, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548762,
   "Latitude": 106.8021482
 },
 {
   "STT": 939,
   "Name": "Quầy thuốc Việt Đức",
   "address": "Ấp Lý Lịch 2, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3049005,
   "Latitude": 107.0605278
 },
 {
   "STT": 940,
   "Name": "Quầy thuốc Nhật Dung",
   "address": "154/D7 Khu C, ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 941,
   "Name": "Quầy thuốc Nguyễn Sen",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 942,
   "Name": "Chi nhánh Công ty cổ phần Dược - Trang thiết bị y tế Bình Định (Bidiphar) tại Đồng Nai",
   "address": "J37, J38, Đường N4, KDC Liên Kế, KP 1, P.Bửu Long, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9565392,
   "Latitude": 106.8053108
 },
 {
   "STT": 943,
   "Name": "Quầy thuốc Thành Nga",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 944,
   "Name": "Quầy thuốc Hoàng Oanh",
   "address": "Ấp 2, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.846819,
   "Latitude": 107.507635
 },
 {
   "STT": 945,
   "Name": "Quầy thuốc Ánh",
   "address": "Chợ Gia Ray, ấp Trung Nghĩa, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9628169,
   "Latitude": 107.402832
 },
 {
   "STT": 946,
   "Name": "Quầy thuốc Nguyễn Hiền",
   "address": "552, ấp Trung Lương, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0082873,
   "Latitude": 107.4329473
 },
 {
   "STT": 947,
   "Name": "Quầy thuốc Kim Nga",
   "address": "Chơ Gia Ray, ấp Trung Nghĩa, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9628169,
   "Latitude": 107.402832
 },
 {
   "STT": 948,
   "Name": "Quầy thuốc Vân",
   "address": "Ấp Trung Tín, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0082873,
   "Latitude": 107.4329473
 },
 {
   "STT": 949,
   "Name": "Quầy thuốc ",
   "address": "2/3, tổ 17, KP 3, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9530495,
   "Latitude": 106.8022768
 },
 {
   "STT": 950,
   "Name": "Quầy thuốc Hoàng Dũng",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 951,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "8B, Khu 3, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 952,
   "Name": "Quầy thuốc Tin Tin",
   "address": "43/1, Ấp Xây Dựng, Xã Giang Điền, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.912027,
   "Latitude": 106.9860384
 },
 {
   "STT": 953,
   "Name": "Nhà thuốc Trinh Ngọc",
   "address": "78/16B, KP 1, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 954,
   "Name": "Quầy thuốc MI-SEN",
   "address": "Số 114, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7371768,
   "Latitude": 107.1120842
 },
 {
   "STT": 955,
   "Name": "Quầy thuốc",
   "address": "Ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 956,
   "Name": "Quầy thuốc Tấn Tài",
   "address": "F3/077, ấp Nguyễn Huệ, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 957,
   "Name": "Quầy thuốc Thu Hường",
   "address": "SN 45, Tổ 10, Khu 6, TT. Tân Phú, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 958,
   "Name": "Quầy thuốc Quỳnh Trang",
   "address": "17/3, ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7567666,
   "Latitude": 106.9889904
 },
 {
   "STT": 959,
   "Name": "Quầy thuốc Thảo Hoa",
   "address": "1436/42 Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 960,
   "Name": "Quầy thuốc - Chi nhánh Công ty TNHH Dược phẩm Sơn Minh",
   "address": "Số 160, Phan Đình Phùng, KP 2, P.Quang Vinh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9522447,
   "Latitude": 106.8181951
 },
 {
   "STT": 961,
   "Name": "Quầy thuốc ",
   "address": "Ấp Thành Công, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 962,
   "Name": "Quầy thuốc Diệu Hiền",
   "address": "Ấp 5, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1691668,
   "Latitude": 107.2378563
 },
 {
   "STT": 963,
   "Name": "Quầy thuốc ",
   "address": "609, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 964,
   "Name": "Nhà thuốc Hiền Tâm",
   "address": "Số 96, KP 1, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9819539,
   "Latitude": 106.8449944
 },
 {
   "STT": 965,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Tổ 2, ấp 6, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 966,
   "Name": "Quầy thuốc Quỳnh Như",
   "address": "Tổ 11, ấp 1B, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6860681,
   "Latitude": 107.0411068
 },
 {
   "STT": 967,
   "Name": "Nhà thuốc Quang Huy Phát",
   "address": "Số 128D, tổ 8, KP 3, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8917194,
   "Latitude": 106.8502559
 },
 {
   "STT": 968,
   "Name": "Quầy thuốc",
   "address": "Ấp 6, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9883339,
   "Latitude": 107.3280593
 },
 {
   "STT": 969,
   "Name": "Quầy thuốc",
   "address": "Ấp 2, xã Tân An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0378257,
   "Latitude": 106.916917
 },
 {
   "STT": 970,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "1153, ấp Suối Quýt, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 971,
   "Name": "Quầy thuốc Minh Cường 2",
   "address": "Ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 972,
   "Name": "Tủ thuốc trạm y tế xã Phú Đông",
   "address": "Trạm Y tế xã Phú Đông, ấp Giồng Ông Đông, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 973,
   "Name": "Quầy thuốc Công ty TNHH Phòng khám đa khoa Tín Đức",
   "address": "Ấp 3, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 974,
   "Name": "Quầy thuốc ",
   "address": "98, ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 975,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "27, ấp An Viễng, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8559604,
   "Latitude": 107.0555911
 },
 {
   "STT": 976,
   "Name": "Quầy thuốc Hà Trinh",
   "address": "054, ấp Hiền Đức, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6911092,
   "Latitude": 107.020279
 },
 {
   "STT": 977,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 978,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Số 155, khu 3, ấp Hiệp Lực, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 979,
   "Name": "Quầy thuốc Ngọc An",
   "address": "Tổ 24, khu Văn Hải, TT Long Thành, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7808939,
   "Latitude": 106.9512377
 },
 {
   "STT": 980,
   "Name": "Quầy thuốc ",
   "address": "E2/34 ấp Nguyễn Huệ, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 981,
   "Name": "Quầy thuốc Phú Minh",
   "address": "41, Trần Đại Nghĩa, Khu 1, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582402,
   "Latitude": 106.9458214
 },
 {
   "STT": 982,
   "Name": "Quầy thuốc ",
   "address": "192, ấp 8, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8267515,
   "Latitude": 107.3408675
 },
 {
   "STT": 983,
   "Name": "Quầy thuốc ",
   "address": "Ấp 5, xã Lâm San, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7159734,
   "Latitude": 107.3318334
 },
 {
   "STT": 984,
   "Name": "Nhà thuốc Bích Thuần",
   "address": "159, Nguyễn Ái Quốc, KP3, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9657745,
   "Latitude": 106.8498654
 },
 {
   "STT": 985,
   "Name": "Quầy thuốc Toàn Tâm",
   "address": "Ấp Tân Đạt, Xã Đồi 61, Huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 986,
   "Name": "Quầy thuốc",
   "address": "Tổ 19, ấp An Hòa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9354705,
   "Latitude": 106.8016235
 },
 {
   "STT": 987,
   "Name": "Quầy thuốc Huy Long",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 988,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "226, tổ 6, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 989,
   "Name": "Quầy thuốc Thanh Hoàng",
   "address": "Ấp Hưng Nghĩa, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 990,
   "Name": "Quầy thuốc Mỹ Lan",
   "address": "Ấp 3, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8255222,
   "Latitude": 107.5598542
 },
 {
   "STT": 991,
   "Name": "Tủ thuốc trạm y tế phường Tam Hòa",
   "address": "Trạm Y Tế phường Tam Hòa, P.Tam Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.946169,
   "Latitude": 106.8686485
 },
 {
   "STT": 992,
   "Name": "Quầy thuốc Thu Liên",
   "address": "18BKC, ấp 6, xã Tân Hiệp, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6923067,
   "Latitude": 107.042907
 },
 {
   "STT": 993,
   "Name": "Tủ thuốc trạm y tế xã Phước Tân",
   "address": "Trạm Y Tế xã Phước Tân, xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8880176,
   "Latitude": 106.8987751
 },
 {
   "STT": 994,
   "Name": "Tủ thuốc trạm y tế phường Tân Mai",
   "address": "Trạm Y tế Tân Mai, P.Tân Mai, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9563127,
   "Latitude": 106.8504578
 },
 {
   "STT": 995,
   "Name": "Tủ thuốc trạm y tế phường Bửu Long",
   "address": "Trạm Y tế P.Bửu Long, KP 3, P.Bửu Long, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9585597,
   "Latitude": 106.7904579
 },
 {
   "STT": 996,
   "Name": "Tủ thuốc trạm y tế xã Tân Hanh",
   "address": "Trạm Y tế xã Tân Hạnh, Ấp IB, Bùi Hữu Nghĩa, Xã Tân Hạnh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9543384,
   "Latitude": 106.7842763
 },
 {
   "STT": 997,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 998,
   "Name": "Nhà thuốc Linh Trần",
   "address": "Tổ 19, KP 1, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 999,
   "Name": "Tủ thuốc trạm y tế phường Tân Tiến",
   "address": "Trạm Y tế P.Tân Tiến, P.Tân Tiến, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9635927,
   "Latitude": 106.8473588
 },
 {
   "STT": 1000,
   "Name": "Tủ thuốc trạm y tế xã An Hòa",
   "address": "Trạm Y tế xã An Hoà, 757, Khu 5, Ấp 2, Xã An Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8928899,
   "Latitude": 106.8639299
 },
 {
   "STT": 1001,
   "Name": "Tủ thuốc trạm y tế phường Quang Vinh",
   "address": "Trạm Y tế P.Quang Vinh, 38, Phan Đình Phùng, P.Quang Vinh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9544098,
   "Latitude": 106.8148879
 },
 {
   "STT": 1002,
   "Name": "Tủ thuốc trạm y tế phường An Bình",
   "address": "Trạm Y tế P.An Bình, Tổ 5, KP 12, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9240197,
   "Latitude": 106.8665367
 },
 {
   "STT": 1003,
   "Name": "Tủ thuốc trạm y tế phường Hòa Bình",
   "address": "Tram Y tế P.Hoà Bình, 293, KP 5, P.Hòa Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9509976,
   "Latitude": 106.8092369
 },
 {
   "STT": 1004,
   "Name": "Nhà thuốc Hưng Hương",
   "address": "SN 2281, Phương Lâm 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 1005,
   "Name": "Tủ thuốc trạm y tế phường Bình Đa",
   "address": "Trạm Y tế P.Bình Đa, P.Bình Đa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9353908,
   "Latitude": 106.8620891
 },
 {
   "STT": 1006,
   "Name": "Tủ thuốc trạm y tế xã Hóa An",
   "address": "Trạm Y tế xã Hoá An, Bùi Hữu Nghĩa, Ấp Đồng Nai, Xã Hóa An, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9427525,
   "Latitude": 106.8072827
 },
 {
   "STT": 1007,
   "Name": "Tủ thuốc trạm y tế xã Hiệp Hòa",
   "address": "Trạm Y tế xã Hiệp Hoà, Ấp Nhất Hòa, Xã Hiệp Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9287116,
   "Latitude": 106.8355372
 },
 {
   "STT": 1008,
   "Name": "Tủ thuốc trạm y tế phường Thống Nhất",
   "address": "Trạm Y tế P.Thống Nhất, P.Thống Nhất, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9479618,
   "Latitude": 106.8287615
 },
 {
   "STT": 1009,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "B16, ấp 6, xã Tân Hiệp, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6984529,
   "Latitude": 107.0598491
 },
 {
   "STT": 1010,
   "Name": "Quầy thuốc Thúy",
   "address": "108/B, tổ 4, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 1011,
   "Name": "Nhà thuốc Mai Như",
   "address": "Số 6B, đường Hùng Vương, P.Xuân Trung, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 1012,
   "Name": "Quầy thuốc Linh Uyên",
   "address": "Ấp Láng Me 1, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 1013,
   "Name": "Quầy thuốc ",
   "address": "432, Nguyễn Ái Quốc, P.Tân Tiến, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9588591,
   "Latitude": 106.8314463
 },
 {
   "STT": 1014,
   "Name": "Quầy thuốc Thu Huyền",
   "address": "Ấp 1, xã Phú Ngọc, Định Quán, Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 1015,
   "Name": "Nhà thuốc ",
   "address": "Số 54, tổ 2, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1016,
   "Name": "Quầy thuốc Bảo Trân",
   "address": "611, Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1017,
   "Name": "Quầy thuốc Phú Công",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1018,
   "Name": "Quầy thuốc Ngọc Dung",
   "address": "Ấp Tân Hạnh, Xuân Bảo, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 1019,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1020,
   "Name": "Quầy thuốc ",
   "address": "507, tổ 2, khu 7, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 1021,
   "Name": "Quầy thuốc Linh Giang",
   "address": "Số 33, tổ 2, ấp Phương Mai 1, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2771519,
   "Latitude": 107.4943786
 },
 {
   "STT": 1022,
   "Name": "Quầy thuốc ",
   "address": "196, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1023,
   "Name": "Quầy thuốc Công ty TNHH Phòng khám đa khoa Lê Thành",
   "address": "76/7, khu Phước Thuận, TT Long Thành, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7811468,
   "Latitude": 106.9447146
 },
 {
   "STT": 1024,
   "Name": "Nhà thuốc Lệ Thu",
   "address": "Số 6, đường Lê A, tổ 7, ấp Suối Chồn, xã Bảo Vinh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9510438,
   "Latitude": 107.2423004
 },
 {
   "STT": 1025,
   "Name": "Tủ thuốc trạm y tế xã Bình An",
   "address": "Trạm Y tế xã Bình An, ấp An Viễng, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8507492,
   "Latitude": 107.0508751
 },
 {
   "STT": 1026,
   "Name": "Quầy thuốc Ngọc Dũng",
   "address": "3140, tổ 5, ấp Việt Kiều, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9200551,
   "Latitude": 107.3671431
 },
 {
   "STT": 1027,
   "Name": "Tủ thuốc trạm y tế xã Bàu Cạn",
   "address": "Trạm Y tế xã Bàu Cạn, ấp 4, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7238459,
   "Latitude": 107.0675775
 },
 {
   "STT": 1028,
   "Name": "Quầy thuốc",
   "address": "402 Phạm Văn Thuận KP1, Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9571063,
   "Latitude": 106.8295196
 },
 {
   "STT": 1029,
   "Name": "Quầy thuốc Hồng Hạnh",
   "address": "65 đường Xí Nghiệp Bông Nam Bộ, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9858571,
   "Latitude": 106.9018757
 },
 {
   "STT": 1030,
   "Name": "Quầy thuốc",
   "address": "651, ấp Suối Tre, xã Suối Tre, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9456269,
   "Latitude": 107.2075388
 },
 {
   "STT": 1031,
   "Name": "Quầy thuốc",
   "address": "168 Nguyễn Văn Tiên, tổ 39, KP 11, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9634052,
   "Latitude": 106.8379609
 },
 {
   "STT": 1032,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "SN 7, ấp 4, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0694236,
   "Latitude": 107.2828674
 },
 {
   "STT": 1033,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Lâm San, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7216641,
   "Latitude": 107.3394486
 },
 {
   "STT": 1034,
   "Name": "Quầy thuốc Diễm Thúy",
   "address": "Ấp Tân Thành, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 1035,
   "Name": "Quầy thuốc Ngọc Diệu",
   "address": "89, Tổ 9, KP1, P.Tân Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1036,
   "Name": "Nhà thuốc An Viên",
   "address": "D26B KP5, P.Tân Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9623154,
   "Latitude": 106.8679899
 },
 {
   "STT": 1037,
   "Name": "Quầy thuốc Nguyên Sơn",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1038,
   "Name": "Quầy thuốc",
   "address": "28, KP3, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9540718,
   "Latitude": 106.8491197
 },
 {
   "STT": 1039,
   "Name": "Quầy thuốc",
   "address": "10/21, tổ 21, KP 8, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916476,
   "Latitude": 106.8502286
 },
 {
   "STT": 1040,
   "Name": "Quầy thuốc Xuân Mây",
   "address": "KP 2, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9588417,
   "Latitude": 107.0124501
 },
 {
   "STT": 1041,
   "Name": "Quầy thuốc Thu Vân",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 1042,
   "Name": "Quầy thuốc Đức Tài",
   "address": "SN 1810, tổ 6, ấp Thọ Lâm 3, xã Phú Thanh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1043,
   "Name": "Quầy thuốc Trung Tâm",
   "address": "46B, Tổ 7, Ấp Long Đức 1, Xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 1044,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "20, Ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.680821,
   "Latitude": 107.0262359
 },
 {
   "STT": 1045,
   "Name": "Quầy thuốc Dũng Loan",
   "address": "Ấp Tam Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 1046,
   "Name": "Nhà thuốc Thanh Hưng",
   "address": "2/6A, Tổ 17, KP 3, Huỳnh Văn Nghệ, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548762,
   "Latitude": 106.8021482
 },
 {
   "STT": 1047,
   "Name": "Quầy thuốc Văn Minh",
   "address": "Tổ 7, khu Phước Hải, TT Long Thành, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1048,
   "Name": "Quầy thuốc Phúc Tâm",
   "address": "Số 44, đường Đông Hòa-Hòa Bình, ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 1049,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 1050,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, Xã An Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8827023,
   "Latitude": 106.8713951
 },
 {
   "STT": 1051,
   "Name": "Quầy thuốc ",
   "address": "387, tổ 7, ấp Miểu, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 1053,
   "Name": "Quầy thuốc ",
   "address": "SN 06, tổ 1, ấp Ngọc Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.5673841,
   "Latitude": 107.3576689
 },
 {
   "STT": 1054,
   "Name": "Nhà thuốc Ngọc Đức",
   "address": "Tổ 19, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai ",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1055,
   "Name": "Quầy thuốc Huỳnh Trâm Anh",
   "address": "122/30A, ấp Tân Hóa, Xã Hóa An, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9356193,
   "Latitude": 106.8017321
 },
 {
   "STT": 1056,
   "Name": "Nhà thuốc Ngọc Minh",
   "address": "Số 53 (số cũ 276), KP 3, Trần Quốc Toản, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9651003,
   "Latitude": 106.8651491
 },
 {
   "STT": 1057,
   "Name": "Nhà thuốc",
   "address": "19/3C, KP 2, P.Tam Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 1058,
   "Name": "Nhà thuốc Đức Chính",
   "address": "270, KP 3, P.Bình Đa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1059,
   "Name": "Tủ thuốc trạm y tế phường Xuân Trung",
   "address": "156, Hồng Thập Tự, KP 5, P.Xuân Trung, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9338337,
   "Latitude": 107.2438144
 },
 {
   "STT": 1060,
   "Name": "Nhà thuốc ",
   "address": "F18B, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1061,
   "Name": "Tủ thuốc trạm y tế phường Xuân An",
   "address": "20, Nguyễn Tri Phương, KP 1, P.Xuân An, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 1062,
   "Name": "Tủ thuốc trạm y tế phường Xuân Hòa",
   "address": "80-81, Quang Trung, P.Xuân Hòa, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9235953,
   "Latitude": 107.2467518
 },
 {
   "STT": 1063,
   "Name": "Tủ thuốc trạm y tế phường Xuân Bình",
   "address": "Quốc lộ 1, KP 3, P.Xuân Bình, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9315809,
   "Latitude": 107.2389653
 },
 {
   "STT": 1064,
   "Name": "Tủ thuốc trạm y tế phường Xuân Thanh",
   "address": "Hẻm 10C, đường Phạm Lạc, P.Xuân Thanh, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9374374,
   "Latitude": 107.2586036
 },
 {
   "STT": 1065,
   "Name": "Tủ thuốc trạm y tế phường Phú Bình",
   "address": "KP 1, P.Phú Bình, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9141609,
   "Latitude": 107.2341324
 },
 {
   "STT": 1066,
   "Name": "Tủ thuốc trạm y tế xã Xuân Tân",
   "address": "Ấp Tân Phong, xã Xuân Tân, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.8903618,
   "Latitude": 107.2237033
 },
 {
   "STT": 1067,
   "Name": "Tủ thuốc trạm y tế xã Hàng Gòn",
   "address": "Ấp Tân Phong, xã Hàng Gòn, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.8870331,
   "Latitude": 107.223114
 },
 {
   "STT": 1068,
   "Name": "Tủ thuốc trạm y tế xã Bàu Trâm",
   "address": "Tổ 8, xã Bàu Trâm, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9284152,
   "Latitude": 107.2835238
 },
 {
   "STT": 1069,
   "Name": "Tủ thuốc trạm y tế xã Bàu Sen",
   "address": "Ấp Bàu Sen, xã Bàu Sen, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9191226,
   "Latitude": 107.2055715
 },
 {
   "STT": 1070,
   "Name": "Quầy thuốc An Khang",
   "address": "Số 333, Quốc lộ 1A, ấp 2, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.823195,
   "Latitude": 107.5521934
 },
 {
   "STT": 1071,
   "Name": "Tủ thuốc trạm y tế xã Bảo Quang",
   "address": "Ấp 18 Gia Đình, xã Bảo Quang, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9776243,
   "Latitude": 107.279669
 },
 {
   "STT": 1072,
   "Name": "Công ty TNHH Dược phẩm Sơn Minh",
   "address": "Số 135, Phan Đình Phùng, KP 4, P.Trung Dũng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.951577,
   "Latitude": 106.8181043
 },
 {
   "STT": 1073,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Thuận Thuận Việt",
   "address": "66/117, tổ 12, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 1074,
   "Name": "Quầy thuốc Nguyên Phương",
   "address": "Số 420, ấp Tân Xuân, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 1075,
   "Name": "Quầy thuốc Trúc Ly",
   "address": "Khu 4, ấp Xuân Thiện, xã Xuân Thiện, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0364891,
   "Latitude": 107.2370874
 },
 {
   "STT": 1076,
   "Name": "Quầy thuốc Thùy Nhung",
   "address": "Khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1077,
   "Name": "Quầy thuốc Như Luyến",
   "address": "SN 36, tổ 10, ấp 4, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 10.9126924,
   "Latitude": 106.6745676
 },
 {
   "STT": 1078,
   "Name": "Quầy thuốc Diệu Hiền",
   "address": "42, tổ 14, KP 3, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1079,
   "Name": "Quầy thuốc Anh Bình",
   "address": "Ấp 3, xã Phú Thạnh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 1080,
   "Name": "Nhà thuốc Hùng Hạnh",
   "address": "45, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.952032,
   "Latitude": 106.8772508
 },
 {
   "STT": 1081,
   "Name": "Quầy thuốc Phương Hòa",
   "address": "Số 68/5, ấp Quảng Phát, Quảng Tiến, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9500339,
   "Latitude": 106.9927351
 },
 {
   "STT": 1082,
   "Name": "Quầy thuốc An Nguyên",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1083,
   "Name": "Quầy thuốc Văn Ngọc 1",
   "address": "026, ấp Hiền Hòa, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6911092,
   "Latitude": 107.020279
 },
 {
   "STT": 1084,
   "Name": "Quầy thuốc Văn Ngọc 2",
   "address": "Số 197, ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 1085,
   "Name": "Quầy thuốc",
   "address": "Số 22/4, KP 10, P. Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 1086,
   "Name": "Nhà thuốc Hoa Đà",
   "address": "103, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9803043,
   "Latitude": 106.8548082
 },
 {
   "STT": 1087,
   "Name": "Quầy thuốc ",
   "address": "1417, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 1088,
   "Name": "Nhà thuốc Minh Thu",
   "address": "1035, KP 7, P.Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 1089,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Tổ 2, ấp 5, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8413355,
   "Latitude": 106.944158
 },
 {
   "STT": 1090,
   "Name": "Quầy thuốc Minh Khánh",
   "address": "14/2B, tổ 12, ấp Bình Hóa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9348991,
   "Latitude": 106.6999313
 },
 {
   "STT": 1091,
   "Name": "Quầy thuốc Thanh Sơn",
   "address": "F1/020, ấp Nam Sơn, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 1092,
   "Name": "Quầy thuốc ",
   "address": "Ấp Long Hiệu, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7499651,
   "Latitude": 106.8796401
 },
 {
   "STT": 1093,
   "Name": "Nhà thuốc Bình Sơn",
   "address": "Số 2, khu QHN ở bệnh viện Tâm Thần, KP 7, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656457,
   "Latitude": 106.846072
 },
 {
   "STT": 1094,
   "Name": "Quầy thuốc",
   "address": "Ấp Hòa Bình, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 1095,
   "Name": "Quầy thuốc ",
   "address": "Số 60A, Nguyễn Văn Tiên, KP11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9797236,
   "Latitude": 106.8423797
 },
 {
   "STT": 1096,
   "Name": "Quầy thuốc Long Gấm",
   "address": "27, Tổ 8, Ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 1097,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 1098,
   "Name": "Quầy thuốc",
   "address": "8/1G, ấp Võ Dõng, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 1099,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1100,
   "Name": "Nhà thuốc Công ty TNHH Xây dựng - Y tế Tâm An",
   "address": "E43-E44, đường A6, KDC đường Võ Thị Sáu, KP 7, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9487122,
   "Latitude": 106.8299871
 },
 {
   "STT": 1101,
   "Name": "Quầy thuốc Quỳnh Mai",
   "address": "Ấp Bắc Hòa, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 1102,
   "Name": "Quầy thuốc Mai Thúy",
   "address": "Ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9522557,
   "Latitude": 107.3325721
 },
 {
   "STT": 1103,
   "Name": "Nhà thuốc Phương Loan",
   "address": "247, Cách Mạng Tháng 8, KP 4, P.Hòa Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9519834,
   "Latitude": 106.8109365
 },
 {
   "STT": 1104,
   "Name": "Quầy thuốc Hồng Gấm",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1105,
   "Name": "Quầy thuốc",
   "address": "118, Tổ 1, ấp 2, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9838138,
   "Latitude": 107.0275775
 },
 {
   "STT": 1106,
   "Name": "Quầy thuốc Nhật Minh",
   "address": "Số nhà 2305, tổ 1, ấp Phương Lâm 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 1107,
   "Name": "Quầy thuốc Quế Anh",
   "address": "11B, ấp Nhân Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.946514,
   "Latitude": 107.0544048
 },
 {
   "STT": 1108,
   "Name": "Quầy thuốc Trang Lê",
   "address": "27A, Nguyễn Ái Quốc, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9656457,
   "Latitude": 106.846072
 },
 {
   "STT": 1109,
   "Name": "Quầy thuốc Nhân Nghĩa",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1110,
   "Name": "Quầy thuốc Hương Quyên",
   "address": "Ki ốt số 34, chợ La Ngà, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1612244,
   "Latitude": 107.2654843
 },
 {
   "STT": 1111,
   "Name": "Quầy thuốc ",
   "address": "4, tổ 1, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 1112,
   "Name": "Quầy thuốc Kim Dung",
   "address": "Ấp 2, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0097053,
   "Latitude": 106.8276976
 },
 {
   "STT": 1113,
   "Name": "Quầy thuốc",
   "address": "38/4, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 1114,
   "Name": "Quầy thuốc Phúc Uy",
   "address": "Tổ 6, ấp 1, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 1115,
   "Name": "Quầy thuốc Đức Huế",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1116,
   "Name": "Nhà thuốc Quỳnh Nhung",
   "address": "173/190, tổ 18, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520451,
   "Latitude": 106.8772376
 },
 {
   "STT": 1117,
   "Name": "Quầy thuốc",
   "address": "Số 189, khu 4, ấp 2, Gia Canh, Định Quán",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 1118,
   "Name": "Quầy thuốc Như Mai",
   "address": "3088 QL1A, ấp Việt Kiều, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9082071,
   "Latitude": 107.3700162
 },
 {
   "STT": 1119,
   "Name": "Nhà thuốc Ngọc Bảo An",
   "address": "120/29, KP 6, P.Tân Tiến, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 1120,
   "Name": "Quầy thuốc phòng khám đa khoa Ái Nghĩa Nhơn Trạch",
   "address": "Đường 25B, KCN Nhơn Trạch 1, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.737187,
   "Latitude": 106.934051
 },
 {
   "STT": 1121,
   "Name": "Quầy thuốc phòng khám đa khoa Ái Nghĩa Long Thành",
   "address": "Quốc lộ 51A, Khu Văn Hải, TT Long Thành, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7911024,
   "Latitude": 106.9468454
 },
 {
   "STT": 1122,
   "Name": "Quầy thuốc Nhật Hải",
   "address": "SN 1/1, phố 4, ấp 2, xã Phú Lợi, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2046485,
   "Latitude": 107.362844
 },
 {
   "STT": 1123,
   "Name": "Quầy thuốc",
   "address": "783, tổ 8, khu 10, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 1124,
   "Name": "Quầy thuốc",
   "address": "Tổ 7, Ấp 3, xã Phú Lập, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 1125,
   "Name": "Quầy thuốc ",
   "address": "Khu 3, ấp Cẩm Tân, xã Xuân Tân, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8950788,
   "Latitude": 107.2283676
 },
 {
   "STT": 1126,
   "Name": "Quầy thuốc Minh Đăng",
   "address": "Tổ 4, ấp 11, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.783333,
   "Latitude": 107.016667
 },
 {
   "STT": 1127,
   "Name": "Quầy thuốc Hạnh Lâm",
   "address": "Tổ 20, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 1128,
   "Name": "Quầy thuốc Hoàng Linh",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1129,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Ấp Phú Quý 2, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1130,
   "Name": "Quầy thuốc ",
   "address": "Số 793, ấp Gia Hòa, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9572525,
   "Latitude": 107.4080686
 },
 {
   "STT": 1131,
   "Name": "Quầy thuốc Ngọc Anh Thư",
   "address": "Tổ 9, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8987411,
   "Latitude": 106.922179
 },
 {
   "STT": 1132,
   "Name": "Quầy thuốc Tôn Minh",
   "address": "Tổ 11, ấp Tân Mai 2, Phước Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8746531,
   "Latitude": 106.9103868
 },
 {
   "STT": 1133,
   "Name": "Quầy thuốc Phú Hữu",
   "address": "Ấp Câu Kê, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 1134,
   "Name": "Nhà thuốc Phương Luân",
   "address": "32B, KP 3, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9522077,
   "Latitude": 106.87746
 },
 {
   "STT": 1135,
   "Name": "Quầy thuốc Minh Uyên",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1136,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "429, tổ 5, ấp Hòa Thành, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 1137,
   "Name": "Quầy thuốc Thu Phương",
   "address": "Ấp 2, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8711443,
   "Latitude": 107.0949002
 },
 {
   "STT": 1138,
   "Name": "Quầy thuốc Tuấn Hưng",
   "address": "Ấp Suối Cát 2, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9083057,
   "Latitude": 107.3658629
 },
 {
   "STT": 1139,
   "Name": "Quầy thuốc Cô Mập",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1140,
   "Name": "Quầy thuốc Trung Đức",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1141,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 1142,
   "Name": "Quầy thuốc Kiệt Hà",
   "address": "Tổ 12, ấp Long Đức 3, Xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 1143,
   "Name": "Nhà thuốc Bình Ân",
   "address": "35/81, KP 12, P. Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 1144,
   "Name": "Quầy thuốc Bảo Nguyên",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1145,
   "Name": "Tủ thuốc trạm y tế xã Xuân Thạnh",
   "address": "Trạm Y tế xã Xuân Thạnh, ấp 9/4, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9125565,
   "Latitude": 107.1390135
 },
 {
   "STT": 1146,
   "Name": "Quầy thuốc Bình Minh",
   "address": "Ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 1147,
   "Name": "Tủ thuốc trạm y tế xã Xuân Thiện",
   "address": "Trạm Y tế xã Xuân Thiện, xã Xuân Thiện, huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0364891,
   "Latitude": 107.2370874
 },
 {
   "STT": 1148,
   "Name": "Tủ thuốc trạm y tế xã Lộ 25",
   "address": "Trạm Y tế xã lộ 25, ấp 2, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8702403,
   "Latitude": 107.0904402
 },
 {
   "STT": 1149,
   "Name": "Tủ thuốc trạm y tế xã Bàu Hàm 2",
   "address": "Trạm Y tế xã Bàu Hàm 2, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9450102,
   "Latitude": 107.1344045
 },
 {
   "STT": 1150,
   "Name": "Tủ thuốc trạm y tế xã Quang Trung",
   "address": "Trạm Y tế xã Quang Trung, Lê Lợi 2, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0005123,
   "Latitude": 107.1572113
 },
 {
   "STT": 1151,
   "Name": "Tủ thuốc trạm y tế xã Gia Tân 3",
   "address": "Trạm Y tế xã Gia Tân 3, ấp Tân Yên, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0595612,
   "Latitude": 107.1675217
 },
 {
   "STT": 1152,
   "Name": "Nhà thuốc Hữu Nghị",
   "address": "22, Nguyễn Ái quốc, KP̉ 6, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.963171,
   "Latitude": 106.8381254
 },
 {
   "STT": 1153,
   "Name": "Tủ thuốc trạm y tế xã Gia Tân 2",
   "address": "Trạm Y tế xã Gia Tân 2, Đức Long 2, xã Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 1154,
   "Name": "Tủ thuốc trạm y tế xã Gia Tân 1",
   "address": "Trạm Y tế xã Gia Tân 1, ấp Dốc Mơ, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0595612,
   "Latitude": 107.1675217
 },
 {
   "STT": 1155,
   "Name": "Nhà thuốc MAI KA",
   "address": "95/471, KP 4, Phạm Văn Thuận, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9522896,
   "Latitude": 106.8536878
 },
 {
   "STT": 1156,
   "Name": "Nhà thuốc Khánh Hưng 1",
   "address": "178-180, CMT8, P.Quyết Thắng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9438056,
   "Latitude": 106.820703
 },
 {
   "STT": 1157,
   "Name": "Quầy thuốc Tuấn Quỳnh",
   "address": "Tổ 15, KP 2, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 1158,
   "Name": "Quầy thuốc Kiều Huyên",
   "address": "203 Hùng Vương, khu 4, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9214982,
   "Latitude": 107.400949
 },
 {
   "STT": 1159,
   "Name": "Quầy thuốc Phước Khang",
   "address": "370, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1160,
   "Name": "Quầy thuốc ",
   "address": "106, Tổ 14, Ấp 2, Xã Tam An, Huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8902353,
   "Latitude": 106.8584837
 },
 {
   "STT": 1161,
   "Name": "Quầy thuốc",
   "address": "Ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1162,
   "Name": "Quầy thuốc ",
   "address": "36D, Tổ 10, An Hòa, Xã Hóa An, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 1163,
   "Name": "Quầy thuốc Thanh Thư",
   "address": "1123, đường Trảng Bom, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9850708,
   "Latitude": 107.0297401
 },
 {
   "STT": 1164,
   "Name": "Quầy thuốc Văn Thuận Phát",
   "address": "Số 20, Nguyễn Văn Tiên, KP 9, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9785731,
   "Latitude": 106.8470534
 },
 {
   "STT": 1165,
   "Name": "Quầy thuốc ",
   "address": "631, ấp Thọ Lộc, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9607577,
   "Latitude": 107.3488873
 },
 {
   "STT": 1166,
   "Name": "Quầy thuốc Phòng khám đa khoa khu vực Xuân Hưng",
   "address": "Ấp 5, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8424776,
   "Latitude": 107.4921857
 },
 {
   "STT": 1167,
   "Name": "Quầy thuốc ",
   "address": "114, Đồng Khởi, KP 4, P.Tân Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9586742,
   "Latitude": 106.856797
 },
 {
   "STT": 1168,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "299 khu 4, ấp Hưng Nghĩa, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 1169,
   "Name": "Quầy thuốc ",
   "address": "304, ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1170,
   "Name": "Quầy thuốc Kim Kiển",
   "address": "302 Đường 1, Ấp 3, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8226677,
   "Latitude": 107.5537039
 },
 {
   "STT": 1171,
   "Name": "Quầy thuốc Hòa Bình",
   "address": "Số 47, tổ 2, ấp 6, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9820287,
   "Latitude": 107.025637
 },
 {
   "STT": 1172,
   "Name": "Quầy thuốc ",
   "address": "Tổ 7, ấp Ngô Quyền, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9485824,
   "Latitude": 107.1315104
 },
 {
   "STT": 1173,
   "Name": "Quầy thuốc Mỹ Dung",
   "address": "Chợ Xuân Đà, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8783753,
   "Latitude": 107.4398782
 },
 {
   "STT": 1174,
   "Name": "Quầy thuốc Dung",
   "address": "Ấp Bình Hòa, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9060699,
   "Latitude": 107.323272
 },
 {
   "STT": 1175,
   "Name": "Quầy thuốc Lan",
   "address": "Ấp Hòa Bình, xã Bảo Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9029564,
   "Latitude": 107.2838081
 },
 {
   "STT": 1176,
   "Name": "Quầy thuốc Hòa Hạnh",
   "address": "33A, ấp Vàm, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 1177,
   "Name": "Quầy thuốc",
   "address": "Khu 3, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 1178,
   "Name": "Quầy thuốc Huyền Trân",
   "address": "351/12, ấp Trung Tâm,xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1179,
   "Name": "Quầy thuốc",
   "address": "Ấp Bến Cam, Phước Thiền, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 1180,
   "Name": "Quầy thuốc",
   "address": "93/1 KP3, P.Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.96259,
   "Latitude": 106.8659431
 },
 {
   "STT": 1181,
   "Name": "Quầy thuốc",
   "address": "71B Tổ 13 KP 3, P.An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1182,
   "Name": "Quầy thuốc Thanh Trâm",
   "address": "Ấp Bảo Vệ, xã Giang Điền, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9269822,
   "Latitude": 106.9846975
 },
 {
   "STT": 1183,
   "Name": "Quầy thuốc Thiên Ân",
   "address": "Ấp Trung Tâm, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1184,
   "Name": "Quầy thuốc Đức Phát",
   "address": "Tổ 21 Khu 4 Ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8364222,
   "Latitude": 106.9406474
 },
 {
   "STT": 1185,
   "Name": "Quầy thuốc Dũng Hiền",
   "address": "510, ấp Bàu Cá, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9687374,
   "Latitude": 107.0304582
 },
 {
   "STT": 1186,
   "Name": "Quầy thuốc Hoàng Kim Bảo",
   "address": "E849C, tổ 14, KP5A, P. Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 1187,
   "Name": "Quầy thuốc An Khang",
   "address": "429, Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8810166,
   "Latitude": 106.9999856
 },
 {
   "STT": 1188,
   "Name": "Quầy thuốc ",
   "address": "640, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1189,
   "Name": "Quầy thuốc Duy Ân",
   "address": "Tổ 3, ấp 3, Phú Lập, Tân Phú, Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 1190,
   "Name": "Quầy thuốc Nhân Ái",
   "address": "31/1E, Võ Dõng 1, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 1191,
   "Name": "Quầy thuốc Nga",
   "address": "Chợ Bảo Hòa, ấp Hòa Hợp, xã Bảo Hòa, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9022012,
   "Latitude": 107.2859484
 },
 {
   "STT": 1192,
   "Name": "Quầy thuốc Thành Mai",
   "address": "96/20 Ấp 3, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8770877,
   "Latitude": 107.544598
 },
 {
   "STT": 1193,
   "Name": "Quầy thuốc Thanh Mẫn",
   "address": "Ấp Bến Sắn, Phước Thiền, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1194,
   "Name": "Quầy thuốc ",
   "address": "598/8, ấp Tân Thành, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 1195,
   "Name": "Quầy thuốc Phương Nhung",
   "address": "319/77, Tổ 11, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 1196,
   "Name": "Quầy thuốc Thảo Linh",
   "address": "27/1, KP1, P. Bửu Long, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9629872,
   "Latitude": 106.7840566
 },
 {
   "STT": 1197,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu",
   "address": "Số 10, Lý Thường Kiệt, Thanh Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9468355,
   "Latitude": 106.8146418
 },
 {
   "STT": 1198,
   "Name": "Nhà thuốc Duy Long",
   "address": "115, Đồng Khởi, KP 8, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9736792,
   "Latitude": 106.8519457
 },
 {
   "STT": 1199,
   "Name": "Quầy thuốc",
   "address": "tổ 39, KP4A, P. Tân Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9708772,
   "Latitude": 106.9093
 },
 {
   "STT": 1200,
   "Name": "Quầy thuốc Đức Linh",
   "address": "2132, ấp Quảng Lộc, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.945607,
   "Latitude": 106.98593
 },
 {
   "STT": 1201,
   "Name": "Tủ thuốc trạm y tế xã Đồi 61",
   "address": "Trạm Y tế xã đồi 61, Đồi 61, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 1202,
   "Name": "Quầy thuốc Hống Phát 1",
   "address": "Ấp Chợ, Suối Nho, Định Quán, Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 1203,
   "Name": "Tủ thuốc trạm y tế xã Bắc Sơn",
   "address": "Trạm Y tế xã Bắc Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9593041,
   "Latitude": 106.9545007
 },
 {
   "STT": 1204,
   "Name": "Tủ thuốc trạm y tế xã Bình Minh",
   "address": "Trạm Y tế xã Bình Minh, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9518242,
   "Latitude": 106.9683677
 },
 {
   "STT": 1205,
   "Name": "Tủ thuốc trạm y tế Thị trấn Trảng Bom",
   "address": "Trạm Y tế TT Trảng Bom, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9528947,
   "Latitude": 106.9967776
 },
 {
   "STT": 1206,
   "Name": "Tủ thuốc trạm y tế xã Quảng Tiến",
   "address": "Trạm Y tế xã Quảng Tiến, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9478175,
   "Latitude": 106.9901258
 },
 {
   "STT": 1207,
   "Name": "Tủ thuốc trạm y tế xã An Viễn",
   "address": "Trạm Y tế xã An Viễn, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8811496,
   "Latitude": 106.997526
 },
 {
   "STT": 1208,
   "Name": "Tủ thuốc trạm y tế xã Hố Nai 3",
   "address": "Trạm Y tế xã Hố Nai 3, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9658188,
   "Latitude": 106.9204902
 },
 {
   "STT": 1209,
   "Name": "Tủ thuốc trạm y tế xã Giang Điền",
   "address": "Trạm Y tế xã Giang Điền, xã Giang Điền, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9246131,
   "Latitude": 106.9821807
 },
 {
   "STT": 1210,
   "Name": "Quầy thuốc",
   "address": "Tổ 1, Kp1, TT Vĩnh An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 1211,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Ấp 3, Long Thọ, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6937381,
   "Latitude": 106.9500662
 },
 {
   "STT": 1212,
   "Name": "Quầy thuốc",
   "address": "KP 1, TT vĩnh an, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 1213,
   "Name": "Quầy thuốc ",
   "address": "Ấp Phước Lý, xã Đại Phước, huyên Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1214,
   "Name": "Quầy thuốc PKĐK Phúc Trạch",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1215,
   "Name": "Quầy thuốc",
   "address": "Ẩp 3, xã Hiếu Liêm, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.1118386,
   "Latitude": 106.966643
 },
 {
   "STT": 1216,
   "Name": "Quầy thuốc Song Phúc",
   "address": "77, đường Chợ Chiều, Khu 2, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9598649,
   "Latitude": 106.9385375
 },
 {
   "STT": 1217,
   "Name": "Quầy thuốc",
   "address": "Ấp Tân Lập 1, Cây Gáo, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1218,
   "Name": "Quầy thuốc",
   "address": "878, ấp Trà Cổ, Bình Minh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.950815,
   "Latitude": 106.9751383
 },
 {
   "STT": 1219,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Phú Thạnh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 1220,
   "Name": "Quầy thuốc Ngọc Nhẫn",
   "address": "Ấp Việt Kiều, Xuân Hiệp, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9091923,
   "Latitude": 107.381107
 },
 {
   "STT": 1221,
   "Name": "Quầy thuốc Thiên Ân",
   "address": "Số nhà 13/05, ấp 1, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 1222,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "134, Tổ 3, ấp Hiền Hòa, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6792113,
   "Latitude": 107.0249535
 },
 {
   "STT": 1223,
   "Name": "Quầy thuốc",
   "address": "KP5, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 1224,
   "Name": "Quầy thuốc Nhất Hưng",
   "address": "Số 1819, Ấp 5, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 1225,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Số 17, tổ 1, ấp Phú Lập, Phú Bình, Tân Phú, Đồng Nai",
   "Longtitude": 11.3649013,
   "Latitude": 107.4026244
 },
 {
   "STT": 1226,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1227,
   "Name": "Nhà thuốc",
   "address": "9/18, tổ 18, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916465,
   "Latitude": 106.8502385
 },
 {
   "STT": 1228,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, xã Bình Lợi, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0384694,
   "Latitude": 106.8237374
 },
 {
   "STT": 1229,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "30/2, tổ 4, ấp 1, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8225843,
   "Latitude": 106.9304075
 },
 {
   "STT": 1230,
   "Name": "Quầy thuốc",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 1231,
   "Name": "Quầy thuốc Hòa Phát",
   "address": "Số 42, Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1232,
   "Name": "Quầy thuốc Mai Khôi",
   "address": "Ấp Suối Râm, xã Long Giao, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8274876,
   "Latitude": 107.2282225
 },
 {
   "STT": 1233,
   "Name": "Quầy thuốc ",
   "address": "14/3, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1234,
   "Name": "Quầy thuốc ",
   "address": "Tổ 3, Thôn 10, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.783333,
   "Latitude": 107.016667
 },
 {
   "STT": 1235,
   "Name": "Quầy thuốc",
   "address": "02, ấp Tân Phát, Đồi 61, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 1236,
   "Name": "Quầy thuốc Khánh Hà",
   "address": "Số 2/5, ấp Hòa Bình, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9430668,
   "Latitude": 107.0709193
 },
 {
   "STT": 1237,
   "Name": "Quầy thuốc Thanh Huế",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1238,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "Tổ 1, ấp Đất Mới, Long Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 1239,
   "Name": "Quầy thuốc Thiên Kim",
   "address": "20/2B, ấp Tây Kim, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 1240,
   "Name": "Quầy thuốc",
   "address": "Khu 2, ấp Bình Thạch, Bình Hòa, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 10.985599,
   "Latitude": 106.7907786
 },
 {
   "STT": 1241,
   "Name": "Quầy thuốc Hiếu Thuận",
   "address": "Số 378, ấp Tân Phát, Đồi 61, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 1242,
   "Name": "Quầy thuốc ",
   "address": "5/3, K 3, ấp Đồng Nai, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 1243,
   "Name": "Quầy thuốc Minh Hiếu",
   "address": "2847, ấp Bình Xuân 1, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9099012,
   "Latitude": 107.340327
 },
 {
   "STT": 1244,
   "Name": "Nhà thuốc Bệnh viện Đa khoa khu vực Định Quán",
   "address": " Bệnh viện Đa khoa khu vực Định Quán",
   "Longtitude": 11.1986981,
   "Latitude": 107.3631752
 },
 {
   "STT": 1245,
   "Name": "Nhà thuốc Vũ Anh",
   "address": "500C/A 2, Ấp Nhị Hoà, Xã Hiệp Hoà, thành phố Biên Hoà ",
   "Longtitude": 10.9307161,
   "Latitude": 106.8285716
 },
 {
   "STT": 1246,
   "Name": "Quầy thuốc",
   "address": "Ấp Bình Hòa, Xuân Phú, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9040775,
   "Latitude": 107.312525
 },
 {
   "STT": 1247,
   "Name": "Quầy thuốc Trường Xuân",
   "address": "Khu I, ấp Bình Thạch, xã Bình Hoà, huyện Vĩnh Cửu, Đồng Nai",
   "Longtitude": 10.9854498,
   "Latitude": 106.7983806
 },
 {
   "STT": 1248,
   "Name": "Quầy thuốc",
   "address": "Tổ 3, Ấp An Viễng, xã Bình An, huyện Long Thành",
   "Longtitude": 10.85,
   "Latitude": 107.05
 },
 {
   "STT": 1249,
   "Name": "Quầy thuốc",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1250,
   "Name": "Quầy thuốc",
   "address": "Ấp Phước Lý, xã Đại Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1251,
   "Name": "Quầy thuốc",
   "address": "Số 133, Phan Chu Trinh, Quang Vinh, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9490293,
   "Latitude": 106.8157526
 },
 {
   "STT": 1252,
   "Name": "Quầy thuốc TRẦN LÊ",
   "address": "882 Ấp 1A, xã Xuân Hưng, huyện Xuân Lộc ",
   "Longtitude": 10.8498936,
   "Latitude": 107.495291
 },
 {
   "STT": 1253,
   "Name": "Quầy thuốc",
   "address": "63/3A, ấp Đông Kinh, xã Gia Kiệm, huyện Thống Nhất",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 1254,
   "Name": "Quầy thuốc Phước Lộc",
   "address": "số 66, ấp Hiệp Quyết, TT Định Quán, huyện Định Quán",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1255,
   "Name": "Quầy thuốc Hiền Ân",
   "address": "Ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 1256,
   "Name": "Quầy thuốc ",
   "address": "Số 73, Ấp Long Khánh 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8315859,
   "Latitude": 106.8853178
 },
 {
   "STT": 1257,
   "Name": "Tủ thuốc trạm y tế xã Trung Hòa",
   "address": "Trạm Y tế xã Trung Hoà, khu 6, ấp Bàu Cá, Trung Hòa, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9687374,
   "Latitude": 107.0304582
 },
 {
   "STT": 1258,
   "Name": "Tủ thuốc trạm y tế xã Sông Trầu",
   "address": "Tạm Y tế xã Sông Trầu, ấp 5, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9780045,
   "Latitude": 107.0231957
 },
 {
   "STT": 1259,
   "Name": "Tủ thuốc trạm y tế xã Bàu Hàm",
   "address": "Trạm Y tế xã Bàu Hàm, ấp Tân Hợp, xã Bàu Hàm, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817776,
   "Latitude": 107.1044963
 },
 {
   "STT": 1260,
   "Name": "Quầy thuốc Lê Dung",
   "address": "Số 287, Hùng Vương, khu 1, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.91344,
   "Latitude": 107.398139
 },
 {
   "STT": 1261,
   "Name": "Quầy thuốc",
   "address": "Khu 8, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9251879,
   "Latitude": 107.4022351
 },
 {
   "STT": 1262,
   "Name": "Quầy thuốc Nhân Minh",
   "address": "26 tổ 1B KP12, P.An Bình, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.928537,
   "Latitude": 106.8583508
 },
 {
   "STT": 1263,
   "Name": "Quầy thuốc Minh Hồng",
   "address": "615, ấp Thọ Lộc, Xuân Thọ, Xuân lộc, Đồng Nai",
   "Longtitude": 10.9591637,
   "Latitude": 107.3494043
 },
 {
   "STT": 1264,
   "Name": "Nhà Thuốc Nghĩa Sơn",
   "address": "240/21, KP1, P.Tân Hiệp, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1265,
   "Name": "Tủ thuốc trạm y tế xã Thanh Bình",
   "address": "Trạm Y tế xã Thanh Bình, ấp Trung Tâm, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 11.0794884,
   "Latitude": 107.1038736
 },
 {
   "STT": 1266,
   "Name": "Quầy thuốc",
   "address": "884, tổ 20, khu 4, ấp 1, xã An Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8916628,
   "Latitude": 106.8634886
 },
 {
   "STT": 1267,
   "Name": "Quầy thuốc",
   "address": "Ấp Trầu, xã Phước Thiền, H.Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1268,
   "Name": "Quầy thuốc Thảo Phước",
   "address": "490A Ấp Ngũ Phúc, xã Hố Nai 3, H.Trảng Bom, Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 1269,
   "Name": "Nhà thuốc Minh Thư",
   "address": "Tổ 5, khu Phước Hải, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1270,
   "Name": "Tủ thuốc trạm y tế xã Cây Gáo",
   "address": "Trạm Y tế xã Cây Gáo, ấp Tân Lập 2, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1271,
   "Name": "Tủ thuốc trạm y tế xã Đông Hòa",
   "address": "Trạm Y tế xã Đông Hoà, ấp Quảng Đà, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9430194,
   "Latitude": 107.070769
 },
 {
   "STT": 1272,
   "Name": "Tủ thuốc trạm y tế xã Tây Hòa",
   "address": "Trạm Y tế xã Tây Hoà, ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9506352,
   "Latitude": 107.0498478
 },
 {
   "STT": 1273,
   "Name": "Tủ thuốc trạm y tế xã Sông Thao",
   "address": "Trạm Y tế xã Sông Thao, xã Sông Thao, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1274,
   "Name": "Quầy thuốc Yến Nhi",
   "address": "139A ấp Tân Lập 1, xã Cây Gáo, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1275,
   "Name": "Tủ thuốc trạm y tế xã Hưng Thịnh",
   "address": "Trạm Y tế xã Hưng Thịnh, ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9414521,
   "Latitude": 107.0811989
 },
 {
   "STT": 1276,
   "Name": "Quầy thuốc",
   "address": "324A tổ 5, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9186757,
   "Latitude": 106.9415721
 },
 {
   "STT": 1277,
   "Name": "Quầy thuốc",
   "address": "36 Cách Mạng Tháng 8, P.Xuân An, TX Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9275344,
   "Latitude": 107.244308
 },
 {
   "STT": 1278,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu",
   "address": "Số 25, ấp Hòa Bình, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 1279,
   "Name": "Quầy thuốc Linh Huệ",
   "address": "1939 ấp Thọ Lâm 1, xã Phú Xuân, H.Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1280,
   "Name": "Quầy thuốc",
   "address": "602 Tây Lạc, ấp An Chu, xã bắc Sơn, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1281,
   "Name": "Quầy thuốc Tâm Việt",
   "address": "238 KP5, TT Trảng Bom, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 1282,
   "Name": "Quầy thuốc",
   "address": "595 Tây Lạc, ấp An Chu, xã Bắc Sơn, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1283,
   "Name": "Nhà thuốc BV Quốc tế chấn thương chỉnh hình Sài Gòn-Đồng nai",
   "address": "F99 Võ Thị Sáu, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9479618,
   "Latitude": 106.8287615
 },
 {
   "STT": 1284,
   "Name": "Quầy thuốc Dũng Thanh",
   "address": "212 ấp Tân Bắc, xã Bình Minh, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 1285,
   "Name": "Quầy thuốc",
   "address": "77/4 ấp Thanh Hóa, xã Hố Nai 3, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9652691,
   "Latitude": 106.934588
 },
 {
   "STT": 1286,
   "Name": "Quầy thuốc",
   "address": "188, 7A/2 ấp An Bình, xã Trung Hòa, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1287,
   "Name": "Quầy thuốc Ngọc Thắng",
   "address": "Ấp Tân Hợp, xã Xuân Thành, H.Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9943485,
   "Latitude": 107.4264005
 },
 {
   "STT": 1288,
   "Name": "Quầy thuốc Linh Quế Hương",
   "address": "23B tổ 6, KP1, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1289,
   "Name": "Quầy thuốc số 1 - Công ty được Bửu Hòa",
   "address": "K3/242D Bùi Hữu Nghĩa, P. Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9297329,
   "Latitude": 106.8207343
 },
 {
   "STT": 1290,
   "Name": "Nhà thuốc Minh Hùng Cường",
   "address": "1/9 tổ 8, KP4, P.Tân Hiệp, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720567,
   "Latitude": 106.9097648
 },
 {
   "STT": 1291,
   "Name": "Nhà thuốc",
   "address": "Ấp Trung Tín, Xuân Trường, H.Xuân lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9727716,
   "Latitude": 107.4178711
 },
 {
   "STT": 1292,
   "Name": "Quầy thuốc",
   "address": "Ấp 2, An Viễn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.878534,
   "Latitude": 107.01237
 },
 {
   "STT": 1293,
   "Name": "Quầy thuốc",
   "address": "Ấp Bình Phú, Long Tân, H.Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 1294,
   "Name": "Quầy thuốc Ngọc Bảo Anh",
   "address": "Ấp 8, An Phước, Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 1295,
   "Name": "Quầy thuốc Bình Hiền",
   "address": "1582A1 tổ 26A, ấp Vườn Dừa, xã Phước Tân, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 1296,
   "Name": "Quầy thuốc ",
   "address": "99 Khu 4, ấp 8, xã An Phước, H.Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.862375,
   "Latitude": 106.963667
 },
 {
   "STT": 1297,
   "Name": "Quầy thuốc Long Tâm",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 1298,
   "Name": "Quầy thuốc",
   "address": "Tổ 6, ấp 5, xã Thạnh Phú, H.Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1299,
   "Name": "Quầy thuốc",
   "address": "104A2, KP5, P.Tân Hòa, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1300,
   "Name": "Quầy thuốc bệnh viện đa khoa Trảng Bom",
   "address": "Bệnh viện đa khoa Trảng Bom",
   "Longtitude": 10.9482486,
   "Latitude": 107.0059051
 },
 {
   "STT": 1301,
   "Name": "Quầy thuốc Thanh Tình",
   "address": "220 đường 2/9 KP5, TT Trảng Bom, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 1302,
   "Name": "Nhà thuốc BV Phụ sản Âu Cơ Biên Hòa",
   "address": "Tổ 6 KP5B, P.Tân Biên, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 1303,
   "Name": "Quầy thuốc Tài Nhân",
   "address": "24, Nguyễn Văn Hoa, KP4, P. Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": 10.955362,
   "Latitude": 106.833762
 },
 {
   "STT": 1304,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "E3/078, ấp Nguyễn Huệ 2, Quang Trung, Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 1305,
   "Name": "Quầy thuốc",
   "address": "534 ấp An Chu, xã Bắc Sơn, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1306,
   "Name": "Quầy thuốc Kim Sinh",
   "address": " Bắc Hợp, ấp An Chu, Bắc Sơn,  H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1307,
   "Name": "Nhà thuốc Hương Trang",
   "address": "91, Vũ Hồng Phô, KP 5, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9371216,
   "Latitude": 106.8607418
 },
 {
   "STT": 1308,
   "Name": "Quầy thuốc Hồng Thu",
   "address": "154 Nam Hòa, ấp Bùi Chu, xã Bắc Sơn, Huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1309,
   "Name": "Quầy thuốc",
   "address": "Ấp Thọ Lộc, xã Xuân Thọ, H.Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9522557,
   "Latitude": 107.3325721
 },
 {
   "STT": 1310,
   "Name": "Quầy thuốc Thùy Châm",
   "address": "Ấp Sông Mây, xã Bắc Sơn, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9704013,
   "Latitude": 106.9525172
 },
 {
   "STT": 1311,
   "Name": "Quầy thuốc Ngọc Phát",
   "address": "Ấp 2, xã Hiệp Phước, H. Hoằng Hóa, tỉnh Đồng Nai",
   "Longtitude": 10.7399492,
   "Latitude": 106.9447617
 },
 {
   "STT": 1312,
   "Name": "Nhà thuốc Cao Phong",
   "address": "239 Phan Đình Phùng, KP2, P.Quang Vinh, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9489846,
   "Latitude": 106.8173765
 },
 {
   "STT": 1313,
   "Name": "Quầy thuốc",
   "address": "119B Nguyễn Ái Quốc, KP3, P.Tân Phong, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656457,
   "Latitude": 106.846072
 },
 {
   "STT": 1314,
   "Name": "Quầy thuốc",
   "address": "Ấp 1, xã Thạnh Phú, H.Vỉnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1315,
   "Name": "Quầy thuốc Phương Loan",
   "address": "Tổ 9, ấp suối Lức, Xuân Đông, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8013782,
   "Latitude": 107.3648914
 },
 {
   "STT": 1316,
   "Name": "Quầy thuốc Vân Anh",
   "address": "Ấp Bùi Chu, xã Bắc Sơn, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1317,
   "Name": "Quầy thuốc",
   "address": "Ấp Vĩnh Tuy, Long Tân, H.Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 1318,
   "Name": "Quầy thuốc Nguyên Phúc",
   "address": "Ấp 5, xã Hiệp Phước, H.Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 1319,
   "Name": "Quầy thuốc Long Hiền",
   "address": "Số 47B, tổ 2, ấp 3, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1321,
   "Name": "Quầy thuốc Huy Khánh",
   "address": "450/3 ấp Quảng Phát, xã Quảng Tiến, H.Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9449414,
   "Latitude": 106.9943646
 },
 {
   "STT": 1322,
   "Name": "Quầy thuốc Kim Phượng 2",
   "address": "9/2H, ấp Võ Dõng 3, Gia Kiệm, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 1323,
   "Name": "Nhà thuốc Đức Sang",
   "address": "27A tổ 7, KP4, P.Trảng Dài, Thành Phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720565,
   "Latitude": 106.9097653
 },
 {
   "STT": 1324,
   "Name": "Quầy thuốc",
   "address": "Số 45/1, ấp Bạch Lâm, Gia Tân 2, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0465249,
   "Latitude": 107.1726519
 },
 {
   "STT": 1325,
   "Name": "Quầy thuốc",
   "address": "Ấp 3, Phú Thạnh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6655903,
   "Latitude": 106.8656363
 },
 {
   "STT": 1326,
   "Name": "Quầy thuốc Thành Lan",
   "address": "Chợ tổ 2, KP1, TT Vĩnh An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0859787,
   "Latitude": 107.0332316
 },
 {
   "STT": 1327,
   "Name": "Quầy thuốc Thúy Nga",
   "address": "Ấp Thái Hòa 1, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252677
 },
 {
   "STT": 1328,
   "Name": "Quầy thuốc ",
   "address": "ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1329,
   "Name": "Quầy thuốc bệnh viện đa khoa huyện Vĩnh Cửu",
   "address": "Bệnh viện đa khoa huyện Vĩnh Cửu",
   "Longtitude": 11.0951823,
   "Latitude": 107.0242541
 },
 {
   "STT": 1330,
   "Name": "Quầy thuốc",
   "address": "Ấp Phước Lý, xã Đại Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1331,
   "Name": "Nhà thuốc Duy Thanh",
   "address": "385, xa lộ ĐỒNG NAI, KP 5B, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9643373,
   "Latitude": 106.8839023
 },
 {
   "STT": 1332,
   "Name": "Quầy thuốc ",
   "address": "14, Nguyễn Thị Giang, KP 1, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9472054,
   "Latitude": 106.815979
 },
 {
   "STT": 1333,
   "Name": "Quầy thuốc ",
   "address": "72A, Ấp Vàm, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 1334,
   "Name": "Quầy thuốc Liên Sơn",
   "address": "Tổ 2, ấp Bàu Mây, Phú Thanh, Tân Phú, Đồng Nai",
   "Longtitude": 11.2354267,
   "Latitude": 107.4676861
 },
 {
   "STT": 1335,
   "Name": "Quầy thuốc ",
   "address": "F22, KP 2, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1336,
   "Name": "Tủ thuốc trạm y tế xã Long Thọ",
   "address": "Trạm Y tế xã Long Thọ, ấp 4, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1337,
   "Name": "Tủ thuốc trạm y tế xã Phước An",
   "address": "Trạm Y tế xã Phước An, ấp Bào Bông, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 1338,
   "Name": "Tủ thuốc trạm y tế xã Vĩnh Thanh",
   "address": "Trạm Y tế xã Vĩnh Thanh, ấp Thành Công, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6796996,
   "Latitude": 106.852627
 },
 {
   "STT": 1339,
   "Name": "Quầy thuốc Hoàng Quỳnh",
   "address": "Số 39, đường Công Chính, ấp Quảng Đà, Đông Hòa, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9449229,
   "Latitude": 107.0714134
 },
 {
   "STT": 1340,
   "Name": "Tủ thuốc trạm y tế xã Phú Hữu",
   "address": "Trạm Y tế xã Phú Hữu, ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7464159,
   "Latitude": 106.7947805
 },
 {
   "STT": 1341,
   "Name": "Tủ thuốc trạm y tế xã Đại Phước",
   "address": "Trạm Y tế xã Đại Phước, ấp Thị Cầu, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1342,
   "Name": "Tủ thuốc trạm y tế xã Phú Thạnh",
   "address": "Trạm Y tế xã Phú Thạnh, ấp 2, xã Phú Thạnh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6796996,
   "Latitude": 106.852627
 },
 {
   "STT": 1343,
   "Name": "Tủ thuốc trạm y tế xã Long Tân",
   "address": "Trạm Y tế xã Long Tân, ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 1344,
   "Name": "Quầy thuốc ",
   "address": "118B/24C, KP 2, P.Tam Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9803043,
   "Latitude": 106.8548082
 },
 {
   "STT": 1345,
   "Name": "Tủ thuốc trạm y tế xã Phú Hội",
   "address": "Trạm Y tế xã Phú Hội, ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1346,
   "Name": "Tủ thuốc trạm y tế xã Hiệp Phước",
   "address": "Trạm Y tế xã Hiệp Phước, ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1347,
   "Name": "Quầy thuốc",
   "address": "Tổ 25, khu Phước Hải, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1348,
   "Name": "Quầy thuốc ",
   "address": "13/1, KP 6, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.953785,
   "Latitude": 106.8572475
 },
 {
   "STT": 1349,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "Tổ 9, KP 8, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 1350,
   "Name": "Quầy thuốc",
   "address": "Tổ 8, ấp Trần Hưng Đạo, Xuân Thạnh, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 1351,
   "Name": "Quầy thuốc Cao Hiền",
   "address": "Ấp Trung Tín, Xuân Trường, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9616033,
   "Latitude": 107.4055508
 },
 {
   "STT": 1352,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1353,
   "Name": "Nhà thuốc",
   "address": "318, tồ 23, KP 3, Trần Quốc Toản, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9368369,
   "Latitude": 106.8606139
 },
 {
   "STT": 1354,
   "Name": "Quầy thuốc Cát Minh",
   "address": "SN 25, tổ 1, ấp Ngọc Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1355,
   "Name": "Quầy thuốc Kiều Ngân",
   "address": "ấp Bến Cộ, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7437649,
   "Latitude": 106.826849
 },
 {
   "STT": 1356,
   "Name": "Quầy thuốc Ngọc Phương",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1357,
   "Name": "Quầy thuốc Trường Thịnh",
   "address": "Ấp Hòa Bình, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 1358,
   "Name": "Nhà thuốc Thanh Hiền",
   "address": "K2/66, tổ 30, Kp3, P. Bửu Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9540718,
   "Latitude": 106.8491197
 },
 {
   "STT": 1359,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Vũ Thị Huyền",
   "address": "Tổ 1, Ấp 1B, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6860681,
   "Latitude": 107.0411068
 },
 {
   "STT": 1360,
   "Name": "Quầy thuốc ",
   "address": "Khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.95
 },
 {
   "STT": 1361,
   "Name": "Tủ thuốc trạm y tế xã Lang Minh",
   "address": "Trạm Y tế xã Lang Minh, ấp Đông Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8650065,
   "Latitude": 107.3789697
 },
 {
   "STT": 1362,
   "Name": "Tủ thuốc trạm y tế xã Suối Cát",
   "address": "Trạm Y tế xã Suối Cát, ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9076957,
   "Latitude": 107.3684825
 },
 {
   "STT": 1363,
   "Name": "Quầy thuốc Thắm Tăng",
   "address": "Ấp 3, Thanh Sơn, Định Quán, Đồng Nai",
   "Longtitude": 11.3022635,
   "Latitude": 107.3061413
 },
 {
   "STT": 1364,
   "Name": "Tủ thuốc trạm y tế xã Xuân Bắc",
   "address": "Trạm Y tế Xuân Bắc, ấp 2B, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0334536,
   "Latitude": 107.3232803
 },
 {
   "STT": 1365,
   "Name": "Quầy thuốc Ngọc Tiến",
   "address": "Ấp 3A, Xuân Bắc, Xuân Lộc, Đồng Nai",
   "Longtitude": 11.0300522,
   "Latitude": 107.3804891
 },
 {
   "STT": 1366,
   "Name": "Tủ thuốc trạm y tế xã Xuân Phú",
   "address": "Trạm Y tế xã Xuân Phú, ấp Bình Tân, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9051218,
   "Latitude": 107.3285669
 },
 {
   "STT": 1367,
   "Name": "Tủ thuốc trạm y tế xã Xuân Thọ",
   "address": "Trạm Y tế xã Xuân Thọ, ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9483537,
   "Latitude": 107.3189056
 },
 {
   "STT": 1368,
   "Name": "Tủ thuốc trạm y tế TT.Gia Ray",
   "address": "Trạm Y tế TT Gia ray, khu 7, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.925556,
   "Latitude": 107.405278
 },
 {
   "STT": 1369,
   "Name": "Tủ thuốc trạm y tế xã Xuân Hưng",
   "address": "Trạm Y tế xã Xuân Hưng, ấp 5, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.850524,
   "Latitude": 107.4944508
 },
 {
   "STT": 1370,
   "Name": "Nhà thuốc Thanh Tuyền",
   "address": "Số 49, Hùng Vương, KP1, P. Xuân An, Long Khánh, Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 1371,
   "Name": "Tủ thuốc trạm y tế xã Xuân Định",
   "address": "Trạm Y tế xã Xuân Định, ấp Bảo Định, xã Xuân Định, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8992274,
   "Latitude": 107.2580007
 },
 {
   "STT": 1372,
   "Name": "Tủ thuốc trạm y tế xã Bảo Hòa",
   "address": "Trạm Y tế xã Bảo Hoà, ấp Hòa Hợp, xã Bảo Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9022012,
   "Latitude": 107.2859484
 },
 {
   "STT": 1373,
   "Name": "Tủ thuốc trạm y tế xã Xuân Tâm",
   "address": "Trạm Y tế xã Xuân Tâm, ấp 3, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.877158,
   "Latitude": 107.451822
 },
 {
   "STT": 1374,
   "Name": "Tủ thuốc trạm y tế xã Xuân Hiệp",
   "address": "Trạm Y tế xã Xuân Hiệp, ấp Tân Tiến, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9652026,
   "Latitude": 107.4321959
 },
 {
   "STT": 1375,
   "Name": "Tủ thuốc trạm y tế xã Xuân Thành",
   "address": "Trạm Y tế xã Xuân Thành, ấp Tân Hòa, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.993901,
   "Latitude": 107.4269422
 },
 {
   "STT": 1376,
   "Name": "Tủ thuốc trạm y tế xã Xuân Trường",
   "address": "Trạm Y tế xã Xuân Trường, ấp Trung Tín, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9651342,
   "Latitude": 107.41009
 },
 {
   "STT": 1377,
   "Name": "Tủ thuốc trạm y tế xã Suối Cao",
   "address": "Trạm Y tế xã Suối Cao, ấp Phượng Vỹ, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9846718,
   "Latitude": 107.3917612
 },
 {
   "STT": 1378,
   "Name": "Quầy thuốc",
   "address": "Chợ Dầu Dây, Bàu Hàm 2, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9442663,
   "Latitude": 107.1393907
 },
 {
   "STT": 1379,
   "Name": "Tủ thuốc trạm y tế xã Xuân Hòa",
   "address": "Trạm Y tế xã Xuân Hoà, ấp 2, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8770877,
   "Latitude": 107.544598
 },
 {
   "STT": 1380,
   "Name": "Quầy thuốc Bình Tâm",
   "address": "Tổ 9, ấp 5, xã La Ngà, Định Quán, Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1381,
   "Name": "Quầy thuốc Lê Phương",
   "address": "Ấp Nhất Trí, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6780556,
   "Latitude": 106.8586111
 },
 {
   "STT": 1382,
   "Name": "Quầy thuốc Thảo An",
   "address": "Tổ 3, ấp 1, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9610006,
   "Latitude": 107.0281203
 },
 {
   "STT": 1383,
   "Name": "Quầy thuốc",
   "address": "Số 295, tổ 7, khu 2, ấp 1, An Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 11.2090815,
   "Latitude": 107.359482
 },
 {
   "STT": 1384,
   "Name": "Quầy thuốc Đức Văn",
   "address": "Ấp Phú Quý 2, La Ngà, Định Quán, Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1385,
   "Name": "Quầy thuốc Thuận An",
   "address": "141B, ấp Sông Mây, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 11.0247553,
   "Latitude": 106.9622415
 },
 {
   "STT": 1386,
   "Name": "Quầy thuốc ",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1387,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu",
   "address": "E141B, Tổ 9, KP 5, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1388,
   "Name": "Quầy thuốc Nguyên Quang",
   "address": "Tổ 19, KP 5, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 1389,
   "Name": "Tủ thuốc trạm y tế xã Tam An",
   "address": "Trạm Y tế xã Tam An, ấp 4, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8096151,
   "Latitude": 106.9272695
 },
 {
   "STT": 1390,
   "Name": "Tủ thuốc trạm y tế xã An Phước",
   "address": "Trạm Y tế xã An Phước, ấp 5, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8146222,
   "Latitude": 106.941433
 },
 {
   "STT": 1391,
   "Name": "Tủ thuốc trạm y tế xã Long Đức",
   "address": "Trạm Y tế xã Long Đức, khu 13, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8121404,
   "Latitude": 106.9459659
 },
 {
   "STT": 1392,
   "Name": "Tủ thuốc trạm y tế Thị trấn Long Thành",
   "address": "Trạm Y tế TT Long Thành, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7820729,
   "Latitude": 106.952253
 },
 {
   "STT": 1393,
   "Name": "Tủ thuốc trạm y tế xã Lộc An",
   "address": "Trạm Y tế xã Lộc An, ấp Thanh Bình, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7902326,
   "Latitude": 106.9645174
 },
 {
   "STT": 1394,
   "Name": "Tủ thuốc trạm y tế xã Suối Trầu",
   "address": "Trạm Y tế xã Suối Trầu, ấp 1, xã Suối Trầu, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 1395,
   "Name": "Tủ thuốc trạm y tế  xã Cẩm Đường",
   "address": "Trạm Y tế xã Cẩm Đường, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7935607,
   "Latitude": 107.1145105
 },
 {
   "STT": 1396,
   "Name": "Tủ thuốc trạm y tế xã Long An",
   "address": "Trạm Y tế xã Long An, ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7574518,
   "Latitude": 106.9626868
 },
 {
   "STT": 1397,
   "Name": "Tủ thuốc trạm y tế xã Tân Hiệp",
   "address": "Trạm Y tế xã Tân Hiệp, ấp 6, xã Tân Hiệp, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6889213,
   "Latitude": 107.0422109
 },
 {
   "STT": 1398,
   "Name": "Tủ thuốc trạm y tế xã Phước Thái",
   "address": "Trạm Y tế xã Phước Thái, ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6787052,
   "Latitude": 107.0244171
 },
 {
   "STT": 1399,
   "Name": "Quầy thuốc",
   "address": "Số 116, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 1400,
   "Name": "Quầy thuốc ",
   "address": "Ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 1401,
   "Name": "Quầy thuốc Tâm Anh",
   "address": "790, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9396847,
   "Latitude": 106.9808018
 },
 {
   "STT": 1402,
   "Name": "Quầy thuốc Nhất Minh",
   "address": "3152, QL 1A, ấp Suối Cát 1, Suối Cát, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9082402,
   "Latitude": 107.368108
 },
 {
   "STT": 1403,
   "Name": "Quầy thuốc ",
   "address": "2A, ấp Long Khánh 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 1404,
   "Name": "Quầy thuốc",
   "address": "01/1, ấp An Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.946514,
   "Latitude": 107.0544048
 },
 {
   "STT": 1405,
   "Name": "Quầy thuốc Ánh Hường",
   "address": "Ấp Đông Hải, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.986689,
   "Latitude": 106.927042
 },
 {
   "STT": 1406,
   "Name": "Quầy thuốc",
   "address": "Số 212C/YT, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9658188,
   "Latitude": 106.9204902
 },
 {
   "STT": 1407,
   "Name": "Quầy thuốc Bệnh viện Da Liễu ",
   "address": "Bệnh viện Da liễu, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9999263,
   "Latitude": 106.852944
 },
 {
   "STT": 1408,
   "Name": "Quầy thuốc Ngọc Diễm",
   "address": "SN 3990, ấp Phú Lâm 1, xã Phú Sơn, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2833245,
   "Latitude": 107.4920592
 },
 {
   "STT": 1409,
   "Name": "Quầy thuốc Thanh Quyên",
   "address": "Ấp Suối Cát 1, Suối Cát, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9091327,
   "Latitude": 107.3633919
 },
 {
   "STT": 1410,
   "Name": "Quầy thuốc Minh Đoan",
   "address": "Âp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1411,
   "Name": "Quầy thuốc Đoàn Ngọc",
   "address": "Tổ 7, ấp 2A, Xuân Hưng, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8483335,
   "Latitude": 107.5011262
 },
 {
   "STT": 1413,
   "Name": "Nhà thuốc Doãn Như",
   "address": "14D/78, KP 12, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1414,
   "Name": "Quầy thuốc Anh Thư",
   "address": "SN 1260, tổ 9, ấp Ngọc Lâm 3, xã Phú Thanh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.240525,
   "Latitude": 107.4736016
 },
 {
   "STT": 1415,
   "Name": "Quầy thuốc",
   "address": "06A, ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9931479,
   "Latitude": 106.9788095
 },
 {
   "STT": 1416,
   "Name": "Tủ thuốc trạm y tế xã Phú Lập",
   "address": "Trạm Y tế xã Phú Lập, ấp 2, xã Phú Lập, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.4275308,
   "Latitude": 107.36123
 },
 {
   "STT": 1417,
   "Name": "Tủ thuốc trạm y tế xã Phú Điền",
   "address": "Trạm Y tế xã Phú Điền, ấp 5, xã Phú Điền, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2054822,
   "Latitude": 107.4438107
 },
 {
   "STT": 1418,
   "Name": "Quầy thuốc Liên Hưng 2",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 1419,
   "Name": "Tủ thuốc trạm y tế xã Nam Cát Tiên",
   "address": "Trạm Y tế xã Nam Cát Tiên, ấp 1, xã Nam Cát Tiên, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.42541,
   "Latitude": 107.434802
 },
 {
   "STT": 1420,
   "Name": "Quầy thuốc ",
   "address": "Tổ 14, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 1421,
   "Name": "Quầy thuốc",
   "address": "ấp Tín Nghĩa, Xuân Thiện, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 1422,
   "Name": "Tủ thuốc trạm y tế xã Xuân Mỹ",
   "address": "Trạm Y tế xã Xuân Mỹ,ấp Láng Lớn, xã Xuân Mỹ, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7771246,
   "Latitude": 107.2447371
 },
 {
   "STT": 1423,
   "Name": "Tủ thuốc trạm y tế xã Lâm San",
   "address": "Trạm Y tếT xã Lâm San, ấp 3, xã Lâm San, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7159734,
   "Latitude": 107.3318334
 },
 {
   "STT": 1424,
   "Name": "Tủ thuốc trạm y tế xã Xuân Đông",
   "address": "Trạm Y tế xã Xuân Đông, ấp Bể Bạc, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 1425,
   "Name": "Quầy thuốc Thái Dương",
   "address": "Chợ mới Trảng Bom, KP1, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9555513,
   "Latitude": 107.0149106
 },
 {
   "STT": 1426,
   "Name": "Tủ thuốc trạm y tế xã Xuân Đường",
   "address": "Trạm Y tế xã Xuân Đường, ấp 1, xã Xuân Đường, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7771246,
   "Latitude": 107.2447371
 },
 {
   "STT": 1427,
   "Name": "Tủ thuốc trạm y tế xã Thanh Sơn",
   "address": "Trạm Y tế xã Thanh Sơn, ấp Cây Dầu, xã Thanh Sơn, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3062388,
   "Latitude": 107.4794894
 },
 {
   "STT": 1428,
   "Name": "Tủ thuốc trạm y tế xã Long Giao",
   "address": "Trạm Y tế xã Long Giao, ấp Suối Râm, xã Long Giao, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8274876,
   "Latitude": 107.2282225
 },
 {
   "STT": 1429,
   "Name": "Nhà thuốc Kim Ngân Phát Đạt",
   "address": "Tổ 28, ấp Vườn Dừa. Phước Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.8951458,
   "Latitude": 106.8930128
 },
 {
   "STT": 1430,
   "Name": "Tủ thuốc trạm y tế xã Sông Ray",
   "address": "Trạm Y tế xã Sông Ray, ấp 1, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7620727,
   "Latitude": 107.3490864
 },
 {
   "STT": 1431,
   "Name": "Tủ thuốc trạm y tế xã Xuân Tây",
   "address": "Trạm Y tế xã Xuân Tây, ấp 1, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 1432,
   "Name": "Tủ thuốc trạm y tế xã Bảo Bình",
   "address": "Trạm Y tế xã Bảo Bình, ấp Tân Bảo, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8358264,
   "Latitude": 107.2935523
 },
 {
   "STT": 1433,
   "Name": "Tủ thuốc trạm y tế xã Thừa Đức",
   "address": "Trạm Y tế xã Thừa Đức, ấp 3, xã Thừa Đức, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7882504,
   "Latitude": 107.1510688
 },
 {
   "STT": 1434,
   "Name": "Tủ thuốc trạm y tế xã Nhân Nghĩa",
   "address": "Trạm Y tế xã Nhân Nghĩa, ấp 4, xã Nhân Nghĩa, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8388888,
   "Latitude": 107.237256
 },
 {
   "STT": 1435,
   "Name": "Tủ thuốc trạm y tế xã Xuân Bảo",
   "address": "Trạm Y tế xã Xuân Bảo, ấp Tân Mỹ, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 1436,
   "Name": "Tủ thuốc trạm y tế xã Xuân Quế",
   "address": "Trạm Y tế xã Xuân Quế, ấp 2, xã Xuân Quế, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8612799,
   "Latitude": 107.1602683
 },
 {
   "STT": 1437,
   "Name": "Tủ thuốc trạm y tế xã Sông Nhạn",
   "address": "Trạm Y tế xã Sông Nhạn, ấp 2, xã Sông Nhạn, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8318544,
   "Latitude": 107.1284188
 },
 {
   "STT": 1438,
   "Name": "Tủ thuốc trạm y tế xã Phú Xuân",
   "address": "Trạm Y tế xã Phú Xuân, ấp Ngọc Lâm 1, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1439,
   "Name": "Quầy thuốc Phúc Ân",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1440,
   "Name": "Quầy thuốc Tuyết Ngân",
   "address": "Ấp Trảng Táo, Xuân Thành, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9943485,
   "Latitude": 107.4264005
 },
 {
   "STT": 1441,
   "Name": "Quầy thuốc Anh Dũng",
   "address": "Chợ ấp 2, ấp 2, Xuân Hưng, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8465186,
   "Latitude": 107.5074615
 },
 {
   "STT": 1442,
   "Name": "Quầy thuốc Nhật Hiền",
   "address": "Ấp Trung Hưng, Xuân Trường, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9700952,
   "Latitude": 107.4328135
 },
 {
   "STT": 1443,
   "Name": "Quầy thuốc Phượng Uyên",
   "address": "Khu 3, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.926233,
   "Latitude": 107.403939
 },
 {
   "STT": 1444,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Ấp Bảo Định, xã Xuân Định, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8989588,
   "Latitude": 107.2576707
 },
 {
   "STT": 1445,
   "Name": "Tủ thuốc trạm y tế xã Phú Thanh",
   "address": "Trạm Y tế xã Phú Thanh, ấp Thọ Lâm 2, xã Phú Thanh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2787503,
   "Latitude": 107.4797004
 },
 {
   "STT": 1446,
   "Name": "Nhà thuốc Hoàng Sơn Nguyên",
   "address": "158H, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1447,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "Số 26/6, CMT8, KP3, P. Quang Vinh, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9391324,
   "Latitude": 106.8245985
 },
 {
   "STT": 1448,
   "Name": "Tủ thuốc trạm y tế xã Núi Tượng",
   "address": "Trạm Y tế xã Núi Tượng, ấp 4, xã Núi Tượng, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3743935,
   "Latitude": 107.4262813
 },
 {
   "STT": 1449,
   "Name": "Quầy thuốc",
   "address": "Ấp Bể Bạc, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 1450,
   "Name": "Quầy thuốc Anh Khoa",
   "address": "Ấp Đất Mới, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7207699,
   "Latitude": 106.9982685
 },
 {
   "STT": 1451,
   "Name": "Quầy thuốc ",
   "address": "Ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9541667,
   "Latitude": 106.9686111
 },
 {
   "STT": 1452,
   "Name": "Quầy thuốc Thiên Thủy",
   "address": "549, Khóm 1, Khu 7, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 1453,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Bắc Hợp, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1454,
   "Name": "Quầy thuốc Ngọc Thoa",
   "address": "Tổ 14, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0159709,
   "Latitude": 106.8355372
 },
 {
   "STT": 1455,
   "Name": "Quầy thuốc",
   "address": "Tổ 2, ấp Cẩm Sơn, xã Xuân Mỹ, huyện.Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 1456,
   "Name": "Quầy thuốc Minh Hưng",
   "address": "277, Bùi Trọng Nghĩa, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9817084,
   "Latitude": 106.8567149
 },
 {
   "STT": 1457,
   "Name": "Quầy thuốc Mai Dung",
   "address": "40/34, KP 5, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9574128,
   "Latitude": 106.8426871
 },
 {
   "STT": 1458,
   "Name": "Quầy thuốc Hoàng Vương",
   "address": "C-75, tổ 9, ấp Tín Nghĩa,  xã Xuân Thiện, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 1459,
   "Name": "Quầy thuốc Ngân Tâm",
   "address": "Số 33, Hà Huy Giáp, KP 2, P.Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93958,
   "Latitude": 106.8249058
 },
 {
   "STT": 1460,
   "Name": "Quầy thuốc ",
   "address": "SN 275, ấp Phù Hợp B, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 1461,
   "Name": "Nhà thuốc Hùng Vương",
   "address": "Số 24, tổ 15, KP 1, P.Xuân Thanh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.933285,
   "Latitude": 107.256296
 },
 {
   "STT": 1462,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "14, đường Hùng Vương, KP 3, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9560455,
   "Latitude": 107.0061223
 },
 {
   "STT": 1463,
   "Name": "Nhà thuốc Ngọc Bích",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1464,
   "Name": "Quầy thuốc Đoàn Hồng",
   "address": "232, ấp Hưng Hiệp, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 1465,
   "Name": "Quầy thuốc Hoàng Thục",
   "address": "Ấp 2, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8274346,
   "Latitude": 107.3640375
 },
 {
   "STT": 1466,
   "Name": "Nhà thuốc Bảo Hương",
   "address": "7/F7, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1467,
   "Name": "Quầy thuốc Nam Dân",
   "address": "12/1, KP 6, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 1468,
   "Name": "Nhà thuốc bệnh viện đa khoa khu vực Long Thành",
   "address": "Bệnh viện đa khoa khu vực Long Thành, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7782801,
   "Latitude": 106.9447146
 },
 {
   "STT": 1469,
   "Name": "Quầy thuốc Như Thủy",
   "address": "1039, ấp 1, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 1470,
   "Name": "Quầy thuốc Hoàng Oanh",
   "address": "Ấp Độc Lập, xã Giang Điền, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9258333,
   "Latitude": 106.9816667
 },
 {
   "STT": 1471,
   "Name": "Quầy thuốc Ngọc Nhung",
   "address": "Ấp Hoàn Quân, xã Long Giao, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8263722,
   "Latitude": 107.2319236
 },
 {
   "STT": 1472,
   "Name": "Quầy thuốc Hiếu Phước",
   "address": "Ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1473,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Ấp Lợi Hà, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.958597,
   "Latitude": 107.0457563
 },
 {
   "STT": 1474,
   "Name": "Nhà thuốc Phòng khám đa khoa -Công ty TNHH BVĐK Nhi Sài Gòn chi nhánh Đồng Nai",
   "address": "185, Phan Trung, KP 2, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9603645,
   "Latitude": 106.848314
 },
 {
   "STT": 1475,
   "Name": "Quầy thuốc Thảo Nguyên",
   "address": "Số 141B/YT, ấp Thanh hoá, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9658188,
   "Latitude": 106.9204902
 },
 {
   "STT": 1476,
   "Name": "Nhà thuốc Minh Hoàng",
   "address": "Tổ 13, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 1478,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "8/1B, Tây Kim, Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304592,
   "Latitude": 107.177428
 },
 {
   "STT": 1479,
   "Name": "Quầy thuốc Trí",
   "address": "Tổ 9, ấp 9/4, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9076183,
   "Latitude": 107.149908
 },
 {
   "STT": 1480,
   "Name": "Quầy thuốc Hồng Ân",
   "address": "Số 2368, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 1481,
   "Name": "Nhà thuốc Đức Hạnh",
   "address": "122, Cách Mạng Tháng 8, KP 3, P.Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9515753,
   "Latitude": 106.8119895
 },
 {
   "STT": 1482,
   "Name": "Quầy thuốc Việt Hương",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1483,
   "Name": "Quầy thuốc ",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1484,
   "Name": "Nhà thuốc Dương Linh",
   "address": "71/10, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1485,
   "Name": "Quầy thuốc Liên Sơn",
   "address": "10/2 Khu 2, ấp Tân Việt, xã Bàu Hàm, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9806475,
   "Latitude": 107.1070402
 },
 {
   "STT": 1486,
   "Name": "Quầy thuốc Tuấn Mân",
   "address": "Ấp 3, xã Phú Lập, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 1487,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Ấp Nam Hà, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 1488,
   "Name": "Quầy thuốc Mỹ Huyền",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 1489,
   "Name": "Quầy thuốc An Nguyên 1",
   "address": "Tổ 5, khu Phước Thuận, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 1490,
   "Name": "Quầy thuốc Phương Liên",
   "address": "36/4, khu 4, ấp 2, xã Xuân Đường, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8274346,
   "Latitude": 107.3640375
 },
 {
   "STT": 1491,
   "Name": "Quầy thuốc Thanh Bình",
   "address": "Ấp Phú Quý 2, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1492,
   "Name": "Nhà thuốc",
   "address": "Số 51C, đường Hùng Vương, P.Xuân An, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9377408,
   "Latitude": 107.2399125
 },
 {
   "STT": 1493,
   "Name": "Quầy thuốc Hiền Nga",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 1494,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "173/4, ấp 9/4, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9423716,
   "Latitude": 107.1428829
 },
 {
   "STT": 1495,
   "Name": "Quầy thuốc ",
   "address": "1/29, tổ 39C, KP 4, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9413783,
   "Latitude": 106.8621359
 },
 {
   "STT": 1496,
   "Name": "Quầy thuốc Mỹ Dung",
   "address": "05/1, ấp Nhân Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9563232,
   "Latitude": 107.0503736
 },
 {
   "STT": 1497,
   "Name": "Quầy thuốc Lâm San",
   "address": "Ấp 5, xã Lâm San, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7159734,
   "Latitude": 107.3318334
 },
 {
   "STT": 1498,
   "Name": "Quầy thuốc An Bình",
   "address": "SN 10, KDC 1, ấp Chợ, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252675
 },
 {
   "STT": 1499,
   "Name": "Quầy thuốc Huyền Trâm",
   "address": "74, đường Bình Minh-Giang Điền, ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9451841,
   "Latitude": 106.9804832
 },
 {
   "STT": 1500,
   "Name": "Quầy thuốc Phúc Tâm",
   "address": "73/2, ấp An Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1501,
   "Name": "Quầy thuốc Bảo Kiên",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1502,
   "Name": "Quầy thuốc ",
   "address": "Số 08, khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 1503,
   "Name": "Quầy thuốc ",
   "address": "M1/20, Nguyễn Ái Quốc, KP 5, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9645059,
   "Latitude": 106.8447821
 },
 {
   "STT": 1504,
   "Name": "Quầy thuốc ",
   "address": "10B/2, tổ 22, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1505,
   "Name": "Quầy thuốc Ngọc Thủy",
   "address": "SN 07, ấp Hiệp Thương, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1506,
   "Name": "Nhà thuốc Nữ Ánh",
   "address": "496, Đồng Khởi, KP 3, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9598049,
   "Latitude": 106.8566494
 },
 {
   "STT": 1507,
   "Name": "Quầy thuốc Phòng khám đa khoa Long Bình",
   "address": "20/38, tổ 7, KP 3, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 1508,
   "Name": "Quầy thuốc ",
   "address": "Tổ 32, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1509,
   "Name": "Quầy thuốc Hồng Thu",
   "address": "Ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1510,
   "Name": "Quầy thuốc ",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1511,
   "Name": "Quầy thuốc Sự",
   "address": "4/3, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1512,
   "Name": "Quầy thuốc Lục Đình Châu",
   "address": "40, ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6808276,
   "Latitude": 107.0254347
 },
 {
   "STT": 1513,
   "Name": "Quầy thuốc Thành Long",
   "address": "Ấp Phú Tân, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1227454,
   "Latitude": 107.1602683
 },
 {
   "STT": 1514,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "278A/6, ấp 9/4, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9423716,
   "Latitude": 107.1428829
 },
 {
   "STT": 1515,
   "Name": "Quầy thuốc ",
   "address": "22/42, KP 7, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 1516,
   "Name": "Quầy thuốc Linh Giang",
   "address": "2317, ấp Quảng Lộc, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1517,
   "Name": "Nhà thuốc Khánh Ngọc An",
   "address": "Số 01, tổ 1, KP12, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.927241,
   "Latitude": 106.8571987
 },
 {
   "STT": 1518,
   "Name": "Nhà thuốc Thái Hiệp",
   "address": "75, KP 8, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9863086,
   "Latitude": 106.8463406
 },
 {
   "STT": 1519,
   "Name": "Quầy thuốc Phú Thịnh 3",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1520,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bến Cộ, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7437649,
   "Latitude": 106.826849
 },
 {
   "STT": 1521,
   "Name": "Nhà thuốc Minh Châu",
   "address": "Số 5A, đường Hoàng Diệu, KP 1, P.Xuân Thanh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.935119,
   "Latitude": 107.2562036
 },
 {
   "STT": 1522,
   "Name": "Quầy thuốc ",
   "address": "Ấp 4, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.880297,
   "Latitude": 106.993089
 },
 {
   "STT": 1523,
   "Name": "Quầy thuốc Ngọc Duyên",
   "address": "Ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1524,
   "Name": "Quầy thuốc Bảo Anh",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1525,
   "Name": "Quầy thuốc Ngọc Nga",
   "address": "1965, ấp 2, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8274346,
   "Latitude": 107.3640375
 },
 {
   "STT": 1526,
   "Name": "Quầy thuốc Thiên Thanh",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1528,
   "Name": "Nhà thuốc Đỗ Lan",
   "address": "8B, Quốc lộ 1A, ấp Núi Tung, xã Suối Tre, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9411472,
   "Latitude": 107.2356523
 },
 {
   "STT": 1529,
   "Name": "Quầy thuốc Ngọc Vân",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1530,
   "Name": "Quầy thuốc Thanh Thiện",
   "address": "Ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1531,
   "Name": "Quầy thuốc Ánh Dương",
   "address": "Ấp Bảo Vệ, xã Giang Điền, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9269822,
   "Latitude": 106.9846975
 },
 {
   "STT": 1532,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "6/1A, Đông Kim, Gia Kiệm, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0327201,
   "Latitude": 107.1803206
 },
 {
   "STT": 1533,
   "Name": "Nhà thuốc Nguyễn Huỳnh",
   "address": "16/4, tổ 10, KP 6, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 1534,
   "Name": "Nhà thuốc Y Lan",
   "address": "K4/144, tổ 93, KP 5, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1535,
   "Name": "Quầy thuốc Anh Tuấn",
   "address": "Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8505029,
   "Latitude": 107.042198
 },
 {
   "STT": 1536,
   "Name": "Nhà thuốc Nguyên Hùng",
   "address": "100/H, KP 3, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9708772,
   "Latitude": 106.9093
 },
 {
   "STT": 1538,
   "Name": "Quầy thuốc Việt Ninh",
   "address": "SN 980, tổ 9, ấp 7, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 1539,
   "Name": "Quầy thuốc ",
   "address": "Tổ 9, ấp 1, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 1540,
   "Name": "Quầy thuốc ",
   "address": "Ấp Tân Lập 2, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1541,
   "Name": "Quầy thuốc Nhân Đức",
   "address": "5/5, ấp Thuận An, xã Sông Thao, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9665393,
   "Latitude": 107.0861405
 },
 {
   "STT": 1542,
   "Name": "Quầy thuốc Tuyết Hoa",
   "address": "Tổ 3, KP Hiệp Đồng, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1543,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Bình Lộc, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9835555,
   "Latitude": 107.2359278
 },
 {
   "STT": 1544,
   "Name": "Quầy thuốc Hồng Vân",
   "address": "Số 16, KP 114, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1545,
   "Name": "Quầy thuốc ",
   "address": "Tổ 2, ấp Hưng Nghĩa, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 1546,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Ấp Quảng Hòa, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9458223,
   "Latitude": 106.9904664
 },
 {
   "STT": 1547,
   "Name": "Quầy thuốc Thiên Phước",
   "address": "Ấp Thành Công, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 1548,
   "Name": "Nhà thuốc Nhã Ca",
   "address": "2/125A, KP 5, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1549,
   "Name": "Nhà thuốc Hạnh Tâm",
   "address": "41/21, KP 9, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 1550,
   "Name": "Quầy thuốc Hiệp Linh",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1551,
   "Name": "Quầy thuốc ",
   "address": "2561, ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9091327,
   "Latitude": 107.3633919
 },
 {
   "STT": 1552,
   "Name": "Quầy thuốc Phúc An II",
   "address": "Tổ 8, ấp 6, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 1553,
   "Name": "Quầy thuốc ",
   "address": "29/1, ấp Bàu Cá, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 1554,
   "Name": "Nhà thuốc Nguyễn Hoàng Yến",
   "address": "Chợ Tân Hiệp, kios 20 KLC, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 1555,
   "Name": "Nhà thuốc Mai Trâm",
   "address": "Số 08 (thửa đất số 21, tờ bản đồ 45), Nguyễn Công Trứ, P.Xuân An, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9307276,
   "Latitude": 107.2444641
 },
 {
   "STT": 1556,
   "Name": "Công ty TNHH IN DI CO Chi nhánh Đồng Nai",
   "address": "Số 27A9, khu dân cư, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9355289,
   "Latitude": 106.8536069
 },
 {
   "STT": 1557,
   "Name": "Nhà thuốc Nguyễn Vinh",
   "address": "A8, KP 3, Trần Quốc Toản, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9388818,
   "Latitude": 106.8576464
 },
 {
   "STT": 1558,
   "Name": "Nhà thuốc Thu",
   "address": "02, Nguyễn Thái Học, P.Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.7664668,
   "Latitude": 106.6963897
 },
 {
   "STT": 1559,
   "Name": "Quầy thuốc Chúc Quyên",
   "address": "Số nhà 2188, ấp Thanh Thọ 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1560,
   "Name": "Nhà thuốc Uyên Linh",
   "address": "Tổ 11, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 1562,
   "Name": "Quầy thuốc Thảo Ngọc",
   "address": "Số nhà 1039, tổ 9, khu 12, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 1563,
   "Name": "Quầy thuốc Hoàng Quyên",
   "address": "Tổ 4, ấp Phú Hợp A, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 1564,
   "Name": "Quầy thuốc Đức Thịnh",
   "address": "599, ấp Thanh Bình, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 1565,
   "Name": "Nhà thuốc Ngọc Tuyền",
   "address": "1C, K 2, chợ Tân Phong, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9574766,
   "Latitude": 106.8294132
 },
 {
   "STT": 1566,
   "Name": "Nhà thuốc Sỹ Mỹ",
   "address": "116/4, KP 10, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1567,
   "Name": "Quầy thuốc ",
   "address": "827B, ấp 2, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1568,
   "Name": "Quầy thuốc Thanh Thanh",
   "address": "1981A, ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1569,
   "Name": "Quầy thuốc Nhân Văn",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1570,
   "Name": "Quầy thuốc Võ Phạm",
   "address": "Ấp 4, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0536006,
   "Latitude": 107.0389271
 },
 {
   "STT": 1571,
   "Name": "Quầy thuốc Phước Lộc",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1572,
   "Name": "Quầy thuốc Khang Nguyễn",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1573,
   "Name": "Quầy thuốc ",
   "address": "Số 140/2, KP 4, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9533828,
   "Latitude": 106.8532198
 },
 {
   "STT": 1574,
   "Name": "Quầy thuốc ",
   "address": "Khu tái định cư ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7577014,
   "Latitude": 106.9883589
 },
 {
   "STT": 1575,
   "Name": "Quầy thuốc Lan Phương",
   "address": "1236, QL1A, Ấp 2A, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.84882,
   "Latitude": 107.5000859
 },
 {
   "STT": 1577,
   "Name": "Quầy thuốc Thân Lan",
   "address": "713, đường Sông Thao, xã Bàu Hàm, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9783535,
   "Latitude": 107.108044
 },
 {
   "STT": 1578,
   "Name": "Nhà thuốc Y Tâm",
   "address": "Tổ 13, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.972058,
   "Latitude": 106.9097623
 },
 {
   "STT": 1579,
   "Name": "Quầy thuốc Vĩnh Long",
   "address": "162/1A, Dốc Mơ, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0595612,
   "Latitude": 107.1675217
 },
 {
   "STT": 1580,
   "Name": "Quầy thuốc ",
   "address": "239, đường Duy Tân, tổ 8A, ấp Ruộng Hời, xã Bảo Vinh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9387906,
   "Latitude": 107.2642281
 },
 {
   "STT": 1581,
   "Name": "Quầy thuốc Lưu An",
   "address": "Ấp Phú Mỹ 1, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1582,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6729097,
   "Latitude": 106.7944492
 },
 {
   "STT": 1583,
   "Name": "Quầy thuốc ",
   "address": "Số 3, Nguyễn Thành Đồng, KP 2, P.Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93966,
   "Latitude": 106.8252989
 },
 {
   "STT": 1584,
   "Name": "Quầy thuốc Lan Ngọc Minh",
   "address": "139, Hương lộ 21, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8656873,
   "Latitude": 106.9388118
 },
 {
   "STT": 1585,
   "Name": "Quầy thuốc Sen Vàng",
   "address": "Tổ 05, ấp 6, xã Vĩnh Tân, huyệnVĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 1586,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1587,
   "Name": "Nhà thuốc Phúc An Khang",
   "address": "A4/445D, KP 4, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9117711,
   "Latitude": 106.8272458
 },
 {
   "STT": 1588,
   "Name": "Nhà thuốc",
   "address": "Số 41C, Nguyễn Trãi, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9468742,
   "Latitude": 106.8166695
 },
 {
   "STT": 1589,
   "Name": "Quầy thuốc ",
   "address": "Ấp Hòa Hợp, xã Bảo Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.902512,
   "Latitude": 107.285589
 },
 {
   "STT": 1590,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7515541,
   "Latitude": 107.3514669
 },
 {
   "STT": 1591,
   "Name": "Nhà thuốc Hiếu Thảo",
   "address": "Số 24, tổ 1, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1592,
   "Name": "Quầy thuốc Liên Châu",
   "address": "90/1, KP 3, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1593,
   "Name": "Quầy thuốc ",
   "address": "24/23, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.994678,
   "Latitude": 106.86104
 },
 {
   "STT": 1594,
   "Name": "Quầy thuốc Thuốc Tiên",
   "address": "197, tổ 5, khu 2, ấp 2, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 1595,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "923, ấp Thống Nhất, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1086911,
   "Latitude": 107.1641629
 },
 {
   "STT": 1596,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 1597,
   "Name": "Nhà thuốc Diệu Trang",
   "address": "1011, Phạm Văn Thuận,  KP 3, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9553183,
   "Latitude": 106.8506301
 },
 {
   "STT": 1598,
   "Name": "Nhà thuốc Ngọc Ánh",
   "address": "288A1, đường 30/4, P.Trung Dũng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9479386,
   "Latitude": 106.8193602
 },
 {
   "STT": 1599,
   "Name": "Quầy thuốc Tuấn Kim Yến",
   "address": "23, tổ 5, ấp Long Khánh 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8315859,
   "Latitude": 106.8853178
 },
 {
   "STT": 1600,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Ấp Bình Thạch, xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 10.9816185,
   "Latitude": 106.798952
 },
 {
   "STT": 1601,
   "Name": "Cơ sở sản xuất thuốc từ dược liệu Hòa Thuận Đường",
   "address": "28, K 2, ấp Tân Phong, xã Xuân Tân, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9239527,
   "Latitude": 107.2195
 },
 {
   "STT": 1602,
   "Name": "Quầy thuốc Phan Dung",
   "address": "Ấp Quảng Hòa, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9458223,
   "Latitude": 106.9904664
 },
 {
   "STT": 1603,
   "Name": "Quầy thuốc Huy Tiền",
   "address": "Số 70/3, ấp An Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1604,
   "Name": "Quầy thuốc Phúc An",
   "address": "Số 17, đường Nguyễn Hải, ấp 1, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.794799,
   "Latitude": 106.9425166
 },
 {
   "STT": 1605,
   "Name": "Nhà thuốc Bệnh viện đa khoa huyện Tân Phú",
   "address": "Bệnh viện đa khoa huyện Tân Phú, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2736746,
   "Latitude": 107.4406568
 },
 {
   "STT": 1606,
   "Name": "Nhà thuốc Mạnh Khang",
   "address": "KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1607,
   "Name": "Nhà thuốc Bệnh viện Tâm thần Trung Ương 2",
   "address": "Đường Nguyễn Ái Quốc, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.963171,
   "Latitude": 106.8381254
 },
 {
   "STT": 1608,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Ấp 1, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 1609,
   "Name": "Quầy thuốc Diễm Châu",
   "address": "Ấp 4, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.880297,
   "Latitude": 106.993089
 },
 {
   "STT": 1610,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "234, ấp Ngũ Phúc, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 1611,
   "Name": "Quầy thuốc Danh Sỹ",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1612,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1613,
   "Name": "Quầy thuốc Như Ý",
   "address": "40/1, tổ 1, ấp Lập Thành, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9423716,
   "Latitude": 107.1428829
 },
 {
   "STT": 1614,
   "Name": "Quầy thuốc An An",
   "address": "Kiot số 7, tổ 3, ấp Lập Thành, xã Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9423716,
   "Latitude": 107.1428829
 },
 {
   "STT": 1615,
   "Name": "Quầy thuốc Minh Tuyền",
   "address": "Ấp Bàu Bông, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 1616,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1617,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 1618,
   "Name": "Nhà thuốc Ngọc Nguyên",
   "address": "101/4, KP 10, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 1619,
   "Name": "Nhà thuốc Lan Ngọc Linh",
   "address": "177/3, KP 4, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 1620,
   "Name": "Nhà thuốc Thư Vinh",
   "address": "Tổ 12, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 1621,
   "Name": "Quầy thuốc Thu Cúc",
   "address": "Ấp Bà Trường, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6468256,
   "Latitude": 106.9363446
 },
 {
   "STT": 1622,
   "Name": "Quầy thuốc Hạnh Nhân",
   "address": "Chợ mới Trảng Bom, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9555513,
   "Latitude": 107.0149106
 },
 {
   "STT": 1623,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1624,
   "Name": "Quầy thuốc Tuấn Hà",
   "address": "Số 224, ấp 6, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7915635,
   "Latitude": 107.0231599
 },
 {
   "STT": 1625,
   "Name": "Quầy thuốc Ngọc Thủy",
   "address": "Ấp Thanh Minh, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 1626,
   "Name": "Quầy thuốc An Phát",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1627,
   "Name": "Nhà thuốc Phòng khám đa khoa Tân Long",
   "address": "Số 06, 23,24/F6, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916484,
   "Latitude": 106.850222
 },
 {
   "STT": 1628,
   "Name": "Nhà thuốc Quý Đức",
   "address": "126A, Hoàng Bá Bích, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.941235,
   "Latitude": 106.8724555
 },
 {
   "STT": 1629,
   "Name": "Quầy thuốc Thúy Vy",
   "address": "Ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1630,
   "Name": "Quầy thuốc Hiếu Đức",
   "address": "Tổ 11, ấp 6, xã Tân Hiệp, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 1631,
   "Name": "Quầy thuốc Khánh Phương",
   "address": "Tổ 15, khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 1632,
   "Name": "Quầy thuốc ",
   "address": "2010, ấp Thống Nhất, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 1633,
   "Name": "Quầy thuốc Đặng Thông",
   "address": "648, đường Hương Lộ 10, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.790224,
   "Latitude": 107.0929474
 },
 {
   "STT": 1634,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "C1/026, Bắc Sơn, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 1635,
   "Name": "Quầy thuốc Ngọc Hoa 2",
   "address": "Tổ 14, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0159709,
   "Latitude": 106.8355372
 },
 {
   "STT": 1636,
   "Name": "Nhà thuốc Vy Ánh Phương",
   "address": "Số 280, ấp Long Đức, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.861517,
   "Latitude": 106.9611
 },
 {
   "STT": 1637,
   "Name": "Quầy thuốc Ngọc An",
   "address": "E595A, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891472,
   "Latitude": 106.8502969
 },
 {
   "STT": 1638,
   "Name": "Quầy thuốc Đức Nguyên",
   "address": "Ấp Trà Cổ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9541667,
   "Latitude": 106.9686111
 },
 {
   "STT": 1639,
   "Name": "Quầy thuốc Vĩnh Khang",
   "address": "SN 52A, phố 3, ấp 2, xã Phú Lợi, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2236734,
   "Latitude": 107.3907968
 },
 {
   "STT": 1640,
   "Name": "Quầy thuốc Huyền Tâm",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 1641,
   "Name": "Nhà thuốc Long Châu",
   "address": "385C, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520452,
   "Latitude": 106.8772377
 },
 {
   "STT": 1642,
   "Name": "Quầy thuốc Hà Tuyết",
   "address": "Số 77, Đường Hùng Vương, tổ 8, KP 1, TT.Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 1643,
   "Name": "Quầy thuốc ",
   "address": "Kiốt số 5, chợ Xuân Thanh, P.Xuân Thanh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9352283,
   "Latitude": 107.2528328
 },
 {
   "STT": 1644,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Kim Long Phụng",
   "address": "55/4, KP 1, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1645,
   "Name": "Quầy thuốc Bảo Phương Anh",
   "address": "68D, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720719,
   "Latitude": 106.9097344
 },
 {
   "STT": 1646,
   "Name": "Quầy thuốc Xuân Thọ",
   "address": "48, ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9483537,
   "Latitude": 107.3189056
 },
 {
   "STT": 1647,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "Số 35, ấp 6, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8355792,
   "Latitude": 106.9417632
 },
 {
   "STT": 1648,
   "Name": "Nhà thuốc Nguyễn Thành",
   "address": "Số 6A, Khổng Tử, P.Xuân Trung, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.935851,
   "Latitude": 107.2423334
 },
 {
   "STT": 1649,
   "Name": "Quầy thuốc Thanh Dũng",
   "address": "370B, ấp Phú Tâm, xã Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1650,
   "Name": "Quầy thuốc Phúc Hậu",
   "address": "113, ấp Quảng Hòa, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9449414,
   "Latitude": 106.9943646
 },
 {
   "STT": 1651,
   "Name": "Nhà thuốc Vân Thu",
   "address": "99/5, Phạm Văn Thuận, KP 7, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9533828,
   "Latitude": 106.8532198
 },
 {
   "STT": 1652,
   "Name": "Nhà thuốc Tuệ Nguyên",
   "address": "540/51, KP 5, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1653,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Khu 8, ấp Cẩm Tân, xã Xuân Tân, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8918739,
   "Latitude": 107.2263931
 },
 {
   "STT": 1654,
   "Name": "Quầy thuốc Hồng Hải",
   "address": "1398, tổ 40, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1655,
   "Name": "Quầy thuốc Túy Oanh",
   "address": "Ấp 3, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8255222,
   "Latitude": 107.5598542
 },
 {
   "STT": 1656,
   "Name": "Nhà thuốc Huỳnh Thảo",
   "address": "38/A1, tổ 19, KP 1, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1657,
   "Name": "Quầy thuốc Bùi Kim Phượng",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 1658,
   "Name": "Quầy thuốc phòng khám đa khoa Duy Khang - chi nhánh Long Thành",
   "address": "A 1-9, A1-10, khu Chợ Mới Long Thành, thị trấn Long Thành, huyện Long thành, tỉnh Đồng Nai",
   "Longtitude": 10.7770943,
   "Latitude": 106.956174
 },
 {
   "STT": 1659,
   "Name": "Nhà thuốc Bình Triệu",
   "address": "9, Nguyễn Văn Tiên, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9788474,
   "Latitude": 106.8462474
 },
 {
   "STT": 1660,
   "Name": "Nhà thuốc Bệnh viện đa khoa Dầu Giây",
   "address": "Xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9331253,
   "Latitude": 107.1369389
 },
 {
   "STT": 1661,
   "Name": "Quầy thuốc Sơn Hà",
   "address": "Ấp Sơn Hà, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 1662,
   "Name": "Quầy thuốc ",
   "address": "Ấp Tân Phong, xã Xuân Tân, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8903618,
   "Latitude": 107.2237033
 },
 {
   "STT": 1663,
   "Name": "Quầy thuốc ",
   "address": "Ấp Hàng Gòn, xã Hàng Gòn, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8803302,
   "Latitude": 107.2075388
 },
 {
   "STT": 1664,
   "Name": "Nhà thuốc Ngọc Bá Việt",
   "address": "Số 137, CMT8, KP3, P.Hòa Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9501875,
   "Latitude": 106.8125367
 },
 {
   "STT": 1665,
   "Name": "Nhà thuốc Bệnh viện Da Liễu",
   "address": "Đường Đồng Khởi, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9791226,
   "Latitude": 106.8483536
 },
 {
   "STT": 1666,
   "Name": "Quầy thuốc Bảo Liêm",
   "address": "Ấp 2, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7399492,
   "Latitude": 106.9447617
 },
 {
   "STT": 1667,
   "Name": "Quầy thuốc ",
   "address": "12, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9274132,
   "Latitude": 106.8349546
 },
 {
   "STT": 1668,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Số 253, đường Trần Phú, P.Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9263927,
   "Latitude": 107.2491024
 },
 {
   "STT": 1669,
   "Name": "Quầy thuốc Linh Tâm",
   "address": "140C, ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9757101,
   "Latitude": 106.9589897
 },
 {
   "STT": 1670,
   "Name": "Công ty TNHH Một thành viên Dược phẩm Tiến Dũng",
   "address": "1334/4, KP 3, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9614615,
   "Latitude": 106.8435458
 },
 {
   "STT": 1671,
   "Name": "Quầy thuốc Ngọc Minh Huế",
   "address": "187B, tổ 7, ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 11.0247553,
   "Latitude": 106.9622415
 },
 {
   "STT": 1672,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "361, ấp Thọ Bình, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9456025,
   "Latitude": 107.3501571
 },
 {
   "STT": 1673,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "ấp Lộc Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1674,
   "Name": "Quầy thuốc ",
   "address": "52/2B, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 1675,
   "Name": "Quầy thuốc Long Châu",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1676,
   "Name": "Quầy thuốc Thùy Trâm",
   "address": "449, ấp Thọ Lộc, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9591637,
   "Latitude": 107.3494043
 },
 {
   "STT": 1677,
   "Name": "Quầy thuốc Âu Thiên Phú",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 1678,
   "Name": "Quầy thuốc Minh Minh Châu",
   "address": "102, Chung cư A4, Nguyễn Ái Quốc, KP 4, P.Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.955549,
   "Latitude": 106.814575
 },
 {
   "STT": 1679,
   "Name": "Quầy thuốc Minh Khôi Phát",
   "address": "Ấp 3, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8807179,
   "Latitude": 106.9940508
 },
 {
   "STT": 1680,
   "Name": "Quầy thuốc ",
   "address": "302, tổ 4, khu 12, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 1681,
   "Name": "Quầy thuốc ",
   "address": "Ấp Xóm Gốc, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7647864,
   "Latitude": 106.9621677
 },
 {
   "STT": 1682,
   "Name": "Nhà thuốc Bệnh viện đa khoa huyện Xuân Lộc",
   "address": "Bệnh viện đa khoa huyện Xuân Lộc, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9088881,
   "Latitude": 107.3688805
 },
 {
   "STT": 1683,
   "Name": "Nhà thuốc Cường My",
   "address": "347, tổ 22, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891648,
   "Latitude": 106.8502253
 },
 {
   "STT": 1684,
   "Name": "Nhà thuốc Thùy Vân",
   "address": "23B, Bùi Trọng Nghĩa, tổ 9, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 1685,
   "Name": "Quầy thuốc Khánh Hưng",
   "address": "Ấp Phú Mỹ 2, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1686,
   "Name": "Quầy thuốc Kỳ Duyên",
   "address": "Tổ 1, ấp An Lâm, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7587903,
   "Latitude": 106.9903637
 },
 {
   "STT": 1687,
   "Name": "Quầy thuốc Liên Hiệp",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1688,
   "Name": "Nhà thuốc Thiền Tâm (trực thuôc Công ty TNHH MTV Thiền Tâm)",
   "address": "Số 80, đường Võ Nguyên Giáp, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9123636,
   "Latitude": 106.9386498
 },
 {
   "STT": 1689,
   "Name": "Quầy thuốc Khánh Đoan",
   "address": "5/1, KP 4, P.Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1690,
   "Name": "Quầy thuốc Thanh Trà",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1691,
   "Name": "Quầy thuốc Tâm Nhân Đức",
   "address": "12/N Phúc Nhạc, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0351046,
   "Latitude": 107.1735973
 },
 {
   "STT": 1692,
   "Name": "Quầy thuốc ",
   "address": "783 (số cũ 222A), Nguyễn Ái Quốc, KP 1, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9707049,
   "Latitude": 106.8672837
 },
 {
   "STT": 1693,
   "Name": "Quầy thuốc ",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1694,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "228, ấp 1, xã Phú Hoà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.200442,
   "Latitude": 107.4144526
 },
 {
   "STT": 1695,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu ",
   "address": "103E, tổ 6, KP 3, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656045,
   "Latitude": 106.866728
 },
 {
   "STT": 1696,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Sơn Hải Mạnh",
   "address": "Số 12A5, khu GĐCB-QĐ4, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9634052,
   "Latitude": 106.8379609
 },
 {
   "STT": 1697,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Nghĩa Quân",
   "address": "268/HF, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9012444,
   "Latitude": 106.8526661
 },
 {
   "STT": 1698,
   "Name": "Quầy thuốc ",
   "address": "Ấp 6, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9798138,
   "Latitude": 107.0250727
 },
 {
   "STT": 1699,
   "Name": "Quầy thuốc ",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 1700,
   "Name": "Quầy thuốc Ngọc Diễm",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 1701,
   "Name": "Quầy thuốc Hoàng Đức",
   "address": "Số 74, Quốc lộ 1A, Ấp 4, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8172006,
   "Latitude": 107.5675264
 },
 {
   "STT": 1702,
   "Name": "Nhà thuốc Nguyễn Hải Linh",
   "address": "Số 2/125A, KP 5, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1703,
   "Name": "Nhà thuốc Hùng Bích",
   "address": "Số 380, Bùi Trọng Nghĩa, tổ 5, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9884113,
   "Latitude": 106.85743
 },
 {
   "STT": 1704,
   "Name": "Nhà thuốc trung tâm chăm sóc sức khỏe sinh sản",
   "address": "239, Phan Đình Phùng, KP 2, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9489846,
   "Latitude": 106.8173765
 },
 {
   "STT": 1705,
   "Name": "Quầy thuốc Vĩnh An",
   "address": "Số 37, khu 5, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7372197,
   "Latitude": 107.1118549
 },
 {
   "STT": 1706,
   "Name": "Nhà thuốc bệnh viện đa khoa huyện Vĩnh Cửu",
   "address": "KP 3, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0815408,
   "Latitude": 107.0244171
 },
 {
   "STT": 1707,
   "Name": "Quầy thuốc bệnh viện đa khoa huyện Vĩnh Cửu - Cơ sở 2",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1708,
   "Name": "Quầy thuốc Trí Nhân",
   "address": "Tổ 5, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 1709,
   "Name": "Quầy thuốc Yến Vương",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 1710,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0498878,
   "Latitude": 107.0211717
 },
 {
   "STT": 1711,
   "Name": "Quầy thuốc Son Tâm",
   "address": "1304/37, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1712,
   "Name": "Quầy thuốc Thanh Hoa",
   "address": "Số 67/4C, tổ 30, khu 2, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.7451012,
   "Latitude": 106.705408
 },
 {
   "STT": 1713,
   "Name": "Quầy thuốc Thành Hoa",
   "address": "Ấp 1, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 1714,
   "Name": "Quầy thuốc Úc Châu",
   "address": "Tổ 12, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 1715,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Số 148/2, ấp Dốc Mơ 1, xã Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 1716,
   "Name": "Nhà thuốc Tâm Đan",
   "address": "523, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1717,
   "Name": "Quầy thuốc Thu Dung",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1718,
   "Name": "Quầy thuốc Mỹ Trang",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1719,
   "Name": "Quầy thuốc Tuấn Khải",
   "address": "Ấp Bình Phú, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 1720,
   "Name": "Quầy thuốc phòng khám đa khoa Thành Tâm",
   "address": "Tổ 14, khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 1721,
   "Name": "Quầy thuốc Phương Ly",
   "address": "349, Trần Phú, khu 5, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.925757,
   "Latitude": 107.4050615
 },
 {
   "STT": 1722,
   "Name": "Quầy thuốc",
   "address": "SN 3064, ấp Phú Thắng, xã Phú Trung, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3065609,
   "Latitude": 107.510672
 },
 {
   "STT": 1723,
   "Name": "Quầy thuốc Nguyệt Anh",
   "address": "SN 79, phố 3, ấp 1, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1724,
   "Name": "Nhà thuốc Mạnh Xinh",
   "address": "A4, KP 3, P.Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1725,
   "Name": "Quầy thuốc",
   "address": "Ấp Chợ, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1726,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "QL1A, Ấp 4, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8323856,
   "Latitude": 107.532696
 },
 {
   "STT": 1727,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Số 21, tổ 8, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1728,
   "Name": "Nhà thuốc An Châu",
   "address": "H1, KP 4, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9586742,
   "Latitude": 106.856797
 },
 {
   "STT": 1729,
   "Name": "Quầy thuốc Khanh",
   "address": "Tổ 7, Khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 1730,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Đội 4, Bạch Lâm 2, Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0529311,
   "Latitude": 107.1714642
 },
 {
   "STT": 1731,
   "Name": "Quầy thuốc Hiệp Ngân",
   "address": "Ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 1732,
   "Name": "Quầy thuốc Chi Linh",
   "address": "77/7A, Khu Văn Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7840803,
   "Latitude": 106.9488901
 },
 {
   "STT": 1733,
   "Name": "Quầy thuốc Đức Tuấn",
   "address": "6/04, tổ 8, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 1734,
   "Name": "Quầy thuốc Quỳnh Anh Anh",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1735,
   "Name": "Nhà thuốc Phùng Anh",
   "address": "F10, tổ 8, KP 4, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9709669,
   "Latitude": 106.871678
 },
 {
   "STT": 1736,
   "Name": "Quầy thuốc Ngọc Tý",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1737,
   "Name": "Quầy thuốc Hằng",
   "address": "Kios số 2, Chợ Dầu Giây, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.942239,
   "Latitude": 107.135928
 },
 {
   "STT": 1738,
   "Name": "Quầy thuốc ",
   "address": "03, tổ 2, ấp Phương Mai 1, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2771519,
   "Latitude": 107.4943786
 },
 {
   "STT": 1739,
   "Name": "Quầy thuốc Minh Phương",
   "address": "305, KP Hiệp Đồng, TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1740,
   "Name": "Quầy thuốc Gia Kiều",
   "address": "780, tổ 9, ấp 6, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 1741,
   "Name": "Quầy thuốc Bảo Nam",
   "address": "Ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 1742,
   "Name": "Quầy thuốc Gia Phú",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 1743,
   "Name": "Nhà thuốc Châu Quang",
   "address": "12/72B, KP 11, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 1745,
   "Name": "Quầy thuốc Phú Nguyên",
   "address": "Tổ 4, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 1746,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Hương Nhung",
   "address": "15C/10, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1747,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Hoàng Anh Thư",
   "address": "Số 98A/4, tổ 13, KP 2, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1748,
   "Name": "Nhà thuốc Đài Hùng",
   "address": "497, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1749,
   "Name": "Nhà thuốc Phòng khám đa khoa Quốc tế Long Bình",
   "address": "Số 1419, Bùi Văn Hòa, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9319693,
   "Latitude": 106.8749571
 },
 {
   "STT": 1750,
   "Name": "Quầy thuốc Khánh Hưng",
   "address": "346, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1751,
   "Name": "Quầy thuốc ",
   "address": "Đường 768, ấp Bình Chánh, xã Tân An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0489595,
   "Latitude": 106.9251259
 },
 {
   "STT": 1752,
   "Name": "Quầy thuốc Lê Văn",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1753,
   "Name": "Quầy thuốc Khánh Tâm",
   "address": "KP 1, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 1754,
   "Name": "Quầy thuốc Tuấn Thảo",
   "address": "Số 592, ấp 6, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 1755,
   "Name": "Quầy thuốc Khải Mi",
   "address": "Khu 3, ấp Xuân Thiện ,xã  Xuân Thiện, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 1756,
   "Name": "Quầy thuốc Huyền Khôi",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 1757,
   "Name": "Quầy thuốc Lan Hương",
   "address": "Tổ 8, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9168346,
   "Latitude": 106.9448957
 },
 {
   "STT": 1758,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "31 tổ 1, ấp 6, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 1759,
   "Name": "Nhà thuốc",
   "address": "Số 14/10A, tổ 6, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1760,
   "Name": "Nhà thuốc Phượng My",
   "address": "277, đường 333, Bùi Văn Hòa, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9005483,
   "Latitude": 106.890887
 },
 {
   "STT": 1761,
   "Name": "Quầy thuốc ",
   "address": "Ấp Long Hiệu, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7499651,
   "Latitude": 106.8796401
 },
 {
   "STT": 1762,
   "Name": "Quầy thuốc Tâm Bình An",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 1763,
   "Name": "Nhà thuốc Nhã Như Thảo",
   "address": "B17, tổ 18, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1764,
   "Name": "Quầy thuốc Đức Thiện",
   "address": "Tổ 41, ấp 4, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 1765,
   "Name": "Quầy thuốc ",
   "address": "31 Khu A, tổ 2, ấp 1, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8281006,
   "Latitude": 106.9318203
 },
 {
   "STT": 1766,
   "Name": "Nhà thuốc Xuân Kiều",
   "address": "53, Hoàng Bá Bích,  KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9402527,
   "Latitude": 106.870548
 },
 {
   "STT": 1767,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Ấp 2, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 1768,
   "Name": "Quầy thuốc Hạnh Tiên",
   "address": "Ấp 5, xã Phú Điền, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 1769,
   "Name": "Nhà thuốc Mai Hà",
   "address": "22/4, tổ 12, KP 2, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.954578,
   "Latitude": 106.798018
 },
 {
   "STT": 1770,
   "Name": "Nhà thuốc Trang Thiên",
   "address": "Số 78, tổ 46, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9819539,
   "Latitude": 106.8449944
 },
 {
   "STT": 1771,
   "Name": "Quầy thuốc ",
   "address": "Ấp 4, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 1772,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Ấp 4, xã Thừa Đức, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7646516,
   "Latitude": 107.1502859
 },
 {
   "STT": 1773,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Ấp 8, xã Thừa Đức, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7371768,
   "Latitude": 107.1120842
 },
 {
   "STT": 1774,
   "Name": "Quầy thuốc Minh Khuê",
   "address": "149, Ấp 5, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 1775,
   "Name": "Quầy thuốc Hoa Lư",
   "address": "Ấp Bình Phước, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 1776,
   "Name": "Quầy thuốc Hải Yến",
   "address": "Tổ 38, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 1777,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "Ấp 7, xã Nhân Nghĩa, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 1778,
   "Name": "Quầy thuốc Sông Mây",
   "address": "Đường 767, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.988191,
   "Latitude": 106.9668265
 },
 {
   "STT": 1779,
   "Name": "Nhà thuốc Bệnh viện Nhi Đồng Nai",
   "address": "Bệnh viện Nhi đồng Đồng Nai, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9561978,
   "Latitude": 106.8743567
 },
 {
   "STT": 1780,
   "Name": "Quầy thuốc Lê Nguyễn",
   "address": "Số 1553, tổ 9, ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 1781,
   "Name": "Quầy thuốc ",
   "address": "Số 385, ấp 1, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7361529,
   "Latitude": 107.0657549
 },
 {
   "STT": 1782,
   "Name": "Quầy thuốc Kim Ngân 2",
   "address": "Ấp 6, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 1783,
   "Name": "Quầy thuốc Ngân Giang",
   "address": "1407, ấp 4, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 1784,
   "Name": "Nhà thuốc Thảo Quân",
   "address": "Tổ 14, KP 5, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1785,
   "Name": "Nhà thuốc Tướng Nga",
   "address": "446/18, Huỳnh Dân Sanh, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9381573,
   "Latitude": 106.8797963
 },
 {
   "STT": 1786,
   "Name": "Quầy thuốc Ái Vi",
   "address": "92/459, Phạm Văn Thuận, KP 4, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9433417,
   "Latitude": 106.8588773
 },
 {
   "STT": 1787,
   "Name": "Quầy thuốc Dương Hoàng Thuận An",
   "address": "Tổ 3, ấp Tín Nghĩa, xã Xuân Thiện, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 1788,
   "Name": "Quầy thuốc Ý Đức 2",
   "address": "Số nhà 1475, ấp Ngọc Lâm 1, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.278514,
   "Latitude": 107.4623053
 },
 {
   "STT": 1789,
   "Name": "Quầy thuốc Trang Thư",
   "address": "SN 15/4, ấp Phú Quý 1, xã La Ngà, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1738348,
   "Latitude": 107.2193578
 },
 {
   "STT": 1790,
   "Name": "Quầy thuốc Lan Chi",
   "address": "Số 34/1, ấp Phú Mỹ, xã Xuân Lập, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9119714,
   "Latitude": 107.177058
 },
 {
   "STT": 1791,
   "Name": "Quầy thuốc Anh Huy",
   "address": "Số 07, ấp Chợ, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 1792,
   "Name": "Quầy thuốc Hữu Nghị",
   "address": "Tổ 2, ấp Đoàn Kết, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 1793,
   "Name": "Quầy thuốc ",
   "address": "Ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1794,
   "Name": "Quầy thuốc Kim Hoàng",
   "address": "Tổ 9, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1795,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Hồng Hải An",
   "address": "Tổ 24, KP 2, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 1796,
   "Name": "Quầy thuốc Đoan Trang",
   "address": "3386 Ấp Phú Lâm 4, Phú Sơn, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 1797,
   "Name": "Nhà thuốc Việt Long",
   "address": "292/1, KP1, P. Tân Biên, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.970533,
   "Latitude": 106.8932141
 },
 {
   "STT": 1798,
   "Name": "Nhà thuốc Việt Thăng Long",
   "address": "35, tổ 7, Kp2, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.967224,
   "Latitude": 106.8608004
 },
 {
   "STT": 1799,
   "Name": "Nhà thuốc Minh Long",
   "address": "508 Tây Lạc, An Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1800,
   "Name": "Quầy thuốc Hiệp Phước",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 1801,
   "Name": "Quầy thuốc Thảo",
   "address": "Ấp 2, An Viễn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.8826605,
   "Latitude": 107.020525
 },
 {
   "STT": 1802,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Ấp An Hòa, xã Tây Hoà, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1803,
   "Name": "Quầy thuốc Dũng Nga",
   "address": "6/B2 Đức Long, Gia Tân 2, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0600887,
   "Latitude": 107.1713506
 },
 {
   "STT": 1804,
   "Name": "Quầy thuốc Thu Anh",
   "address": "Tổ 3, khu phố 6, TT Vĩnh An, Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0766492,
   "Latitude": 107.0284252
 },
 {
   "STT": 1805,
   "Name": "Nhà thuốc Chuyên Linh",
   "address": "Tổ 13 KP4, Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.972058,
   "Latitude": 106.9097623
 },
 {
   "STT": 1806,
   "Name": "Quầy thuốc Lê Phương",
   "address": "ĐT 769, Tổ 2, ấp Hàng Gòn, Lộc An, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.782571,
   "Latitude": 106.957155
 },
 {
   "STT": 1807,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "96/3B Tây Kim, Gia Kiệm, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 1808,
   "Name": "Nhà thuốc Nam Long",
   "address": "Số 1/2HV, KP1, QL 51, Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9003715,
   "Latitude": 106.8549163
 },
 {
   "STT": 1809,
   "Name": "Nhà thuốc Thủy Anh",
   "address": "Số 137 Tổ 8 KP3, Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9648537,
   "Latitude": 106.8651467
 },
 {
   "STT": 1810,
   "Name": "Quầy thuốc Quỳnh Ánh",
   "address": "Ấp Trầu, Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1811,
   "Name": "Quầy thuốc Ánh Hồng",
   "address": "Ấp 3, An Viễn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9462257,
   "Latitude": 107.0404754
 },
 {
   "STT": 1812,
   "Name": "Quầy thuốc An Bình",
   "address": "Tổ 8, khu Cầu Xéo, TT Long Thành, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7777705,
   "Latitude": 106.9408496
 },
 {
   "STT": 1813,
   "Name": "Quầy thuốc Phước Lý",
   "address": "Ấp Phước Lý, Đại Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1814,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "234/3 ấp Cây Dầu, Thanh Sơn, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3169155,
   "Latitude": 107.4736016
 },
 {
   "STT": 1815,
   "Name": "Quầy thuốc ",
   "address": "Tổ 8, KP6, TT Gia Ray, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9281732,
   "Latitude": 107.4144526
 },
 {
   "STT": 1816,
   "Name": "Quầy thuốc Tùng Anh",
   "address": "Tây Lạc, Ấp An Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 1817,
   "Name": "Quầy thuốc Bảo Lộc",
   "address": "Tổ 28 Ấp 3, Tam An, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8637012,
   "Latitude": 106.9244978
 },
 {
   "STT": 1818,
   "Name": "Quầy thuốc Thuận Thành",
   "address": "910A, Tổ 7, Ấp 8, Xuân Bắc, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 1819,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "93 Ấp 1, Sông Trầu, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9782781,
   "Latitude": 107.0185123
 },
 {
   "STT": 1820,
   "Name": "Quầy thuốc Bảo Yến",
   "address": "245 tổ 3, ấp Thanh Trung, Thanh Sơn, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1821,
   "Name": "Quầy thuốc ",
   "address": "Số 79A, ấp 5, Lộ 25, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.8704278,
   "Latitude": 107.0952864
 },
 {
   "STT": 1822,
   "Name": "Nhà thuốc Hoàng Thúy",
   "address": "Số 48/88, Hoàng Bá Bích, KP5, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9410376,
   "Latitude": 106.8712515
 },
 {
   "STT": 1823,
   "Name": "Nhà thuốc Vũ Minh Châu",
   "address": "Tổ 29, KP8, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 1824,
   "Name": "Nhà thuốc Xuân Đồng-Cty TNHH PKĐK Nguyễn An Phúc",
   "address": "Số 613, Đồng Khởi, KP8, Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9701047,
   "Latitude": 106.8531608
 },
 {
   "STT": 1825,
   "Name": "Quầy thuốc Tân Nhã Kỳ",
   "address": "Số 21, Bùi Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1826,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "SN 2578, ấp Phú Dũng, Phú Bình, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 1827,
   "Name": "Nhà thuốc Chân Hưng",
   "address": "32, KP5, Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9921407,
   "Latitude": 106.8709404
 },
 {
   "STT": 1828,
   "Name": "Quầy thuốc ",
   "address": "234 Ấp 4, Tam An, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 1829,
   "Name": "Quầy thuốc Thiện Tâm",
   "address": "Ấp Trầu, Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1830,
   "Name": "Nhà thuốc Huỳnh Ngọc",
   "address": "K4/233C, KP2, Bửu Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.960593,
   "Latitude": 106.8483173
 },
 {
   "STT": 1831,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "115 Trần Phú, Xuân An, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9332359,
   "Latitude": 107.2463284
 },
 {
   "STT": 1832,
   "Name": "Quầy thuốc Phương Thúy",
   "address": "Tổ 1, Khu Phước Long , TT Long Thành, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.778118,
   "Latitude": 106.9335355
 },
 {
   "STT": 1833,
   "Name": "Quầy thuốc Khôi Nguyễn",
   "address": "Ấp Phước Lương, Phú Hữu, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7405091,
   "Latitude": 106.7902063
 },
 {
   "STT": 1834,
   "Name": "Quầy thuốc Hiếu Phượng",
   "address": "Đường Suối Dinh, ấp Lộc Hòa, Tây Hoà, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1835,
   "Name": "Quầy thuốc",
   "address": "Tổ 3 Chọ Suối Quýt, Cẩm Đường, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.790341,
   "Latitude": 107.0941794
 },
 {
   "STT": 1836,
   "Name": "Quầy thuốc",
   "address": "109/4K, khu 2, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 1837,
   "Name": "Tủ thuốc trạm y tế xã Phước Khánh",
   "address": "Ấp 2, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 1838,
   "Name": "Quầy thuốc Hiền Nga",
   "address": "334 Tổ 8 Ngô Quyền, Bàu Hàm 2, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9554974,
   "Latitude": 107.1307289
 },
 {
   "STT": 1839,
   "Name": "Quầy thuốc",
   "address": "9/5, Quốc lộ 1, Phú Bình, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.8953124,
   "Latitude": 107.2285054
 },
 {
   "STT": 1840,
   "Name": "Quầy thuốc Đức Thịnh",
   "address": "Ấp 1, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7503268,
   "Latitude": 106.9437108
 },
 {
   "STT": 1841,
   "Name": "Quầy thuốc Minh Anh",
   "address": "Ấp 1, Tân An, Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 1842,
   "Name": "Quầy thuốc Tuyết Nhung",
   "address": "Ấp Cây Dầu, Thanh Sơn, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.3169155,
   "Latitude": 107.4736016
 },
 {
   "STT": 1843,
   "Name": "Quầy thuốc Kim Sơn",
   "address": "Tổ 19, khu Kim Sơn, TT Long Thành, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7783288,
   "Latitude": 106.9429001
 },
 {
   "STT": 1844,
   "Name": "Quầy thuốc Công Thành",
   "address": "55/1G Võ Dõng, Gia Kiệm, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 1845,
   "Name": "Nhà thuốc Bệnh viện ĐKKV Long Khánh",
   "address": "Số 911, đường 21 tháng 4, ấp Suối Tre, xã Suối Tre, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9425932,
   "Latitude": 107.2078821
 },
 {
   "STT": 1846,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "số 316, An Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9650763,
   "Latitude": 106.9496077
 },
 {
   "STT": 1847,
   "Name": "Quầy thuốc Trường Phú",
   "address": "Ấp Đất Mới, Phú Hội, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1848,
   "Name": "Nhà thuốc PKĐK An Bình Nasa",
   "address": "04 Bùi Văn Hòa, Kp11, An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.927072,
   "Latitude": 106.878744
 },
 {
   "STT": 1849,
   "Name": "Quầy thuốc Thảo Tuyền",
   "address": "Tổ 9, Ấp 3, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 1850,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, Phú Thạnh, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 1851,
   "Name": "Quầy thuốc Anh Thu",
   "address": "số 12, đường 141, ấp Việt Kiều, Xuân Hiệp, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9089472,
   "Latitude": 107.3845538
 },
 {
   "STT": 1852,
   "Name": "Quầy thuốc Đức Tin",
   "address": "Tổ 5, ấp 9/4, Xuân Thạnh, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9483523,
   "Latitude": 107.1602683
 },
 {
   "STT": 1853,
   "Name": "Quầy thuốc Sa Ly",
   "address": "Ấp Cát Lái, Phú Hữu, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 1854,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Ấp Bùi Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1855,
   "Name": "Quầy thuốc Tâm Loan",
   "address": "1437, Phạm Văn Thuận, Kp2, P. Thống Nhất, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9575593,
   "Latitude": 106.8413329
 },
 {
   "STT": 1856,
   "Name": "Quầy thuốc Vân",
   "address": "Ki ốt số 1, Chợ Phú Túc, xã Phú Túc, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.0893629,
   "Latitude": 107.2109551
 },
 {
   "STT": 1857,
   "Name": "Quầy thuốc Mai Khôi",
   "address": "ấp 2, xã Sông Trầu, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1858,
   "Name": "Quầy thuốc ",
   "address": "Số nhà 222, tổ ̉6, ấp Phú Hợp B, Phú Bình, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 1859,
   "Name": "Quầy thuốc Hiếu Duyên",
   "address": "2160 ấp Quảng Lộc , Quảng Tiến, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.945607,
   "Latitude": 106.98593
 },
 {
   "STT": 1860,
   "Name": "Quầy thuốc ",
   "address": "Ấp 2, Lộ 25, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.8700501,
   "Latitude": 107.0907468
 },
 {
   "STT": 1861,
   "Name": "Nhà thuốc Duyên Ngọc",
   "address": "19/4, KP7, Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9813789,
   "Latitude": 106.891983
 },
 {
   "STT": 1862,
   "Name": "Quầy thuốc Thiện Ngọc",
   "address": "Tổ 13, ấp 5, Lâm San, Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.6997304,
   "Latitude": 107.3257545
 },
 {
   "STT": 1863,
   "Name": "Quầy thuốc Tân Phát",
   "address": "Ấp Tân Phát, Đồi 61, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9180556,
   "Latitude": 107.0241667
 },
 {
   "STT": 1864,
   "Name": "Quầy thuốc Thanh Hà",
   "address": "Ấp 5, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7593232,
   "Latitude": 106.9425975
 },
 {
   "STT": 1865,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Phương Anh",
   "address": "30 Ấp Núi Đỏ, Bàu Sen, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9265893,
   "Latitude": 107.2291193
 },
 {
   "STT": 1866,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Hoàng Sơn Thảo",
   "address": "194A, Tổ 6, KP 2, Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 1867,
   "Name": "Tủ thuốc Trạm y tế P. Hố Nai",
   "address": "Trạm Y tế phường Hố Nai",
   "Longtitude": 10.9719139,
   "Latitude": 106.8739485
 },
 {
   "STT": 1868,
   "Name": "Tủ thuốc trạm y tế phường Trảng Dài",
   "address": "Trạm Y tế phường Trảng Dài",
   "Longtitude": 10.9829515,
   "Latitude": 106.8565333
 },
 {
   "STT": 1869,
   "Name": "Quầy thuốc Hoài Ân",
   "address": "Số nhà 73, tổ 9, ấp Ngọc Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 1870,
   "Name": "Quầy thuốc Hoàng Dung",
   "address": "418, ấp Phan Bội Châu, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1871,
   "Name": "Nhà thuốc Thiên Nghĩa",
   "address": "70, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1872,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Tổ 16, ấp Trần Cao Vân, xã Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1873,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Lâm San, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7216641,
   "Latitude": 107.3394486
 },
 {
   "STT": 1874,
   "Name": "Quầy thuốc Ý Nguyện",
   "address": "Ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1875,
   "Name": "Quầy thuốc phòng khám chuyên khoa răng hàm mặt Hoàng",
   "address": "Số 597, đường 767, thôn Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1876,
   "Name": "Nhà thuốc Hoàng Bảo Long",
   "address": "105, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 1877,
   "Name": "Quầy thuốc Hạnh Phúc",
   "address": "1532, ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9424055,
   "Latitude": 106.9816253
 },
 {
   "STT": 1878,
   "Name": "Quầy thuốc Dân An",
   "address": "Ấp 4, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.702072,
   "Latitude": 106.958422
 },
 {
   "STT": 1879,
   "Name": "Quầy thuốc Thảo Ly",
   "address": "Số 10, Lê Văn Hưu, KP 2, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 1880,
   "Name": "Quầy thuốc Yến Thịnh",
   "address": "Số nhà 1783, ấp 4B, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 1881,
   "Name": "Quầy thuốc Hoàng Phúc",
   "address": "1436, tổ 1, ấp Ngọc Lâm 3, xã Phú Thanh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.278514,
   "Latitude": 107.4623053
 },
 {
   "STT": 1882,
   "Name": "Nhà thuốc công ty TNHH MTV Âu Mỹ Việt",
   "address": "203A, Phạm Văn Thuận, KP 1, P.Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9582816,
   "Latitude": 106.8384847
 },
 {
   "STT": 1883,
   "Name": "Quầy thuốc Hân",
   "address": "Ấp Tân Hữu, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9940176,
   "Latitude": 107.4232248
 },
 {
   "STT": 1884,
   "Name": "Nhà thuốc phòng khám đa khoa Ái Nghĩa Đồng Khởi",
   "address": "C4-C5, đường Đồng Khởi, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9566014,
   "Latitude": 106.8624563
 },
 {
   "STT": 1885,
   "Name": "Quầy thuốc Anh Tuấn 2",
   "address": "Khu phố 1, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9521847,
   "Latitude": 106.9978466
 },
 {
   "STT": 1886,
   "Name": "Quầy thuốc Trà Giang",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 1887,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Bình Lộc, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9835555,
   "Latitude": 107.2359278
 },
 {
   "STT": 1888,
   "Name": "Quầy thuốc Như Mai",
   "address": "3088, ấp Việt Kiều, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.908204,
   "Latitude": 107.376038
 },
 {
   "STT": 1889,
   "Name": "Quầy thuốc Khánh Phương",
   "address": "19A, đường Chợ 2, ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 1890,
   "Name": "Quầy thuốc Kim Cúc ",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1891,
   "Name": "Quầy thuốc Huyền Thoại",
   "address": "Số 36, Nguyễn Văn Cừ, Khu phố 5, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9496517,
   "Latitude": 107.0021742
 },
 {
   "STT": 1892,
   "Name": "Quầy thuốc",
   "address": "Ấp Bào Bông, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 1893,
   "Name": "Quầy thuốc Mai Khôi",
   "address": "Ấp Việt Kiều, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9091923,
   "Latitude": 107.381107
 },
 {
   "STT": 1894,
   "Name": "Quầy thuốc An Bình",
   "address": "Tổ 2, KP 7, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 1895,
   "Name": "Quầy thuốc Thùy Trâm",
   "address": "Tổ 8, KP 8, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0888889,
   "Latitude": 107.0338889
 },
 {
   "STT": 1896,
   "Name": "Nhà thuốc",
   "address": "KP 4, đường Trường Chinh, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9504576,
   "Latitude": 107.0078788
 },
 {
   "STT": 1897,
   "Name": "Quầy thuốc Minh Hằng",
   "address": "Ấp 5, Thạnh Phú, Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 1898,
   "Name": "Quầy thuốc Hùng Vân",
   "address": "Ấp Đất Mới, Phú Hội, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1899,
   "Name": "Quầy thuốc Văn Trí",
   "address": "03/02 Ấp Nhân Hòa, Tây Hoà, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1900,
   "Name": "Quầy thuốc  Bảo Long",
   "address": "Ấp Thái Hòa, Hố Nai 3, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9649433,
   "Latitude": 106.9351566
 },
 {
   "STT": 1901,
   "Name": "Nhà thuốc  Nhã Phương",
   "address": "Số 70B, tổ 13, KP3, An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9574128,
   "Latitude": 106.8426871
 },
 {
   "STT": 1902,
   "Name": "Quầy thuốc Quốc Huy",
   "address": "Tổ 34, ấp 4, Tam An, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 1903,
   "Name": "Quầy thuốcBảo Thanh",
   "address": "ấp 3, Long Thọ, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.6937381,
   "Latitude": 106.9500662
 },
 {
   "STT": 1904,
   "Name": "Nhà thuốc PKĐK Sài Gòn - Long khánh ",
   "address": "57, Nguyễn Thị Minh Khai, Xuân An, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9296278,
   "Latitude": 107.2517222
 },
 {
   "STT": 1905,
   "Name": "Quầy thuốcNgọc Tới",
   "address": "Ấp Sông Mây, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 1906,
   "Name": "Nhà thuốc Kim Đơn",
   "address": "134C/124 KP7, Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9890275,
   "Latitude": 106.8296372
 },
 {
   "STT": 1907,
   "Name": "Quầy thuốcNgọc Dung",
   "address": "Số 50, phố 1, ấp 2, Phú Lợi, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2236734,
   "Latitude": 107.3907968
 },
 {
   "STT": 1908,
   "Name": "Nhà thuốc Hoàng Thiên",
   "address": "KP1, Xuân Trung, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9394105,
   "Latitude": 107.2444751
 },
 {
   "STT": 1909,
   "Name": "Quầy thuốcVITO",
   "address": "139 đường 3/2 KP5, TT Trảng Bom, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.7728297,
   "Latitude": 106.6763448
 },
 {
   "STT": 1910,
   "Name": "Quầy thuốcNgọc Yến",
   "address": "Ấp Sông Mây, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 1911,
   "Name": "Nhà thuốc PKĐK 115 Đại Phước",
   "address": "D295A Ấp Phước Lý, Đại Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 1912,
   "Name": "Nhà thuốc Kim Thanh",
   "address": "Số 1058, KP3, Đồng Khởi, Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9684006,
   "Latitude": 106.859825
 },
 {
   "STT": 1913,
   "Name": "Quầy thuốcNgọc Yến",
   "address": "Tổ 6, ấp 1, Thanh Sơn, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2322972,
   "Latitude": 107.2975882
 },
 {
   "STT": 1914,
   "Name": "Quầy thuốcThanh Huệ",
   "address": "192, tổ 6, ấp 6, Xuân Tây, Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 1915,
   "Name": "Quầy thuốcNhân Ái",
   "address": "31/1E Võ Dõng 1, Gia Kiệm, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 1916,
   "Name": "Quầy thuốcKhánh Hồng",
   "address": "Chợ Phú Cường, Phú Cường, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.088071,
   "Latitude": 107.1837001
 },
 {
   "STT": 1917,
   "Name": "Quầy thuốc1680",
   "address": "743 Tổ 18 Ấp Tân Bảo, Bảo Bình, Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.8352258,
   "Latitude": 107.3018591
 },
 {
   "STT": 1918,
   "Name": "Quầy thuốcNgọc Thu",
   "address": "994 ấp Tân Hòa, Xuân Thành, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 1919,
   "Name": "Quầy thuốcBảo Đan",
   "address": "Ấp Suối Sóc, Xuân Mỹ, Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 1920,
   "Name": "Công ty TNHH Dược Phẩm Trang Phúc",
   "address": "1/10/20 KP 3, Đồng Khởi, Tam Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9567182,
   "Latitude": 106.8613744
 },
 {
   "STT": 1921,
   "Name": "Quầy thuốcPhương Diễm",
   "address": "Đường Nguyễn Trãi, Kp Hiệp Đồng, TT Định Quán, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.1966787,
   "Latitude": 107.3486147
 },
 {
   "STT": 1922,
   "Name": "Quầy thuốcNgọc Diễm",
   "address": "Tổ 17, ấp Bàu Trâm, Bàu Trâm, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9284152,
   "Latitude": 107.2835238
 },
 {
   "STT": 1923,
   "Name": "Quầy thuốcAnh Thư",
   "address": "Ấp Bùi Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1924,
   "Name": "Quầy thuốcNgọc Yến",
   "address": "Ấp 1, Sông Trầu, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 1925,
   "Name": "Quầy thuốcNhật Hiếu",
   "address": "Ấp Thọ Phước, Xuân Thọ, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9539163,
   "Latitude": 107.3413172
 },
 {
   "STT": 1926,
   "Name": "Quầy thuốcTường Nguyên",
   "address": "Khu 3, TT Gia Ray, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.926233,
   "Latitude": 107.403939
 },
 {
   "STT": 1927,
   "Name": "Quầy thuốcCây Dầu 2",
   "address": "Ấp Phú Mỹ 1, Phú Hội, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 1928,
   "Name": "Quầy thuốcLan Duy",
   "address": "71/3, khu 3, ấp An Hòa, Tây Hoà, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 1929,
   "Name": "Quầy thuốcPhúc An",
   "address": "2257, ấp Phương Lâm 2, Phú Lâm, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 1930,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Hiếu Minh Tâm",
   "address": "Tổ 32, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 1931,
   "Name": "Tủ thuốc trạm Y tế Tam Hiệp",
   "address": "Tủ thuốc trạm y tế phường Tam Hiệp, , Tỉnh Đồng Nai",
   "Longtitude": 10.9555507,
   "Latitude": 106.8616724
 },
 {
   "STT": 1932,
   "Name": "Tủ thuốc trạm Y tế Tân Vạn",
   "address": "Tủ thuốc Trạm Y tế phường Tân Vạn, , Tỉnh Đồng Nai",
   "Longtitude": 10.9236722,
   "Latitude": 106.8144892
 },
 {
   "STT": 1933,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu Đại Tâm Đức",
   "address": "7/A3, Nguyễn Văn Tiên, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9787897,
   "Latitude": 106.8466517
 },
 {
   "STT": 1934,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu ",
   "address": "10-11/D2, tổ 12, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9012444,
   "Latitude": 106.8526661
 },
 {
   "STT": 1935,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu ",
   "address": "Tổ 25, KP 4, Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9980636,
   "Latitude": 106.8626176
 },
 {
   "STT": 1936,
   "Name": "Cơ sở mua bán thuốc đông y, thuốc từ dược liệu, thực phẩm chức năng, dụng cụ y tế, mỹ phẩm",
   "address": "Số 40, Lý Văn Sâm, Kp6, Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9556489,
   "Latitude": 106.8579557
 },
 {
   "STT": 1937,
   "Name": "Tủ thuốc trạm y tế phường Tam Hiệp",
   "address": "Trạm Y tế phường Tam Hiệp, tổ 10, KP 6,  P. Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9555507,
   "Latitude": 106.8616724
 },
 {
   "STT": 1938,
   "Name": "Tủ thuốc trạm y tế phường Tân Vạn",
   "address": "Trạm Y tế P.Tân Vạn, A4/90a, Bùi Hữu Nghĩa, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9104749,
   "Latitude": 106.8284718
 },
 {
   "STT": 1939,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "Ấp 1, xã Phú Tân, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 1940,
   "Name": "Nhà thuốc Quyết Thắng",
   "address": "33 Hà Huy Giáp, phường Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93958,
   "Latitude": 106.8249058
 },
 {
   "STT": 1941,
   "Name": "Quầy thuốc ",
   "address": "Đường trường Giáo Dưỡng số 4, Khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8442215,
   "Latitude": 106.9382361
 },
 {
   "STT": 1942,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "Ấp Cây Điệp , xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 1943,
   "Name": "Nhà thuốc Thùy Chi",
   "address": "Tổ 13, KP5, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9702123,
   "Latitude": 106.8711324
 },
 {
   "STT": 1944,
   "Name": "Nhà thuốc trung tâm Y tế huyện Cẩm Mỹ",
   "address": "Trung tâm Y tế huyện Cẩm Mỹ, ấp Suối Cả, xã Long Giao, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8144786,
   "Latitude": 107.2187796
 },
 {
   "STT": 1945,
   "Name": "Quầy thuốc Minh Hằng",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1946,
   "Name": "Quầy thuốc Gia Khánh",
   "address": "Ấp Sông Mây , xã  Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 1947,
   "Name": "Quầy thuốc Ngọc Qúy",
   "address": "Ấp Vũng Gấm, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 1948,
   "Name": "Nhà thuốc phòng khám đa khoa Aí Nghĩa Long Khánh",
   "address": "505 Hồ thị Hương, xã Bàu Trâm, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9284152,
   "Latitude": 107.2835238
 },
 {
   "STT": 1949,
   "Name": "Quầy thuốc Đoàn Hồng",
   "address": "209, ấp Hưng Hiệp, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 1950,
   "Name": "Quầy thuốc Ngọc Y",
   "address": "12 Hai Bà Trưng, Khu Phước Long, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7804518,
   "Latitude": 106.9522847
 },
 {
   "STT": 1951,
   "Name": "Nhà thuốc Thu Hiên",
   "address": "56, Tổ 13 KP2, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9672269,
   "Latitude": 106.8608001
 },
 {
   "STT": 1952,
   "Name": "Quầy thuốc Nguyễn Hoàng",
   "address": "Ấp 3, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8255222,
   "Latitude": 107.5598542
 },
 {
   "STT": 1953,
   "Name": "Quầy thuốc Nhã Uyên",
   "address": "289, Tổ 1, Ấp Cây Da, xã  Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9545183,
   "Latitude": 106.5356951
 },
 {
   "STT": 1954,
   "Name": "Quầy thuốc Minh Sương",
   "address": "414 Trần Phú, Khu 3, TT Gia Ray, huyện  Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9263329,
   "Latitude": 107.4033655
 },
 {
   "STT": 1955,
   "Name": "Quầy thuốc Võ Hoàng Thùy Dung",
   "address": "432/3 đường Đông Kim, ấp Tín Nghĩa, xã Xuân Thiện, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0322794,
   "Latitude": 107.2325837
 },
 {
   "STT": 1956,
   "Name": "Quầy thuốc Phúc Sinh",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 1957,
   "Name": "Nhà thuốc Ngọc Hà My",
   "address": "89N, tổ 8B, KP3, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9648537,
   "Latitude": 106.8651467
 },
 {
   "STT": 1958,
   "Name": "Nhà thuốc PKĐK Aí Nghĩa Biên Hòa",
   "address": "Số 122-124 Đồng khởi, KP4, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9566014,
   "Latitude": 106.8624563
 },
 {
   "STT": 1959,
   "Name": "Quầy thuốc Như Thảo",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 1960,
   "Name": "Nhà thuốc Long Nhi",
   "address": "Số 148, tổ 24, KP6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 1961,
   "Name": "Quầy thuốc Đông Hưng",
   "address": "Ấp 4, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3001594,
   "Latitude": 107.150533
 },
 {
   "STT": 1962,
   "Name": "Quầy thuốc Ngọc Hạnh",
   "address": "Tổ 5, ấp 1, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 1963,
   "Name": "Nhà thuốc Qúy Phương",
   "address": "8/2, KP1, phường  Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1964,
   "Name": "Nhà thuốc Quốc Hùng Phát",
   "address": "Tổ 23, KP6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9221396,
   "Latitude": 106.8824977
 },
 {
   "STT": 1965,
   "Name": "Nhà thuốc Anh Tùng",
   "address": "226A, tổ 17, KP 3, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9100971,
   "Latitude": 106.8831153
 },
 {
   "STT": 1966,
   "Name": "Quầy thuốc Nguyễn Lâm Thanh Tùng",
   "address": "Khu Phước Long, TT Long Thành, huyện  Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7782801,
   "Latitude": 106.9447146
 },
 {
   "STT": 1967,
   "Name": "Quầy thuốc My My",
   "address": "01 Phố 4, Ấp 1, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2046485,
   "Latitude": 107.362844
 },
 {
   "STT": 1968,
   "Name": "Quầy thuốc Hồng Lan",
   "address": "305, Ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9203782,
   "Latitude": 107.085525
 },
 {
   "STT": 1969,
   "Name": "Quầy thuốc Phúc Khang 1",
   "address": "Ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7371768,
   "Latitude": 107.1120842
 },
 {
   "STT": 1970,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 1971,
   "Name": "Công ty cổ phần AZENCA",
   "address": "33C3, KP 11, phường Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9827185,
   "Latitude": 106.8395407
 },
 {
   "STT": 1972,
   "Name": "Quầy thuốc Sơn Trang",
   "address": "Ấp Bến Cộ, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7437649,
   "Latitude": 106.826849
 },
 {
   "STT": 1973,
   "Name": "Quầy thuốc Tuyết Nhung",
   "address": "A3/064C Nguyễn Huệ 2, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.991378,
   "Latitude": 107.154914
 },
 {
   "STT": 1974,
   "Name": "Quầy thuốc Tố Tâm",
   "address": "671, tổ 2, ấp 1, xã phú Điền, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 1975,
   "Name": "Quầy thuốc Lê Quang",
   "address": "Hẻm 308, tổ 19, khu Văn Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7840803,
   "Latitude": 106.9488901
 },
 {
   "STT": 1976,
   "Name": "Nhà thuốc Diệu Trâm",
   "address": "141/2, KP2, phường Tân Biên, thành phố Biên Hòa, tinh Đồng Nai",
   "Longtitude": 10.9827826,
   "Latitude": 106.8884848
 },
 {
   "STT": 1977,
   "Name": "Quầy thuốc Thanh Thương",
   "address": "16, Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1978,
   "Name": "Quầy thuốc Kiều Hạnh",
   "address": "131, Khu 2, ấp Hưng Nghĩa, xã Hưng Lộc, huyện thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 1979,
   "Name": "Quầy thuốc Bảo Linh",
   "address": "Ấp 6, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.813407,
   "Latitude": 106.9396
 },
 {
   "STT": 1980,
   "Name": "Quầy thuốc Hà Lợi",
   "address": "Ấp Tây Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai ",
   "Longtitude": 10.8614417,
   "Latitude": 107.3714339
 },
 {
   "STT": 1981,
   "Name": "Quầy thuốc Thịnh Châm",
   "address": "Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.8505029,
   "Latitude": 107.042198
 },
 {
   "STT": 1982,
   "Name": "Quầy thuốc Gia Huy",
   "address": "02 Lê Duẫn, KP 4, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9442736,
   "Latitude": 107.0120033
 },
 {
   "STT": 1983,
   "Name": "Quầy thuốc Hoa Sơn",
   "address": "579 Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 1984,
   "Name": "Nhà thuốc Thục Tâm",
   "address": "28 Nguyễn Phúc Cgu, tổ 1, KP 5, Phường Trảng Dài, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 1985,
   "Name": "Cty TNHH SX thương mại và dịch vụ Dược minh Quân",
   "address": "I 20, khu Liên Kế Bửu Long, KP 1, phường Bửu Long, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9586438,
   "Latitude": 106.8053993
 },
 {
   "STT": 1986,
   "Name": "Quầy thuốc Tâm Thư",
   "address": "242, ấp Bảo Thị, xã Xuân Định, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8825811,
   "Latitude": 107.2646659
 },
 {
   "STT": 1987,
   "Name": "Nhà thuốc Quang Trung",
   "address": "34/13, KP 5, phường Trảng Dài, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 1988,
   "Name": "Quầy thuốc Hoàng Duyên",
   "address": "E3/51, ấp Lê Lợi II, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.981733,
   "Latitude": 107.1515596
 },
 {
   "STT": 1989,
   "Name": "Quầy thuốc Thanh Thanh 3",
   "address": "Tổ 7, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 1990,
   "Name": "Quầy thuốc Công Minh",
   "address": "Khu phố Hiệp Đồng, TT Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 1991,
   "Name": "Quầy thuốc Hiệp Long",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 1992,
   "Name": "Nhà thuốc Phòng khám đa khoa Quốc tế Mỹ Đức",
   "address": "392 Bùi Văn Hòa, tổ 20, KP 3, phường Long Bình Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.902515,
   "Latitude": 106.8899412
 },
 {
   "STT": 1993,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "52B/3 ấp Nhân Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.946514,
   "Latitude": 107.0544048
 },
 {
   "STT": 1994,
   "Name": "Quầy thuốc Qúy Bình",
   "address": "351, tổ 28, ấp Thái Hòa, xã Hố Nai 3, huyện trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9922594,
   "Latitude": 106.8700238
 },
 {
   "STT": 1995,
   "Name": "Nhà Thuốc Hòa Hiệp",
   "address": "9/11D, KP 2, phường Trảng Dài, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 1996,
   "Name": "Quầy thuốc Tâm Nghĩa",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 1997,
   "Name": "Quầy thuốc Duy Tâm",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 1998,
   "Name": "Quầy thuốc Vũ Thị Minh Trang",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 1999,
   "Name": "Nhà Thuốc Anh Tùng",
   "address": "226,tổ 17, KP 3, Phường Long Bình Tân,  thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916461,
   "Latitude": 106.8502418
 },
 {
   "STT": 2000,
   "Name": "Quầy thuốc Anh Phát",
   "address": "510, đường 1, ấp 1, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8286472,
   "Latitude": 107.5774815
 },
 {
   "STT": 2001,
   "Name": "Quầy thuốc Long Phước",
   "address": "Tổ 12, ấp Xóm Gò, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2002,
   "Name": "Quầy thuốc Bảo Tín",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2003,
   "Name": "Quầy thuốc Thịnh Phát Đạt",
   "address": "Ấp 3A,  xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8461842,
   "Latitude": 107.5092529
 },
 {
   "STT": 2004,
   "Name": "Quầy thuốc T. Liễu",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2005,
   "Name": "Quầy thuốc Tố Tâm",
   "address": "671, tổ 2, ấp 1, xã phú Điền, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 2006,
   "Name": "Quầy thuốc Lê Quang",
   "address": "Hẻm 308, tổ 19, khu Văn Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7840803,
   "Latitude": 106.9488901
 },
 {
   "STT": 2007,
   "Name": "Nhà thuốc Diệu Trâm",
   "address": "141/2, KP2, phường Tân Biên, thành phố Biên Hòa, tinh Đồng Nai",
   "Longtitude": 10.9827826,
   "Latitude": 106.8884848
 },
 {
   "STT": 2008,
   "Name": "Quầy thuốc Thanh Thương",
   "address": "16, Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2009,
   "Name": "Quầy thuốc Kiều Hạnh",
   "address": "131, Khu 2, ấp Hưng Nghĩa, xã Hưng Lộc, huyện thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 2010,
   "Name": "Quầy thuốc Bảo Linh",
   "address": "Ấp 6, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.813407,
   "Latitude": 106.9396
 },
 {
   "STT": 2011,
   "Name": "Quầy thuốc Gia Bảo",
   "address": "168 ấp Hưng Nhơn, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai ",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 2012,
   "Name": "Quầy thuốc Hà Lợi",
   "address": "Ấp Tây Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai ",
   "Longtitude": 10.8614417,
   "Latitude": 107.3714339
 },
 {
   "STT": 2013,
   "Name": "Quầy thuốc Gia Huy",
   "address": "02 Lê Duẫn, KP 4, TT Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9442736,
   "Latitude": 107.0120033
 },
 {
   "STT": 2014,
   "Name": "Quầy thuốc Hoa Sơn",
   "address": "579 Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2015,
   "Name": "Quầy thuốc Tâm Thư",
   "address": "242, ấp Bảo Thị, xã Xuân Định, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8825811,
   "Latitude": 107.2646659
 },
 {
   "STT": 2016,
   "Name": "Quầy thuốc Hoàng Duyên",
   "address": "E3/51, ấp Lê Lợi II, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9749244,
   "Latitude": 107.1471299
 },
 {
   "STT": 2017,
   "Name": "Quầy thuốc Thanh Thanh 3",
   "address": "Tổ 7, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 2018,
   "Name": "Quầy thuốc Hiệp Long",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2019,
   "Name": "Nhà thuốc Phòng khám đa khoa Quốc tế Mỹ Đức",
   "address": "392 Bùi Văn Hòa, tổ 20, KP 3, phường Long Bình Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.902515,
   "Latitude": 106.8899412
 },
 {
   "STT": 2020,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "52B/3 ấp Nhân Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2021,
   "Name": "Quầy thuốc Qúy Bình",
   "address": "351, tổ 28, ấp Thái Hòa, xã Hố Nai 3, huyện trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9922594,
   "Latitude": 106.8700238
 },
 {
   "STT": 2022,
   "Name": "Nhà Thuốc Hòa Hiệp",
   "address": "9/11D, KP 2, phường Trảng Dài, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 2023,
   "Name": "Quầy thuốc Tâm Nghĩa",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 2024,
   "Name": "Quầy thuốc Duy Tâm",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2025,
   "Name": "Quầy thuốc ",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2026,
   "Name": "Quầy thuốc ",
   "address": "154, Hoàng Minh Chánh, ấp An Hòa, xã Hóa An, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9362136,
   "Latitude": 106.7970025
 },
 {
   "STT": 2027,
   "Name": "Nhà Thuốc Anh Tùng",
   "address": "226,tổ 17, KP 3, Phường Long Bình Tân,  thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916461,
   "Latitude": 106.8502418
 },
 {
   "STT": 2028,
   "Name": "Quầy thuốc Anh Phát",
   "address": "510, đường 1, ấp 1, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8286472,
   "Latitude": 107.5774815
 },
 {
   "STT": 2029,
   "Name": "Quầy thuốc Long Phước",
   "address": "Tổ 12, ấp Xóm Gò, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2030,
   "Name": "Quầy thuốc ",
   "address": "G143, KP7, Phường Long Bình,  thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9360576,
   "Latitude": 106.907768
 },
 {
   "STT": 2031,
   "Name": "Quầy thuốc Bảo Tín",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2032,
   "Name": "Quầy thuốc Thịnh Phát Đạt",
   "address": "Ấp 3A,  xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8461842,
   "Latitude": 107.5092529
 },
 {
   "STT": 2033,
   "Name": "Quầy thuốc T. Liễu",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2034,
   "Name": "CS BL TP TTDL HTX SXTMDV Bình Đa",
   "address": "201, Vũ Hồng Phô, KP 3, phường Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9340846,
   "Latitude": 106.8635334
 },
 {
   "STT": 2035,
   "Name": "Cơ sở bán lẻ thuốc Đông Y, thuốc từ dược liệu Chấn hưng Đường",
   "address": "Tổ 20, khu Văn Hải, thị trấn Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7819755,
   "Latitude": 106.945763
 },
 {
   "STT": 2036,
   "Name": "Cơ sở bán lẻ thuốc Đông Y, thuốc từ dược liệu Ngọc Khánh An",
   "address": "D363, tổ 8 , KP 4, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9100971,
   "Latitude": 106.8831153
 },
 {
   "STT": 2037,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu  Nguyên Thảo",
   "address": "5/46, Kp7, Hố Nai, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9890275,
   "Latitude": 106.8296372
 },
 {
   "STT": 2038,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu An Thảo",
   "address": "E 582, tổ 18, Kp5A, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 2039,
   "Name": "Cơ sở bán lẻ thuốc đông y, thuốc từ dược liệu  Hương Ngọc Ánh",
   "address": "Tổ 25, KP 4, Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9980636,
   "Latitude": 106.8626176
 },
 {
   "STT": 2040,
   "Name": "Nhà thuốc Kim Tuyến",
   "address": "T8B, ấp Đồng Nai, Xã Hóa An, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 2041,
   "Name": "Nhà thuốc Thanh Hoan",
   "address": "92, tổ 17, khu phố 5, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 2042,
   "Name": "Nhà thuốc Hoa Sen",
   "address": "19/4, khu phố 7, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9632068,
   "Latitude": 106.9111885
 },
 {
   "STT": 2043,
   "Name": "Quầy thuốc Minh Khang",
   "address": "khu 6, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9366404,
   "Latitude": 107.4055621
 },
 {
   "STT": 2045,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "Ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9653,
   "Latitude": 106.922
 },
 {
   "STT": 2046,
   "Name": "Quầy thuốc Minh Tiến",
   "address": "209/01, Dốc Mơ 1, xã  Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 2047,
   "Name": "Quầy thuốc Phương Oanh",
   "address": "2198/17, ấp Phương Mai 1, xã Phú Lâm, huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 2048,
   "Name": "Quầy thuốc Thành An",
   "address": "Tổ 2, ấp 5, xã Nam Cát Tiên, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.4207108,
   "Latitude": 107.4410775
 },
 {
   "STT": 2049,
   "Name": "Quầy thuốc Hưng Thịnh",
   "address": "12 Ngô Quyền, ấp Bảo Vinh, xã Bảo Vinh, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9376706,
   "Latitude": 107.2671025
 },
 {
   "STT": 2050,
   "Name": "Quầy thuốc Võ Dương",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2051,
   "Name": "Quầy thuốc Minh Đan",
   "address": "Ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 2052,
   "Name": "Nhà thuốc  Hữu Nghị",
   "address": "22, khu phố 6, phường Tân Tiến thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548786,
   "Latitude": 106.8499534
 },
 {
   "STT": 2053,
   "Name": "Quầy thuốc",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch,tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2054,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "103/05, ấp Trường An, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 11.0545417,
   "Latitude": 107.0946266
 },
 {
   "STT": 2055,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "Khu phố 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 2056,
   "Name": "Nhà thuốc Phượng Tiến",
   "address": "45, Tổ 41B, khu phố 8, phường Tân Phong, thành phố Biên Hòa,tỉnh Đồng Nai",
   "Longtitude": 10.9683726,
   "Latitude": 106.9123465
 },
 {
   "STT": 2057,
   "Name": "Quầy thuốc Minh Châu",
   "address": "2495, tổ 3, ấp Phú Thành, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 2058,
   "Name": "Quầy thuốc Đức Hạnh",
   "address": "Tổ 8, ấp Thiên Bình, Xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2059,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bình Phú, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 2060,
   "Name": "Quầy thuốc Kim Nhung",
   "address": "Ấp Vàm, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 2061,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2062,
   "Name": "Quầy thuốc Ngọc Như",
   "address": "Số 2624, ấp Phú Dũng, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 2063,
   "Name": "Quầy thuốc - Công ty cổ phần Dược Đồng Nai",
   "address": "221B, Phạm Văn Thuận, phường Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9577709,
   "Latitude": 106.8322585
 },
 {
   "STT": 2064,
   "Name": "Quầy thuốc Hoàng Nhung  ",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2065,
   "Name": "Quầy thuốc Hoàng Yến ",
   "address": "Tây Lạc, An Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9641892,
   "Latitude": 106.9480419
 },
 {
   "STT": 2066,
   "Name": "Quầy thuốc ",
   "address": "2009E, tổ 8, ấp 5,xã Thạnh Phú, huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2067,
   "Name": "Quầy thuốc Thanh Huyền",
   "address": "Ấp Sông Mây,  xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 2068,
   "Name": "Nhà thuốc Minh Anh",
   "address": "Số nhà Y12, ,khu phố 4, phườngTân Hiệp, thành phố  Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9709669,
   "Latitude": 106.871678
 },
 {
   "STT": 2069,
   "Name": "Quầy thuốc My Hồng ",
   "address": "24/11 Mạc Đỉnh Chi, khu phố 10, phường An Bình, thành phố  Biên Hòa, tỉnh Đồng  Nai",
   "Longtitude": 10.9054991,
   "Latitude": 106.8468764
 },
 {
   "STT": 2070,
   "Name": "Nhà thuốc Mai Chi",
   "address": "70, Nguyễn Ái Quốc, khu phố 6, phường Tân Tiến, thành phố  Biên Hòa, tỉnh Đồng  Nai",
   "Longtitude": 10.9652019,
   "Latitude": 106.8477029
 },
 {
   "STT": 2071,
   "Name": "Quầy thuốc Phượng",
   "address": "Ấp Hòa Bình, xã Vĩnh Thanh, huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 2072,
   "Name": "Nhà thuốc Ngọc Thu Nhi",
   "address": "15, Nguyễn Thành Phương , khu phố 6, phường Thống Nhất, thành phố  Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9392923,
   "Latitude": 106.8307459
 },
 {
   "STT": 2073,
   "Name": "Quầy thuốc Hoa Kim",
   "address": "113/24, Tổ 16, ấp An Hòa, xã Hóa An, thành phố  Biên Hòa, Tỉnh Đồng Nai ",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 2074,
   "Name": "Quầy thuốc ",
   "address": "5/8, Ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7670666,
   "Latitude": 106.9592287
 },
 {
   "STT": 2075,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "393, Tổ 12, Ấp 1, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 2076,
   "Name": "Nhà thuốc Nhất Vân",
   "address": "32A/4 Phạm Văn Thuận, KP8, phường Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9413182,
   "Latitude": 106.8623827
 },
 {
   "STT": 2077,
   "Name": "Quầy thuốc ",
   "address": "Tổ 14, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0159709,
   "Latitude": 106.8355372
 },
 {
   "STT": 2078,
   "Name": "Quầy thuốc ",
   "address": "Kios số A 50 Chợ Long Khánh, đường Hùng Vương, khu phố 1, phường Xuân Trung, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9374243,
   "Latitude": 107.2408436
 },
 {
   "STT": 2079,
   "Name": "Quầy thuốc Tam Phước",
   "address": "Số 15, Ấp 3, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8556393,
   "Latitude": 106.9370717
 },
 {
   "STT": 2080,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "137 thôn Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2081,
   "Name": "Quầy thuốc Thiên Huế",
   "address": "34, tổ 23, khu phố 6, phường Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9635413,
   "Latitude": 106.9111398
 },
 {
   "STT": 2082,
   "Name": "Quầy thuốc Hạnh Dung",
   "address": "Tổ 61, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 2083,
   "Name": "Quầy thuốc Nguyễn Minh",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2084,
   "Name": "Quầy thuốc công ty TNHH phòng khám đa khoa quốc tế Long Bình - chi nhánh Bàu Xéo",
   "address": "Số 1, quốc lộ 1, ấp Tân Đat, xã Đồi 61, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 2085,
   "Name": "Quầy thuốc Bình An",
   "address": "Tổ 2, ấp 10, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 2086,
   "Name": "Quầy thuốc Hạnh Trang",
   "address": "Ấp 5, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7421318,
   "Latitude": 107.3493185
 },
 {
   "STT": 2087,
   "Name": "Quầy thuốc phòng khám đa khoa Sài Gòn Tâm Trí",
   "address": "Ấp Việt Kiều, xã  Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9091923,
   "Latitude": 107.381107
 },
 {
   "STT": 2088,
   "Name": "Quầy thuốc Trân Châu",
   "address": "83 Quang Trung, phường Xuân Hòa, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9246388,
   "Latitude": 107.2447038
 },
 {
   "STT": 2089,
   "Name": "Nhà thuốc Nguyên Duy",
   "address": "69/2, tổ 6, khu phố 1, Đồng Khởi, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9672235,
   "Latitude": 106.8608004
 },
 {
   "STT": 2090,
   "Name": "Quầy thuốc",
   "address": "212, tổ 3, ấp 2, Phước Bình, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8902353,
   "Latitude": 106.8584837
 },
 {
   "STT": 2091,
   "Name": "Quầy thuốc Phượng Hồng",
   "address": "Tổ 10, ấp 3, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 2092,
   "Name": "Nhà thuốc Thảo Minh Kha",
   "address": "36A/4, Khu phố 6,  phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9499079,
   "Latitude": 106.8478422
 },
 {
   "STT": 2093,
   "Name": "Quầy thuốc Hoàng An",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 2094,
   "Name": "Quầy thuốc Đức Hòa",
   "address": "Ấp Bà Trường, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6468256,
   "Latitude": 106.9363446
 },
 {
   "STT": 2095,
   "Name": "Quầy thuốc Phúc Lộc",
   "address": "567, ấp 5, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 2096,
   "Name": "Quầy thuốc Lê Kim Hiền",
   "address": "Ấp Giồng Ông Đông, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 2097,
   "Name": "Quầy thuốc Lê Minh Quang ",
   "address": "Số 7, khu 4, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 2098,
   "Name": "Nhà thuốc Số 1 - Công ty TNHH MTV Dược phâm Phúc An Nguyên",
   "address": "QL51,Khu phố 7, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8976109,
   "Latitude": 106.8840826
 },
 {
   "STT": 2099,
   "Name": "Nhà thuốc Anh Khoa",
   "address": " SN 30, Thích Quảng Đức, khu phố 2, phường Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.929261,
   "Latitude": 107.245825
 },
 {
   "STT": 2100,
   "Name": "Quầy thuốc Tiến Đạt",
   "address": "141/3IT, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 2101,
   "Name": "Nhà thuốc Sơn Minh - Tân Hiệp",
   "address": "Y4 Đồng Khởi, tổ 4, khu phố 4, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2102,
   "Name": "Quầy thuốc Nguyệt Nga",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2103,
   "Name": "Quầy thuốc Nhựt Hoàng",
   "address": "26/1/2, ấp Tân Việt, xã Bàu Hàm, huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9806475,
   "Latitude": 107.1070402
 },
 {
   "STT": 2104,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Ấp Đức Thắng 1, xã Túc Trưng, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 2105,
   "Name": "Quầy thuốc phòng khám đa khoa phước Thiền",
   "address": "Ấp Chợ, Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2106,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 2107,
   "Name": "Quầy thuốc Ngọc Thìn",
   "address": "Tổ 11, đường Tạ Uyên, khu Phước Thuận, TT Long Thành, Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7782702,
   "Latitude": 106.9512967
 },
 {
   "STT": 2108,
   "Name": "Quầy thuốc Ngọc Phát",
   "address": "363, Ấp Bảo Vệ, xã Giang Điền, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.912027,
   "Latitude": 106.9860384
 },
 {
   "STT": 2109,
   "Name": "Quầy thuốc Phương Nam",
   "address": "14, Ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom,tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 2110,
   "Name": "Quầy thuốc Nam Anh",
   "address": "1A/3, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2111,
   "Name": "Quầy thuốc Nhân Đức",
   "address": "Ấp Tam Bung, xã  Phú Cường, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0849702,
   "Latitude": 107.190277
 },
 {
   "STT": 2112,
   "Name": "Quầy thuốc Vân Anh",
   "address": "Tổ 18, Khu Văn Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2113,
   "Name": "Cơ sở bán lẻ thuốc Đông Y, thuốc từ dược liệu Vạn An",
   "address": "Ấp Cát Lái, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 2114,
   "Name": "Cơ sở bán lẻ thuốc Đông Y, thuốc từ dược liệu Huy Chương",
   "address": "Ấp Phước Lương, xã  Phú Hữu, huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7405091,
   "Latitude": 106.7902063
 },
 {
   "STT": 2115,
   "Name": "Cơ sở chuyên bán lẻ dược liệu,  thuốc  dược liệu,thuốc cổ truyền Hiếu Minh Tâm",
   "address": "Tổ 32, khu phố 3,  đường Nguyễn Văn Tiên, phườngTrảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9737069,
   "Latitude": 106.8861581
 },
 {
   "STT": 2116,
   "Name": "Cơ sở chuyên bán lẻ dược liệu,  thuốc  dược liệu,thuốc cổ truyền Nam Anh",
   "address": "40 Lý Văn Sâm, khu phố 6, phường Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.953785,
   "Latitude": 106.8572475
 },
 {
   "STT": 2117,
   "Name": "Quầy thuốc Minh Thư",
   "address": "108/4D đường Chợ Chiều, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9594425,
   "Latitude": 106.9383417
 },
 {
   "STT": 2118,
   "Name": "Quầy thuốc Thanh Tình",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 2119,
   "Name": "Quầy thuốc Hồng Ân",
   "address": "951,tổ7, ấp Cẩm Đường, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 2120,
   "Name": "Quầy thuốc Thu Sương",
   "address": "011, ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6776844,
   "Latitude": 107.0277678
 },
 {
   "STT": 2121,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Ấp 01, xã Sông Nhạn, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8486711,
   "Latitude": 107.1178418
 },
 {
   "STT": 2122,
   "Name": "Quầy thuốc ",
   "address": "18, đường 30-4, phường Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9506896,
   "Latitude": 106.822708
 },
 {
   "STT": 2123,
   "Name": "Quầy thuốc ",
   "address": "Số 799A, , ấp Ngũ Phúc, xâ Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 2124,
   "Name": "Quầy thuốc ",
   "address": "Số 122, đường Khổng Tử, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9362725,
   "Latitude": 107.2464204
 },
 {
   "STT": 2125,
   "Name": "Nhà thuốc  Khánh Thy",
   "address": "34/2, Bùi Trọng Nghĩa, khu phố 3, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9737069,
   "Latitude": 106.8861581
 },
 {
   "STT": 2126,
   "Name": "Quầy thuốc Huyền Cường",
   "address": "Tổ 7, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2127,
   "Name": "Nhà thuốc Hiệu Nga",
   "address": "19/23, tổ 7, khu phố 1,  phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.967224,
   "Latitude": 106.8608004
 },
 {
   "STT": 2128,
   "Name": "Quầy thuốc ",
   "address": "395, ấp 2, xã  Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8694548,
   "Latitude": 107.0899432
 },
 {
   "STT": 2129,
   "Name": "Quầy thuốc Hạ Vy",
   "address": "Ấp 6, xã  Xuân Tâm,huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 2130,
   "Name": "Quầy thuốc Minh Phụng",
   "address": "Ấp Phú Sơn, xã  Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2131,
   "Name": "Quầy thuốc Tiến Hương",
   "address": "Ấp 3, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2098918,
   "Latitude": 107.4450231
 },
 {
   "STT": 2132,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "Số 03, phố 2, ấp 1, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2629318,
   "Latitude": 107.3730563
 },
 {
   "STT": 2133,
   "Name": "Quầy thuốc ",
   "address": "83/1, ấp 2, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 2134,
   "Name": "Nhà thuốc Thanh Thủy",
   "address": "443A/10, , ấp Lập Thành, xã  Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9523587,
   "Latitude": 107.1439297
 },
 {
   "STT": 2135,
   "Name": "Quầy thuốc công ty TNHH phòng khám đa khoa Lê Thiện Nhân",
   "address": "20, đường Đồng Khởi, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0085276,
   "Latitude": 106.8554573
 },
 {
   "STT": 2136,
   "Name": "Quầy thuốc ",
   "address": "Ấp Đất Mới, xã  Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 2137,
   "Name": "Quầy thuốc ",
   "address": "A429, khu phố 1, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9674476,
   "Latitude": 106.860912
 },
 {
   "STT": 2138,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "220, ấp 4,tổ 28, xã  Tam An, huyện  Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 2139,
   "Name": "Quầy thuốc Bình Minh",
   "address": "Ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2140,
   "Name": "Quầy thuốc ",
   "address": "Số 319, đường Trần Phú, phường Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9340737,
   "Latitude": 107.2524312
 },
 {
   "STT": 2141,
   "Name": "Quầy thuốc Tuấn Kim",
   "address": "E284, tổ 9, khu phố 5A, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9759241,
   "Latitude": 106.8693359
 },
 {
   "STT": 2142,
   "Name": "Quầy thuốc ",
   "address": "Lô số 8, Nguyễn Thị Tồn, khu phố 2, phường Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9387766,
   "Latitude": 106.813618
 },
 {
   "STT": 2143,
   "Name": "Nhà thuốc Bảo Dung",
   "address": "162A, Đồng Khởi, tổ 30, khu phố 2, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2144,
   "Name": "Quầy thuốc Thanh Liễu",
   "address": "387, ấp 2, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 2145,
   "Name": "Quầy thuốc Phương",
   "address": "532, tổ 2, khu 5, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 2146,
   "Name": "Nhà thuốc Khôi Thoa",
   "address": "Tổ 16,  khu phố 2, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9672283,
   "Latitude": 106.8607999
 },
 {
   "STT": 2147,
   "Name": "Quầy thuốc ",
   "address": "152, khu phố 1, phường Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9695727,
   "Latitude": 106.8939686
 },
 {
   "STT": 2148,
   "Name": "Quầy thuốc Cát Phượng",
   "address": "Khu phố 3, phường An Phước, huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8355792,
   "Latitude": 106.9417632
 },
 {
   "STT": 2149,
   "Name": "Quầy thuốc Phú Thọ ",
   "address": "92, Võ Thị Sáu, phường Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9464475,
   "Latitude": 106.8253111
 },
 {
   "STT": 2150,
   "Name": "Quầy thuốc Minh Dung ",
   "address": "90, Trần Quốc Toản, phường Bình Đa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9395515,
   "Latitude": 106.8618107
 },
 {
   "STT": 2151,
   "Name": "Quầy thuốc Cẩm Nguyên ",
   "address": "Tổ 3, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2152,
   "Name": "Quầy thuốc Phương Minh",
   "address": "F3, khu phố 4, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9731501,
   "Latitude": 106.9079379
 },
 {
   "STT": 2153,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "81, tổ 2, ấp Phú Hợp A, xã Phú Bình, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3670471,
   "Latitude": 107.3923532
 },
 {
   "STT": 2154,
   "Name": "Quầy thuốc Ngọc Phượng",
   "address": "1/7, ấp 1, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1633263,
   "Latitude": 107.2869762
 },
 {
   "STT": 2155,
   "Name": "Quầy thuốc Hồng Hạnh",
   "address": "166, khu phố Hiệp Nhất, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1076386,
   "Latitude": 107.164507
 },
 {
   "STT": 2156,
   "Name": "Quầy thuốc Hồng Huệ",
   "address": "173, tổ 3, khu phố Hiệp Nhất, TT Định Quán huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2157,
   "Name": "Quầy thuốc ",
   "address": "03, Nam Hòa, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2158,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Suối Trầu, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7639182,
   "Latitude": 107.04988
 },
 {
   "STT": 2159,
   "Name": "Quầy thuốc Hồng Nhung ",
   "address": "Khu 4, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.91344,
   "Latitude": 107.398139
 },
 {
   "STT": 2160,
   "Name": "Quầy thuốc Minh Châu ",
   "address": "494 Hùng Vương Khu 2, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9350159,
   "Latitude": 107.405053
 },
 {
   "STT": 2161,
   "Name": "Quầy thuốc Thiên Phúc ",
   "address": "Chợ Sông Ray, ấp 1, xã Sông Ray , huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7364074,
   "Latitude": 107.3510492
 },
 {
   "STT": 2162,
   "Name": "Quầy thuốc Tuấn Ánh ",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2163,
   "Name": "Quầy thuốc ",
   "address": "Ấp Vũng Gấm, xã Phước An,huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 2164,
   "Name": "Quầy thuốc Nhật Anh ",
   "address": "Tổ 4, khu A, ấp 5, xã An Phước,huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2165,
   "Name": "Quầy thuốc Le Nhung ",
   "address": "Ấp Phú Mỹ 2, xã  Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 2166,
   "Name": "Quầy thuốc Thiên Y ",
   "address": "Tổ 12, ấp Xóm Gò- Bà Ký, Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2167,
   "Name": "Quầy thuốc ",
   "address": "1749, tổ 1, ấp Hiền Hòa, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6860717,
   "Latitude": 107.0411054
 },
 {
   "STT": 2168,
   "Name": "Quầy thuốc Trần Nhựt Phượng ",
   "address": "B2/030,ấp Lạc Sơn, xã  Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 2169,
   "Name": "Quầy thuốc Trung Đức",
   "address": "336, tổ 30, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 2170,
   "Name": "Quầy thuốc Hồng Phượng",
   "address": "Số 30, tổ 1, khu phố 114, TT Định Quán,huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2171,
   "Name": "Quầy thuốc ",
   "address": "153, Bùi Trọng Nghĩa, khu phố 2, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9768669,
   "Latitude": 106.8546924
 },
 {
   "STT": 2172,
   "Name": "Quầy thuốc ",
   "address": "29/5, tổ 3, khu phố 1, phường Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9530495,
   "Latitude": 106.8022768
 },
 {
   "STT": 2173,
   "Name": "Quầy thuốc Hoàng Trung Quân",
   "address": "53, Phan Đình Phùng, phường Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9544551,
   "Latitude": 106.8180359
 },
 {
   "STT": 2174,
   "Name": "Quầy thuốc Thiên Phúc",
   "address": "90B, tổ 14, khu 5,đường Trà Cổ , Tân Phú, Đồng Nai",
   "Longtitude": 11.2310735,
   "Latitude": 107.4402467
 },
 {
   "STT": 2175,
   "Name": "Quầy thuốc ",
   "address": "110/4A, khu 2, Thanh Hóa, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9646665,
   "Latitude": 106.9394712
 },
 {
   "STT": 2176,
   "Name": "Quầy thuốc Phương Nguyên",
   "address": "Ấp Bến Sắn, Phước Thiền,  Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2177,
   "Name": "Quầy thuốc Võ Lý ",
   "address": "454, tổ 4, ấp 5,  Trà Cổ, Tân Phú, Đồng Nai",
   "Longtitude": 11.2426589,
   "Latitude": 107.4381106
 },
 {
   "STT": 2178,
   "Name": "Quầy thuốc Hải Hưng",
   "address": "Tổ 2, ấp 5, Nam Cát Tiên, Tân Phú,  Đồng Nai",
   "Longtitude": 11.4207108,
   "Latitude": 107.4410775
 },
 {
   "STT": 2179,
   "Name": "Nhà thuốc Hiệp Bình",
   "address": "66/3, khu phố 4, Tân Biên, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.9720555,
   "Latitude": 106.9097673
 },
 {
   "STT": 2180,
   "Name": "Quầy thuốc Phương Uyên",
   "address": "Số 344C, ấp Tân Bắc, Bình Minh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 2181,
   "Name": "Quầy thuốc ",
   "address": "A1/006, Lê Lợi, Quang Trung, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9749244,
   "Latitude": 107.1471299
 },
 {
   "STT": 2182,
   "Name": "Quầy thuốc Mỹ Lệ",
   "address": "Ấp 4, An Viễn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.8715133,
   "Latitude": 106.9770427
 },
 {
   "STT": 2183,
   "Name": "Quầy thuốc Ngọc Linh ",
   "address": "3182, tổ 3, ấp Phú Lợi, Phú Trung, Tân Phú, Đồng Nai",
   "Longtitude": 11.5673841,
   "Latitude": 107.3576689
 },
 {
   "STT": 2184,
   "Name": "Quầy thuốc Kim Khánh ",
   "address": "3138, ấp Việt Kiều, Suối Cát, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.9083761,
   "Latitude": 107.3669958
 },
 {
   "STT": 2185,
   "Name": "Quầy thuốc Thái Bình ",
   "address": "85, Nguyễn Văn Cừ, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7795678,
   "Latitude": 106.9488707
 },
 {
   "STT": 2186,
   "Name": "Quầy thuốc Nhật Linh ",
   "address": "3, Phố 2, Ấp 2, Phú Vinh, Định Quán,  Đồng Nai",
   "Longtitude": 11.2374121,
   "Latitude": 107.3434916
 },
 {
   "STT": 2187,
   "Name": "Nhà thuốc Tuyết Minh Minh",
   "address": "196/20, Phạm Văn Thuận, khu phố 2, Tam Hiệp, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.9458111,
   "Latitude": 106.8568046
 },
 {
   "STT": 2188,
   "Name": "Quầy thuốc ",
   "address": "532, Ngũ Phúc, Hố Nai 3, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9662627,
   "Latitude": 106.920893
 },
 {
   "STT": 2189,
   "Name": "Nhà thuốc Bình Sơn",
   "address": "Ấp Phước Lý, Đại Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 2190,
   "Name": "Nhà thuốc 134",
   "address": "46, QL 51, KP Bình Dương, Long Bình Tân, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.904836,
   "Latitude": 106.848942
 },
 {
   "STT": 2191,
   "Name": "Nhà thuốc An An Khang",
   "address": "Tổ 11, ấp Long Đức, Tam Phước, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2192,
   "Name": "Quầy thuốc Đức Hiếu ",
   "address": "Tổ 4, ấp Hiền Hòa, Phước Thái, Long Thành, Đồng Nai",
   "Longtitude": 10.6792113,
   "Latitude": 107.0249535
 },
 {
   "STT": 2193,
   "Name": "Quầy thuốc ",
   "address": "97, đường số 1, tổ 13, khu AB, ấp Trung tâm, Xuân Lập, TX.Long Khánh, Đồng Nai",
   "Longtitude": 10.9092302,
   "Latitude": 107.1779936
 },
 {
   "STT": 2194,
   "Name": "Quầy thuốc Thanh Quyên",
   "address": "Ấp 1, Phú Thạnh, Nhơn Trạch, Đồng Nai ",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 2195,
   "Name": "Quầy thuốc Kim Ly",
   "address": "Ấp Bến Cam, Phước Thiền, Nhơn Trạch, Đồng Nai ",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 2196,
   "Name": "Quầy thuốc Ngọc Diệp",
   "address": "2520,tổ 5, ấp Phú Dũng, Phú Bình, Tân Phú, Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 2197,
   "Name": "Quầy thuốc Thanh Lợi",
   "address": "Tổ 9, ấp 6, Phú Thịnh, Tân Phú, Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 2198,
   "Name": "Quầy thuốc Hữu Tâm",
   "address": "Kiot số 1, chợ Phan Bội Châu, Bàu Hàm 2, Thống Nhất, Đồng Nai",
   "Longtitude": 10.9335178,
   "Latitude": 107.137651
 },
 {
   "STT": 2199,
   "Name": "Quầy thuốc Trọng Nghĩa",
   "address": "Tổ 5, ấp 4, Phú Thịnh, Tân Phú, Đồng Nai",
   "Longtitude": 11.3270239,
   "Latitude": 107.3967105
 },
 {
   "STT": 2200,
   "Name": "Nhà thuốc Hà Mỹ Anh",
   "address": "70/6Q, tổ 10, khu phố 2, An Bình, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9672255,
   "Latitude": 106.8608002
 },
 {
   "STT": 2201,
   "Name": "Quầy thuốc Kim Phát",
   "address": "702, ấp 2, Long An, Long Thành, Đồng Nai",
   "Longtitude": 10.7567666,
   "Latitude": 106.9889904
 },
 {
   "STT": 2202,
   "Name": "Quầy thuốc Sơn Hải",
   "address": "Ấp 3, Phú Lý, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.3203867,
   "Latitude": 107.1631115
 },
 {
   "STT": 2203,
   "Name": "Nhà thuốc Trang Trân",
   "address": "Tổ 29, khu phố 6, Tân Phong, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.9806362,
   "Latitude": 106.8430428
 },
 {
   "STT": 2204,
   "Name": "Quầy thuốc ",
   "address": "201, Phú hợp A, Phú Bình, Tân Phú, Đồng Nai",
   "Longtitude": 11.2602022,
   "Latitude": 107.5090974
 },
 {
   "STT": 2205,
   "Name": "Quầy thuốc PKĐK Trung Thanh",
   "address": "254, ấp Dốc Mơ 3, Gia Tân 1, Thống Nhất, Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 2206,
   "Name": "Quầy thuốc  Thanh Tuyền",
   "address": "Tây Lạc, ấp An Chu, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2207,
   "Name": "Quầy thuốc  Bảo Trân",
   "address": "054, tổ 3, ấp 5, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9903382,
   "Latitude": 107.0146791
 },
 {
   "STT": 2208,
   "Name": "Quầy thuốc  Kim Hằng",
   "address": "36, Khu phố 5, đường 3/2, TT Trảng Bom, Trảng Bom, Đồng Nai",
   "Longtitude": 10.7653413,
   "Latitude": 106.662725
 },
 {
   "STT": 2209,
   "Name": "Nhà thuốc Phúc Đức Long Bình",
   "address": "E287, tổ 6, khu phố 5, Long Bình, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.9539508,
   "Latitude": 106.8294081
 },
 {
   "STT": 2210,
   "Name": "Quầy thuốc  Thanh Hòa",
   "address": "04, phố 3, ấp 1, Phú Lợi, Định Quán, Đồng Nai",
   "Longtitude": 11.2067846,
   "Latitude": 107.3628036
 },
 {
   "STT": 2211,
   "Name": "Quầy thuốc  Bích Ngọc",
   "address": "Ấp 3, Phú Ngọc, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.0335306,
   "Latitude": 107.3165886
 },
 {
   "STT": 2212,
   "Name": "Nhà Thuốc Phùng Gia Phát",
   "address": "61, Nguyễn Văn Hoa, khu phố 2, Thống Nhất, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": 10.9468697,
   "Latitude": 106.8126645
 },
 {
   "STT": 2213,
   "Name": "Nhà Thuốc Minh Huyền",
   "address": "384, Bùi Trọng Nghĩa, tổ 5, khu phố 3, Trảng Dài, thành phố  Biên Hòa, Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2214,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền  Vũ Minh Huy",
   "address": "118A, tổ 39, khu phố 11, phường Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9818819,
   "Latitude": 106.8428624
 },
 {
   "STT": 2216,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền  Văn Đương",
   "address": "346/1, khu phố 1, phường Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9720552,
   "Latitude": 106.9097678
 },
 {
   "STT": 2217,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền   Hương Vy ",
   "address": "166,  khu phố 4,  phường Thống Nhất, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9508885,
   "Latitude": 106.8342509
 },
 {
   "STT": 2218,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền ",
   "address": "330, tổ 22, khu phố 3, phường  Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9626624,
   "Latitude": 106.9055456
 },
 {
   "STT": 2219,
   "Name": "Quầy thuốc Thiên Thanh",
   "address": "2/10, ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai ",
   "Longtitude": 10.7075262,
   "Latitude": 107.0022941
 },
 {
   "STT": 2220,
   "Name": "Quầy thuốc  Thanh Thoảng",
   "address": "Tổ 60,  ấp 5, xã Tam An,huyện Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 2221,
   "Name": "Nhà thuốc Hiền Lam",
   "address": "14/1, tổ 35A, KP11, Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9729979,
   "Latitude": 106.8237374
 },
 {
   "STT": 2222,
   "Name": "Quầy thuốc  Kim Thảo",
   "address": "331, ấp Tân Bảo, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8358264,
   "Latitude": 107.2935523
 },
 {
   "STT": 2223,
   "Name": "Nhà thuốc Nhân Hiếu",
   "address": "234, KP 2, Hoàng Diệu, Xuân Thanh, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9346871,
   "Latitude": 107.252934
 },
 {
   "STT": 2224,
   "Name": "Quầy thuốc  ",
   "address": "1681, tổ 28, ấp Vườn Dừa, Xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2225,
   "Name": "Quầy thuốc Ngọc Minh Châu  ",
   "address": "Ấp 3, Phú Thạnh, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 2226,
   "Name": "Quầy thuốc  Minh Phát",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 2227,
   "Name": "Quầy thuốc  Đông Hưng",
   "address": "Ấp Bến Cộ, Xã Đại Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7437649,
   "Latitude": 106.826849
 },
 {
   "STT": 2228,
   "Name": "Nhà thuốc An Phú",
   "address": "Lô 123, KP 7, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 2229,
   "Name": "Nhà thuốc Ngọc Tuyền",
   "address": "Số 7, KP 2, Tân Phong, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 2230,
   "Name": "Quầy thuốc  Diệu Anh",
   "address": "Ấp 5, Hiệp Phước, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7593232,
   "Latitude": 106.9425975
 },
 {
   "STT": 2231,
   "Name": "Quầy thuốc  Hồng Hạnh",
   "address": "Số 2, ấp Tân Bình, Bình Minh, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9560993,
   "Latitude": 106.9733
 },
 {
   "STT": 2232,
   "Name": "Quầy thuốc  ",
   "address": "10/A, ấp An Viễng, Bình An, Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8404645,
   "Latitude": 107.0480378
 },
 {
   "STT": 2233,
   "Name": "Quầy thuốc Đoàn Hương ",
   "address": "Ấp Bến Sắn, Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2234,
   "Name": "Nhà thuốc Mỹ Châu Lộc",
   "address": "109/4B, Trần Quốc Toản, KP1, An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9394961,
   "Latitude": 106.8615706
 },
 {
   "STT": 2235,
   "Name": "Quầy thuốc Khôi Nguyên ",
   "address": "Ấp Lò Than, Bảo Bình, Cẩm Mỹ, Đồng Nai ",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2236,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Tổ 17,  ấp Xóm Gò, Bà Ký, Long Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2237,
   "Name": "Nhà thuốc Thiện Nhân",
   "address": "73B/5, KP 9,Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 2238,
   "Name": "Quầy thuốc Thuận Hương",
   "address": "71/4B, KP 1,Tân Mai, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9593993,
   "Latitude": 106.8481135
 },
 {
   "STT": 2239,
   "Name": "Quầy thuốc Văn Hiệu",
   "address": "Tổ 9,  ấp Đất Mới, Long Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 2240,
   "Name": "Quầy thuốc Hải Vân",
   "address": "Lô A1, khu tái định cư,  Long An, Long Thành, Đồng Nai",
   "Longtitude": 10.7568564,
   "Latitude": 106.9794813
 },
 {
   "STT": 2241,
   "Name": "Quầy thuốc công ty TNHH phòng khám đa khoa Tam Phước",
   "address": "592, ấp Long Đức, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2242,
   "Name": "Quầy thuốc ",
   "address": "144/KB, Hưng Long, Hưng Thịnh, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2243,
   "Name": "Bán buôn thuốc, nguyên liệu làm thuốc",
   "address": "N3/12, quốc lộ 51, P. Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8966027,
   "Latitude": 106.8631189
 },
 {
   "STT": 2244,
   "Name": "Quầy thuốc Đức Thành",
   "address": "Ấp 4, Xuân Bắc, Xuân Lộc, Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 2245,
   "Name": "Quầy thuốc Đức Phương",
   "address": "Ấp Quảng Phát, Quảng Tiến, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9500339,
   "Latitude": 106.9927351
 },
 {
   "STT": 2246,
   "Name": "Quầy thuốc Chấn Hưng",
   "address": "K11, chợ Túc Trưng, xã Túc Trưng, Định Quán, Đồng Nai",
   "Longtitude": 11.0889633,
   "Latitude": 107.1976667
 },
 {
   "STT": 2247,
   "Name": "Quầy thuốc ",
   "address": "Tổ 4, ấp 1, xã Thanh Sơn, Định Quán, Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 2248,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "KP3, TT Trảng Bom, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 2249,
   "Name": "Quầy thuốc Tân Triều",
   "address": "Số 15, Hương Lộ 9, ấp Tân Triều, Tân Bình, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 10.9964212,
   "Latitude": 106.7863139
 },
 {
   "STT": 2250,
   "Name": "Quầy thuốc ",
   "address": "Ấp 4, Hiệp Phước, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 2251,
   "Name": "Quầy thuốc Dũng Tuyết",
   "address": "Ấp 4, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 2252,
   "Name": "Quầy thuốc Quế Châu",
   "address": "Tổ 4, ấp 8, An Phước, Long Thành, Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 2253,
   "Name": "Quầy thuốc Bảo Nam",
   "address": "Ấp Bến Cam, Phước Thiền, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 2254,
   "Name": "Quầy thuốc ",
   "address": "36, Nguyễn Văn Cừ khu Phước Thuận, TT Long Thành, Long Thành, Đồng Nai",
   "Longtitude": 10.7782801,
   "Latitude": 106.9447146
 },
 {
   "STT": 2255,
   "Name": "Nhà thuốc Thanh Tùng",
   "address": "53/22, KP1, Tam Hiệp, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9419963,
   "Latitude": 106.8634047
 },
 {
   "STT": 2256,
   "Name": "Quầy thuốc Minh Thức",
   "address": "A1/19B, ấp Bắc Sơn, Quang Trung, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 2257,
   "Name": "Quầy thuốc ",
   "address": "Ấp 5, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2258,
   "Name": "Nhà thuốc Ngọc Duy",
   "address": "286, đường 9/4, KP 2, Xuân Thanh, TX. Long Khánh, Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2259,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Ấp Lợi Hà, Thanh Bình, Trảng Bom, Đồng Nai",
   "Longtitude": 11.083389,
   "Latitude": 107.0763866
 },
 {
   "STT": 2260,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "SN 14, ấp chợ, xã Phú Túc, Định Quán, Đồng Nai",
   "Longtitude": 11.0807749,
   "Latitude": 107.2252675
 },
 {
   "STT": 2261,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Ấp 5, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2262,
   "Name": "Quầy thuốc Sang Khang ",
   "address": "Số 45, ấp Sông Mây, Bắc Sơn, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9757101,
   "Latitude": 106.9589897
 },
 {
   "STT": 2263,
   "Name": "Quầy thuốc Kim Ngân Thanh",
   "address": "2/50, KP 7, P. Hố Nai, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.8916924,
   "Latitude": 106.8496478
 },
 {
   "STT": 2264,
   "Name": "Quầy thuốc Bắc Thần",
   "address": "Ấp Hòa Bình, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 2265,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Chợ Phú Hòa, Phú Hoà, Định Quán, Đồng Nai",
   "Longtitude": 11.2092762,
   "Latitude": 107.4319983
 },
 {
   "STT": 2266,
   "Name": "Quầy thuốc ",
   "address": "791, tổ 1, ấp 3, Phú Điền, Tân Phú, Đồng Nai",
   "Longtitude": 11.1983189,
   "Latitude": 107.4499404
 },
 {
   "STT": 2267,
   "Name": "Quầy thuốc Phương Thúy",
   "address": "Ấp 1, Sông Trầu, Trảng Bom, Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2268,
   "Name": "Quầy thuốc Hải Hường",
   "address": "13/1, KP6, Tam Hiệp, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.953785,
   "Latitude": 106.8572475
 },
 {
   "STT": 2269,
   "Name": "Quầy thuốc Khánh Ngân",
   "address": "Ấp 5, Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2270,
   "Name": "Quầy thuốc ",
   "address": "Ấp Suối Nhát, Xuân Đông, Cẩm Mỹ, Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2271,
   "Name": "Quầy thuốc Đức Thiện",
   "address": "Ấp 4, Phú Lý, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.2876108,
   "Latitude": 107.142274
 },
 {
   "STT": 2272,
   "Name": "Quầy thuốc Vĩnh Khang",
   "address": "Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 2273,
   "Name": "Quầy thuốc Hoàng Hải",
   "address": "441, tỉnh lộ 768, ấp Bình Chánh, Tân An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0514108,
   "Latitude": 106.9253621
 },
 {
   "STT": 2274,
   "Name": "Quầy thuốc Thận Ngân",
   "address": "Ấp Phú Mỹ, xã Xuân Lập, TX.Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 10.9118999,
   "Latitude": 107.1769324
 },
 {
   "STT": 2275,
   "Name": "Quầy thuốc ",
   "address": "Ấp Bình Chánh, Tân An, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.0457967,
   "Latitude": 106.929903
 },
 {
   "STT": 2276,
   "Name": "Quầy thuốc Phương Hà",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 2277,
   "Name": "Quầy thuốc Thảo Ngân",
   "address": "3/3, ấp An Bình, xã Trung Hòa, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9450388,
   "Latitude": 107.0623406
 },
 {
   "STT": 2278,
   "Name": "Quầy thuốc Hưng Thịnh",
   "address": "1015, tổ 11, ấp 2, Xuân Hưng, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8421826,
   "Latitude": 107.4913489
 },
 {
   "STT": 2279,
   "Name": "Nhà thuốc Phương Thụy",
   "address": "185, quốc lộ 1A, khu phố 6, Tân Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9675527,
   "Latitude": 106.9093056
 },
 {
   "STT": 2280,
   "Name": "Quầy thuốc Phúc An Khang",
   "address": "2521, ấp Thanh Hóa, Hố Nai 3, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 2281,
   "Name": "Quầy thuốc Ngọc Diễm",
   "address": "167, tổ 4, ấp Trảng Táo, Xuân Thành, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.962368,
   "Latitude": 107.489875
 },
 {
   "STT": 2282,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "3889, Phú Lâm 1, Phú Sơn, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2771519,
   "Latitude": 107.4943786
 },
 {
   "STT": 2283,
   "Name": "Quầy thuốc Hồng Phước ",
   "address": "166, quốc lộ 1A, Hưng Nhơn Hưng Lộc, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9367491,
   "Latitude": 107.1117908
 },
 {
   "STT": 2284,
   "Name": "Quầy thuốc Bắc Sâm ",
   "address": "Tổ 6, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2285,
   "Name": "Quầy thuốc Mỹ Trinh",
   "address": "690, ấp Thọ Lộc, Xuân Thọ, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9591637,
   "Latitude": 107.3494043
 },
 {
   "STT": 2286,
   "Name": "Quầy thuốc Giáp Trần",
   "address": "Ấp 1, Xuân Tâm, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8680729,
   "Latitude": 107.4665451
 },
 {
   "STT": 2287,
   "Name": "Nhà thuốc Nguyên Nhung",
   "address": "10H, Phan Đình Phùng, KP 1,Quang Vinh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9544098,
   "Latitude": 106.8148879
 },
 {
   "STT": 2288,
   "Name": "Quầy thuốc Như Thúy",
   "address": "Chợ Dầu Giây, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9491681,
   "Latitude": 107.1407791
 },
 {
   "STT": 2289,
   "Name": "Quầy thuốc Lâm Tuyên",
   "address": "Ấp Chính Nghĩa, Vĩnh Thanh, Nhơn Trạch, Đồng Nai",
   "Longtitude": 10.6780556,
   "Latitude": 106.8586111
 },
 {
   "STT": 2290,
   "Name": "Quầy thuốc Nhân Tâm",
   "address": "389A, đường Bình Minh, ấp Hòa Bình, Giang Điền, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9451841,
   "Latitude": 106.9804832
 },
 {
   "STT": 2291,
   "Name": "Quầy thuốc ",
   "address": "407, tổ 11, ấp Phan Bội Châu, xã Bàu Hàm II,  huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9207976,
   "Latitude": 107.1267271
 },
 {
   "STT": 2292,
   "Name": "Quầy thuốc ",
   "address": "07, Bàu Ao, Bàu Hàm 2, Thống Nhất,  Đồng Nai",
   "Longtitude": 10.9420157,
   "Latitude": 107.1340273
 },
 {
   "STT": 2293,
   "Name": "Quầy thuốc Mai Phương",
   "address": "Tổ 2, ấp 7, Xuân Tâm, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.8274346,
   "Latitude": 107.3640375
 },
 {
   "STT": 2294,
   "Name": "Quầy thuốc Hân Linh",
   "address": "Ấp 5, Xuân Hưng, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.8426041,
   "Latitude": 107.4908983
 },
 {
   "STT": 2295,
   "Name": "Nhà thuốc Bảo Anh Khang",
   "address": "E19, KP 1, Bửu Long, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9609021,
   "Latitude": 106.7908395
 },
 {
   "STT": 2296,
   "Name": "Nhà thuốc Khánh Lê",
   "address": "19/86/9, tổ 15, KP 1, Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2297,
   "Name": "Quầy thuốc Thảo Hương",
   "address": "1438C, tổ 22, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2298,
   "Name": "Quầy thuốc Đại An",
   "address": "Khu phố 1, thị trấn Trảng Bom, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9521847,
   "Latitude": 106.9978466
 },
 {
   "STT": 2299,
   "Name": "Quầy thuốc Hoàng Mai",
   "address": "140, ấp 2, Cẩm Đường, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 2300,
   "Name": "Quầy thuốc Bảo Tín",
   "address": "Tổ 4, ấp 4, Long An, Long Thành, Tỉnh Đồng Nai",
   "Longtitude": 10.757452,
   "Latitude": 106.9881911
 },
 {
   "STT": 2301,
   "Name": "Quầy thuốc Tuyết Hạnh",
   "address": "224/4, Tân Yên, Gia Tân 3, Thống Nhất,  Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 2302,
   "Name": "Quầy thuốc Nhật Nam",
   "address": "Ấp 4, xã Thạnh Phú, Vĩnh Cửu, Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 2303,
   "Name": "Quầy thuốc Hồng Thắm",
   "address": "Phố 3, ấp 1, xã Phú Vinh, Định Quán, Đồng Nai",
   "Longtitude": 11.207014,
   "Latitude": 107.362402
 },
 {
   "STT": 2304,
   "Name": "Quầy thuốc Vạn Hạnh",
   "address": "64, Ngô Quyền, khu 4, TT Gia Ray, Xuân Lộc, Đồng Nai",
   "Longtitude": 10.91344,
   "Latitude": 107.398139
 },
 {
   "STT": 2305,
   "Name": "Quầy thuốc ",
   "address": "325/A2, ấp Nhị Hòa, Xã Hiệp Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.926897,
   "Latitude": 106.8160971
 },
 {
   "STT": 2306,
   "Name": "Quầy thuốc Thanh Dung",
   "address": "Ấp 1, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7503268,
   "Latitude": 106.9437108
 },
 {
   "STT": 2307,
   "Name": "Quầy thuốc Ngọc Loan",
   "address": "Ấp Bưng Cần, Bảo Hòa, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.880913,
   "Latitude": 107.3027384
 },
 {
   "STT": 2308,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Ấp 1, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7503268,
   "Latitude": 106.9437108
 },
 {
   "STT": 2309,
   "Name": "Quầy thuốc Hiền Đông",
   "address": "1954, tổ 1, ấp Thọ Lâm 1, Phú Thanh, Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.2354267,
   "Latitude": 107.4676861
 },
 {
   "STT": 2310,
   "Name": "Quầy thuốc Tâm Minh",
   "address": "229, Phạm Văn Thuận, khu phố 2, Tân Tiến, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9577719,
   "Latitude": 106.8411493
 },
 {
   "STT": 2311,
   "Name": "Quầy thuốc Hoàng Phúc Khang",
   "address": "1/46, KP8, An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9240197,
   "Latitude": 106.8665367
 },
 {
   "STT": 2312,
   "Name": "Quầy thuốc Tâm Ái",
   "address": "Km 05, ấp Sông Mây, xã Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9955825,
   "Latitude": 106.9661135
 },
 {
   "STT": 2313,
   "Name": "Quầy thuốc  Phúc An",
   "address": "Ấp 1, Hiệp Phước, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7503268,
   "Latitude": 106.9437108
 },
 {
   "STT": 2314,
   "Name": "Quầy thuốc  Thảo Nguyên",
   "address": "Ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai ",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2315,
   "Name": "Quầy thuốc  ",
   "address": "19/3, khu phố 4, An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9720555,
   "Latitude": 106.9097673
 },
 {
   "STT": 2316,
   "Name": "Quầy thuốc ",
   "address": "322/9, khu phố 1, Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9546017,
   "Latitude": 106.8250331
 },
 {
   "STT": 2317,
   "Name": "Quầy thuốc Hồng Yến",
   "address": "3647,quốc lộ 1A, ấp Hoà Bình, Bảo Hòa, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9037406,
   "Latitude": 107.3113588
 },
 {
   "STT": 2318,
   "Name": "Quầy thuốc Thanh Vinh",
   "address": "Số 12, tổ 1, khu phố 114, TT Định Quán, Định Quán, Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2319,
   "Name": "Quầy thuốc Ánh Tuyết",
   "address": "956, ấp Tân Hòa, Xuân Thành, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 2320,
   "Name": "Quầy thuốc Nguyễn Giỏi",
   "address": "38, Đỗ Văn Thi, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9367911,
   "Latitude": 106.8342466
 },
 {
   "STT": 2321,
   "Name": "Quầy thuốc Thủy Trà",
   "address": "711A, tổ 15, khu 4, ấp 1, xã An Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8916628,
   "Latitude": 106.8634886
 },
 {
   "STT": 2322,
   "Name": "Quầy thuốc Phú Linh",
   "address": "Ấp 5, An Viễn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.8873657,
   "Latitude": 106.9881102
 },
 {
   "STT": 2323,
   "Name": "Nhà thuốc Đức Trinh",
   "address": "11, khu phố 5, Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 2324,
   "Name": "Quầy thuốc Tam Tam Phước",
   "address": "27D, tổ 1, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2325,
   "Name": "Nhà thuốc Thanh Hải",
   "address": "F19, khu phố 2, Long Bình Tân , thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9664588,
   "Latitude": 106.860892
 },
 {
   "STT": 2326,
   "Name": "Quầy thuốc Công Luận",
   "address": "Ấp Tam Hiệp, Xuân Hiệp, Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 2327,
   "Name": "Quầy thuốc ",
   "address": "A1/008, Nguyễn Huệ 1, Quang Trung, Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.9919197,
   "Latitude": 107.1602683
 },
 {
   "STT": 2328,
   "Name": "Quầy thuốc Phước An",
   "address": "Ấp An Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2329,
   "Name": "Quầy thuốc Hải Anh",
   "address": "Ấp Phước Lý, Đại Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 2330,
   "Name": "Quầy thuốc Tuấn Kiệt",
   "address": "Ấp Trung Tâm, Thanh Bình, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2331,
   "Name": "Quầy thuốc Mỹ Quyên",
   "address": "Ấp Tân Phát, Đồi 61, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9180556,
   "Latitude": 107.0241667
 },
 {
   "STT": 2332,
   "Name": "Quầy thuốc Hạnh Như",
   "address": "Khu 8, ấp Cẩm Tân, Xuân Tân, TX. Long Khánh, Đồng Nai",
   "Longtitude": 10.8918739,
   "Latitude": 107.2263931
 },
 {
   "STT": 2333,
   "Name": "Quầy thuốc Trâm Ánh",
   "address": "Tổ 9, ấp Tân Cang, xã  Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2334,
   "Name": "Nhà thuốc Thu Hiển",
   "address": "D33, khu phố 4, Quang Vinh, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9731501,
   "Latitude": 106.9079379
 },
 {
   "STT": 2335,
   "Name": "Quầy thuốc Đức Kỳ",
   "address": "Ấp 1, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7503268,
   "Latitude": 106.9437108
 },
 {
   "STT": 2336,
   "Name": "Quầy thuốc Phúc Hậu",
   "address": "73, ấp Tân Đạt, Đồi 61, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 2337,
   "Name": "Quầy thuốc Minh Khoa",
   "address": "Tổ 5, ấp 7, Sông Trầu, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9782781,
   "Latitude": 107.0185123
 },
 {
   "STT": 2338,
   "Name": "Quầy thuốc Minh Khoa",
   "address": "Tổ 2, ấp Hàng gòn,  Lộc An, , Long Thành, Đồng Nai",
   "Longtitude": 10.8111548,
   "Latitude": 106.9889904
 },
 {
   "STT": 2339,
   "Name": "Nhà thuốc Quyên Hoàng",
   "address": "11, khu phố 2, phường Bình Đa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9672259,
   "Latitude": 106.8608002
 },
 {
   "STT": 2340,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Ấp Bến Sắn, Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2341,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền Vy Hoàng",
   "address": "21/2, tổ 4,  khu phố 2, phường Long Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9672005,
   "Latitude": 106.8608028
 },
 {
   "STT": 2342,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu , thuốc cổ truyền Công Dung",
   "address": "251, tổ 5, khu phố 11, phường An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9811388,
   "Latitude": 106.8427348
 },
 {
   "STT": 2343,
   "Name": "Nhà thuốc Như Phương Anh",
   "address": "A3/243, khu phố 3, phường Tân Vạn, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9737069,
   "Latitude": 106.8861581
 },
 {
   "STT": 2344,
   "Name": "Quầy thuốc Tri Ân",
   "address": "101/5, Dốc Mơ 3, Gia Tân, Thống Nhất, Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2345,
   "Name": "Quầy thuốc Lộc Loan",
   "address": "478, khu phố 1, Trung Dũng, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9419963,
   "Latitude": 106.8634047
 },
 {
   "STT": 2346,
   "Name": "Quầy thuốc Vĩ Phương",
   "address": "Ấp Bến Đình, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 2347,
   "Name": "Quầy thuốc Long Châu Kiệt",
   "address": "B12, tổ 20, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa,tỉnh Đồng Nai",
   "Longtitude": 10.9265399,
   "Latitude": 106.806079
 },
 {
   "STT": 2348,
   "Name": "Quầy thuốc Nho Ngọc Dung",
   "address": "111/3, ấp Tam Hòa, xã Hiệp Hòa, thành phố Biên Hòa,tỉnh Đồng Nai",
   "Longtitude": 10.9327662,
   "Latitude": 106.8293503
 },
 {
   "STT": 2349,
   "Name": "Quầy thuốc Thiên Thanh",
   "address": "236/5/2/9, ấp 4, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 2350,
   "Name": "Quầy thuốc Mai Thúy",
   "address": "Ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9522557,
   "Latitude": 107.3325721
 },
 {
   "STT": 2351,
   "Name": "Quầy thuốc PKĐK KV Phú Lý",
   "address": "Ấp 4, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.3001594,
   "Latitude": 107.150533
 },
 {
   "STT": 2352,
   "Name": "Nhà thuốc Việt Nguyễn",
   "address": "Tổ 7B, khu phố 4, Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9720565,
   "Latitude": 106.9097653
 },
 {
   "STT": 2353,
   "Name": "Quầy thuốc Kim Oanh",
   "address": "Tổ 19, khu Cầu Xéo, TT long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7782801,
   "Latitude": 106.9447146
 },
 {
   "STT": 2354,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "46, tổ 1, khu phố Hiệp Quyết,TT Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2355,
   "Name": "Chi nhánh Nhà thuốc Đồng Nai - Công ty cổ phần Dược phẩm Pha No",
   "address": "1048A, Phạm Văn Thuận, khu phố 2, Tân Mai, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9580619,
   "Latitude": 106.8468985
 },
 {
   "STT": 2356,
   "Name": "Quầy thuốc Bảo An",
   "address": "Ấp 5, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1775809,
   "Latitude": 107.3410392
 },
 {
   "STT": 2357,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "176D, tổ 2, ấp 7, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 2358,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "17, tổ 1, Phú Lập, Phú Bình, Tân Phú, Đồng Nai",
   "Longtitude": 11.3649013,
   "Latitude": 107.4026244
 },
 {
   "STT": 2359,
   "Name": "Nhà thuốc Phát An Bình - bệnh viện Quân Y 7B",
   "address": "Khu phố 6, phường Tân Tiến, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9527485,
   "Latitude": 106.8494924
 },
 {
   "STT": 2360,
   "Name": "Quầy thuốc ",
   "address": "Số 4. tổ 3, ấp Hàng Gòn, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8111548,
   "Latitude": 106.9889904
 },
 {
   "STT": 2361,
   "Name": "Quầy thuốc Hồng Phượng",
   "address": "Ấp 2A, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 2362,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "201, ấp Tân Bắc, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 2363,
   "Name": "Quầy thuốc Thanh Điệp",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2364,
   "Name": "Quầy thuốc Tú Xuân",
   "address": "Ấp Đông Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 2365,
   "Name": "Quầy thuốc Tùng Lâm",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2366,
   "Name": "Quầy thuốc Kiều Nga",
   "address": "Ấp Tam Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 2367,
   "Name": "Nhà thuốc số 02 - trực thuộc công ty TNHH Dược Anh Anh",
   "address": "Trong khuôn viên bệnh viện Đại học Y Dược Sing Mark, quốc lộ 51, khu phố 7, phường Long Bình Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8966027,
   "Latitude": 106.8631189
 },
 {
   "STT": 2368,
   "Name": "Phúc Hưng Đường",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 2369,
   "Name": "Nhà thuốc Trương Bình Phương",
   "address": "A27, tổ 8B, phường An Bình, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9240197,
   "Latitude": 106.8665367
 },
 {
   "STT": 2370,
   "Name": "Quầy thuốc Tâm An",
   "address": "Ấp Bùi Chu, Bắc Sơn, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2371,
   "Name": "Quầy thuốc Đức Hiếu",
   "address": "Tổ 4, ấp Hiền Hòa,xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6792113,
   "Latitude": 107.0249535
 },
 {
   "STT": 2372,
   "Name": "Quầy thuốc An Gia Khang",
   "address": "662, tổ 16, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2373,
   "Name": "Quầy thuốc ",
   "address": "Ấp 3, Hiệp Phước, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7267839,
   "Latitude": 106.9417632
 },
 {
   "STT": 2374,
   "Name": "Quầy thuốc Hà Anh Thảo",
   "address": "Tổ 4, ấp Long Đức, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.861517,
   "Latitude": 106.9611
 },
 {
   "STT": 2375,
   "Name": "Quầy thuốc Thu Huệ",
   "address": "Tổ 6, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2376,
   "Name": "Quầy thuốc Bảo Trâm",
   "address": "Tổ 6, ấp Dốc Mơ 3, Gia Tân 1, Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0600804,
   "Latitude": 107.1666761
 },
 {
   "STT": 2377,
   "Name": "Quầy thuốc Sĩ Toàn",
   "address": "Ấp Phú Mỹ I, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 2378,
   "Name": "Quầy thuốcTâm Tuấn",
   "address": "Ấp 1, đường N1, khu tái định cư, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.739949,
   "Latitude": 106.942012
 },
 {
   "STT": 2379,
   "Name": "Quầy thuốc Nguyễn Thanh Việt",
   "address": "Ấp 7, Bàu Cạn, Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 2380,
   "Name": "Quầy thuốc Dân An An",
   "address": "Ấp Đất Mới, Phú xã Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 2381,
   "Name": "Quầy thuốc Quảng Tiến",
   "address": "Tổ 5, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2383,
   "Name": "Quầy thuốc Khải Nguyên",
   "address": "Ấp 4,xã An Viễn,Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8749005,
   "Latitude": 106.991496
 },
 {
   "STT": 2384,
   "Name": "Quầy thuốc Ngọc Tâm Vinh",
   "address": "37, tổ 3, ấp 4, xã An Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 11.0109913,
   "Latitude": 106.840417
 },
 {
   "STT": 2385,
   "Name": "Quầy thuốc Ánh Ngân Tâm ",
   "address": "624, tổ 24, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2386,
   "Name": "Quầy thuốc Trúc Dung",
   "address": "31A/76, Khu phố 3, phường Tam Hòa, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9540718,
   "Latitude": 106.8491197
 },
 {
   "STT": 2387,
   "Name": "Quầy thuốc Nguyễn Thùy",
   "address": "111, ấp 4, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 11.0072096,
   "Latitude": 106.8406332
 },
 {
   "STT": 2388,
   "Name": "Quầy thuốc Thái Oanh",
   "address": "207, ấp Hưng Hiệp, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 2389,
   "Name": "Quầy thuốc Thúy Nhi",
   "address": "Ấp Việt Kiều, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9091923,
   "Latitude": 107.381107
 },
 {
   "STT": 2390,
   "Name": "Quầy thuốc Hồng Loan",
   "address": "93A, ấp 8, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0355499,
   "Latitude": 107.3207712
 },
 {
   "STT": 2391,
   "Name": "Quầy thuốc Bảo Nam",
   "address": "Ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2392,
   "Name": "Quầy thuốc Duy Tùng Anh",
   "address": "Tổ 5, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8893925,
   "Latitude": 106.9027018
 },
 {
   "STT": 2393,
   "Name": "Nhà thuốc Việt Hòa",
   "address": "42, Đặng văn Trơn, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9337205,
   "Latitude": 106.8305162
 },
 {
   "STT": 2394,
   "Name": "Quầy thuốc Kim Oanh",
   "address": "Ấp Bến Cam, xã  Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 2395,
   "Name": "Quầy thuốc Minh Tú",
   "address": "24//I, Tân Lập, Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0763599,
   "Latitude": 107.2075388
 },
 {
   "STT": 2396,
   "Name": "Quầy thuốc Soa Triều",
   "address": "Tổ 22, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8838136,
   "Latitude": 106.9038214
 },
 {
   "STT": 2397,
   "Name": "Quầy thuốc Thiên Vũ",
   "address": "1730, ấp Thọ Lâm 3, Phú Thanh, Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 2398,
   "Name": "Quầy thuốc Duy Lộc",
   "address": "330, tổ 7, ấp Thiên Bình, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8654124,
   "Latitude": 106.926616
 },
 {
   "STT": 2399,
   "Name": "Quầy thuốc Song Hòa",
   "address": "Ấp Cát Lái, Phú Hữu, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 2400,
   "Name": "Quầy thuốc Thành Tín",
   "address": "41, Nguyễn Đình Chiểu, tổ 19, khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2401,
   "Name": "Quầy thuốc Thành Bảo",
   "address": "197, ấp 2, xã An Hòa, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 2402,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Ấp Bến Sắn,  Phước Thiền, Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2403,
   "Name": "Nhà thuốc Hằng Hà",
   "address": "D313, tổ 9, khu phố 4, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.972057,
   "Latitude": 106.9097643
 },
 {
   "STT": 2404,
   "Name": "Quầy thuốc Thảo",
   "address": "Ấp Ruộng Hời, xã Bảo Vinh, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9369869,
   "Latitude": 107.2652964
 },
 {
   "STT": 2405,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "01A, Võ Thị Sáu,  khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.790233,
   "Latitude": 106.9505176
 },
 {
   "STT": 2406,
   "Name": "Nhà thuốc phòng khám đa khoa Liên Chi",
   "address": "26, Bùi Văn Hòa, tổ 5, khu phố 6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2407,
   "Name": "Quầy thuốc ",
   "address": "Ấp Phước Lý, xã Đại Phước , huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 2408,
   "Name": "Quầy thuốc Ngọc Như",
   "address": "Số 2849, ấp Phú Thạch, Phú Trung, Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 2409,
   "Name": "Quầy thuốc Luy Đường",
   "address": "Ấp 3, Phú Hoà, Định Quán, Tỉnh Đồng Nai",
   "Longtitude": 11.2098918,
   "Latitude": 107.4450231
 },
 {
   "STT": 2410,
   "Name": "Quầy thuốc Thảo Hường",
   "address": "Ấp Bể Bạc, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 2411,
   "Name": "Quầy thuốc Ánh Nguyệt",
   "address": "Ấp Láng Me, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8332277,
   "Latitude": 107.394658
 },
 {
   "STT": 2412,
   "Name": "Quầy thuốc Phương Hồng",
   "address": "Ấp Lý Lịch 1, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 2413,
   "Name": "Nhà thuốc Xuân Tiến",
   "address": "134/83, khu phố 7, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9639334,
   "Latitude": 106.9112675
 },
 {
   "STT": 2414,
   "Name": "Quầy thuốc Công Lập",
   "address": "Ấp Tân Hạnh, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2415,
   "Name": "Quầy thuốc Kim Liền",
   "address": "Ấp Tân Bình, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8358264,
   "Latitude": 107.2935523
 },
 {
   "STT": 2416,
   "Name": "Quầy thuốc Linh Hòa",
   "address": "Tổ 51, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8690959,
   "Latitude": 106.9215782
 },
 {
   "STT": 2417,
   "Name": "Quầy thuốc Ngọc Đăng",
   "address": "Ấp Tân Xuân, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2418,
   "Name": "Nhà thuốc Nguyên Minh",
   "address": "305C, Nguyễn Khuyến, khu phố 3A, phường Trảng Dài, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9846076,
   "Latitude": 106.8691285
 },
 {
   "STT": 2419,
   "Name": "Nhà thuốc Lan Khuê",
   "address": "220/4, khu phố 7, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9632068,
   "Latitude": 106.9111885
 },
 {
   "STT": 2420,
   "Name": "Quầy thuốc Lương Liên",
   "address": "Tổ 1, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2421,
   "Name": "Quầy thuốc Hoài Ân",
   "address": "484, tổ 10, ấp Tân Phát, xã Đồi 61, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9180141,
   "Latitude": 107.0244171
 },
 {
   "STT": 2422,
   "Name": "Nhà thuốc Kiên Phúc",
   "address": "236, khu phố 1 phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656045,
   "Latitude": 106.866728
 },
 {
   "STT": 2423,
   "Name": "Quầy thuốc Phương Loan",
   "address": "Ấp 3, xã Hiếu Liêm, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.118788,
   "Latitude": 106.969403
 },
 {
   "STT": 2424,
   "Name": "Quầy thuốc Châu Ngân",
   "address": "Ấp Đất Mới, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7207699,
   "Latitude": 106.9982685
 },
 {
   "STT": 2425,
   "Name": "Quầy thuốc Nhung Cường",
   "address": "Thôn Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2426,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Ấp Tân Hưng, xã Đồi 61, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9032262,
   "Latitude": 107.0234309
 },
 {
   "STT": 2427,
   "Name": "Nhà thuốc Vũ Như Thuận",
   "address": "Tổ 15, khu phố Bình Dương, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9037887,
   "Latitude": 106.882335
 },
 {
   "STT": 2428,
   "Name": "Quầy thuốc Xuân Anh",
   "address": "132, tổ 34, đường số 1, xã Xuân Lập, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9321512,
   "Latitude": 107.1851752
 },
 {
   "STT": 2429,
   "Name": "Quầy thuốc Kim Quân",
   "address": "Tổ 28, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.8959925,
   "Latitude": 106.9082868
 },
 {
   "STT": 2430,
   "Name": "Quầy thuốc An Thảo",
   "address": "Ấp 1, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 2431,
   "Name": "Quầy thuốc Ngọc Thoa",
   "address": "Chợ Trà Cổ, xã Bình Minh, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9451841,
   "Latitude": 106.9804832
 },
 {
   "STT": 2432,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 2433,
   "Name": "Quầy thuốc Thiện Hiệp",
   "address": "Tổ 3, ấp Long Khánh 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2434,
   "Name": "Nhà thuốc Trâm Mai",
   "address": "55/2, tổ 7, khu phố 2, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.967224,
   "Latitude": 106.8608004
 },
 {
   "STT": 2435,
   "Name": "Quầy thuốc Vinh Hoa",
   "address": "Đường Hùng Vương, ấp Đoàn Kết, xã Vĩnh Thanh, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6788678,
   "Latitude": 106.8559095
 },
 {
   "STT": 2436,
   "Name": "Cơ sở chuyên bán lẻ Dược liệu, thuốc Dược liệu, thuốc cổ truyền Bảo Khang",
   "address": "167, đường P88, Hoàng Bá Bích, khu phố 5A, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.935335,
   "Latitude": 106.8769734
 },
 {
   "STT": 2437,
   "Name": "Cơ sở chuyên bán lẻ Dược liệu, thuốc Dược liệu, thuốc cổ truyền Loan Uyên",
   "address": "33/183, khu phố 2, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9672766,
   "Latitude": 106.8607949
 },
 {
   "STT": 2438,
   "Name": "Cơ sở chuyên bán lẻ Dược liệu, thuốc Dược liệu, thuốc cổ truyền Hồng Đan Sâm",
   "address": "93, Hoàng Bá Bích, khu phố 4, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9410282,
   "Latitude": 106.8712433
 },
 {
   "STT": 2439,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2440,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "279, ấp Suối Cát II, xã Suối Cát , huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9091327,
   "Latitude": 107.3633919
 },
 {
   "STT": 2441,
   "Name": "Quầy thuốc Hưng Thịnh",
   "address": "89, ấp Cọ Dầu I, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8421101,
   "Latitude": 107.375704
 },
 {
   "STT": 2442,
   "Name": "Nhà thuốc Hạnh Trâm",
   "address": "64, Phan Đình Phùng, khu phố 2, phường Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9542124,
   "Latitude": 106.8179756
 },
 {
   "STT": 2443,
   "Name": "Nhà thuốc Phúc Thanh Hải",
   "address": "436, Bùi Văn Hòa, khu phố 6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9209859,
   "Latitude": 106.8823402
 },
 {
   "STT": 2444,
   "Name": "Công ty TNHH DP Thùy Châu",
   "address": "Số 13, xa lộ ĐỒNG NAI, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9700122,
   "Latitude": 106.891318
 },
 {
   "STT": 2445,
   "Name": "Quầy thuốc Nguyễn Lộc",
   "address": "Trần Hưng Đạo, khu 6, TT Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9145632,
   "Latitude": 107.4118212
 },
 {
   "STT": 2446,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "5317, ấp 5, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 2447,
   "Name": "Quầy thuốc Kim Trinh",
   "address": "Số 2, ấp I, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8465186,
   "Latitude": 107.5074615
 },
 {
   "STT": 2448,
   "Name": "Quầy thuốc Trọng Anh",
   "address": "Tổ 4, ấp Long Đức 1, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.861517,
   "Latitude": 106.9611
 },
 {
   "STT": 2449,
   "Name": "Quầy thuốc Minh Phú",
   "address": "Ấp Bình Chánh, xã Phú Lý,  huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.2832516,
   "Latitude": 107.1484298
 },
 {
   "STT": 2450,
   "Name": "Quầy thuốc Thu Hường",
   "address": "Tổ 20, ấp Vườn Dừa, xã Phước Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8959925,
   "Latitude": 106.9082868
 },
 {
   "STT": 2451,
   "Name": "Quầy thuốc ",
   "address": "69A/4, Khu phố 10, Tân Biên, thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9691663,
   "Latitude": 106.9011485
 },
 {
   "STT": 2452,
   "Name": "Quầy thuốc Phúc Thịnh",
   "address": "Ấp 2, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.03262,
   "Latitude": 107.019326
 },
 {
   "STT": 2453,
   "Name": "Quầy thuốc Hồng Oanh",
   "address": "Số 25, đường 15, tổ 7, ấp Cấp Rang, xã Suối Tre, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9454329,
   "Latitude": 107.1959857
 },
 {
   "STT": 2454,
   "Name": "Chi nhánh Trảng Bom - công ty CP Dược Đồng Nai",
   "address": "01, Nguyễn Văn Cừ, khu phố 5, TT Trảng Bom, Trảng Bom, Tỉnh Đồng Nai",
   "Longtitude": 10.9510399,
   "Latitude": 107.0021526
 },
 {
   "STT": 2455,
   "Name": "Quầy thuốc Thiên Hương",
   "address": "Ấp Trảng Táo, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9943485,
   "Latitude": 107.4264005
 },
 {
   "STT": 2456,
   "Name": "Quầy thuốc Dung",
   "address": "Đường 5, ấp 5, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.828406,
   "Latitude": 107.5533089
 },
 {
   "STT": 2457,
   "Name": "Quầy thuốc Trúc Khanh",
   "address": "Ấp Lí Lịch, xã Phú Lý, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 2459,
   "Name": "Quầy thuốc Quốc Minh",
   "address": "Tổ 7, khu 94, ấp Long Đức 1, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2460,
   "Name": "Quầy thuốc Thiện Hằng",
   "address": "Tổ 20A,  ấp Vườn Dừa, xã Phước Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2461,
   "Name": "Quầy thuốc Ngọc Minh",
   "address": "35/1, ấp Nhân Hòa, xã Tây Hòa, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.946514,
   "Latitude": 107.0544048
 },
 {
   "STT": 2462,
   "Name": "Nhà thuốc An phúc Sài Gòn",
   "address": "2368, ấp thanh Hóa, xã Hố Nai 3, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 2463,
   "Name": "Quầy thuốc  Minh Duy",
   "address": "Số 182, tổ 1, ấp Thanh Thọ 3, xã Phú Lâm, Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 2464,
   "Name": "Quầy thuốc  Bình Minh II ",
   "address": "Ấp 4, Suối Nho, Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0561144,
   "Latitude": 107.2717953
 },
 {
   "STT": 2465,
   "Name": "Quầy thuốc  Tiến Chánh",
   "address": "Số 1905, ấp 2, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8274346,
   "Latitude": 107.3640375
 },
 {
   "STT": 2466,
   "Name": "Quầy thuốc  Tú Trinh",
   "address": "9/1, ấp Chợ, Suối Nho, Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 2467,
   "Name": "Quầy thuốc  Bảo Hân",
   "address": "Ấp Quảng Lộc, xã Quảng Tiến, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.945607,
   "Latitude": 106.98593
 },
 {
   "STT": 2468,
   "Name": "Quầy thuốc  Tuyết Nhung",
   "address": "Tổ 9, ấp Xóm Gốc, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7567666,
   "Latitude": 106.9889904
 },
 {
   "STT": 2469,
   "Name": "Nhà thuốc Yến Lê",
   "address": "385, Bùi Văn Hòa, khu phố 6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8908675,
   "Latitude": 106.8516406
 },
 {
   "STT": 2470,
   "Name": "Quầy thuốc  Suối Tre",
   "address": "88A, đường Suối Tre,, ấp Bình Lộc, xã Suối Tre, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9967026,
   "Latitude": 107.2376019
 },
 {
   "STT": 2471,
   "Name": "Quầy thuốc  Kim Liên",
   "address": "46, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 2472,
   "Name": "Quầy thuốc  Hải Yến",
   "address": "673, ấp Tân Hoa, xã Bàu Hàm, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 2473,
   "Name": "Quầy thuốc  Linh Nhi",
   "address": "Số 05, ấp 7, xã Nam Cát Tiên, Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.4190148,
   "Latitude": 107.4612752
 },
 {
   "STT": 2474,
   "Name": "Quầy thuốc  Nam Hà",
   "address": "Ấp Nam Hà, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2475,
   "Name": "Quầy thuốc  Việt Liễu",
   "address": "SN 11, ấp Hiệp Nghĩa, TT Định Quán, huyện  Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2476,
   "Name": "Quầy thuốc  Tâm Bình",
   "address": "44, tổ 2, ấp 1, xã Sông Trầu, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2477,
   "Name": "Nhà thuốc   Bình Long Phước",
   "address": "44/3, Hoàng Tam Kỳ, tổ 8, , khu phố 6, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9320761,
   "Latitude": 106.875879
 },
 {
   "STT": 2478,
   "Name": "Quầy thuốc Thảo Nhi",
   "address": "Ấp 4, Suối Nho, Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0561144,
   "Latitude": 107.2717953
 },
 {
   "STT": 2479,
   "Name": "Quầy thuốc Hương Trà",
   "address": "Tổ 3, ấp Long Khánh 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2480,
   "Name": "Nhà thuốc  PKĐK trường Cao đẳng Y tế Đồng Nai",
   "address": "Khu phố 9, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9679043,
   "Latitude": 106.8982861
 },
 {
   "STT": 2481,
   "Name": "Nhà thuốc  Phạm Hương",
   "address": "07, khu phố 5, P. An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9691905,
   "Latitude": 106.9085712
 },
 {
   "STT": 2482,
   "Name": "Quầy thuốc",
   "address": "13/3B, Nguyễn Ái Quốc, ấp An Hòa, xã Hóa An, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9562337,
   "Latitude": 106.8154657
 },
 {
   "STT": 2483,
   "Name": "Quầy thuốc Thanh Nhiệm",
   "address": "Ấp Thọ Hòa, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9841497,
   "Latitude": 107.3276856
 },
 {
   "STT": 2484,
   "Name": "Quầy thuốc Đô Bình",
   "address": "1726, tổ 26,  ấp Vườn Dừa, xã Phước Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2485,
   "Name": "Quầy thuốc Kim Oanh",
   "address": "Ấp 11, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7935712,
   "Latitude": 107.0294444
 },
 {
   "STT": 2486,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Tổ 4, ấp 1, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 2487,
   "Name": "Quầy thuốc ",
   "address": "16/G, tổ 1,ấp 1, Sông Trầu, Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9903382,
   "Latitude": 107.0146791
 },
 {
   "STT": 2488,
   "Name": "Quầy thuốc Bảo An",
   "address": "1446/E, ấp Phú Tân, Phú Cường, Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1227454,
   "Latitude": 107.1602683
 },
 {
   "STT": 2489,
   "Name": "Nhà thuốc Việt Mông Cổ - Thiên Phước",
   "address": "1187, Khu phố 7, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9086607,
   "Latitude": 106.8912907
 },
 {
   "STT": 2490,
   "Name": "Quầy thuốc Anh Kiệt",
   "address": "Ấp Tân Thành, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 2491,
   "Name": "Quầy thuốc Quang Phương",
   "address": "Tổ 9, ấp Long Đức 3, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631216,
   "Latitude": 106.9237951
 },
 {
   "STT": 2492,
   "Name": "Quầy thuốc Hà Nhung",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 2493,
   "Name": "Quầy thuốc Ngọc Thủy",
   "address": "Ấp Bến Sắn, Phước Thiền, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2494,
   "Name": "Quầy thuốc Thụy Hằng 2841",
   "address": "Ấp 2, xã  Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6680846,
   "Latitude": 106.8237374
 },
 {
   "STT": 2495,
   "Name": "Quầy thuốc Phan Thương",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2496,
   "Name": "Quầy thuốc Chí Thanh",
   "address": "Tổ 6, ấpTân Mai, xã Phước Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891359,
   "Latitude": 106.918153
 },
 {
   "STT": 2497,
   "Name": "Nhà thuốc phòng khám chuyên khoa RHM Ngọc Anh",
   "address": "40, Bùi Văn Hòa, khu phố 11, P. An Bình, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9279192,
   "Latitude": 106.8775256
 },
 {
   "STT": 2498,
   "Name": "Quầy thuốc Khả Hương",
   "address": "Tổ 10, khu Phước Hải, TT Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 2499,
   "Name": "Nhà thuốc Nhi Đơn",
   "address": "81B, tổ 9, khu phố 4, P. Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.972057,
   "Latitude": 106.9097643
 },
 {
   "STT": 2500,
   "Name": "Quầy thuốc Huy Giáp ",
   "address": "Tổ 28, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8959925,
   "Latitude": 106.9082868
 },
 {
   "STT": 2501,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Ấp Thanh Thọ 3, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2799278,
   "Latitude": 107.4815826
 },
 {
   "STT": 2502,
   "Name": "Quầy thuốc Ngọc Nhung",
   "address": "Ấp 5, xã Phú Lập, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3696461,
   "Latitude": 107.3982239
 },
 {
   "STT": 2503,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "598, ấp Tây Lạc, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2504,
   "Name": "Quầy thuốc Phong Nguyễn",
   "address": "Số 235,, ấp Núi Tung, xã Suối Tre, TX. Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9408816,
   "Latitude": 107.2336917
 },
 {
   "STT": 2505,
   "Name": "Quầy thuốc An Khang",
   "address": "152/3, ấp Lộc Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2506,
   "Name": "Quầy thuốc Thành Nga",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 2507,
   "Name": "Quầy thuốc Thịnh Thơm",
   "address": "Tổ 11, ấp Hương Phước, xã Phước Tân, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2508,
   "Name": "Quầy thuốc Xuân Sang",
   "address": "04, Đông Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 2509,
   "Name": "Quầy thuốc Thu Hà",
   "address": "87/2, khu 1, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2510,
   "Name": "Công ty TNHH MTV Dược Pha Nam Đồng Nai",
   "address": "CN6 - LK1, CN6-LK2, khu phố 7, phường Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9527485,
   "Latitude": 106.8494924
 },
 {
   "STT": 2511,
   "Name": "Quầy thuốc Phú Thuận",
   "address": "Ấp Phú Mỹ 1, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7287547,
   "Latitude": 106.9063491
 },
 {
   "STT": 2512,
   "Name": "Quầy thuốc Sài Gòn",
   "address": "41, ấp Tân Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.910671,
   "Latitude": 107.4016111
 },
 {
   "STT": 2513,
   "Name": "Quầy thuốc Xuân Thảo",
   "address": "19, tổ 2, khu 1, TT Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 2514,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "Tổ 8, ấp Bình Lâm, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 2515,
   "Name": "Quầy thuốc Cẩm Thu",
   "address": "295, ấp Bắc Hòa, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9437877,
   "Latitude": 106.9623785
 },
 {
   "STT": 2516,
   "Name": "Quầy thuốc Bạch Yến",
   "address": "3049, ấp Bình Tân, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9032782,
   "Latitude": 107.3202253
 },
 {
   "STT": 2517,
   "Name": "Quầy thuốc",
   "address": "135A, Phúc Nhạc 2, Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0351046,
   "Latitude": 107.1735973
 },
 {
   "STT": 2518,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2519,
   "Name": "Quầy thuốc",
   "address": "326A, ấp 2, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9838138,
   "Latitude": 107.0275775
 },
 {
   "STT": 2520,
   "Name": "Quầy thuốc Hồng Vi",
   "address": "Kios 51, Chợ Mới, Long Thành, khu Cầu Xéo, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7770943,
   "Latitude": 106.956174
 },
 {
   "STT": 2521,
   "Name": "Nhà Thuốc - Công ty TNHH phòng khám đa khoa Y Đức",
   "address": "93/81/2B, khu phố 8, phường Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9727895,
   "Latitude": 106.8510259
 },
 {
   "STT": 2522,
   "Name": "Quầy thuốc Minh Hoàng",
   "address": "12/3, khu 2, Thuận Hòa, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9839553,
   "Latitude": 107.0218665
 },
 {
   "STT": 2523,
   "Name": "Quầy thuốc Trung Thủy",
   "address": "Tổ 1, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2524,
   "Name": "Quầy thuốc Minh Vy",
   "address": "Số 39, đường 26, tổ 7, ấp Cẩm Tân, xã Xuân Tân, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.841366,
   "Latitude": 106.7992711
 },
 {
   "STT": 2525,
   "Name": "Quầy thuốc Phương Uyên",
   "address": "269A, ấp Gia Tỵ, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9724109,
   "Latitude": 107.3956962
 },
 {
   "STT": 2526,
   "Name": "Quầy thuốc Thanh Thư",
   "address": "Ấp Tân Lập 2, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2527,
   "Name": "Quầy thuốc ",
   "address": "Ấp 1, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.24909,
   "Latitude": 107.0539434
 },
 {
   "STT": 2528,
   "Name": "Quầy thuốc Tuấn Ngọc",
   "address": "Tổ 6, ấp Hương Phước, Xã Phước Tân, thành phố Biên Hòa,tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2529,
   "Name": "Quầy thuốc Ngọc Phương Linh",
   "address": "Ấp Bàu Bông, Phước An, Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 2530,
   "Name": "Quầy thuốc",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2531,
   "Name": "Quầy thuốc Kim Long",
   "address": "373, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2532,
   "Name": "Nhà thuốc Nhi Đồng Việt - trực thuộc Công ty TNHH chuyên khoa Nhi Đồng Việt",
   "address": "429, Hồ Thị Hương, KP 3, phường Xuân Thanh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9314666,
   "Latitude": 107.2545754
 },
 {
   "STT": 2533,
   "Name": "Quầy thuốc Bảo Xuân",
   "address": "152, tổ 12, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2534,
   "Name": "Quầy thuốc Linh Ngọc Yến",
   "address": "3/93, KP 13, phường Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 2535,
   "Name": "Quầy thuốc Nhân Văn",
   "address": "88, ấp 3, xã Tân Hạnh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9514129,
   "Latitude": 106.7787053
 },
 {
   "STT": 2536,
   "Name": "Quầy thuốc Khánh An",
   "address": "Tổ 17, khu Văn Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7857213,
   "Latitude": 106.9477147
 },
 {
   "STT": 2537,
   "Name": "Quầy thuốc Phòng khám đa khoa Việt Hương",
   "address": "Số 128, quốc lộ 20, KP 114, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2000878,
   "Latitude": 107.356476
 },
 {
   "STT": 2538,
   "Name": "Quầy thuốc Phòng khám đa khoa Trung tâm Y tế Long Thành",
   "address": "45, Tôn Đức Thắng, khu Phước Hải, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.790233,
   "Latitude": 106.9505176
 },
 {
   "STT": 2539,
   "Name": "Quầy thuốc Thúy Phượng",
   "address": "4/7A, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9265399,
   "Latitude": 106.806079
 },
 {
   "STT": 2540,
   "Name": "Quầy thuốc Hồng Phong",
   "address": "Khu 12 mẫu, ấp Tân Hạnh, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2541,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Huệ Hương",
   "address": "D466, tổ 7, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8939464,
   "Latitude": 106.8514777
 },
 {
   "STT": 2542,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Trầm Mai",
   "address": "631/20, tổ 1, KP 1, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2543,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Xuân An",
   "address": "346/69, KP 1, phường Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2544,
   "Name": "Cơ sở chuyên bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền An Tâm Phát",
   "address": "02, KP Bình Dương, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2545,
   "Name": "Quầy thuốc Minh Nhật",
   "address": "Khu 14, xã Long Đức, huyện  Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 2546,
   "Name": "Quầy thuốc Phượng Loan",
   "address": "1138, ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9458223,
   "Latitude": 106.9904664
 },
 {
   "STT": 2547,
   "Name": "Quầy thuốc",
   "address": "Tổ 13, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8920528,
   "Latitude": 106.9051963
 },
 {
   "STT": 2548,
   "Name": "Quầy thuốc Lệ Thu",
   "address": "Số 06, Đường Lê A, ấp Suối Chồn, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9545108,
   "Latitude": 107.2368715
 },
 {
   "STT": 2549,
   "Name": "Quầy thuốc ",
   "address": "101, tổ 4, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2550,
   "Name": "Nhà thuốc Minh Hoàng",
   "address": "K1/72, Nguyễn Tri Phương, KP 1,  P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9284764,
   "Latitude": 106.8198347
 },
 {
   "STT": 2551,
   "Name": "Nhà thuốc Uyên Vân",
   "address": "Số 104, đường Suối Tre - Bình Lộc, ấp Suối Tre, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9470116,
   "Latitude": 107.2280134
 },
 {
   "STT": 2552,
   "Name": "Quầy thuốc Lâm Tùng",
   "address": "29, tổ 9, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.899283,
   "Latitude": 106.9183432
 },
 {
   "STT": 2553,
   "Name": "Quầy thuốc Ngọc Phương",
   "address": "349, ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 2554,
   "Name": "Quầy thuốc Ngọc Kim",
   "address": "Tổ 7, khu Phước Long, Thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811468,
   "Latitude": 106.9447146
 },
 {
   "STT": 2555,
   "Name": "Quầy thuốc Lý Mai",
   "address": "Tổ 11, ấp 3, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3022635,
   "Latitude": 107.3061413
 },
 {
   "STT": 2556,
   "Name": "Quầy thuốc Thu Hường",
   "address": "SN 45, tổ 10, khu 6, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 2557,
   "Name": "Nhà thuốc Nguyễn Lê",
   "address": "A162, tổ 34, KP 7, phường Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.970575,
   "Latitude": 106.835759
 },
 {
   "STT": 2558,
   "Name": "Quầy thuốc Phương Thủy",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2559,
   "Name": "Quầy thuốc Ngọc Hường",
   "address": "Tổ 19, ấp Bà Ký, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 2560,
   "Name": "Quầy thuốc Phúc Hưng",
   "address": "21 Nguyễn Hoàng, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8673511,
   "Latitude": 106.9611472
 },
 {
   "STT": 2561,
   "Name": "Quầy thuốc Minh Nga",
   "address": "Tổ 6, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2562,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Ấp Tân Bảo, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8358264,
   "Latitude": 107.2935523
 },
 {
   "STT": 2563,
   "Name": "Quầy thuốc",
   "address": "A44/K2000, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2564,
   "Name": "Quầy thuốc Thu Phan",
   "address": "Tổ 24, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8960957,
   "Latitude": 106.8928706
 },
 {
   "STT": 2565,
   "Name": "Nhà thuốc Quang Minh Đức",
   "address": "119, Bùi Văn Hòa, tổ 11, KP 3A, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9117743,
   "Latitude": 106.8886598
 },
 {
   "STT": 2566,
   "Name": "Quầy thuốc Thiện Nhã",
   "address": "Số 81, tổ 5, ấp 5, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8224719,
   "Latitude": 106.9178769
 },
 {
   "STT": 2567,
   "Name": "Công ty TNHH sản xuất thương mại và dịch vụ Dược Minh Quân",
   "address": "I 20, Khu liên kế Bửu Long, phường Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9579628,
   "Latitude": 106.8051526
 },
 {
   "STT": 2568,
   "Name": "Quầy thuốc Thái Dương",
   "address": "Số 67, ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2569,
   "Name": "Quầy thuốc Vũ Chi",
   "address": "13/15A, đường Hàm Nghi, ấp Ruộng Hời, xã Bảo Vinh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9440087,
   "Latitude": 107.2735215
 },
 {
   "STT": 2570,
   "Name": "Quầy thuốc Huệ Minh",
   "address": "Tổ 20, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9285859,
   "Latitude": 106.80644
 },
 {
   "STT": 2571,
   "Name": "Nhà thuốc Dân An - trực thuộc Phòng khám đa khoa Dân Y",
   "address": "1/C2, QL 51, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8971552,
   "Latitude": 106.8595244
 },
 {
   "STT": 2572,
   "Name": "Quầy thuốc Lộc An",
   "address": "16/8, tổ 20, KP 2, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 2573,
   "Name": "Nhà thuốc Lộc Phước An",
   "address": "D104, KP 1, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548762,
   "Latitude": 106.8021482
 },
 {
   "STT": 2574,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "48/4A, Đông Kim, Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0327201,
   "Latitude": 107.1803206
 },
 {
   "STT": 2575,
   "Name": "Nhà thuốc Bệnh viện Tâm Hồng Phước",
   "address": "148A, Nguyễn Ái Quốc, KP 1, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9684006,
   "Latitude": 106.859825
 },
 {
   "STT": 2576,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2577,
   "Name": "Quầy thuốc Ngọc Thanh",
   "address": "SN 28, tổ 6, ấp Hòa Thành, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 2578,
   "Name": "Quầy thuốc Việt Hương",
   "address": "KP 114, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2579,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2580,
   "Name": "Quầy thuốc Khánh Đoan",
   "address": "162/38, ấp An Hòa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335954,
   "Latitude": 106.79719
 },
 {
   "STT": 2581,
   "Name": "Quầy thuốc Viễn Dương",
   "address": "36D, tổ 10, ấp An Hòa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 2582,
   "Name": "Nhà thuốc phòng khám đa khoa Y Sài Gòn",
   "address": "2/8, KP 6, phường Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.953785,
   "Latitude": 106.8572475
 },
 {
   "STT": 2583,
   "Name": "Quầy thuốc Ngọc Liên",
   "address": "Ấp Hưng Hiệp, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 2584,
   "Name": "Quầy thuốc Linh Thi",
   "address": "Thôn Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2585,
   "Name": "Quầy thuốc Minh Đăng",
   "address": "Ấp Bình Ý, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 2586,
   "Name": "Quầy thuốc Vân Anh",
   "address": "Tổ 10, ấp Tân Mai, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.874716,
   "Latitude": 106.9063918
 },
 {
   "STT": 2587,
   "Name": "Quầy thuốc",
   "address": "247, tổ 13, ấp 2, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0511835,
   "Latitude": 107.0208512
 },
 {
   "STT": 2588,
   "Name": "Quầy thuốc Bảo Nguyên",
   "address": "Ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.843933,
   "Latitude": 106.9319755
 },
 {
   "STT": 2589,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "SN 14, ấp 1, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0694236,
   "Latitude": 107.2828674
 },
 {
   "STT": 2590,
   "Name": "Quầy thuốc Hoàn Duyên",
   "address": "1759, tổ 28, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2591,
   "Name": "Quầy thuốc Ngọc Quỳnh",
   "address": "Tổ 4, khu 15, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8622222,
   "Latitude": 106.9633333
 },
 {
   "STT": 2592,
   "Name": "Nhà thuốc Trường My",
   "address": "42/208, Phạm Văn Thuận, tổ 16, KP 3, P.Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9581639,
   "Latitude": 106.8397155
 },
 {
   "STT": 2593,
   "Name": "Quầy thuốc Quân Long",
   "address": "29, tổ 21, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.869546,
   "Latitude": 106.907211
 },
 {
   "STT": 2594,
   "Name": "Quầy thuốc An Thu",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2595,
   "Name": "Quầy thuốc Minh Phương",
   "address": "84, xóm 4, tổ 23, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9635122,
   "Latitude": 106.933771
 },
 {
   "STT": 2596,
   "Name": "Quầy thuốc Lan Minh Ngọc",
   "address": "199A, tổ 3, khu 1, ấp 3, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916628,
   "Latitude": 106.8634886
 },
 {
   "STT": 2597,
   "Name": "Quầy thuốc Mai Nhung",
   "address": "SN 323, KDC 7, ấp 2, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 2598,
   "Name": "Quầy thuốc Việt Hưng",
   "address": "SN 2305, tổ 1, ấp Phương Lâm 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.284366,
   "Latitude": 107.487501
 },
 {
   "STT": 2599,
   "Name": "Quầy thuốc Cát Minh",
   "address": "SN 25, tổ 1, ấp Ngọc Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 2601,
   "Name": "Quầy thuốc Lệ Mỹ",
   "address": "Tổ 5, ấp Long Đức 1, xã Tam Phước, thành phố  Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2602,
   "Name": "Quầy thuốc Minh Đức",
   "address": "Số 366, khu 5, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 2603,
   "Name": "Quầy thuốc Phương Dung",
   "address": "Ấp 3, xã Bình Lợi, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0384694,
   "Latitude": 106.8237374
 },
 {
   "STT": 2604,
   "Name": "Quầy thuốc Tân Hữu",
   "address": "Ấp Tân Hữu, xã Xuân Thành, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9940176,
   "Latitude": 107.4232248
 },
 {
   "STT": 2605,
   "Name": "Quầy thuốc Tùng Phương",
   "address": "05, ấp 3, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.877158,
   "Latitude": 107.451822
 },
 {
   "STT": 2606,
   "Name": "Nhà thuốc Hạnh Huân",
   "address": "5/55B, KP 9, phường Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 2607,
   "Name": "Quầy thuốc Thảo Nguyễn",
   "address": "471B/A2, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.926897,
   "Latitude": 106.8160971
 },
 {
   "STT": 2608,
   "Name": "Quầy thuốc Mai Diệp",
   "address": "730, tổ 11, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2609,
   "Name": "Quầy thuốc Tuệ Lâm",
   "address": "Số 39B, ấp Tân Bắc, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 2610,
   "Name": "Quầy thuốc Minh Tuấn",
   "address": "Tổ 3, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.800846,
   "Latitude": 106.910962
 },
 {
   "STT": 2611,
   "Name": "Quầy thuốc Nam Đăng",
   "address": "20, tổ 3, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2612,
   "Name": "Quầy thuốc Ngọc Thảo",
   "address": "Đường Lê Quang Định, tổ 26, khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 2613,
   "Name": "Quầy thuốc An Phước Tân",
   "address": "Tổ 12, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.893828,
   "Latitude": 106.9074869
 },
 {
   "STT": 2614,
   "Name": "Nhà thuốc Long Châu",
   "address": "385C, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520452,
   "Latitude": 106.8772377
 },
 {
   "STT": 2615,
   "Name": "Quầy thuốc Tám Huệ",
   "address": "Số 35/2, ấp An Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2616,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp Nam Hà, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 2617,
   "Name": "Quầy thuốc Quý Vy",
   "address": "908, Bùi Hữu Nghĩa, ấp Đồng Nai, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9428773,
   "Latitude": 106.8014808
 },
 {
   "STT": 2618,
   "Name": "Nhà thuốc phòng khám đa khoa Tâm An Pou Chen - chi nhánh công ty TNHH xây dựng y tế Tâm An",
   "address": "Đường Nguyễn Ái Quốc, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai (trong khuôn viên Công ty TNHH Pou Chen Việt Nam)",
   "Longtitude": 10.9336549,
   "Latitude": 106.8082511
 },
 {
   "STT": 2619,
   "Name": "Quầy thuốc Phước Thành",
   "address": "Ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7235141,
   "Latitude": 106.9562455
 },
 {
   "STT": 2620,
   "Name": "Quầy thuốc Phương Anh",
   "address": "30, ấp Núi Đỏ, xã Bàu Sen, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9233185,
   "Latitude": 107.2233125
 },
 {
   "STT": 2621,
   "Name": "Quầy thuốc Phú An Khang",
   "address": "Tổ 1, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2622,
   "Name": "Quầy thuốc Chí Trung",
   "address": "797, đường Tà Lài, tổ 13, ấp 6, xã Phú Thịnh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2986537,
   "Latitude": 107.4144526
 },
 {
   "STT": 2623,
   "Name": "Nhà thuốc An Phước",
   "address": "Số 465, Hồ Thị Hương, tổ 2, KP 4, P.Xuân Thanh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9269448,
   "Latitude": 107.2572251
 },
 {
   "STT": 2624,
   "Name": "Quầy thuốc Nguyễn Lan Duy",
   "address": "Số 71/3, ấp An Hòa, xã Tây Hòa, huyện Trảng  Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2625,
   "Name": "Quầy thuốc Đức Khang",
   "address": "Số 37/2/2, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2626,
   "Name": "Quầy thuốc Minh Thảo",
   "address": "07, đường 3, ấp 3, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 2627,
   "Name": "Nhà thuốc Dũng Nhân",
   "address": "166, Hồ Văn Leo, KP 2, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9428471,
   "Latitude": 106.8652891
 },
 {
   "STT": 2628,
   "Name": "Quầy thuốc",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2629,
   "Name": "Nhà thuốc Ngọc Huế",
   "address": "206, tổ 2, KP 11, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.92479,
   "Latitude": 106.862747
 },
 {
   "STT": 2630,
   "Name": "Quầy thuốc Như Thịnh",
   "address": "Ấp 1, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 2631,
   "Name": "Quầy thuốc",
   "address": "Ấp Cẩm Sơn, xã Xuân Mỹ, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7740095,
   "Latitude": 107.2607289
 },
 {
   "STT": 2632,
   "Name": "Quầy thuốc phòng khám đa khoa Hoàng Tiến Dũng",
   "address": "Số 131, Quốc lộ 1A, xã Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9365178,
   "Latitude": 107.1090644
 },
 {
   "STT": 2633,
   "Name": "Quầy thuốc Nhân Đức",
   "address": "450A, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 2634,
   "Name": "Quầy thuốc Phương Uyên",
   "address": "Ấp 3, xã Thừa Đức, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7882504,
   "Latitude": 107.1510688
 },
 {
   "STT": 2635,
   "Name": "Nhà thuốc Sơn Nghĩa",
   "address": "446/50, Huỳnh Dân Sanh, tổ 12, KP 5, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 2636,
   "Name": "Quầy thuốc ",
   "address": "488/2, Gia Yên, Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 2637,
   "Name": "Quầy thuốc Xinh Xinh",
   "address": "Kiot số 01-02, Chợ Xuân Tân, xã Xuân Tân, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8959614,
   "Latitude": 107.2273028
 },
 {
   "STT": 2638,
   "Name": "Quầy thuốc Kim Hằng",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 2639,
   "Name": "Quầy thuốc Đoan Trang",
   "address": "Kiot 06A, chợ Xuân Thọ, ấp Thọ Chánh, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9533644,
   "Latitude": 107.3294718
 },
 {
   "STT": 2640,
   "Name": "Quầy thuốc",
   "address": "Ấp Trung Tín, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0082873,
   "Latitude": 107.4329473
 },
 {
   "STT": 2641,
   "Name": "Nhà thuốc Bích Thuận",
   "address": "1436, Nguyễn Ái Quốc, KP 3, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9635437,
   "Latitude": 106.839965
 },
 {
   "STT": 2642,
   "Name": "Quầy thuốc Bích Diễm",
   "address": "639, ấp Thọ Hòa, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9471397,
   "Latitude": 107.3136686
 },
 {
   "STT": 2643,
   "Name": "Nhà thuốc Tâm Nguyên",
   "address": "Tổ 2, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9921407,
   "Latitude": 106.8709404
 },
 {
   "STT": 2644,
   "Name": "Quầy thuốc Hồng Phát",
   "address": "SN 05, ấp Chợ, xã Suối Nho, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 2645,
   "Name": "Quầy thuốc Bích Phụng",
   "address": "KP 5, TT Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0815408,
   "Latitude": 107.0244171
 },
 {
   "STT": 2646,
   "Name": "Quầy thuốc Phúc Dung",
   "address": "Tổ 8, ấp La Hoa, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8410566,
   "Latitude": 107.37811
 },
 {
   "STT": 2647,
   "Name": "Quầy thuốc Linh Đan",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2648,
   "Name": "Nhà thuốc Phú Mỹ",
   "address": "11, Phạm Văn Khoai, KP 3, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9619374,
   "Latitude": 106.8600011
 },
 {
   "STT": 2649,
   "Name": "Quầy thuốc Phan Điền",
   "address": "41, Trương Hán Siêu, tổ 9, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.869546,
   "Latitude": 106.907211
 },
 {
   "STT": 2650,
   "Name": "Quầy thuốc Tâm Tú",
   "address": "Ấp 1, xã Trị An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0986681,
   "Latitude": 106.9650823
 },
 {
   "STT": 2651,
   "Name": "Nhà thuốc Tài Tiến",
   "address": "71/4, KP 7, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9890275,
   "Latitude": 106.8296372
 },
 {
   "STT": 2652,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "Ấp 1, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 2653,
   "Name": "Quầy thuốc Phúc Khang Đường",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2654,
   "Name": "Quầy thuốc Phúc An Khang",
   "address": "Tổ 1, khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 2655,
   "Name": "Quầy thuốc Thiên Khôi",
   "address": "Đường Trần Phú, ấp Quảng Phát, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9456897,
   "Latitude": 106.996568
 },
 {
   "STT": 2656,
   "Name": "Quầy thuốc Pháp Anh",
   "address": "Tổ 7, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2657,
   "Name": "Nhà thuốc 2 - trực thuộc Phòng khám đa khoa Sinh Hậu",
   "address": "Số 27A/13, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 2658,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Nhật Trường Phát",
   "address": "78I, tổ 43-45, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9819539,
   "Latitude": 106.8449944
 },
 {
   "STT": 2659,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Hùng Hằng",
   "address": "408, tổ 23, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8987437,
   "Latitude": 106.8908676
 },
 {
   "STT": 2660,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "SN 06, KDC 2, ấp Bình Hòa, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.15,
   "Latitude": 107.233333
 },
 {
   "STT": 2661,
   "Name": "Quầy thuốc Phúc Vinh",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2662,
   "Name": "Quầy thuốc Khánh An",
   "address": "Số 336, tổ 10, ấp Tân Bình, xã Bảo Bình, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8352258,
   "Latitude": 107.3018591
 },
 {
   "STT": 2663,
   "Name": "Nhà thuốc Anh Khôi",
   "address": "Số 36, Trần Phú, KP 1, P.Xuân An, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9309061,
   "Latitude": 107.2503855
 },
 {
   "STT": 2664,
   "Name": "Quầy thuốc Minh Nhật",
   "address": "Thửa đất số 76, tờ bản đồ số 25, ấp Cẩm Tân, xã Xuân Tân, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9442612,
   "Latitude": 107.2311774
 },
 {
   "STT": 2665,
   "Name": "Quầy thuốc Trung Liêm",
   "address": "11/2B, Dốc Mơ 1,  Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0572437,
   "Latitude": 107.1698921
 },
 {
   "STT": 2666,
   "Name": "Nhà thuốc Đức Hưng Phát",
   "address": "246B, Đồng Khởi, tổ 47, KP 9, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9695065,
   "Latitude": 106.8534925
 },
 {
   "STT": 2667,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp Tam Hiệp xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 2668,
   "Name": "Quầy thuốc Khoa Nhân",
   "address": "Khu 5, ấp Hòa Bình, xã Đông Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8724442,
   "Latitude": 107.0598491
 },
 {
   "STT": 2669,
   "Name": "Quầy thuốc Thu Huyên",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2670,
   "Name": "Quầy thuốc Thảo Anh",
   "address": "129, Nguyễn Hoàng, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8673511,
   "Latitude": 106.9611472
 },
 {
   "STT": 2671,
   "Name": "Nhà thuốc Thắng Thanh Hải",
   "address": "G669, Bùi Văn Hòa, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9319693,
   "Latitude": 106.8749571
 },
 {
   "STT": 2672,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Ấp Bình Ý, xã Tân Bình, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.007064,
   "Latitude": 106.8001396
 },
 {
   "STT": 2673,
   "Name": "Quầy thuốc Vân Anh",
   "address": "1498, Trần Cao Vân, Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 2674,
   "Name": "Quầy thuốc Khôi Khoa",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2675,
   "Name": "Quầy thuốc",
   "address": "Ấp Ông Hường, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0140912,
   "Latitude": 106.855241
 },
 {
   "STT": 2676,
   "Name": "Quầy thuốc Liên Tâm",
   "address": "Tổ 6, khu 2, ấp 2, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8927713,
   "Latitude": 106.875601
 },
 {
   "STT": 2677,
   "Name": "Quầy thuốc Mai Trang 2004",
   "address": "2477A81, QL1A, ấp Việt Kiều, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9078925,
   "Latitude": 107.3808592
 },
 {
   "STT": 2678,
   "Name": "Quầy thuốc",
   "address": "Số 39, đường Võ Văn Tần, ấp Núi Tung, xã Suối Tre, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9408816,
   "Latitude": 107.2336917
 },
 {
   "STT": 2679,
   "Name": "Nhà thuốc An Tâm Phúc",
   "address": "177, Hoàng Bá Bích, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9414904,
   "Latitude": 106.8735033
 },
 {
   "STT": 2680,
   "Name": "Quầy thuốc Nguyệt An Hoa",
   "address": "Khu Văn Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7819755,
   "Latitude": 106.945763
 },
 {
   "STT": 2681,
   "Name": "Quầy thuốc Hoàng Sáng",
   "address": "Số 87/B, tổ 3, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2682,
   "Name": "Quầy thuốc Phương Huyền",
   "address": "G12, K92, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2683,
   "Name": "Quầy thuốc Thi",
   "address": "Chợ Túc Trưng, ấp Đồng Xoài, xã Túc Trưng, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1185924,
   "Latitude": 107.2311774
 },
 {
   "STT": 2684,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Ấp 3A, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8461842,
   "Latitude": 107.5092529
 },
 {
   "STT": 2685,
   "Name": "Quầy thuốc Diễm Thư",
   "address": "Ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2686,
   "Name": "Quầy thuốc Nhật Huyền",
   "address": "399, tổ 4, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2687,
   "Name": "Nhà thuốc Hòa Bình An",
   "address": "Tổ 20, Nguyễn Thái Học, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9731501,
   "Latitude": 106.9079379
 },
 {
   "STT": 2688,
   "Name": "Quầy thuốc Bảo Anh",
   "address": "Ấp Cây Điệp, xã Cây Gáo, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2689,
   "Name": "Quầy thuốc Thiên Phúc 1",
   "address": "Ấp 4, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.702072,
   "Latitude": 106.958422
 },
 {
   "STT": 2690,
   "Name": "Quầy thuốc Thu Hạnh",
   "address": "F4/1040 Nguyễn Huệ 2, Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.991378,
   "Latitude": 107.154914
 },
 {
   "STT": 2691,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Khu phố 4, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9731501,
   "Latitude": 106.9079379
 },
 {
   "STT": 2692,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Hòa Liên",
   "address": "D3, Yết Kiêu, KP 2, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9522332,
   "Latitude": 106.8774348
 },
 {
   "STT": 2693,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Hồng Lệ",
   "address": "222, tổ 2, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916312,
   "Latitude": 106.8503608
 },
 {
   "STT": 2694,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Mi Nga",
   "address": "A1/229, Bùi Hữu Nghĩa, KP 1, P.Tân Vạn, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9130116,
   "Latitude": 106.8274286
 },
 {
   "STT": 2695,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Thanh Nhàn",
   "address": "Tổ 93B, KP 13, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 2696,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Tâm Hằng",
   "address": "48, KP 6, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 2697,
   "Name": "Cơ sở bán lẻ dược liệu, thuốc dược liệu, thuốc cổ truyền Minh Phú Trí",
   "address": "Tổ 6, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 2698,
   "Name": "Quầy thuốc Hòa Nguyên",
   "address": "Tổ 7B, KDC Phú Tín, ấp Long Đức 3 (tờ bản đồ số 68, thửa đất số 652), xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9607516,
   "Latitude": 106.8562856
 },
 {
   "STT": 2699,
   "Name": "Nhà thuốc phòng khám đa khoa Hoàng Anh Đức",
   "address": "Trung đoàn 22, Quân đoàn 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9045207,
   "Latitude": 106.8826349
 },
 {
   "STT": 2700,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Số 144, ấp Bình Lâm, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 2701,
   "Name": "Quầy thuốc Linh Thịnh",
   "address": "Số 599, ấp An Bình, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8560944,
   "Latitude": 107.0700134
 },
 {
   "STT": 2702,
   "Name": "Quầy thuốc Thiên Gia Phúc",
   "address": "Tổ 23, KP 3, P.Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9873489,
   "Latitude": 106.858696
 },
 {
   "STT": 2703,
   "Name": "Quầy thuốc Hùng Trang",
   "address": "89, tổ 9, KP 1, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2704,
   "Name": "Quầy thuốc Quỳnh Anh Phát",
   "address": "Tổ 20, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8959925,
   "Latitude": 106.9082868
 },
 {
   "STT": 2705,
   "Name": "Quầy thuốc Khang Nhi",
   "address": "Tổ 8, ấp Long Đức 1 (tờ bản đồ số 110, thửa số 12), xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 2706,
   "Name": "Nhà thuốc Quỳnh Trân",
   "address": "Số 54, đường Khổng Tử, KP 1, P.Xuân Trung, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9394105,
   "Latitude": 107.2444751
 },
 {
   "STT": 2707,
   "Name": "Quầy thuốc Nguyễn Minh Anh",
   "address": "Tổ 4, ấp Long Đức 1 (tờ bản đồ số 52, thửa số 12), xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 2708,
   "Name": "Quầy thuốc Mỹ Thân",
   "address": "Số 20, đường Hàm Nghi, tổ 12A, ấp Ruộng Lớn, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9440087,
   "Latitude": 107.2735215
 },
 {
   "STT": 2709,
   "Name": "Quầy thuốc Long An",
   "address": "Ấp Xóm Gốc, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7647864,
   "Latitude": 106.9621677
 },
 {
   "STT": 2710,
   "Name": "Quầy thuốc Nam Anh",
   "address": "SN 31/3, ấp 7, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 2711,
   "Name": "Quầy thuốc Hương Ngọc Thuận",
   "address": "18/20, ấp Tân Mai, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8746531,
   "Latitude": 106.9103868
 },
 {
   "STT": 2712,
   "Name": "Quầy thuốc Minh Tuấn",
   "address": "39/7, ấp 2, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 2713,
   "Name": "Quầy thuốc Thu Dung II",
   "address": "Ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 2714,
   "Name": "Quầy thuốc Quỳnh An",
   "address": "Ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 2715,
   "Name": "Quầy thuốc Lộc Anh",
   "address": "202, tổ 4, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2716,
   "Name": "Nhà thuốc Thanh Phúc",
   "address": "570, tổ 7, KP 4, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656045,
   "Latitude": 106.866728
 },
 {
   "STT": 2717,
   "Name": "Quầy thuốc Châu",
   "address": "Ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7400618,
   "Latitude": 106.9448719
 },
 {
   "STT": 2718,
   "Name": "Quầy thuốc",
   "address": "37, KP 2, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 2719,
   "Name": "Quầy thuốc",
   "address": "Ấp Bà Trường, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6468256,
   "Latitude": 106.9363446
 },
 {
   "STT": 2720,
   "Name": "Quầy thuốc Ngọc Huy",
   "address": "Chợ Xuân Bắc, ấp 2B, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0340657,
   "Latitude": 107.3223415
 },
 {
   "STT": 2721,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2722,
   "Name": "Quầy thuốc Trí Khang",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 2723,
   "Name": "Quầy thuốc An Khang",
   "address": "SN 83, phố 2, ấp 3, xã Phú Lợi, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2236734,
   "Latitude": 107.3907968
 },
 {
   "STT": 2724,
   "Name": "Quầy thuốc Tuyết Nhung",
   "address": "Bắc Hợp, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2725,
   "Name": "Quầy thuốc Thịnh Châm",
   "address": "Tổ 2, khu 2007, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2726,
   "Name": "Quầy thuốc Thành Ân",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 2727,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp 1C, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6790847,
   "Latitude": 107.0267131
 },
 {
   "STT": 2728,
   "Name": "Quầy thuốc Hồng Phúc",
   "address": "Ấp 2, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2729,
   "Name": "Quầy thuốc Tú Hưng",
   "address": "Số 309, Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9675327,
   "Latitude": 106.9516941
 },
 {
   "STT": 2730,
   "Name": "Chi nhánh Long Thành - Công ty cổ phần dược Đồng Nai",
   "address": "Số 482/19, Lê Duẩn, khu Phước Thuận, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7982456,
   "Latitude": 106.9448195
 },
 {
   "STT": 2731,
   "Name": "Quầy thuốc Tố Tâm 3",
   "address": "Tổ 2, khu 2, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 2732,
   "Name": "Quầy thuốc Khanh Hạnh",
   "address": "Tổ 6, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 2733,
   "Name": "Công ty cổ phần Dược phẩm Boston Miền Đông",
   "address": "J28, khu phố 1, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9419963,
   "Latitude": 106.8634047
 },
 {
   "STT": 2734,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Ấp Tân Thành, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9867655,
   "Latitude": 106.9653756
 },
 {
   "STT": 2735,
   "Name": "Quầy thuốc Bình An",
   "address": "Tổ 4, ấp 5, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8622222,
   "Latitude": 106.9633333
 },
 {
   "STT": 2736,
   "Name": "Quầy thuốc Trí Tiến Minh",
   "address": "32, Hồ Văn Huê, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8746531,
   "Latitude": 106.9103868
 },
 {
   "STT": 2737,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Thửa đất số 66, tờ bản đồ số 9, ấp Bàu Cối, xã Bảo Quang, thị xã Long Khánh, tinh Đồng Nai",
   "Longtitude": 11.0139135,
   "Latitude": 107.2775721
 },
 {
   "STT": 2738,
   "Name": "Quầy thuốc Mỹ Phú",
   "address": "67, tổ 4, ấp Đồng, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2739,
   "Name": "Quầy thuốc Nhật Duy",
   "address": "Tổ 3, ấp Bưng Môn, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7898569,
   "Latitude": 106.943622
 },
 {
   "STT": 2740,
   "Name": "Nhà thuốc Minh Sĩ",
   "address": "91/26, KP 3, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916495,
   "Latitude": 106.8502121
 },
 {
   "STT": 2741,
   "Name": "Quầy thuốc Mai Phương Hiếu",
   "address": "76A, Đặng Văn Trơn, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.931996,
   "Latitude": 106.8318543
 },
 {
   "STT": 2742,
   "Name": "Quầy thuốc Duy An",
   "address": "Tổ 22, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8838136,
   "Latitude": 106.9038214
 },
 {
   "STT": 2743,
   "Name": "Nhà thuốc Hoàng Chương",
   "address": "Ấp 4, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 2744,
   "Name": "Quầy thuốc",
   "address": "Ấp Xóm Hố, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.727574,
   "Latitude": 106.879978
 },
 {
   "STT": 2745,
   "Name": "Nhà thuốc Thiện Lộc",
   "address": "3/13, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 2746,
   "Name": "Quầy thuốc Kiều Diễm",
   "address": "60/20, Tây Nam, Gia Kiệm, Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 2747,
   "Name": "Quầy thuốc Anh Phúc",
   "address": "Tổ 6, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2748,
   "Name": "Quầy thuốc Tuệ Nam",
   "address": "Tổ 24C, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2749,
   "Name": "Quầy thuốc",
   "address": "173/320, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520451,
   "Latitude": 106.8772376
 },
 {
   "STT": 2750,
   "Name": "Quầy thuốc Hồng Phúc",
   "address": "Tổ 2, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2751,
   "Name": "Quầy thuốc Nguyên Khôi",
   "address": "Ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 2752,
   "Name": "Nhà thuốc Khoa Thy",
   "address": "22/10, tổ 4, KP 8, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9467204,
   "Latitude": 106.8587246
 },
 {
   "STT": 2753,
   "Name": "Quầy thuốc Mai Nguyễn",
   "address": "Tổ 3, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2754,
   "Name": "Quầy thuốc phòng khám đa khoa Tín Đức",
   "address": "Ấp 3, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0185316,
   "Latitude": 106.8384873
 },
 {
   "STT": 2755,
   "Name": "Quầy thuốc Linh Linh",
   "address": "Ấp 3, xã Phú Thạnh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7211524,
   "Latitude": 106.8473377
 },
 {
   "STT": 2756,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "Ấp Hòa Hiệp, xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2016789,
   "Latitude": 107.3021069
 },
 {
   "STT": 2757,
   "Name": "Quầy thuốc Phương Nhung",
   "address": "319/77, tổ 11, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 2758,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "Tổ 9, KP 8, thị trấn Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0859726,
   "Latitude": 107.0421324
 },
 {
   "STT": 2759,
   "Name": "Quầy thuốc Ôn Hương",
   "address": "28, Đỗ Văn Thi, ấp Nhị Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9326416,
   "Latitude": 106.8267149
 },
 {
   "STT": 2760,
   "Name": "Quầy thuốc Phúc Sinh An",
   "address": "Ấp Câu Kê, xã Phú Hữu, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7250628,
   "Latitude": 106.7765443
 },
 {
   "STT": 2761,
   "Name": "Quầy thuốc Thiên Lộc",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2762,
   "Name": "Quầy thuốc Lài",
   "address": "Ấp Thọ Lộc, xã Xuân Thọ, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9522557,
   "Latitude": 107.3325721
 },
 {
   "STT": 2763,
   "Name": "Quầy thuốc Tiến Phát",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2764,
   "Name": "Quầy thuốc Việt Đức",
   "address": "Ấp Sông Mây, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9436397,
   "Latitude": 106.9506696
 },
 {
   "STT": 2765,
   "Name": "Quầy thuốc Bảo Minh Châu",
   "address": "1890, tổ 27, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8960957,
   "Latitude": 106.8928706
 },
 {
   "STT": 2766,
   "Name": "Quầy thuốc Thu Anh",
   "address": "Tổ 3, ấp 2, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0498878,
   "Latitude": 107.0211717
 },
 {
   "STT": 2767,
   "Name": "Quầy thuốc Hiếu Thảo",
   "address": "Ấp Vàm, xã Thiện Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0126161,
   "Latitude": 106.8945456
 },
 {
   "STT": 2768,
   "Name": "Quầy thuốc Lê Phương",
   "address": "137, tổ 19, ấp An Hòa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 2769,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Tô Phúc Thịnh",
   "address": "23/C1, tổ 1, KP 1, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2770,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Minh Đại",
   "address": "13, lô B5, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2771,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Thùy An",
   "address": "36, tổ 37, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 2772,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Minh Sen",
   "address": "63, KP 7, P.Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9115113,
   "Latitude": 106.8904066
 },
 {
   "STT": 2773,
   "Name": "Quầy thuốc Dũng Loan",
   "address": "5 đường số 2, ấp Tam Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9089472,
   "Latitude": 107.3845538
 },
 {
   "STT": 2774,
   "Name": "Quầy thuốc Đỗ Quyên",
   "address": "Ấp 1, xã Thanh Sơn, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.3020613,
   "Latitude": 107.2666396
 },
 {
   "STT": 2775,
   "Name": "Quầy thuốc Đức Minh",
   "address": "27/3 ấp Bạch Lâm, xã Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 2776,
   "Name": "Quầy thuốc Phúc Lợi",
   "address": "263/B, ấp Phúc Nhạc 1, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 2777,
   "Name": "Nhà thuốc Ngọc Khanh",
   "address": "Số 29, Trần Phú, KP 1, P.Xuân An, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9309061,
   "Latitude": 107.2503855
 },
 {
   "STT": 2778,
   "Name": "Nhà thuốc Thiện Tâm",
   "address": "Số 70, Hồ Thị Hương, KP 1, P.Xuân Trung, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9339565,
   "Latitude": 107.2531193
 },
 {
   "STT": 2779,
   "Name": "Quầy thuốc Hoàng Kiên",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 2780,
   "Name": "Quầy thuốc Tân Sài Gòn",
   "address": "21/2, ấp Bạch Lâm 2, xã Gia Tân 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0611285,
   "Latitude": 107.1693584
 },
 {
   "STT": 2781,
   "Name": "Quầy thuốc Hoàng Nhung",
   "address": "Tổ 4, ấp 3, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 2782,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2783,
   "Name": "Quầy thuốc Ngọc Trâm",
   "address": "Số 127, đường Phú Sơn, ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9412043,
   "Latitude": 106.9498535
 },
 {
   "STT": 2784,
   "Name": "Quầy thuốc Thiên Phước",
   "address": "Ấp Trung Hưng, xã Xuân Trường, huyện  Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9700952,
   "Latitude": 107.4328135
 },
 {
   "STT": 2785,
   "Name": "Quầy thuốc Hiền Đức",
   "address": "Khu tái định cư, đường Nguyễn Trung Trực, ấp Bảo Vinh B, xã Bảo Vinh, TX.Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9510438,
   "Latitude": 107.2423004
 },
 {
   "STT": 2786,
   "Name": "Quầy thuốc Kim Anh",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 2787,
   "Name": "Quầy thuốc Lâm Thanh Hà",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2788,
   "Name": "Quầy thuốc Minh Anh",
   "address": "Tổ 5, ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 2789,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Phúc Sinh",
   "address": "20B1, tổ 10, KP 11, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.928537,
   "Latitude": 106.8583508
 },
 {
   "STT": 2790,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Thái Lộc",
   "address": "D724, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520451,
   "Latitude": 106.8772376
 },
 {
   "STT": 2791,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Long Việt",
   "address": "638, tổ 1, KP 4B, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2792,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Xuân Chung",
   "address": "1A/5, tổ 5, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891472,
   "Latitude": 106.8502969
 },
 {
   "STT": 2793,
   "Name": "Quầy thuốc Ngọc Anh Thư",
   "address": "576, tổ 10, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2794,
   "Name": "Quầy thuốc Linh Thanh",
   "address": "1058, tổ 2, ấp Đồng, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2795,
   "Name": "Quầy thuốc Kim Duy",
   "address": "430A Đinh Quang Ân, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2796,
   "Name": "Nhà thuốc Ngọc Hiếu",
   "address": "338/88, KP 13, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 2797,
   "Name": "Quầy thuốc Trần Tâm",
   "address": "113/14, tổ 4B, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2798,
   "Name": "Quầy thuốc Phúc Sinh Đường",
   "address": "Ấp Quới Thạnh, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6340856,
   "Latitude": 106.947666
 },
 {
   "STT": 2799,
   "Name": "Quầy thuốc Hoàng Khang",
   "address": "Ấp Bà Trường, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6468256,
   "Latitude": 106.9363446
 },
 {
   "STT": 2800,
   "Name": "Quầy thuốc Nhuận Hòa",
   "address": "02, khu 2000, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2801,
   "Name": "Nhà thuốc Tiến Đạt",
   "address": "523/182, QL 51, tổ 10, KP 2, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2802,
   "Name": "Quầy thuốc Phúc Lâm",
   "address": "Ấp Bảo Thị, xã Xuân Định, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8989588,
   "Latitude": 107.2576707
 },
 {
   "STT": 2803,
   "Name": "Nhà thuốc Tâm Vinh",
   "address": "62/5, tổ 4, KP 10, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.928537,
   "Latitude": 106.8583508
 },
 {
   "STT": 2804,
   "Name": "Quầy thuốc Cẩm Hương",
   "address": "73A5, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8914453,
   "Latitude": 106.8487483
 },
 {
   "STT": 2805,
   "Name": "Quầy thuốc Ngọc Thiện Bảo",
   "address": "24, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2806,
   "Name": "Quầy thuốc Hy Hạnh",
   "address": "Ấp 4, xã Thừa Đức, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7646516,
   "Latitude": 107.1502859
 },
 {
   "STT": 2807,
   "Name": "Quầy thuốc Tâm Phúc",
   "address": "Tổ 10, ấp 2, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9838138,
   "Latitude": 107.0275775
 },
 {
   "STT": 2808,
   "Name": "Nhà thuốc Ngọc Ánh Ga BH",
   "address": "77, Hưng Đạo Vương, KP 1, P.Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.950918,
   "Latitude": 106.8206605
 },
 {
   "STT": 2809,
   "Name": "Quầy thuốc Ngọc Bích Pharmacy",
   "address": "408, tổ 14, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8637012,
   "Latitude": 106.9244978
 },
 {
   "STT": 2810,
   "Name": "Quầy thuốc Thanh Hiền",
   "address": "Ki ốt số 6, tổ 1, khu 3, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.5673841,
   "Latitude": 107.3576689
 },
 {
   "STT": 2811,
   "Name": "Quầy thuốc Hoàng Dương",
   "address": "Ấp 4, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.880297,
   "Latitude": 106.993089
 },
 {
   "STT": 2812,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Vĩnh Khang 2",
   "address": "Số 747, đường Sông Thao-Bàu Hàm, xã Bàu Hàm, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9783535,
   "Latitude": 107.108044
 },
 {
   "STT": 2813,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "107/3H, Võ Dõng 3, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 2814,
   "Name": "Nhà thuốc Xuân Thương Phát",
   "address": "20/C3, tổ 10, KP 11, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9292629,
   "Latitude": 106.8583352
 },
 {
   "STT": 2815,
   "Name": "Quầy thuốc Lương Sơn",
   "address": "Tổ 7, ấp 2, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9838138,
   "Latitude": 107.0275775
 },
 {
   "STT": 2816,
   "Name": "Nhà thuốc Huỳnh Phú Linh",
   "address": "128, tổ 25, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 2817,
   "Name": "Quầy thuốc Thanh Hằng",
   "address": "Ấp Lợi Hà, xã Thanh Bình, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.958597,
   "Latitude": 107.0457563
 },
 {
   "STT": 2818,
   "Name": "Quầy thuốc Nguyễn Hoàn",
   "address": "Tổ 4, ấp 6, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 2819,
   "Name": "Quầy thuốc Long Thọ",
   "address": "Ấp 5, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6943374,
   "Latitude": 106.9514877
 },
 {
   "STT": 2820,
   "Name": "Quầy thuốc Gia Phạm",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2821,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "234, ấp 6, xã Xuân Bắc, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0359229,
   "Latitude": 107.3208528
 },
 {
   "STT": 2822,
   "Name": "Quầy thuốc Gia Phúc",
   "address": "Ấp Thị Cầu, xã Phú Đông, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7128827,
   "Latitude": 106.8001396
 },
 {
   "STT": 2823,
   "Name": "Quầy thuốc Thảo Hiền",
   "address": "Ấp Tam Hiệp, xã Xuân Hiệp, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9065786,
   "Latitude": 107.387487
 },
 {
   "STT": 2824,
   "Name": "Quầy thuốc Kim Phát",
   "address": "SN33, KDC4, ấp Cây Xăng, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0900376,
   "Latitude": 107.2145647
 },
 {
   "STT": 2825,
   "Name": "Quầy thuốc Minh Đức",
   "address": "KDC 7, ấp Cây Xăng, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0900376,
   "Latitude": 107.2145647
 },
 {
   "STT": 2826,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "Ấp Cây Xăng, xã Phú Túc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0900376,
   "Latitude": 107.2145647
 },
 {
   "STT": 2827,
   "Name": "Nhà thuốc Bông Sen Vàng",
   "address": "19/62, tổ 8, KP 1, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2828,
   "Name": "Quầy thuốc Phương Ngọc",
   "address": "Ấp Bến Cộ, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7437649,
   "Latitude": 106.826849
 },
 {
   "STT": 2829,
   "Name": "Quầy thuốc Phạm Đức Nhân",
   "address": "119, tổ 2, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2830,
   "Name": "Quầy thuốc",
   "address": "Ấp 2, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8413355,
   "Latitude": 106.944158
 },
 {
   "STT": 2831,
   "Name": "Quầy thuốc Thiên Ý",
   "address": "Ấp Trung Lương, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0082873,
   "Latitude": 107.4329473
 },
 {
   "STT": 2832,
   "Name": "Quầy thuốc",
   "address": "Tổ 18, đường Hương Lộ 12, ấp Bà Ký, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7238793,
   "Latitude": 106.9566365
 },
 {
   "STT": 2833,
   "Name": "Quầy thuốc Hạnh Ngọc Phong Trâm",
   "address": "924, tổ 18, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2834,
   "Name": "Quầy thuốc Hồng Nhân",
   "address": "Tổ 2, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.981049,
   "Latitude": 107.019946
 },
 {
   "STT": 2835,
   "Name": "Nhà thuốc Doanh Nhân",
   "address": "71/29, KP 1, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2836,
   "Name": "Quầy thuốc Trí Đạt",
   "address": "Số 1/18, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2837,
   "Name": "Quầy thuốc Toàn Nga",
   "address": "Ấp Thống Nhất, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6810076,
   "Latitude": 106.8553448
 },
 {
   "STT": 2838,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Ấp Chợ, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2839,
   "Name": "Nhà thuốc Dương Nguyên",
   "address": "995D, tổ 12, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2840,
   "Name": "Quầy thuốc Bảo Nam",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2841,
   "Name": "Quầy thuốc Kim Huyền",
   "address": "Số 107/4A, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9585201,
   "Latitude": 106.9380709
 },
 {
   "STT": 2842,
   "Name": "Quầy thuốc Phúc Trang",
   "address": "69, tổ 2, ấp 6, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9820287,
   "Latitude": 107.025637
 },
 {
   "STT": 2843,
   "Name": "Quầy thuốc Hoài Thương",
   "address": "Số 19/2, khu 1, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 2844,
   "Name": "Quầy thuốc Hoàng Hải II",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 2845,
   "Name": "Nhà thuốc Thái Hòa Phát",
   "address": "2E/1, tổ 7, KP 3, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9656045,
   "Latitude": 106.866728
 },
 {
   "STT": 2846,
   "Name": "Quầy thuốc Đức Minh",
   "address": "Số 2132, tổ 9, ấp Quảng Lộc, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2847,
   "Name": "Quầy thuốc Tuấn Ngọc",
   "address": "Số 235, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9652691,
   "Latitude": 106.934588
 },
 {
   "STT": 2848,
   "Name": "Nhà thuốc Thảo Ân",
   "address": "153E, tổ 3, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891472,
   "Latitude": 106.8502969
 },
 {
   "STT": 2849,
   "Name": "Nhà thuốc Thiên Lộc Thiên Lộc",
   "address": "186, tổ 2, KP 11, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.927241,
   "Latitude": 106.8571987
 },
 {
   "STT": 2850,
   "Name": "Quầy thuốc Đại Việt",
   "address": "Số 03, đường Hàng Gòn-Xuân Quế, ấp Hàng Gòn, xã Hàng Gòn, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8803302,
   "Latitude": 107.2075388
 },
 {
   "STT": 2851,
   "Name": "Quầy thuốc Vy Ngân",
   "address": "Đường Hàm Nghi, tổ 30B, ấp Ruộng Lớn, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9440087,
   "Latitude": 107.2735215
 },
 {
   "STT": 2852,
   "Name": "Nhà thuốc phòng khám đa khoa Ái Nghĩa Xuân Lộc",
   "address": "Khu phố 8, Quốc lộ 1A, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9425098,
   "Latitude": 107.1556804
 },
 {
   "STT": 2853,
   "Name": "Quầy thuốc Huy Phụng",
   "address": "Ấp Hòa Bình, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7005984,
   "Latitude": 106.8261131
 },
 {
   "STT": 2854,
   "Name": "Quầy thuốc Hoa Linh",
   "address": "Ấp Bình Phú, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 2855,
   "Name": "Quầy thuốc Hiền Tài",
   "address": "Số 76 Hai Bà Trưng, khu Phước Long, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7826025,
   "Latitude": 106.9569351
 },
 {
   "STT": 2856,
   "Name": "Nhà thuốc Thu Trần",
   "address": "61B, Nguyễn Văn Tiên, KP 11, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9788092,
   "Latitude": 106.8459773
 },
 {
   "STT": 2857,
   "Name": "Nhà thuốc Dung Ngọc",
   "address": "9/17, KP 9, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9456478,
   "Latitude": 106.8544474
 },
 {
   "STT": 2858,
   "Name": "Nhà thuốc Giang Hiền",
   "address": "89, KP Bình Dương, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9037887,
   "Latitude": 106.882335
 },
 {
   "STT": 2859,
   "Name": "Nhà thuốc Hoa Thăng",
   "address": "D138, tổ 4, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2860,
   "Name": "Nhà thuốc Tâm An Khang",
   "address": "238A, Nguyễn Trường Tộ, KP 4A, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9668904,
   "Latitude": 106.9079296
 },
 {
   "STT": 2861,
   "Name": "Quầy thuốc Hằng Thu",
   "address": "161A, tổ 3, ấp Long Khánh 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2862,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Gia Thiền",
   "address": "33/33, Tổ 9,  KP 2, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8917422,
   "Latitude": 106.8501892
 },
 {
   "STT": 2863,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Tấn Xoan",
   "address": "Tổ 38,  KP 4B, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.994678,
   "Latitude": 106.86104
 },
 {
   "STT": 2864,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Nam Hải Phát",
   "address": "39, Tổ 39,  KP 4C, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.994678,
   "Latitude": 106.86104
 },
 {
   "STT": 2865,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Năm Hậu",
   "address": "Tổ 4A, Nguyễn Thái Học, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.952032,
   "Latitude": 106.8772508
 },
 {
   "STT": 2866,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Việt Hà Tùng",
   "address": "33/33, Tổ 9,  KP 2, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891962,
   "Latitude": 106.8497778
 },
 {
   "STT": 2867,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Lệ Phượng",
   "address": "07 Tổ 33,  KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916522,
   "Latitude": 106.8501889
 },
 {
   "STT": 2868,
   "Name": "Quầy thuốc",
   "address": "Tổ 2, ấp 2, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 2869,
   "Name": "Quầy thuốc Thu Huyền",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2870,
   "Name": "Quầy thuốc Mỹ Hạnh 2",
   "address": "66 Trần Phú, khu 7, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.921468,
   "Latitude": 107.4156782
 },
 {
   "STT": 2871,
   "Name": "Nhà thuốc Nguyễn Khánh Ngân",
   "address": "04, Đồng Khởi, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9791226,
   "Latitude": 106.8483536
 },
 {
   "STT": 2872,
   "Name": "Nhà thuốc Kiên Hải Linh",
   "address": "H45, KP 7, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9890275,
   "Latitude": 106.8296372
 },
 {
   "STT": 2873,
   "Name": "Quầy thuốc Cẩm Trang",
   "address": "21/1, tổ 16, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9802913,
   "Latitude": 106.8547914
 },
 {
   "STT": 2874,
   "Name": "Quầy thuốc Thiên An",
   "address": "6/1A Đông Kim, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0327201,
   "Latitude": 107.1803206
 },
 {
   "STT": 2875,
   "Name": "Nhà thuốc Thúy Loan",
   "address": "211, tổ 8, KP 4, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.952032,
   "Latitude": 106.8772508
 },
 {
   "STT": 2876,
   "Name": "Quầy thuốc Huy Ngân",
   "address": "Tổ 3C, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2877,
   "Name": "Quầy thuốc Văn Cường",
   "address": "Kiot số 1, chợ Bàu Cối, xã Bảo Quang, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9922342,
   "Latitude": 107.2671443
 },
 {
   "STT": 2878,
   "Name": "Quầy thuốc Hạnh Phúc",
   "address": "Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8505029,
   "Latitude": 107.042198
 },
 {
   "STT": 2879,
   "Name": "Quầy thuốc",
   "address": "217-K3, Hưng Hiệp, Hưng Lộc, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9394754,
   "Latitude": 107.1026467
 },
 {
   "STT": 2880,
   "Name": "Quầy thuốc Tân Toàn Tâm",
   "address": "Ấp Hưng Long, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2881,
   "Name": "Nhà thuốc Bình Nguyên",
   "address": "10, Hoàng Bá Bích, tổ 3, KP 5, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 2882,
   "Name": "Quầy thuốc Lâm Đức",
   "address": "Tổ 14, khu 13, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7932946,
   "Latitude": 107.0135297
 },
 {
   "STT": 2883,
   "Name": "Quầy thuốc Tâm Đức 2",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2884,
   "Name": "Quầy thuốc Minh Hoài",
   "address": "Ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9781286,
   "Latitude": 107.0233561
 },
 {
   "STT": 2885,
   "Name": "Quầy thuốc Văn Thư",
   "address": "179, tổ 5, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 2886,
   "Name": "Quầy thuốc Thu Điền",
   "address": "72, Dương Diên Nghệ, tổ 3, ấp Long Khánh 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8667175,
   "Latitude": 106.9138562
 },
 {
   "STT": 2887,
   "Name": "Quầy thuốc Thanh Xuân",
   "address": "14, ấp Suối Cát 1, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9123172,
   "Latitude": 107.3644817
 },
 {
   "STT": 2888,
   "Name": "Quầy thuốc Bích Thu",
   "address": "Tổ 27, ấp 2A, xã Xuân Hưng, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8461842,
   "Latitude": 107.5092529
 },
 {
   "STT": 2889,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Ấp Chà Rang, xã Suối Cao, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 11.0095589,
   "Latitude": 107.3622501
 },
 {
   "STT": 2890,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "A3/065C, Nguyễn Huệ 1, xã Quang Trung, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.991378,
   "Latitude": 107.154914
 },
 {
   "STT": 2891,
   "Name": "Quầy thuốc Hoàng Dung",
   "address": "457A, tổ 12, ấp Miễu, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8913594,
   "Latitude": 106.9181532
 },
 {
   "STT": 2892,
   "Name": "Quầy thuốc Minh Khôi",
   "address": "Ấp Phước Lý, xã Đại Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7442163,
   "Latitude": 106.8237374
 },
 {
   "STT": 2893,
   "Name": "Nhà thuốc Phước Tâm An",
   "address": "279, tổ 26A, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720613,
   "Latitude": 106.9097557
 },
 {
   "STT": 2894,
   "Name": "Công ty cổ phần dược phẩm OPV",
   "address": "Số 27, đường 3A, KCN Biên Hòa II, phường An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9230099,
   "Latitude": 106.8720388
 },
 {
   "STT": 2895,
   "Name": "Quầy thuốc Minh Liên",
   "address": "1/2, ấp Long Khánh 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.833891,
   "Latitude": 106.9079736
 },
 {
   "STT": 2896,
   "Name": "Quầy thuốc Bảo Phương Anh",
   "address": "68D, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720719,
   "Latitude": 106.9097344
 },
 {
   "STT": 2897,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "2159, ấp 5, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 2898,
   "Name": "Quầy thuốc Nghị Lợi",
   "address": "496, ấp Tây Minh, xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 2900,
   "Name": "Quầy thuốc Phúc Huệ",
   "address": "Số 24, đường Hàm Nghi, tổ 21, ấp Ruộng Hời, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9440087,
   "Latitude": 107.2735215
 },
 {
   "STT": 2901,
   "Name": "Quầy thuốc Tố Hảo",
   "address": "KDC 7, ấp 5, xã Phú Tân, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1775809,
   "Latitude": 107.3410392
 },
 {
   "STT": 2902,
   "Name": "Công ty cổ phần dược An Hảo",
   "address": "Số 309B, tổ 11, ấp Miễu, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2903,
   "Name": "Công ty cổ phần xuất nhập khẩu y tế Domesco-chi nhánh Miền Đông",
   "address": "Số 144-146, đường N4, KDC Liên Kế, KP 1, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2904,
   "Name": "Chi nhánh công ty cổ phần dược phẩm Imexpharm tại Đồng Nai",
   "address": "K44, KP 1, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 2905,
   "Name": "Quầy thuốc Thiên Ân",
   "address": "Ấp Thuận Trường, xã Sông Thao, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2906,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Khu 6, ấp Nam Hà, xã Xuân Bảo, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8266178,
   "Latitude": 107.3410716
 },
 {
   "STT": 2907,
   "Name": "Quầy thuốc",
   "address": "174/B, Phúc Nhạc 1, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 2908,
   "Name": "Quầy thuốc Quỳnh Mai",
   "address": "Số 09/3, xóm 1A/3, ấp An Bình, xã Trung Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9435145,
   "Latitude": 107.0680868
 },
 {
   "STT": 2909,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Số 95, đường 2/9, khu phố 4, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9720716,
   "Latitude": 106.9097763
 },
 {
   "STT": 2910,
   "Name": "Quầy thuốc Hồng Lan",
   "address": "Số 204, QL1A, ấp Hưng Bình, xã Hưng Thịnh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9406956,
   "Latitude": 107.0842015
 },
 {
   "STT": 2911,
   "Name": "Quầy thuốc Cát Tường",
   "address": "SN 864, tổ 5, khu 8, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 2912,
   "Name": "Quầy thuốc Đỗ Hiền",
   "address": "Ấp 4, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2913,
   "Name": "Quầy thuốc",
   "address": "Khu 5, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8642324,
   "Latitude": 106.9667294
 },
 {
   "STT": 2914,
   "Name": "Nhà thuốc Minh Châu 5",
   "address": "Số 214, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 2915,
   "Name": "Nhà thuốc Lộc Thịnh",
   "address": "E627, tổ 35, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.891653,
   "Latitude": 106.8501823
 },
 {
   "STT": 2916,
   "Name": "Nhà thuốc Tam Đức-trực thuộc Phòng khám đa khoa Tam Đức",
   "address": "528/15, xa lộ ĐỒNG NAI, KP 4, P.Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.955306,
   "Latitude": 106.8735829
 },
 {
   "STT": 2917,
   "Name": "Quầy thuốc Tâm Y Đức",
   "address": "1968, ấp An Hòa, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9359157,
   "Latitude": 106.8038873
 },
 {
   "STT": 2918,
   "Name": "Nhà thuốc phòng khám đa khoa Đại Thiện",
   "address": "Số 93, đường Đồng Khởi, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9561284,
   "Latitude": 106.8629837
 },
 {
   "STT": 2919,
   "Name": "Quầy thuốc Hoa Mai",
   "address": "KP 1, thị trấn Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0852697,
   "Latitude": 107.0332289
 },
 {
   "STT": 2920,
   "Name": "Quầy thuốc Toàn Anh",
   "address": "410, tổ 9, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2921,
   "Name": "Quầy thuốc Thảo Châu",
   "address": "Ấp Bình Trung, xã Tân An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0499492,
   "Latitude": 106.9254309
 },
 {
   "STT": 2922,
   "Name": "Quầy thuốc Lê Chiến",
   "address": "170A/20, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9265399,
   "Latitude": 106.806079
 },
 {
   "STT": 2923,
   "Name": "Quầy thuốc Nguyễn Hải Yến",
   "address": "237, Đinh Quang Ân, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2924,
   "Name": "Quầy thuốc Tiên Thịnh Phát ",
   "address": "1162, tổ 4, ấp Miễu, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8926724,
   "Latitude": 106.9103322
 },
 {
   "STT": 2925,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Tổ 4, ấp Hiền Đức, xã Phước Thái, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6940728,
   "Latitude": 107.0214957
 },
 {
   "STT": 2926,
   "Name": "Quầy thuốc Bảo Thiên",
   "address": "190, ấp Long Khánh 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8631954,
   "Latitude": 106.9241116
 },
 {
   "STT": 2927,
   "Name": "Nhà thuốc Tuệ Tâm",
   "address": "197, Phan Đình Phùng, KP 2, P.Thanh Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9498346,
   "Latitude": 106.8177494
 },
 {
   "STT": 2928,
   "Name": "Quầy thuốc",
   "address": "60, ấp Tân Lập, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9168346,
   "Latitude": 106.9448957
 },
 {
   "STT": 2929,
   "Name": "Quầy thuốc Xuân Liễu",
   "address": "972, Bùi Hữu Nghĩa, ấp Đồng Nai, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9397905,
   "Latitude": 106.8115818
 },
 {
   "STT": 2930,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Nga Bộ",
   "address": "01A, Tổ 1, KP 6, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 2931,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Phúc Tâm",
   "address": "18/82, KP 12, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9738414,
   "Latitude": 106.8799658
 },
 {
   "STT": 2932,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Vạn Phúc Gia An",
   "address": "E83, Tổ 4, KP 5A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 2933,
   "Name": "Quầy thuốc Nhật Lệ",
   "address": "Số 39, đường Yên Thế, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9668655,
   "Latitude": 106.9357131
 },
 {
   "STT": 2934,
   "Name": "Quầy thuốc",
   "address": "15 Thành Thái, tổ 5, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9168346,
   "Latitude": 106.9448957
 },
 {
   "STT": 2935,
   "Name": "Quầy thuốc Phúc Đức",
   "address": "Ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7630434,
   "Latitude": 106.959596
 },
 {
   "STT": 2936,
   "Name": "Quầy thuốc Thiện Ân",
   "address": "192, ấp 2, xã Lộ 25, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.8694548,
   "Latitude": 107.0899432
 },
 {
   "STT": 2937,
   "Name": "Quầy thuốc Tâm Tâm Anh",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 2938,
   "Name": "Quầy thuốc Bảo Khang",
   "address": "Tổ 17, khu Văn Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7857213,
   "Latitude": 106.9477147
 },
 {
   "STT": 2939,
   "Name": "Quầy thuốc Thanh Liêm 2",
   "address": "Tổ 11, ấp Tập Phước, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7070229,
   "Latitude": 107.0079678
 },
 {
   "STT": 2940,
   "Name": "Quầy thuốc Như Ý",
   "address": "Số 13, đường số 15, ấp Hàng Gòn, xã Hàng Gòn, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9262373,
   "Latitude": 107.1871024
 },
 {
   "STT": 2941,
   "Name": "Quầy thuốc Hồng Lê",
   "address": "Số 93, khu phố 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 2942,
   "Name": "Quầy thuốc Hồng Ân",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 2943,
   "Name": "Quầy thuốc Kim Yến",
   "address": "1L, Trần Hưng Đạo, khu 6, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9259723,
   "Latitude": 107.4055814
 },
 {
   "STT": 2944,
   "Name": "Quầy thuốc Đức Hạnh",
   "address": "Thửa đất số 51, tờ bản đồ số 15, ấp Trung Tâm, xã Xuân Lập, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9092302,
   "Latitude": 107.1779936
 },
 {
   "STT": 2945,
   "Name": "Quầy thuốc ",
   "address": "Số 450, tổ 15, ấp 10, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 2946,
   "Name": "Quầy thuốc Hoàng Châu",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 2947,
   "Name": "Quầy thuốc Duy Tân",
   "address": "SN 1243, ấp Ngọc Lâm 3, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3074298,
   "Latitude": 107.4499404
 },
 {
   "STT": 2948,
   "Name": "Quầy thuốc Như Tâm",
   "address": "2224, Quốc lộ 1A, ấp 4, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8808391,
   "Latitude": 107.4357912
 },
 {
   "STT": 2949,
   "Name": "Quầy thuốc Duy Oanh",
   "address": "Tổ 47, ấp 4, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 2950,
   "Name": "Nhà thuốc Vinh Thanh Hải",
   "address": "238, Bùi Văn Hòa, KP 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9256148,
   "Latitude": 106.8799259
 },
 {
   "STT": 2951,
   "Name": "Nhà thuốc Tân Thanh Hải",
   "address": "1365, Bùi Văn Hòa, KP 7, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9319693,
   "Latitude": 106.8749571
 },
 {
   "STT": 2952,
   "Name": "Quầy thuốc Trần Tú Trinh",
   "address": "7/28, KP 6, P.Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 2953,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Ấp Ngọc Lâm 2, xã Phú Xuân, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2856209,
   "Latitude": 107.4551725
 },
 {
   "STT": 2954,
   "Name": "Quầy thuốc Lê Thảo",
   "address": "Ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 2955,
   "Name": "Quầy thuốc Tuấn Kiệt",
   "address": "Lô A23, đường Lý Thái Tổ, khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.792271,
   "Latitude": 106.9479545
 },
 {
   "STT": 2956,
   "Name": "Quầy thuốc",
   "address": "403, tổ 14, ấp 10, xã Xuân Tây, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7417945,
   "Latitude": 107.3494043
 },
 {
   "STT": 2957,
   "Name": "Quầy thuốc phòng khám đa khoa Hoàng Dũng",
   "address": "Số 41/A, QL 1A, ấp Nhân Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9439407,
   "Latitude": 107.0656976
 },
 {
   "STT": 2958,
   "Name": "Quầy thuốc Thanh",
   "address": "SN 168, KP 114, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2959,
   "Name": "Quầy thuốc Tâm An",
   "address": "Số 554, ấp 6, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7258801,
   "Latitude": 107.0763453
 },
 {
   "STT": 2960,
   "Name": "Quầy thuốc Phúc Lộc",
   "address": "SN 35, tổ 1, KP Hiệp Quyết, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2961,
   "Name": "Quầy thuốc Thảo Mơ",
   "address": "Khu 3, ấp Hiệp Tâm 1, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 2962,
   "Name": "Quầy thuốc Tín Hằng",
   "address": "337/11 Trần Cao Vân, Bàu Hàm 2, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9420305,
   "Latitude": 107.13386
 },
 {
   "STT": 2963,
   "Name": "Quầy thuốc Anh Thư",
   "address": "SN 13/6, ấp 1, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.0562105,
   "Latitude": 107.2725505
 },
 {
   "STT": 2964,
   "Name": "Quầy thuốc Thúy Vân",
   "address": "Tổ 15, khu 12, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8438035,
   "Latitude": 106.9889904
 },
 {
   "STT": 2965,
   "Name": "Nhà thuốc Phạm Thảo",
   "address": "2B, KP 9, P.Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9458319,
   "Latitude": 106.8569755
 },
 {
   "STT": 2966,
   "Name": "Quầy thuốc Song Long",
   "address": "55A/20, ấp Cầu Hang, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9265399,
   "Latitude": 106.806079
 },
 {
   "STT": 2967,
   "Name": "Quầy thuốc Hoàng Dung",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 2968,
   "Name": "Quầy thuốc Khánh Thành",
   "address": "162, tổ 9, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2969,
   "Name": "Quầy thuốc Thuận Anh",
   "address": "Tổ 4, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 2970,
   "Name": "Quầy thuốc Ngọc Nhung",
   "address": "1566A1, tổ 26, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2971,
   "Name": "Quầy thuốc Vĩnh Lộc",
   "address": "Số 15/4, ấp Lộc Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2972,
   "Name": "Quầy thuốc",
   "address": "K1/27C, Nguyễn Thị Tồn, KP 2, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9387766,
   "Latitude": 106.813618
 },
 {
   "STT": 2973,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Tổ 3B, khu phố 4, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9720555,
   "Latitude": 106.9097673
 },
 {
   "STT": 2974,
   "Name": "Quầy thuốc Thanh Liêm",
   "address": "Tổ 32, khu Cầu Xéo, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7736188,
   "Latitude": 106.9567852
 },
 {
   "STT": 2975,
   "Name": "Quầy thuốc Kim Hương",
   "address": "Tổ 2, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.981049,
   "Latitude": 107.019946
 },
 {
   "STT": 2976,
   "Name": "Quầy thuốc Huỳnh Phú Linh",
   "address": "Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8505029,
   "Latitude": 107.042198
 },
 {
   "STT": 2977,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Tổ 3, ấp Thanh Bình, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 2978,
   "Name": "Quầy thuốc Tâm An 1",
   "address": "Ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 2979,
   "Name": "Quầy thuốc  Đức Tâm Khang",
   "address": "469, Đinh Quang Ân, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 2980,
   "Name": "Quầy thuốc Hà Giao",
   "address": "94B, tổ 8, ấp Tân Cang, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9168346,
   "Latitude": 106.9448957
 },
 {
   "STT": 2981,
   "Name": "Quầy thuốc Bích Hồng",
   "address": "Số 407, tổ 17, ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7584612,
   "Latitude": 106.9343848
 },
 {
   "STT": 2982,
   "Name": "Quầy thuốc Song My",
   "address": "Ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 2983,
   "Name": "Quầy thuốc Nam Quỳnh Anh",
   "address": "496, tổ 9, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 2984,
   "Name": "Quầy thuốc Ngân Châu",
   "address": "Ấp Bà Trường, xã Phước An, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6468256,
   "Latitude": 106.9363446
 },
 {
   "STT": 2985,
   "Name": "Nhà thuốc Y Bình",
   "address": "53/56A, KP 2, P.Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9266663,
   "Latitude": 106.8498825
 },
 {
   "STT": 2986,
   "Name": "Quầy thuốc Bích Hồng 1",
   "address": "Nguyễn Văn Ký, tổ 4, ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 2987,
   "Name": "Quầy thuốc Mạnh Thiện",
   "address": "1447, tổ 30, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9244804,
   "Latitude": 106.9508428
 },
 {
   "STT": 2988,
   "Name": "Quầy thuốc Phước Nguyên",
   "address": "1332, tổ 9, ấp Tân Mai 2, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.869546,
   "Latitude": 106.907211
 },
 {
   "STT": 2989,
   "Name": "Quầy thuốc Hoàng Minh Phúc",
   "address": "743, tổ 12, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2990,
   "Name": "Quầy thuốc Nhật Nam",
   "address": "Ấp Lộc Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 2991,
   "Name": "Quầy thuốc Phúc Hiền",
   "address": "A17, khu 2003, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 2992,
   "Name": "Quầy thuốc Lan Cường",
   "address": "Số 43, khu phố 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 2993,
   "Name": "Quầy thuốc Sức Khỏe",
   "address": "96 đường 2, ấp 3, xã Xuân Hòa, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8770877,
   "Latitude": 107.544598
 },
 {
   "STT": 2994,
   "Name": "Quầy thuốc Ngọc Dung",
   "address": "Ấp Bình Hòa, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9060699,
   "Latitude": 107.323272
 },
 {
   "STT": 2995,
   "Name": "Quầy thuốc phòng khám đa khoa Đông Sài Gòn",
   "address": "Số 1A, tỉnh lộ 16, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.939225,
   "Latitude": 106.812027
 },
 {
   "STT": 2996,
   "Name": "Quầy thuốc Cẩm Hà",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 2997,
   "Name": "Quầy thuốc Hoàng Xuân Kiên",
   "address": "SN 1668, tổ 4, ấp 5, xã Tà Lài, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.3670477,
   "Latitude": 107.3671431
 },
 {
   "STT": 2998,
   "Name": "Quầy thuốc Trang Phương",
   "address": "1762 ấp 1, xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8680729,
   "Latitude": 107.4665451
 },
 {
   "STT": 2999,
   "Name": "Quầy thuốc Trúc Quỳnh",
   "address": "Ấp Suối Cát, xã Suối Cát, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9084393,
   "Latitude": 107.3654294
 },
 {
   "STT": 3000,
   "Name": "Quầy thuốc Bệnh viện đa khoa Cao Su Đồng Nai",
   "address": "Ấp Dưỡng Đường, xã Suối Tre, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9456269,
   "Latitude": 107.2075388
 },
 {
   "STT": 3001,
   "Name": "Quầy thuốc Hiếu",
   "address": "100, Hùng Vương, tổ 5, khu 8, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9177143,
   "Latitude": 107.3998582
 },
 {
   "STT": 3002,
   "Name": "Quầy thuốc Ngọc Thu",
   "address": "Ấp La Hoa, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8410566,
   "Latitude": 107.37811
 },
 {
   "STT": 3003,
   "Name": "Quầy thuốc Ý Châu",
   "address": "SN 84, khu phố 7, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 3004,
   "Name": "Quầy thuốc Ngọc Thu",
   "address": "Tổ 3A, khu phố 4, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9720555,
   "Latitude": 106.9097673
 },
 {
   "STT": 3005,
   "Name": "Nhà thuốc Nhung Bảo",
   "address": "10, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9807717,
   "Latitude": 106.8715891
 },
 {
   "STT": 3006,
   "Name": "Quầy thuốc Minh Hằng",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 3007,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Số 768, ấp Trà Cồ, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448786,
   "Latitude": 106.9807112
 },
 {
   "STT": 3008,
   "Name": "Quầy thuốc Khánh Huyền",
   "address": "SN 100 G, Tà Lài, tổ 6, khu 3, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 3009,
   "Name": "Quầy thuốc Lê Như",
   "address": "Ấp Bình Hòa, xã Xuân Phú, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9060699,
   "Latitude": 107.323272
 },
 {
   "STT": 3010,
   "Name": "Nhà thuốc Thảo Nga",
   "address": "66A, tổ 25, KP 5, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 3011,
   "Name": "Quầy thuốc Tâm An",
   "address": "Số 113, tổ 5, ấp Bàu Cối, Xã Bảo Quang, Thị xã Long Khánh, Tỉnh Đồng Nai",
   "Longtitude": 11.0139135,
   "Latitude": 107.2775721
 },
 {
   "STT": 3012,
   "Name": "Quầy thuốc Vũ Chi 2",
   "address": "Số 07/8B, Duy Tân, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9369869,
   "Latitude": 107.2652964
 },
 {
   "STT": 3013,
   "Name": "Quầy thuốc Trà My",
   "address": "Số 184, ấp Quảng Hòa, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 3014,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Hòa Bình",
   "address": "số nhà 33, khu phố Hiệp Thương, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 3015,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Sơn Ngọc Minh",
   "address": "62D32, tổ 3A, khu phố 3, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9643768,
   "Latitude": 106.9072103
 },
 {
   "STT": 3016,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Nguyên Yên",
   "address": "103/4, KP 4, P.Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9708772,
   "Latitude": 106.9093
 },
 {
   "STT": 3017,
   "Name": "Quầy thuốc An Phát",
   "address": "Số 450A, khu 4, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8623041,
   "Latitude": 106.961174
 },
 {
   "STT": 3018,
   "Name": "Quầy thuốc Mỹ Trinh",
   "address": "Ấp Suối Nhát, xã Xuân Đông, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.8225699,
   "Latitude": 107.2666396
 },
 {
   "STT": 3019,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp 4, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.008936,
   "Latitude": 106.8404623
 },
 {
   "STT": 3020,
   "Name": "Quầy thuốc Bảo An",
   "address": "Ấp 2, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0097053,
   "Latitude": 106.8276976
 },
 {
   "STT": 3021,
   "Name": "Quầy thuốc Khánh Ngọc Đan",
   "address": "84B/1, Đỗ Văn Thi, ấp Nhất Hòa, xã Hiệp Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 3022,
   "Name": "Quầy thuốc Đức Nguyên",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 3023,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Ấp Đất Mới, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7282487,
   "Latitude": 106.9066924
 },
 {
   "STT": 3024,
   "Name": "Quầy thuốc Vũ Châu",
   "address": "Tổ 11, khu 4, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.853688,
   "Latitude": 106.9618341
 },
 {
   "STT": 3025,
   "Name": "Quầy thuốc Duy Tiên",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 3026,
   "Name": "Chi nhánh công ty TNHH Thương mại Dịch vụ Nam Giang",
   "address": "Số G243, Bùi Văn Hòa, KP 7, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9411075,
   "Latitude": 106.9004059
 },
 {
   "STT": 3027,
   "Name": "Quầy thuốc Thủy Tiên",
   "address": "49, ấp Trung Nghĩa, xã Xuân Trường, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 3028,
   "Name": "Quầy thuốc Anh Thư",
   "address": "SN 1260, tổ 9, ấp Ngọc Lâm 2, xã Phú Thanh, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.240525,
   "Latitude": 107.4736016
 },
 {
   "STT": 3029,
   "Name": " Quầy thuốc Hoàng Dân",
   "address": "Khu 1, ấp Nhân Hòa, xã Tây Hòa, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 3030,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "Số 304, ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 3031,
   "Name": "Quầy thuốc Khánh Ngân",
   "address": "Ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3032,
   "Name": "Quầy thuốc An Phú",
   "address": "Tổ 13, khu 2, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8281006,
   "Latitude": 106.9318203
 },
 {
   "STT": 3033,
   "Name": "Quầy thuốc Thơm Thịnh",
   "address": "1273/63/2, tổ 8, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 3034,
   "Name": "Quầy thuốc Hoàng Trâm",
   "address": "Khu 6 (thửa đất số 3, tờ bản đồ số 76), thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9259723,
   "Latitude": 107.4055814
 },
 {
   "STT": 3035,
   "Name": "Nhà thuốc Thành Loan",
   "address": "68A, QL 51, KP Bình Dương, P.Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9029687,
   "Latitude": 106.8501239
 },
 {
   "STT": 3036,
   "Name": "Quầy thuốc Ngọc Ân",
   "address": "516, tổ 12, khu 4, ấp 2, xã An Hoà, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 3037,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Tổ 2, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8140113,
   "Latitude": 106.9179583
 },
 {
   "STT": 3038,
   "Name": "Quầy thuốc phòng khám An Việt",
   "address": "Kiot 01, số 152A, đường 769, ấp Bình Lâm, xã Lộc An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7966587,
   "Latitude": 106.9702458
 },
 {
   "STT": 3039,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "260, tổ 8, khu 3, ấp 2, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8831245,
   "Latitude": 106.8709404
 },
 {
   "STT": 3040,
   "Name": "Nhà thuốc Tâm Đức Thiện",
   "address": "21, Lô L3, KDC An Hưng Phát, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.898813,
   "Latitude": 106.851801
 },
 {
   "STT": 3041,
   "Name": "Nhà thuốc Phan Ngọc Anh",
   "address": "21/F2, tổ 14, khu phố 1, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8994104,
   "Latitude": 106.8598763
 },
 {
   "STT": 3042,
   "Name": "Quầy thuốc Tuấn Ngà",
   "address": "916, Ngô Quyền, tổ 20, khu 4, ấp 1, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916628,
   "Latitude": 106.8634886
 },
 {
   "STT": 3043,
   "Name": "Quầy thuốc Minh Sáu",
   "address": "270H, tổ 6, ấp 1, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916628,
   "Latitude": 106.8634886
 },
 {
   "STT": 3044,
   "Name": "Nhà thuốc Sơn Minh Long Bình Tân",
   "address": "F1/2, KP 1, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 3045,
   "Name": " Nhà thuốc Ngân Thắng",
   "address": "T19, Bùi Trọng Nghĩa, tổ 2, KP 3, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.994678,
   "Latitude": 106.86104
 },
 {
   "STT": 3046,
   "Name": "Nhà thuốc Phú Trí",
   "address": "Tổ 2, KP 2, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9921407,
   "Latitude": 106.8709404
 },
 {
   "STT": 3047,
   "Name": " Quầy thuốc Ngọc Xuân",
   "address": "100, Bùi Trọng Nghĩa, KP 4, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9772674,
   "Latitude": 106.8549034
 },
 {
   "STT": 3048,
   "Name": "Quầy thuốc Kim Thu",
   "address": "Tổ 13, ấp 1, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 3049,
   "Name": "Nhà thuốc Trang Liên Minh",
   "address": "8, Nguyễn Thị Tồn, KP 2, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9387766,
   "Latitude": 106.813618
 },
 {
   "STT": 3050,
   "Name": "Nhà thuốc Ngọc Xinh",
   "address": "15, Trần Thị Hoa, KP 3, P.An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9330336,
   "Latitude": 106.8544324
 },
 {
   "STT": 3051,
   "Name": "Quầy thuốc Hằng Hiệp",
   "address": "154, tổ 12, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 3052,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 3053,
   "Name": "Quầy thuốc Thương Cần",
   "address": "751, tổ 16, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 3054,
   "Name": "Nhà thuốc Ngân Thủy Châu",
   "address": "46, tổ 27, KP 4, Phường Trảng Dài, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.9720615,
   "Latitude": 106.9097552
 },
 {
   "STT": 3055,
   "Name": "Quầy thuốc Trường Thủy",
   "address": "Tổ 12, ấp Miễu, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8834408,
   "Latitude": 106.9255617
 },
 {
   "STT": 3056,
   "Name": "Quầy thuốc Thành Công",
   "address": "Số 32, đường 767, tổ 2, ấp 1, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0511835,
   "Latitude": 107.0208512
 },
 {
   "STT": 3057,
   "Name": "Quầy thuốc số 12 - trực thuộc Công ty cổ phần dược Đồng Nai",
   "address": "Tổ 1, khu phố 5, thị trấn Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0884395,
   "Latitude": 107.0331652
 },
 {
   "STT": 3058,
   "Name": "Quầy thuốc",
   "address": "Tổ 12, khu Phước Hải, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7811517,
   "Latitude": 106.9524466
 },
 {
   "STT": 3059,
   "Name": "Quầy thuốc",
   "address": "Số 44, khu Nông Trường Cao Su An Viễng, ấp An Bình, xã Bình An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8559248,
   "Latitude": 107.0572653
 },
 {
   "STT": 3060,
   "Name": "Nhà thuốc Dương Hằng",
   "address": "451/4, KP 10, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 3061,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Tổ 5, ấp 2, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 3062,
   "Name": "Quầy thuốc Trung tâm y tế huyện Nhơn Trạch",
   "address": "Ấp Xóm Hố, xã Phú Hội, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.727574,
   "Latitude": 106.879978
 },
 {
   "STT": 3063,
   "Name": "Quầy thuốc Minh Đăng",
   "address": "Khu B, tổ 9, ấp 1, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.95
 },
 {
   "STT": 3064,
   "Name": "Nhà thuốc Ngọc Long Nguyễn",
   "address": "92/5B, Trần Quốc Toản, KP 3, Phường An Bình, Thành phố Biên Hòa, Tỉnh Đồng Nai",
   "Longtitude": 10.927271,
   "Latitude": 106.8517238
 },
 {
   "STT": 3065,
   "Name": "Quầy thuốc Nhân Ái ",
   "address": "553, Lý Thái Tổ, ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7556989,
   "Latitude": 106.9199757
 },
 {
   "STT": 3066,
   "Name": " Quầy thuốc Thanh Vân 1",
   "address": "Ấp 3, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6935151,
   "Latitude": 106.9535691
 },
 {
   "STT": 3067,
   "Name": "Quầy thuốc Liêm Thanh",
   "address": "ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 3068,
   "Name": " Quầy thuốc",
   "address": "Tổ 16, ấp 2, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 3069,
   "Name": "Quầy thuốc Phúc An Khang 2",
   "address": "Hùng Vương, ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.721683,
   "Latitude": 106.956648
 },
 {
   "STT": 3070,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Tổ 7, ấp Cẩm Đường, xã Cẩm Đường, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7827374,
   "Latitude": 107.1071
 },
 {
   "STT": 3071,
   "Name": "Quầy thuốc Long Châu 1",
   "address": "232, Tôn Đức Thắng, ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7381668,
   "Latitude": 106.9467628
 },
 {
   "STT": 3072,
   "Name": "Quầy thuốc Nhi Khoa Sài Gòn",
   "address": "Ấp 3, khu Bàu Cá, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8458011,
   "Latitude": 106.9535691
 },
 {
   "STT": 3073,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Số 402, tổ 27, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 3074,
   "Name": "Nhà thuốc Hạnh Phúc (HAPPY PHARMACY)",
   "address": "158, Đồng Khởi, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9547215,
   "Latitude": 106.8676149
 },
 {
   "STT": 3075,
   "Name": "Nhà thuốc An Phương Nam",
   "address": "552/25, KP 5, phường Hố Nai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 3076,
   "Name": "Nhà thuốc Mẫn Sơn Minh",
   "address": "160, Phan Đình Phùng, khu phố 2, phường Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.951577,
   "Latitude": 106.8181043
 },
 {
   "STT": 3077,
   "Name": "Nhà thuốc Trí Hoàng",
   "address": "28/15, KP 8, phường Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9668904,
   "Latitude": 106.9079296
 },
 {
   "STT": 3078,
   "Name": "Nhà thuốc phòng khám đa khoa Tâm An 2 - Tea Kwang",
   "address": "Khu công nghiệp Agtex Long Bình, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9291493,
   "Latitude": 106.8806029
 },
 {
   "STT": 3079,
   "Name": "Nhà thuốc phòng khám đa khoa Tâm An 2 - chi nhánh công ty TNHH xây dựng - Y tế Tâm An ",
   "address": "Số 8, đường 9A, KCN Biên Hòa 2, phường An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9191694,
   "Latitude": 106.872125
 },
 {
   "STT": 3080,
   "Name": "Quầy thuốc Cẩm Vi Phát",
   "address": "Ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3081,
   "Name": "Quầy thuốc Thịnh Phú",
   "address": "Tổ 2B, KP 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9488889,
   "Latitude": 107.0016667
 },
 {
   "STT": 3082,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "Đường Hùng Vương, tổ 1, khu phố 3, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9513704,
   "Latitude": 107.0126076
 },
 {
   "STT": 3083,
   "Name": "Quầy thuốc Ngọc Tiên",
   "address": "Số 10, tổ 1, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 3084,
   "Name": "Quầy thuốc Ngọc Ánh",
   "address": "Số 514, thôn Tây Lạc, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3085,
   "Name": "Nhà thuốc Nguyễn Ngân",
   "address": "Tổ 8C, KP 3 (tờ bản đồ số 06, thửa đất 268), phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9607516,
   "Latitude": 106.8562856
 },
 {
   "STT": 3086,
   "Name": "Nhà thuốc Hồng Ân",
   "address": "Ấp Vĩnh Cửu, xã Vĩnh Thanh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6770054,
   "Latitude": 106.8591387
 },
 {
   "STT": 3087,
   "Name": "Quầy thuốc Phương Thủy",
   "address": "442, ấp Bàu Cá, xã Trung Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9687374,
   "Latitude": 107.0304582
 },
 {
   "STT": 3088,
   "Name": "Quầy thuốc Song An",
   "address": "Ấp 1, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7288078,
   "Latitude": 106.9397033
 },
 {
   "STT": 3089,
   "Name": "Quầy thuốc Phú Thịnh",
   "address": "Ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7355542,
   "Latitude": 106.9421065
 },
 {
   "STT": 3090,
   "Name": "Quầy thuốc Hải Yến",
   "address": "189B/1, Dốc Mơ 1, Gia Tân 1, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0545288,
   "Latitude": 107.1688573
 },
 {
   "STT": 3091,
   "Name": "Quầy thuốc Hoàng Oanh",
   "address": "SN 30, KDC 1, ấp Hòa Đồng,  xã Ngọc Định, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 3092,
   "Name": "Quầy thuốc Xuân Hưng",
   "address": "Khu A1-C1, Lập Thành, Xuân Thạnh, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.9483523,
   "Latitude": 107.1602683
 },
 {
   "STT": 3093,
   "Name": "Chi nhánh công ty cổ phần dược phẩm TV.PHARM tại Đồng Nai",
   "address": "C34, KP 1, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548762,
   "Latitude": 106.8021482
 },
 {
   "STT": 3094,
   "Name": "Quầy thuốc Thanh Hà",
   "address": "số nhà 220A, khu 4, Ấp 3, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2090815,
   "Latitude": 107.359482
 },
 {
   "STT": 3095,
   "Name": "Quyầy thuốc Hồng Phúc",
   "address": "176 Khu 3, khu phố Hiệp Quyết, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 3096,
   "Name": "Quầy thuốc Kim Hằng",
   "address": "382 Tổ 4, khu 2, thị trấn Tân Phú, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2677446,
   "Latitude": 107.4292386
 },
 {
   "STT": 3097,
   "Name": "Quầy thuốc Lan Hương",
   "address": "Khu 5, ấp Suối Soong 2, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1775809,
   "Latitude": 107.3410392
 },
 {
   "STT": 3098,
   "Name": "Quầy thuốc Hà Anh",
   "address": "SN 264, khu 11, ấp 1, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 3099,
   "Name": "Quầy thuốc Quang Vinh",
   "address": "Ấp Bến Cam, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7564007,
   "Latitude": 106.9225798
 },
 {
   "STT": 3100,
   "Name": "Quầy thuốc Minh Lai",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 3101,
   "Name": "Quầy thuốc Bà Mười 1",
   "address": "89, Phạm Thái Bường, xã Phước Khánh, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.6910684,
   "Latitude": 106.8033302
 },
 {
   "STT": 3102,
   "Name": "Quầy thuốc Thân Dung",
   "address": "Đội 2, Đông Kim, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0327201,
   "Latitude": 107.1803206
 },
 {
   "STT": 3103,
   "Name": "Quầy thuốc Nhất Nhất",
   "address": "Tổ 30, ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 3104,
   "Name": "Quầy thuốc Ánh Hòa",
   "address": "Hùng Vương, ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7378019,
   "Latitude": 106.9449006
 },
 {
   "STT": 3105,
   "Name": "Quầy thuốc Phương Dung",
   "address": "Ấp Vĩnh Tuy, xã Long Tân, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7524773,
   "Latitude": 106.8709404
 },
 {
   "STT": 3106,
   "Name": "Quầy thuốc Tuyết Hải",
   "address": "16, tổ 24, ấp Vườn Dừa, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8960957,
   "Latitude": 106.8928706
 },
 {
   "STT": 3107,
   "Name": "Nhà thuốc Xuân Linh Trang",
   "address": "910 Nguyễn Ái Quốc, KP8A, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9709166,
   "Latitude": 106.8931958
 },
 {
   "STT": 3108,
   "Name": "Nhà thuốc Sĩ Mẫn",
   "address": "136, Võ Thị Sáu, KP 7, phường Thống Nhất, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9467973,
   "Latitude": 106.8271388
 },
 {
   "STT": 3109,
   "Name": "Nhà thuốc Thiện Long",
   "address": "377, KP 1, phường Tân Mai, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9663793,
   "Latitude": 106.8702602
 },
 {
   "STT": 3110,
   "Name": "Quầy thuốc Nam Thanh",
   "address": "805/13, Nguyễn Ái Quốc, tổ 12, KP 1, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9644337,
   "Latitude": 106.8432641
 },
 {
   "STT": 3111,
   "Name": "Nhà thuốc Dân An Biên Hòa",
   "address": "Số A17, A18+19, KP 5, phường An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.932847,
   "Latitude": 106.852407
 },
 {
   "STT": 3112,
   "Name": "Nhà thuốc Thiên Lộc Bình",
   "address": "983, Bùi Văn Hòa, tổ 9, KP 7, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 3113,
   "Name": "Nhà thuốc Binh Đức",
   "address": "1/GA, tổ 17, KP 1, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8987306,
   "Latitude": 106.8547775
 },
 {
   "STT": 3114,
   "Name": "Quầy thuốc Tiến Trà Giang",
   "address": "731, tổ 12, ấp Hương Phước, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8872034,
   "Latitude": 106.8995794
 },
 {
   "STT": 3115,
   "Name": "Quầy thuốc Xuân Khôi",
   "address": "11, tổ 8, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 3116,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Linh Hòa ",
   "address": "1023/29, tổ 15, KP 7, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9368378,
   "Latitude": 106.8989902
 },
 {
   "STT": 3117,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Hải An",
   "address": "B2, KP 4, phường Quang Vinh, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9561302,
   "Latitude": 106.8139787
 },
 {
   "STT": 3118,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Ánh Châu",
   "address": "số 880A, Tổ 4, KP 8A, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9705705,
   "Latitude": 106.893577
 },
 {
   "STT": 3119,
   "Name": "Quầy thuốc Bảy Vân",
   "address": "19/8, ấp 2, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7567666,
   "Latitude": 106.9889904
 },
 {
   "STT": 3120,
   "Name": "Quầy thuốc Linh Đan",
   "address": "SN 02, KDC 6, ấp 7, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 3121,
   "Name": "Quầy thuốc Nghĩa Chi",
   "address": "Số 156 Hùng Vương, tổ 2, ấp 2, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.725363,
   "Latitude": 106.9518439
 },
 {
   "STT": 3122,
   "Name": "Quầy thuốc Cường Phụng",
   "address": "số 76, Lê A, tổ 1, ấp 1, , xã Bình Lộc, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9835555,
   "Latitude": 107.2359278
 },
 {
   "STT": 3123,
   "Name": "Quầy thuốc Linh Hiệp",
   "address": "Tổ 10, đường Thành Thái, ấp Bảo Vinh B, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9450152,
   "Latitude": 107.2641836
 },
 {
   "STT": 3124,
   "Name": "Quầy thuốc Tâm Minh",
   "address": "Đường số 2, tổ 5, ấp Hàng Gòn, xã Hàng Gòn, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8803302,
   "Latitude": 107.2075388
 },
 {
   "STT": 3125,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Ấp 5 (thửa đất số 378, tờ bản đồ số 98), xã Xuân Tâm, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8666632,
   "Latitude": 107.4440254
 },
 {
   "STT": 3126,
   "Name": "Quầy thuốc Phụng Uyên",
   "address": "Số 26/25, tổ 5, ấp Cẩm Tân, xã Xuân Tân, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.8950788,
   "Latitude": 107.2283676
 },
 {
   "STT": 3127,
   "Name": "Quầy thuốc Khánh Ngân",
   "address": "Ấp Tây Minh (thửa đất số 352, tờ bản đồ số 15), xã Lang Minh, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.8603549,
   "Latitude": 107.3717237
 },
 {
   "STT": 3128,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Ấp 9, xã Sông Ray, huyện Cẩm Mỹ, tỉnh Đồng Nai",
   "Longtitude": 10.7424691,
   "Latitude": 107.3493185
 },
 {
   "STT": 3129,
   "Name": "Quầy thuốc Minh Anh",
   "address": "24 Ngô Quyền, ấp Ruộng Hời, xã Bảo Vinh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9387906,
   "Latitude": 107.2642281
 },
 {
   "STT": 3130,
   "Name": "Quầy thuốc Thảo Tâm",
   "address": "Khu phố 4, thị trấn Vĩnh An, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.083333,
   "Latitude": 107.033333
 },
 {
   "STT": 3131,
   "Name": "Quầy thuốc Vạn An",
   "address": "Ấp 5, xã An Viễn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.8505029,
   "Latitude": 107.042198
 },
 {
   "STT": 3132,
   "Name": "Quầy thuốc Mỹ Huyền",
   "address": "Ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 3133,
   "Name": " Quầy thuốc Thành Toàn",
   "address": "Khu phố 1, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9521847,
   "Latitude": 106.9978466
 },
 {
   "STT": 3134,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "Số 6, đường Cách Mạng Tháng 8, khu phố 4, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9720567,
   "Latitude": 106.9097648
 },
 {
   "STT": 3135,
   "Name": "Quầy thuốc Anh Huệ",
   "address": "Số 105C, ấp 1, xã Sông Trầu, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9817443,
   "Latitude": 107.025637
 },
 {
   "STT": 3136,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Ấp 5, xã Thạnh Phú, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.012687,
   "Latitude": 106.8520996
 },
 {
   "STT": 3137,
   "Name": "Quầy thuốc Yến Nhi",
   "address": "700 Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3138,
   "Name": "Quầy thuốc Ngọc Duyên",
   "address": "Ấp 6, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 3139,
   "Name": "Quầy thuốc Vũ Nguyên",
   "address": "470, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3140,
   "Name": "Quầy thuốc Ngọc Quân",
   "address": "Số 361, ấp Thái Hòa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9652691,
   "Latitude": 106.934588
 },
 {
   "STT": 3141,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Ấp Tân Bắc, xã Bình Minh, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9538012,
   "Latitude": 106.9685943
 },
 {
   "STT": 3142,
   "Name": "Nhà thuốc Bệnh viện đa khoa Thống nhất Đồng Nai",
   "address": "234, Quốc lộ 1, phường Tân Biên, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9659291,
   "Latitude": 106.8857423
 },
 {
   "STT": 3143,
   "Name": "Quầy thuốc Anh Đào",
   "address": "1122, ấp Quảng Biên, xã Quảng Tiến, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9667039,
   "Latitude": 107.0303221
 },
 {
   "STT": 3144,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Việt Kim Long",
   "address": "7/11, Lê Quý Đôn, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9616213,
   "Latitude": 106.8628189
 },
 {
   "STT": 3145,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Phương Thảo Nguyên",
   "address": "62D, tổ 3, KP 3, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916385,
   "Latitude": 106.8503073
 },
 {
   "STT": 3146,
   "Name": "Quầy thuốc An Phước",
   "address": "Khu 3, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.95
 },
 {
   "STT": 3147,
   "Name": "Quầy thuốc Thái Duy Bảo",
   "address": "Tổ 6C, KP 4 (tờ bản đồ số 17, thửa đất số 733), phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720562,
   "Latitude": 106.9097658
 },
 {
   "STT": 3148,
   "Name": "Quầy thuốc Khang An",
   "address": "66, khu 1, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7250232,
   "Latitude": 107.1177882
 },
 {
   "STT": 3149,
   "Name": "Nhà thuốc Nguyễn Hai",
   "address": "79A, tổ 10B, KP 4, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720572,
   "Latitude": 106.9097638
 },
 {
   "STT": 3150,
   "Name": "Quầy thuốc An An Khang",
   "address": "39, tổ 11, ấp Long Đức 1, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 3151,
   "Name": "Quầy thuốc Đỗ Đức Hộ",
   "address": "09/2E Võ Dõng, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0304006,
   "Latitude": 107.1543601
 },
 {
   "STT": 3152,
   "Name": "Quầy thuốc",
   "address": "385/4 ấp Tân Yên, xã Gia Tân 3, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 10.994359,
   "Latitude": 107.1547158
 },
 {
   "STT": 3153,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "Tổ 1, ấp 7, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8355792,
   "Latitude": 106.9417632
 },
 {
   "STT": 3154,
   "Name": "Quầy thuốc Hải Châu",
   "address": "Tổ 01, ấp 3, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8096151,
   "Latitude": 106.9272695
 },
 {
   "STT": 3155,
   "Name": "Quầy thuốc Hải Anh",
   "address": "Tổ 5, ấp 8, xã An Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8642324,
   "Latitude": 106.9667294
 },
 {
   "STT": 3156,
   "Name": "Nhà thuốc Quân Đức Mạnh",
   "address": "179, Hà Huy Giáp, KP 1, phường Quyết Thắng, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9497068,
   "Latitude": 106.822304
 },
 {
   "STT": 3157,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Tổ 10, khu 15, xã Long Đức, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.808844,
   "Latitude": 106.9530943
 },
 {
   "STT": 3158,
   "Name": "Quầy thuốc",
   "address": "Tổ 47, ấp 4, xã Tam An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.8106836,
   "Latitude": 106.9004472
 },
 {
   "STT": 3159,
   "Name": "Quầy thuốc Tâm Ý",
   "address": "Tổ 28, khu Cầu Xéo, thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7689033,
   "Latitude": 106.9552782
 },
 {
   "STT": 3160,
   "Name": "Nhà thuốc Đức Khánh",
   "address": "91A, Trần Văn Xã, KP 3, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9837554,
   "Latitude": 106.8619709
 },
 {
   "STT": 3161,
   "Name": "Quầy thuốc",
   "address": "260, ấp Tân Lập, xã Phước Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9533624,
   "Latitude": 106.8558135
 },
 {
   "STT": 3162,
   "Name": "Nhà thuốc Thảo Khôi",
   "address": "số 69, Trần Văn xã, khu phố 3, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9892817,
   "Latitude": 106.8573076
 },
 {
   "STT": 3163,
   "Name": "Quầy thuốc Đoàn Hòa",
   "address": "04, ấp Long Khánh 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 3164,
   "Name": "Quầy thuốc Thiện Tâm",
   "address": "Tổ 1, ấp 4, xã Long An, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.757452,
   "Latitude": 106.9881911
 },
 {
   "STT": 3165,
   "Name": "Quầy thuốc Thanh",
   "address": "65/2E Võ Dõng 1, xã Gia Kiệm, huyện Thống Nhất, tỉnh Đồng Nai",
   "Longtitude": 11.0269236,
   "Latitude": 107.1745769
 },
 {
   "STT": 3166,
   "Name": "Nhà thuốc Phúc Đức Bùi Thái",
   "address": "160, Hồ Văn Leo, KP 1, phường Tam Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9428456,
   "Latitude": 106.8653773
 },
 {
   "STT": 3167,
   "Name": "Công ty TNHH Dược phẩm Shinpoong Daewoo",
   "address": "Số 13, đường 9A, KCN Biên Hòa II, phường An Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.924378,
   "Latitude": 106.8784242
 },
 {
   "STT": 3168,
   "Name": "Quầy thuốc Chính Bồng 6",
   "address": "Khu 4, ấp Ba Tầng, xã Phú Vinh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.2090815,
   "Latitude": 107.359482
 },
 {
   "STT": 3169,
   "Name": "Quầy thuốc Vĩnh An",
   "address": "Km 104, ấp 1, xã Phú Ngọc, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1362281,
   "Latitude": 107.3021069
 },
 {
   "STT": 3170,
   "Name": "Nhà thuốc Ngọc Sơn",
   "address": "số nhà 258 B, Phương Lâm 2, xã Phú Lâm, huyện Tân Phú, tỉnh Đồng Nai",
   "Longtitude": 11.2747338,
   "Latitude": 107.4943069
 },
 {
   "STT": 3171,
   "Name": "Công ty TNHH Dược phẩm Bửu Hòa",
   "address": "158, đường Đồng Khởi, phường Tân Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9547215,
   "Latitude": 106.8676149
 },
 {
   "STT": 3172,
   "Name": "Quầy thuốc Thái Uyên",
   "address": "SN 61, KP Hiệp Nhất, thị trấn Định Quán, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1937645,
   "Latitude": 107.3434916
 },
 {
   "STT": 3173,
   "Name": "CÔNG TY CỔ PHẦN DƯỢC ĐỒNG NAI",
   "address": "221B, Phạm Văn Thuận, phường Tân Tiến, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9577709,
   "Latitude": 106.8322585
 },
 {
   "STT": 3174,
   "Name": "Quầy thuốc Quốc Linh",
   "address": "SN 137 KDC 3, ấp 4, xã Gia Canh, huyện Định Quán, tỉnh Đồng Nai",
   "Longtitude": 11.1190282,
   "Latitude": 107.4085384
 },
 {
   "STT": 3175,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Chuyên Hương",
   "address": "16, tổ 10, KP 4, phường Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9720572,
   "Latitude": 106.9097638
 },
 {
   "STT": 3176,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Nguyễn Hùng",
   "address": "173/200A, tổ 16, KP 8, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8916461,
   "Latitude": 106.8502418
 },
 {
   "STT": 3177,
   "Name": " Quầy thuốc Quỳnh Khang",
   "address": "561 Đông Bình, ấp Bùi Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9570723,
   "Latitude": 106.9505826
 },
 {
   "STT": 3178,
   "Name": " Quầy thuốc Thiên Bảo",
   "address": "Tổ 16, ấp 3, xã Vĩnh Tân, huyện Vĩnh Cửu, tỉnh Đồng Nai",
   "Longtitude": 11.0247447,
   "Latitude": 107.0020938
 },
 {
   "STT": 3179,
   "Name": "Nhà thuốc Trung tâm Y tế huyện Trảng Bom",
   "address": "Khu phố 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 3180,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "Ấp Phú Sơn, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9448491,
   "Latitude": 106.9607993
 },
 {
   "STT": 3181,
   "Name": "Quầy thuốc Long Tiến",
   "address": "Ấp Nhân Hòa, xã Tây Hoà, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9464816,
   "Latitude": 107.0544486
 },
 {
   "STT": 3182,
   "Name": "Quầy thuốc Ngọc Thủy",
   "address": "Số 67/4C, tổ 30, khu 2, ấp Thanh Hóa, xã Hố Nai 3, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.7451012,
   "Latitude": 106.705408
 },
 {
   "STT": 3183,
   "Name": "Quầy thuốc Thái Thủy",
   "address": "Số 77, khu phố 5, thị trấn Trảng Bom, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9631529,
   "Latitude": 106.9108939
 },
 {
   "STT": 3184,
   "Name": "Quầy thuốc Ngọc Huyền",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 3185,
   "Name": "Công ty Cổ phần AZENCA",
   "address": "Số 33C3, khu phố 11, phường Tân Phong, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.981615,
   "Latitude": 106.8428213
 },
 {
   "STT": 3186,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Tây Lạc, ấp An Chu, xã Bắc Sơn, huyện Trảng Bom, tỉnh Đồng Nai",
   "Longtitude": 10.9582527,
   "Latitude": 106.9485777
 },
 {
   "STT": 3187,
   "Name": "Quầy thuốc An Tâm Anh",
   "address": "2/3A, Hoàng Minh Chánh, ấp Đồng Nai, xã Hóa An, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9362136,
   "Latitude": 106.7970025
 },
 {
   "STT": 3188,
   "Name": "Quầy thuốc",
   "address": "Khu 2, ấp Phước Hòa, xã Long Phước, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7183848,
   "Latitude": 106.9948945
 },
 {
   "STT": 3189,
   "Name": "Quầy thuốc An Khang",
   "address": "Tổ 2, ấp 3, xã Bình Sơn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7447054,
   "Latitude": 107.0543005
 },
 {
   "STT": 3190,
   "Name": "Quầy thuốc Thiên Long",
   "address": "Đồi 26, ấp 8, xã Bàu Cạn, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.7368485,
   "Latitude": 107.0886932
 },
 {
   "STT": 3191,
   "Name": "Quầy thuốc Hoa Tính",
   "address": "Tổ 1, ấp 6, xã Phước Bình, huyện Long Thành, tỉnh Đồng Nai",
   "Longtitude": 10.6747458,
   "Latitude": 107.0952864
 },
 {
   "STT": 3192,
   "Name": "Quầy thuốc Thanh Hường",
   "address": "Ấp 5, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7294291,
   "Latitude": 106.957955
 },
 {
   "STT": 3193,
   "Name": "Nhà thuốc Khải Minh",
   "address": "E961, tổ 12, KP 5A, phường Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.93909,
   "Latitude": 106.8844884
 },
 {
   "STT": 3194,
   "Name": "Quầy thuốc Bình Phương",
   "address": "Tôn Đức Thắng, ấp 3, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7381668,
   "Latitude": 106.9467628
 },
 {
   "STT": 3195,
   "Name": "Quầy thuốc Dương Hiền",
   "address": "Khu 4, thị trấn Gia Ray, huyện Xuân Lộc, tỉnh Đồng Nai",
   "Longtitude": 10.9246028,
   "Latitude": 107.4034785
 },
 {
   "STT": 3196,
   "Name": "Quầy thuốc Hồ Bảo Ngân",
   "address": "407, tổ 14, ấp Thiên Bình, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.873688,
   "Latitude": 106.945145
 },
 {
   "STT": 3197,
   "Name": "Quầy thuốc Đức Dung",
   "address": "36, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8573033,
   "Latitude": 106.92756
 },
 {
   "STT": 3198,
   "Name": "Quầy thuốc Thiên Thanh 3",
   "address": "470/7 Hùng Vương, tổ 8, ấp 4, xã Hiệp Phước, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7253655,
   "Latitude": 106.9518077
 },
 {
   "STT": 3199,
   "Name": "Nhà thuốc Hồng Ngát",
   "address": "46/11, Nguyễn Văn Tỏ, KP. Long Điềm, phường Long Bình Tân, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8918227,
   "Latitude": 106.8524381
 },
 {
   "STT": 3200,
   "Name": "Nhà thuốc Phúc Anh",
   "address": "9/13F, KP 6, phường Tam Hiệp, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9335982,
   "Latitude": 106.875735
 },
 {
   "STT": 3201,
   "Name": "Quầy thuốc Đỗ Linh",
   "address": "Lô 18, KDC Tín Nghĩa, ấp Long Đức 3, xã Tam Phước, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.85,
   "Latitude": 106.9
 },
 {
   "STT": 3202,
   "Name": "Nhà thuốc Lộ Đức",
   "address": "96/2A, KP 5, phường Tân Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9537274,
   "Latitude": 106.8563548
 },
 {
   "STT": 3203,
   "Name": "Quầy thuốc Châu Minh",
   "address": "Hùng Vương, ấp 1, xã Long Thọ, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.721683,
   "Latitude": 106.956648
 },
 {
   "STT": 3204,
   "Name": "Quầy thuốc Tâm Đức 1",
   "address": "Số 66, ấp Trầu, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.760068,
   "Latitude": 106.9299579
 },
 {
   "STT": 3205,
   "Name": "Quầy thuốc ",
   "address": "272 Lý Thái Tổ, ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.758948,
   "Latitude": 106.9326802
 },
 {
   "STT": 3206,
   "Name": "Quầy thuốc Tài Lộc",
   "address": "Tổ 25, ấp Bến Sắn, xã Phước Thiền, huyện Nhơn Trạch, tỉnh Đồng Nai",
   "Longtitude": 10.7582937,
   "Latitude": 106.9336324
 },
 {
   "STT": 3207,
   "Name": " Quầy thuốc Gia Bảo Tây",
   "address": "11, ấp 1, xã An Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.8919431,
   "Latitude": 106.9136091
 },
 {
   "STT": 3208,
   "Name": "Nhà thuốc Thư Phát",
   "address": "191, Huỳnh Văn Nghệ, KP 2, P.Bửu Long, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9548762,
   "Latitude": 106.8021482
 },
 {
   "STT": 3209,
   "Name": "Nhà thuốc Hoàng",
   "address": "Số 02, Hoàng Diệu, phường Xuân Thanh, thị xã Long Khánh, tỉnh Đồng Nai",
   "Longtitude": 10.9347581,
   "Latitude": 107.2529262
 },
 {
   "STT": 3210,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Hoa Hồng",
   "address": "883, Bùi Hữu Nghĩa, KP 2, P.Bửu Hòa, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9377752,
   "Latitude": 106.8130831
 },
 {
   "STT": 3211,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Yến Hưng ",
   "address": "173/438T, tổ 1, KP 8, P.Long Bình, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.9520451,
   "Latitude": 106.8772376
 },
 {
   "STT": 3212,
   "Name": "Cơ sở chuyên bán lẻ thuốc dược liệu, thuốc cổ truyền Niềm Tin",
   "address": "27, tổ 45, KP 4C, P.Trảng Dài, thành phố Biên Hòa, tỉnh Đồng Nai",
   "Longtitude": 10.994678,
   "Latitude": 106.86104
 }
];